package com.algorithm.work;

import java.time.LocalDateTime;
import java.util.*;

/**
 * @description:
 * @author: zyh
 * @create: 2021-11-09 19:57
 **/
public class GetCombination {
    public static void main(String[] args) {
        System.out.println(LocalDateTime.now());
        //假设某个数组由 1,2,4,8,16,32,64任取n个数字组成,求包含1的可能的组合
        HashMap<Integer, Set<Integer>> map=getAll();

        for (Map.Entry<Integer, Set<Integer>> entry : map.entrySet()) {
            System.out.println(entry.getKey()+"可能的结果有"+entry.getValue().size()+"种"+":"+entry.getValue());
        }
    }

    private static HashMap getAll() {
        HashMap<Integer,Set<Integer>> res=new HashMap(8);
        List<Integer> nums= Arrays.asList(1,2,4,8,16,32,64);

        for (int key = 0; key < nums.size(); key++) {
            Set<Integer> temp =new HashSet<>();
            temp.add(nums.get(key));
            getCombination(0,nums.get(key),key,nums,temp);
            res.put(nums.get(key),temp);
        }
        return res;
    }

    /**
     * 获取数据包含第i个数字可能的组成
     * @author zyh
     * @date 2021/11/9
     */
    private static void getCombination(int curr,int sum,int key, List<Integer> nums, Set<Integer> res) {
        if (curr>=nums.size()){
            return;
        }

        if (curr!=key){
            int temp=nums.get(curr);
            sum+=temp;
            res.add(sum);
            curr++;
            getCombination(curr,sum,key,nums,res);

            sum-=temp;
            getCombination(curr,sum,key,nums,res);
        }else{
            curr++;
            getCombination(curr,sum,key,nums,res);
        }
    }

//    public static void main(String[] args) {
//        List<Integer> nums= Arrays.asList(1,2,4,8,16,32,64);
//        Set<Integer> temp =new HashSet<>();
//        temp.add(nums.get(0));
//        getCombination(0,nums.get(0),0,nums,temp);
//        System.out.println("总共可能情况有:"+temp.size()+"种");
//        for (Integer integer : temp) {
//            System.out.println(getIntegers(integer));
//        }
//    }

    /**
     * nums是由1,2,4,8这样的等比数列组成的和,每个数字只出现一次,求出其组成元素
     *
     * @return 返回从小达到的顺序hashset
     * @author zyh
     * @date 2021/10/29
     */
    public static List<Integer> getIntegers(Integer nums) {
        final String s = Integer.toBinaryString(nums);
        List<Integer> res = new LinkedList<>();
        final char[] chars = s.toCharArray();
        for (int curr = 0, len = chars.length; curr < len; curr++) {
            if (chars[curr] == '1') {
                res.add(((Double) Math.pow(2, len - curr - 1)).intValue());
            }
        }
        return res;
    }
}
