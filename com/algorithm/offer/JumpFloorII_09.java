package com.algorithm.offer;

/**
 * @description: 变态跳台阶
 * @author: zyh
 * @create: 2021-03-11 20:48
 **/

import org.junit.Test;

/**
 * 一只青蛙一次可以跳上1级台阶，也可以跳上2级……它也可以跳上n级。求该青蛙跳上一个n级的台阶总共有多少种跳法。
 *
 * 没一级都可能从前n-1级直接跳上来的
 * 0 1 2 3 4   5
 *
 * 1 1 2 4 8  16
 */
public class JumpFloorII_09 {
    public int jumpFloorII(int target) {
        if (target==0){
            return 0;
        }
        return new Double(Math.pow(2, target - 1)).intValue();
    }

    @Test
    public void  test(){
        System.out.println(jumpFloorII(2));
    }

}
