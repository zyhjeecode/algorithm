package com.algorithm.offer;

public class HasSubtree_17 {
    //判断树2是否是树1的子结构
    //思路：
    //若树2是树1的子结构那么必然存在一条结点后面的树的结构和树2完全一致，包括左右子树
    public boolean HasSubtree(TreeNode root1,TreeNode root2) {
        if (root1==null||root2==null){
            return false;
        }
        boolean res=false;
        //如果两个结点值相同则进行子树判断
        if (root1.val==root2.val){
            res=judge(root1,root2);
        }
        //继续找结点值相同的结点
        if (!res){
            res=HasSubtree(root1.left,root2);
        }

        if (!res){
            res=HasSubtree(root1.right,root2);
        }
        return res;
    }

    //判断两个树是否完全一致
    public boolean judge(TreeNode root1 ,TreeNode root2){
        if (root2==null){
            return true;
        }
        if (root1==null){
            return false;
        }
        if (root1.val!=root2.val){
            return false;
        }

        return judge(root1.left,root2.left)&&judge(root1.right,root2.right);
    }
}
