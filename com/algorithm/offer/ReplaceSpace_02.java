package com.algorithm.offer;

import org.junit.Test;

public class ReplaceSpace_02 {
    /**
     * 题目描述
     * 请实现一个函数，将一个字符串中的每个空格替换成“%20”。例如，当字符串为We Are Happy.则经过替换之后的字符串为We%20Are%20Happy。
     * 示例1
     *
     * 输入
     * "We Are Happy"
     * 返回值
     * "We%20Are%20Happy"
     */
    public String replaceSpace (String s) {
        if (s==null||s.length()==0){
            return "";
        }else {
            return s.replaceAll(" ","%20");
        }
    }

    @Test
    public void  test(){
        String testStr="We Are Happy";
        System.out.println(replaceSpace(testStr));
    }
}
