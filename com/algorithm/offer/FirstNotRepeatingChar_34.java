package com.algorithm.offer;

import org.junit.Test;

import java.util.HashMap;

/**
 * @description: 第一个只出现一次的字符
 * @author: zyh
 * @create: 2021-04-26 19:34
 **/
public class FirstNotRepeatingChar_34 {
    /**
     * 在一个字符串(0<=字符串长度<=10000，全部由字母组成)中找到第一个只出现一次的字符,并返回它的位置,
     * 如果没有则返回 -1（需要区分大小写）.（从0开始计数）
     *
     * 输入
     * "google"
     *
     * 返回值
     * 4
     */
    public int FirstNotRepeatingChar_m1(String str) {
        HashMap<Character,Integer> charCount=new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            charCount.put(str.charAt(i),charCount.get(str.charAt(i))==null?1:(charCount.get(str.charAt(i))+1));
        }

        for (int i = 0; i < str.length(); i++) {
            if (charCount.get(str.charAt(i))==1){
                return i;
            }
        }

        return -1;
    }


    public int FirstNotRepeatingChar_m2(String str) {
        int[] charCount=new int[58];
        for (int i = 0; i < str.length(); i++) {
            charCount[str.charAt(i)-'A']++;
        }


        for (int i = 0; i < str.length(); i++) {
            if (charCount[str.charAt(i)-'A']==1){
                return i;
            }
        }

        return -1;
    }


    @Test
    public void  test(){
        System.out.println(FirstNotRepeatingChar_m2("NXWtnzyoHoBhUJaPauJaAitLWNMlkKwDYbbigdMMaYfkVPhGZcrEwp"));
    }
}
