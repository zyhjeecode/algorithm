package com.algorithm.offer;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 输入一颗二叉树的根节点和一个整数，按 字典序 打印出二叉树中结点值的和为输入整数的所有路径。
 * 路径定义为从 树的根结点开始往下一直到叶结点 所经过的结点形成一条路径。
 */
public class FindPath_24 {
    public ArrayList<ArrayList<Integer>> FindPath(TreeNode root, int target) {
        ArrayList<ArrayList<Integer>> pathList = new ArrayList<>();
        ArrayList<Integer> currList = new ArrayList<>();
        if (root==null){
            return pathList;
        }

        getAllPath(root, target, 0, currList, pathList);


        Collections.sort(pathList,(list1,list2)->list2.size()-list1.size());
        return pathList;
    }

    private void getAllPath(TreeNode root, int target, int currentSum, ArrayList<Integer> currList, ArrayList<ArrayList<Integer>> pathList) {
        if (root.left == null && root.right == null) {
            if (target == root.val + currentSum) {
                currList.add(root.val);
                pathList.add(currList);
            }
            return;
        }
        if (root.left != null) {
            currentSum += root.val;
            currList.add(root.val);
            getAllPath(root.left, target, currentSum, new ArrayList<>(currList), pathList);
            currentSum-=root.val;
            currList.remove(currList.size()-1);
        }
        if (root.right != null) {
            currentSum += root.val;
            currList.add(root.val);
            getAllPath(root.right, target, currentSum, new ArrayList<>(currList), pathList);
        }
    }

    @Test
    public void test() {
        TreeNode root = new TreeNode(10);
        root.left = new TreeNode(5);
        root.right = new TreeNode(12);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(7);
        ArrayList<ArrayList<Integer>> arrayLists = FindPath(root, 22);
        arrayLists.forEach(item->{
            System.out.println(item.toString());
        });
    }
}
