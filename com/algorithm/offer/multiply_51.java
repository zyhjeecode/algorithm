package com.algorithm.offer;

import org.junit.Test;

import java.util.Arrays;

/**
 * @description:
 * @author: zyh
 * @create: 2021-05-20 20:29
 **/
public class multiply_51 {
    /**
     题目描述
     给定一个数组A[0,1,...,n-1],请构建一个数组B[0,1,...,n-1],
     其中B中的元素B[i]=A[0]*A[1]*...*A[i-1]*A[i+1]*...*A[n-1]。不能使用除法。
     （注意：规定B[0] = A[1] * A[2] * ... * A[n-1]，B[n-1] = A[0] * A[1] * ... * A[n-2];）
     除了自己以外其他数字的乘积
     对于A长度为1的情况，B无意义，故而无法构建，因此该情况不会存在。
     示例1
     输入
     [1,2,3,4,5]
     返回值
     [120,60,40,30,24]

     1  A1 A2 A3 A4
     A0  1 A2 A3 A4
     A0 A1  1 A3 A4
     A0 A1 A2  1 A4
     A0 A1 A2 A3  1
     */
    public int[] multiply(int[] A) {
        if (A==null||A.length<2){
            return null;
        }

        int[] res=new int[A.length];
        res[0]=1;
        for (int i = 1; i < A.length; i++) {
            //算出各位左测相乘的积存储到B
            res[i]=res[i-1]*A[i-1];
        }

        //temp代表右下角的1
        int temp=1;
        for (int j = A.length-2; j >=0 ; j--) {
            temp*=A[j+1];
            res[j]*=temp;
        }
        return res;
    }

    @Test
    public void  test(){
        final int[] multiply = multiply(new int[]{1, 2, 3, 4, 5});
        Arrays.stream(multiply).forEach(System.out::println);
    }
}
