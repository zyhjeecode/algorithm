package com.algorithm.offer;

import org.junit.Test;

/**
 * @description:
 * @author: zyh
 * @create: 2021-05-17 20:25
 **/
public class StrToInt_49 {
    /**
     题目描述
     将一个字符串转换成一个整数，要求不能使用字符串转换整数的库函数。 数值为0或者字符串不是一个合法的数值则返回0
     输入描述:
     输入一个字符串,包括数字字母符号,可以为空
     返回值描述:
     如果是合法的数值表达则返回该数字，否则返回0


     示例1
     输入
     "+2147483647"
     返回值
     2147483647
     示例2
     输入
     "1a33"
     返回值
     0
     */
    public int StrToInt(String str) {
        if (str==null||str.length()<1){
            return 0;
        }

        //正负值,默认正
        int flag=1;
        int num=0;

        for (int i = 0; i < str.toCharArray().length; i++) {
            if (i==0){
                if ((str.charAt(i)=='+'&&str.length()>1)){
                }else if (str.charAt(i)=='-'&&str.length()>1){
                    flag=-1;
                }else if (str.charAt(i)=='0'&&str.length()==1){
                }else if (str.charAt(i)>'0'&&str.charAt(i)<='9'){
                    num=str.charAt(i)-'0';
                } else{
                    return 0;
                }
            }else {
                if (str.charAt(i)<'0'||str.charAt(i)>'9'){
                    return 0;
                }else {
                    num=num*10+(str.charAt(i)-'0');
                }
            }
        }

        return flag*num;
    }

    @Test
    public void  test(){
        System.out.println(StrToInt("-2147483647"));
    }
}
