package com.algorithm.offer;

import org.junit.Test;

public class ReverseSentence_44 {
    /**
     * 题目描述
     * 牛客最近来了一个新员工Fish，每天早晨总是会拿着一本英文杂志，写些句子在本子上。
     * <p>
     * 同事Cat对Fish写的内容颇感兴趣，有一天他向Fish借来翻看，但却读不懂它的意思。例如，“nowcoder. a am I”。
     * 后来才意识到，这家伙原来把句子单词的顺序翻转了，正确的句子应该是“I am a nowcoder.”。
     * Cat对一一的翻转这些单词顺序可不在行，你能帮助他么？
     * 示例1
     * 输入
     * "nowcoder. a am I"
     * 返回值
     * "I am a nowcoder."
     */
    public String ReverseSentence(String str) {
        if (str.length() < 1) {
            return str;
        }

        String res = "", temp = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ' ') {
                temp += str.charAt(i);
            } else {
                res = " " + temp + res;
                temp="";
            }
        }

        return temp+res;
    }

    @Test
    public void test() {
        System.out.println(ReverseSentence("nowcoder. a am I"));
    }
}
