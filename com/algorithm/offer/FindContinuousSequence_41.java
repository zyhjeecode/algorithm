package com.algorithm.offer;

import org.junit.Test;

import java.util.ArrayList;

/**
 * @description: 合为s的连续正数序列
 * @author: zyh
 * @create: 2021-04-30 15:49
 * <p>
 * 题目描述
 * 小明很喜欢数学,有一天他在做数学作业时,要求计算出9~16的和,他马上就写出了正确答案是100。
 * 但是他并不满足于此,他在想究竟有多少种连续的正数序列的和为100(至少包括两个数)。
 * 没多久,他就得到另一组连续正数和为100的序列:18,19,20,21,22。
 * 现在把问题交给你,你能不能也很快的找出所有和为S的连续正数序列? Good Luck!
 * <p>
 * (a+b)*n/2=res
 * <p>
 * 2a=201
 * a=101
 * <p>
 * 返回值描述:
 * 输出所有和为S的连续正数序列。序列内按照从小至大的顺序，序列间按照开始数字从小到大的顺序
 * <p>
 * 输入 9
 * 输出 [[2,3,4],[4,5]]
 **/
public class FindContinuousSequence_41 {
    public ArrayList<ArrayList<Integer>> FindContinuousSequence(int sum) {
        ArrayList<ArrayList<Integer>> res=new ArrayList<>();

        int leftIndex = 1;
        int rightIndex = 2;

        while (rightIndex>leftIndex){
            int curSum=(leftIndex + rightIndex) * (rightIndex - leftIndex + 1) / 2;
            if (curSum>sum){
                leftIndex++;
            }else if (curSum<sum){
                rightIndex++;
            }else {
                ArrayList<Integer> item=new ArrayList<>();

                for (int i = leftIndex; i <=rightIndex ; i++) {
                    item.add(i);
                }
                res.add(item);

                leftIndex++;
            }
        }

        return res;
    }

    @Test
    public void  test(){
        ArrayList<ArrayList<Integer>> lists = FindContinuousSequence(10);
        lists.forEach(item->{
            item.forEach(itemj->{
                System.out.print(itemj+" ");
            });
            System.out.println();
        });
    }
}
