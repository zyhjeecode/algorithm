package com.algorithm.offer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @description:
 * @author: zyh
 * @create: 2021-05-24 14:17
 **/
public class Print_60 {
    /**
     * 题目描述
     * 从上到下按层打印二叉树，同一层结点从左至右输出。每一层输出一行。
     */
    ArrayList<ArrayList<Integer>> Print(TreeNode pRoot) {
        ArrayList<ArrayList<Integer>> res=new ArrayList<>();

        if (pRoot==null){
            return res;
        }

        Queue<TreeNode> oddQueue=new LinkedList<>();
        Queue<TreeNode> evenQueue=new LinkedList<>();
        oddQueue.add(pRoot);

        //当前遍历奇书行
        boolean isOdd=true;
        //当前遍历的是
        while (!oddQueue.isEmpty()||!evenQueue.isEmpty()){
            ArrayList<Integer> itemRow= new ArrayList<>();

            if (isOdd){
                while (!oddQueue.isEmpty()){
                    final TreeNode poll = oddQueue.poll();
                    itemRow.add(poll.val);
                    if (poll.left!=null){
                        evenQueue.add(poll.left);
                    }
                    if (poll.right!=null){
                        evenQueue.add(poll.right);
                    }
                }
            }else {
                while (!evenQueue.isEmpty()){
                    final TreeNode poll = evenQueue.poll();
                    itemRow.add(poll.val);
                    if (poll.left!=null){
                        oddQueue.add(poll.left);
                    }
                    if (poll.right!=null){
                        oddQueue.add(poll.right);
                    }
                }
            }
            res.add(itemRow);
            isOdd=!isOdd;
            continue;
        }

        return res;
    }
}
