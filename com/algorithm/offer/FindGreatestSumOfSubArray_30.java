package com.algorithm.offer;

/**
 * 连续子数组的和的最大值_30
 */
public class FindGreatestSumOfSubArray_30 {
    public int FindGreatestSumOfSubArray(int[] array) {
        int max=array[0];
        for (int i = 1; i < array.length; i++) {
            array[i]+=Math.max(array[i-1],0);
            max=Math.max(array[i],max);
        }
        return max;
    }
}
