package com.algorithm.offer;

import org.junit.Test;

/**
 * 题目描述
 * 大家都知道斐波那契数列，现在要求输入一个整数n，请你输出斐波那契数列的第n项（从0开始，第0项为0，第1项是1）。0 1 1 2 3 5
 * 除了第0位为0 第1为为1 其他的都是前两者的和
 */
public class Fibonacci_07 {
    public int fibonacci(int n) {
        if (n==1||n==0){
            return n;
        }else {
            return fibonacci(n-1) + fibonacci(n-2);
        }
    }
    public int fibonacciSimple(int n) {
        int [] nums=new int[n+1];

        nums[0]=0;
        nums[1]=1;
        for (int i = 2; i <= n; i++) {
            nums[i]=nums[i-1]+nums[i-2];
        }
        return nums[n];

    }

    public int fibonacciSimplest(int n) {

        //用于存放
        int sum=1;
        int one=0;
        for (int i = 2; i <= n; i++) {
            sum=sum+one;
            one=sum-one;
        }
        return sum;

    }

    @Test
    public void  test(){
        System.out.println(fibonacciSimple(0));
    }
}
