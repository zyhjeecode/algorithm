package com.algorithm.offer;

/**
 * @description: 两个链表的第一个公共结点
 * @author: zyh
 * @create: 2021-04-26 21:38
 **/
public class FindFirstCommonNode_36 {
    /**
     * 输入两个链表，找出它们的第一个公共结点。（注意因为传入数据是链表，所以错误测试数据的提示是用其他方式显示的，保证传入数据是正确的）
     * aaaa
           xaaaaa
     *   aa
     */
    public ListNode FindFirstCommonNode(ListNode pHead1, ListNode pHead2) {

        ListNode pHead1Index=pHead1;
        ListNode pHead2Index=pHead2;
        while (pHead1Index!=pHead2Index){
            pHead1Index=pHead1Index==null?pHead2:pHead1Index.next;
            pHead2Index=pHead2Index==null?pHead1:pHead2Index.next;
        }
        return pHead1Index;
    }
}
