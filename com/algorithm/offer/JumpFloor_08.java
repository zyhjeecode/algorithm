package com.algorithm.offer;

import org.junit.Test;

public class JumpFloor_08 {
    /**
     * 一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个n级的台阶总共有多少种跳法（先后次序不同算不同的结果）。
     * 0 1 2 3 5 8
     */
    public int jumpFloor(int target) {
        if (target==0||target==1){
            return target;
        }
        int sum=1;
        int one =1;
        for (int i = 2; i <=target ; i++) {
            sum=sum+one;
            one=sum-one;
        }
        return sum;
    }

    @Test
    public void  test(){
        System.out.println(jumpFloor(1));
    }
}
