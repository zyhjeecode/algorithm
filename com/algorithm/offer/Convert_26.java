package com.algorithm.offer;

/**
 * 输入一棵二叉 搜索树，将该二叉搜索树转换成一个排序的双向链表。
 * 要求不能创建任何新的结点，只能调整树中结点指针的指向。
 */
public class Convert_26 {
    /**
     * 已排链表的最后一个结点
     */
    private TreeNode lastNode=null;


    public TreeNode Convert(TreeNode pRootOfTree) {
        if (pRootOfTree==null){
            return null;
        }

        //获取其左子树双向链表的头结点
        TreeNode head = Convert(pRootOfTree.left);

        // 如果左子树为空，那么根节点root为此时双向链表的头节点
        if (head==null){
            head=pRootOfTree;
        }

        //连接当前结点和之前的链表
        pRootOfTree.left=lastNode;
        if (lastNode!=null) {
            lastNode.right=pRootOfTree;
        }

        //更新最后一个结点
        lastNode=pRootOfTree;


        //安排右节点
        Convert(pRootOfTree.right);

        return  head;
    }

}
