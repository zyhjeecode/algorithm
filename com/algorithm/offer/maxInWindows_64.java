package com.algorithm.offer;

import org.junit.Test;

import java.util.ArrayList;

/**
 * @description:
 * @author: zyh
 * @create: 2021-05-25 22:05
 **/
public class maxInWindows_64 {
    /**
     * 题目描述
     * 给定一个数组和滑动窗口的大小，找出所有滑动窗口里数值的最大值。
     * 例如，如果输入数组{2,3,4,2,6,2,5,1}及滑动窗口的大小3，那么一共存在6个滑动窗口，他们的最大值分别为{4,4,6,6,6,5}；
     * 针对数组{2,3,4,2,6,2,5,1}的滑动窗口有以下6个：
     * {[2,3,4],2,6,2,5,1}， {2,[3,4,2],6,2,5,1}， {2,3,[4,2,6],2,5,1}， {2,3,4,[2,6,2],5,1}， {2,3,4,2,[6,2,5],1}， {2,3,4,2,6,[2,5,1]}。
     * <p>
     * 窗口大于数组长度的时候，返回空
     * 示例1
     * 输入
     * [2,3,4,2,6,2,5,1],3
     * 返回值
     * [4,4,6,6,6,5]
     */
    public ArrayList<Integer> maxInWindows(int[] num, int size) {
        ArrayList<Integer> res = new ArrayList<>(num.length);
        if (size > num.length||num.length<1||size<1) {
            return res;
        }
        int left = 0;
        int right = size - 1;
        while (right <= num.length - 1) {
            res.add(getMax(num, left, right));
            left++;
            right++;
        }

        return res;
    }

    private int getMax(int[] num, int left, int right) {
        int max = Integer.MIN_VALUE;
        //找到left到right里的最大数值
        for (int i = left; i <= right; i++) {
            max = Math.max(num[i], max);
        }
        return max;
    }

    @Test
    public void  test(){
        int[] input=new int[]{2,3,4,2,6,2,5,1};
        ArrayList<Integer> integers = maxInWindows(input, 3);
        integers.stream().forEach(System.out::print);
    }
}
