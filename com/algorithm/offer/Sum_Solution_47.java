package com.algorithm.offer;

import org.junit.Test;

/**
 * @description: 1+..n
 * @author: zyh
 * @create: 2021-05-13 23:23
 **/
public class Sum_Solution_47 {
    //    题目描述
//    求1+2+3+...+n，要求不能使用乘除法、for、while、if、else、switch、case等关键字及条件判断语句（A?B:C）。
//    示例1
//    输入 5
//    输出 15
    public int Sum_Solution(int n) {
        //求和,那么就用递归呗
        if (n == 1) {
            return 1;
        }

        return n + Sum_Solution(n - 1);
    }

    public int Sum_Solution2(int n) {
        int sum=n;
        boolean temp = (n > 0) && ((sum += Sum_Solution(n - 1)) > 0);
        return sum;
    }

    @Test
    public void test() {
        System.out.println(Sum_Solution(5));
    }

}
