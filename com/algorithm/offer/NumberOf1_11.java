package com.algorithm.offer;

import org.junit.Test;

/**
 * @description: 二进制中1的个数
 * @author: zyh
 * @create: 2021-03-11 21:36
 **/
public class NumberOf1_11 {

    //题目:输入一个整数，输出该数32位二进制表示中1的个数。其中负数用补码表示。

    //思路:
    // 如果一个整数不为0，那么这个整数至少有一位是1。如果我们把这个整数减1，
    // 那么原来处在整数最右边的1就会变为0，原来在1后面的所有的0都会变成1(如果最右边的1后面还有0的话)。
    // 其余所有位将不会受到影响。
    // 4 -> 1 0 0
    // 5 -> 1 0 1

    // 12     1 1 0 0

    public int NumberOf1(int n) {
        int count=0;
        while(n!=0){
            count++;
            //这里做与运算正好可以把原本最右边的1后面的0都给去掉
            //1 1 0 0 & 1 0 1 1=10000
            n=n&(n-1);
        }
        return count;
    }


    @Test
    public void  test(){
        System.out.println(NumberOf1(12));
    }
}
