package com.algorithm.offer;

/**
 * @description:
 * @author: zyh
 * @create: 2021-05-24 10:35
 **/
public class isSymmetrical_58 {
    /**
     题目描述
     请实现一个函数，用来判断一棵二叉树是不是对称的。
     注意，如果一个二叉树同此二叉树的镜像是同样的，定义其为对称的。

     示例1
     输入
     {8,6,6,5,7,7,5}
     返回值
     true

     示例2
     输入
     {8,6,9,5,7,7,5}
     返回值
     false
     */

    boolean isSymmetrical(TreeNode pRoot) {
        if (pRoot==null){
            return true;
        }
        return check(pRoot.left,pRoot.right);
    }

    private boolean check(TreeNode left, TreeNode right) {
        if (left==null){
            return right==null;
        }
        if (right==null){
            return false;
        }
        if (left.val!=right.val){
            return false;
        }

        return check(left.left,right.right)&&check(left.right,right.left);
    }
}
