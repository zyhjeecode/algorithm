package com.algorithm.offer;

import org.junit.Test;

public class ReOrderArray_13 {
    public int[] reOrderArray (int[] array) {
        // write code here
        //输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有的奇数位于数组的前半部分，
        //所有的偶数位于数组的后半部分，并保证奇数和奇数，偶数和偶数之间的相对位置不变。
        //只要是奇数就与前面的偶数进行交换，直到前面的也是奇数
        for (int i = 0; i < array.length; i++) {
            int tempIndex=i;
            while ((array[tempIndex]&1)==1&&tempIndex>0&&(array[tempIndex-1]&1)==0){
                swap(array,tempIndex,tempIndex-1);
                tempIndex--;
            }
        }
        return array;
    }
    public void swap(int[] arr,int indexa,int indexb){
        int temp=arr[indexa];
        arr[indexa]=arr[indexb];
        arr[indexb]=temp;
    }

    @Test
    public void  test(){
        for (int i :reOrderArray(new int[]{1,2,4,5,2,13,6,7,4})) {
            System.out.print(i+" ");
        }
    }
}
