package com.algorithm.offer;

import java.util.LinkedList;
import java.util.Queue;

public class Serialize_61 {
    /**
     * 题目描述
     * 请实现两个函数，分别用来序列化和反序列化二叉树
     *
     * 二叉树的序列化是指：把一棵二叉树按照某种遍历方式的结果以某种格式保存为字符串，从而使得内存中建立起来的二叉树可以持久保存。
     * 序列化可以基于先序、中序、后序、层序的二叉树遍历方式来进行修改，
     * 序列化的结果是一个字符串，序列化时通过 某种符号表示空节点（#），以 ！ 表示一个结点值的结束（value!）。
     *
     * 二叉树的反序列化是指：根据某种遍历顺序得到的序列化字符串结果str，重构二叉树。
     *
     * 例如，我们可以把一个只有根节点为1的二叉树序列化为"1,"，然后通过自己的函数来解析回这个二叉树
     */

    String Serialize(TreeNode root) {
        if (root==null){
            return "#!";
        }
        //先序遍历
        String res=root.val+"!";
        res+=Serialize(root.left);
        res+=Serialize(root.right);
        return res;
    }

    TreeNode Deserialize(String str) {
        if (str==null){
            return null;
        }
        String[] nodes=str.split("!");
        Queue<String> queue= new LinkedList<>();
        for (String node : nodes) {
            queue.offer(node);
        }
        return reBuildTree(queue);
    }

    private TreeNode reBuildTree(Queue<String> queue) {
        String poll = queue.poll();
        if ("#".equals(poll)){
            return null;
        }
        TreeNode currNode=new TreeNode(Integer.parseInt(poll));
        currNode.left=reBuildTree(queue);
        currNode.right=reBuildTree(queue);
        return currNode;
    }
}
