package com.algorithm.offer;

import org.junit.Test;

import java.util.Stack;

//输入两个整数序列，第一个序列表示栈的压入顺序，请判断第二个序列是否可能为该栈的弹出顺序。
// 假设压入栈的所有数字均不相等。
// 例如序列1,2,3,4,5是某栈的压入顺序，序列4,5,3,2,1是该压栈序列对应的一个弹出序列，
// 但4,3,5,1,2就不可能是该压栈序列的弹出序列。（注意：这两个序列的长度是相等的）
public class IsPopOrder_21 {

    public boolean IsPopOrder(int [] pushA,int [] popA) {
        Stack<Integer> stackA=new Stack();
        if (pushA.length==0||popA.length==0){
            return false;
        }

        int hasPopIndex=0;

        for (int item : pushA) {
            stackA.push(item);

            //如果栈不为空，且栈顶元素等于弹出序列
            while (!stackA.isEmpty()&&stackA.peek()==popA[hasPopIndex]){
                stackA.pop();
                hasPopIndex++;
            }
        }

        return stackA.isEmpty();
    }

    @Test
    public void  test(){
        int[] pushNum=new int[]{1,2,3,4,5};
        int[] num1=new int[]{4,5,3,2,1};
        int[] num2=new int[]{4,3,5,1,2};
        System.out.println(IsPopOrder(pushNum,num1));

        System.out.println(IsPopOrder(pushNum,num2));
    }
}
