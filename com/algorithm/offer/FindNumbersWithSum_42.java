package com.algorithm.offer;

import java.util.ArrayList;

/**
 * 题目描述
 * 输入一个 递增排序 的数组和一个数字S，
 * 在数组中查找两个数，使得他们的和正好是S，如果有多对数字的和等于S，输出两个数的乘积最小的。
 * 返回值描述:
 * 对应每个测试案例，输出两个数，小的先输出。
 *
 *
 * 示例1
 * 输入
 * [1,2,4,7,11,15],15
 * 返回值
 * [4,11]
 */
public class FindNumbersWithSum_42 {
    public ArrayList<Integer> FindNumbersWithSum(int [] array, int sum) {
        ArrayList<Integer> res=new ArrayList<>();
        if (array==null||array.length<2){
            return res;
        }

        int low=0,high=array.length-1;
        while (low<high){
            if (array[low]+array[high]>sum){
                high--;
            }else if (array[low]+array[high]<sum){
                low++;
            }else {
                res.add(array[low]);
                res.add(array[high]);
                return res;
            }
        }
        return res;
    }

}
