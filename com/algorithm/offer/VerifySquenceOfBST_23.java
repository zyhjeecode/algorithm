package com.algorithm.offer;

import org.junit.Test;

/**
 * @description: 输入一个整数数组，判断该数组是不是某二叉搜索树的后序遍历的结果。
 * 如果是则返回true,否则返回false。
 * 假设输入的数组的任意两个数字都互不相同。（ps：我们约定空树不是二叉搜素树）
 * @author: zyh
 * @create: 2021-04-12 19:39
 **/
public class VerifySquenceOfBST_23 {
    /**
     * 思路:判断是否能根据数组成功重建二叉树
     */
    public boolean VerifySquenceOfBST(int [] sequence) {
        if (sequence==null||sequence.length<1){
            return false;
        }
        if (sequence.length==1){
            return true;
        }

        //每个子数组中最后一个元素为根节点,找到第一个大于根节点的位置，则该位置左边为左子树，右边为右子树；
        return checkArr(sequence,0,sequence.length-1);
    }

    private boolean checkArr(int[] sequence, int startIndex, int endIndex) {
        if (startIndex>=endIndex){
            return true;
        }

        //最后一个数字为根
        int rootNum=sequence[endIndex];

        //找到左子树结束的点
        int leftEndIndex=startIndex-1;

        for (int i = startIndex; i <endIndex ; i++) {
            if (sequence[i]<rootNum){
                leftEndIndex++;
            }else {
                break;
            }
        }

        //左子树的节点值都应该小于根
        for (int i = startIndex; i <=leftEndIndex ; i++) {
            if (sequence[i]>rootNum){
                return false;
            }
        }

        //右子树的节点值都应该大于根
        for (int i = leftEndIndex+1; i <=endIndex-1 ; i++) {
            if (sequence[i]<rootNum){
                return false;
            }
        }

        return checkArr(sequence,startIndex,leftEndIndex)&&checkArr(sequence,leftEndIndex+1,endIndex-1);
    }

    @Test
    public void test(){
        int[] arr=new int[]{5,4,3,2,1};
        System.out.println(VerifySquenceOfBST(arr));
    }
}
