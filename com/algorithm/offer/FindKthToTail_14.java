package com.algorithm.offer;

import org.junit.Test;

public class FindKthToTail_14 {
    //输入一个链表，输出该链表中倒数第k个结点。
    //如果该链表长度小于k，请返回空。

    public ListNode FindKthToTail (ListNode pHead, int k) {
        // 可以用双指针走
        //第一个指针先走k步，第二个指针再与第一个指针同时走，当前面的指针走到最后一个结点时候，第二个指针即为倒数第k个

        //为啥呢？
        //假设总共有10个结点，我们要倒数第4个结点，那么其实是第二个结点就要走6步，所以我们第一个结点其实是负责通知第二个结点啥时候开始走的,啥时候停的，你看看多负责，有始有终的

        if (pHead==null||k<=0){
            return null;
        }
        // 1 2 3 4 5 ->2
        ListNode pre=pHead,last=pHead;
        int step=1;
        while (pre.next!=null){
            pre=pre.next;
            step++;
            if (step>k){
                last=last.next;
            }
        }
        return step>=k?last:null;
    }


    @Test
    public void  test(){
      ListNode listNode= new ListNode(1) ;
      listNode.next=new ListNode(2);
      listNode.next.next=new ListNode(3);
      listNode.next.next.next=new ListNode(4);
      listNode.next.next.next.next=new ListNode(5);
      System.out.println(FindKthToTail(listNode,6).val);
    }
}
