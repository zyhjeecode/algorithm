package com.algorithm.offer;

import java.util.*;

public class FirstAppearingOnce_54 {

    /**
     题目描述
     请实现一个函数用来找出字符流中 第一个只出现一次的字符。
     例如，当从字符流中只读出前两个字符"go"时，第一个只出现一次的字符是"g"。当从该字符流中读出前六个字符“google"时，第一个只出现一次的字符是"l"。

     后台会用以下方式调用Insert 和 FirstAppearingOnce 函数
     string caseout = "";
     1.读入测试用例字符串casein
     2.如果对应语言有Init()函数的话，执行Init() 函数
     3.循环遍历字符串里的每一个字符ch {
     Insert(ch);
     caseout += FirstAppearingOnce()
     }
     2. 输出caseout，进行比较。

     返回值描述:
     如果当前字符流没有存在出现一次的字符，返回#字符。
     */

    //存放每个字符以及其数量
    Map<Character,Integer> countRes=new HashMap<>();
    //由于hashmap是无需的因此这里存放一个原始字符串，存放字符串的每个字符，其索引及其位置
    List<Character> listStr=new LinkedList<>();
    public void Insert(char ch)
    {
        if (countRes.containsKey(ch)){
            countRes.put(ch,countRes.get(ch)+1);
        }else {
            countRes.put(ch,1);
        }
        listStr.add(ch);
    }

    public char FirstAppearingOnce()
    {
        for (char charItem: listStr) {
            if (countRes.get(charItem)==1){
                return charItem;
            }
        }
        return '#';
    }

//
//    Map<Character,Integer> res=new LinkedHashMap<>();
//    public void Insert(char ch)
//    {
//        if (res.containsKey(ch)){
//            res.put(ch,res.get(ch)+1);
//        }else {
//            res.put(ch,1);
//        }
//    }
//
//    public char FirstAppearingOnce()
//    {
//        for (char key:res.keySet()) {
//            if (res.get(key)==1){
//                return key;
//            }
//        }
//        return '#';
//    }
}
