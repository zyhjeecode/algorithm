package com.algorithm.offer;

import org.junit.Test;

import java.util.Arrays;

public class IsContinuous_45 {
    /**
     * 题目描述
     * 现在有五张扑克牌，我们需要来判断一下是不是顺子。
     * <p>
     * <p>
     * 有如下规则：
     * A为1,J为11,Q为12,K为13
     * 数据中的0可以看作任意牌
     * 如果给出的五张牌能组成顺子（即这五张牌是连续的）就输出true，否则就输出false。
     * 例如：给出数据[6,0,2,0,4]
     * 中间的两个0一个看作3，一个看作5 。即：[6,3,2,5,4]
     * 这样这五张牌在[2,6]区间连续，输出true
     * 数据保证每组5个数字，每组最多含有4个零
     * <p>
     * 示例1
     * 输入
     * [0,3,2,6,4]
     * 返回值
     * true
     */
    public boolean IsContinuous(int[] numbers) {
        Arrays.sort(numbers);
        //判断相邻的非0整数之间总共相差几
        int len = 0;
        int zeroCount = 0;
        //上个非0数字
        int last = -1;
        for (int number : numbers) {
            if (number == last) {
                return false;
            } else {
                if (number != 0) {
                    if (last != -1) {
                        len += number - last-1;
                    }
                    last = number;
                } else {
                    zeroCount++;
                }
            }
        }
        return len == 0 || len == zeroCount;
    }

    public boolean IsContinuous_2(int[] numbers) {
        Arrays.sort(numbers);
        int k = 0;
        for (int i = 0; i < numbers.length - 1; i++) {
            if (numbers[i] == 0) {
                k++;
            } else if (numbers[i] == numbers[i + 1]) {
                return false;
            }
        }
        return numbers[4] - numbers[k] <= 4;
    }

    @Test
    public void test() {
        System.out.println(IsContinuous(new int[]{1, 2, 0, 0, 0}));
    }
}
