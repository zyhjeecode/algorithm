package com.algorithm.offer;

import org.junit.Test;

public class Power_12 {
    public double Power(double base, int exponent) {
        double res=1;
        boolean negative=false;
        if (exponent==0){
            if (base==0){
                throw new RuntimeException("分母不能为0");
            }
        }else if (exponent<0){
            negative=true;
            exponent*=-1;
        }


        while (exponent>0){
            if ((exponent&1)==1){
                res*=base;
            }
            base*=base;
            exponent>>=1;
        }
        return negative?1/res:res;
    }

    @Test
    public void  test(){
        double res = Power(2, -3);
        System.out.println(res);
    }
}
