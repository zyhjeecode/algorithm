package com.algorithm.offer;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 题目描述
 * 把只包含质因子2、3和5的数称作丑数（Ugly Number）。
 * 例如6、8都是丑数，但14不是，因为它包含质因子7。
 * 习惯上我们把1当做是第一个丑数。求按从小到大的顺序的第N个丑数
 *
 * 输入
 * 7
 * 返回值
 * 8
 */
public class GetUglyNumber_Solution_33 {


    public int GetUglyNumber_Solution_2(int index) {
        if (index<7){
            return index;
        }
        //乘2最小数的索引
        int min2Index=0;
        //乘3最小数的索引
        int min3Index=0;
        //乘5最小数的索引
        int min5Index=0;
        List<Integer> res=new ArrayList<>();
        res.add(1);
        while (res.size()<index){
            int min2=res.get(min2Index)*2;
            int min3=res.get(min3Index)*3;
            int min5=res.get(min5Index)*5;

            int minValue=Math.min(min2,Math.min(min3,min5));
            res.add(minValue);

            if (minValue==min2){
                min2Index++;
            }
            if (minValue==min3){
                min3Index++;
            }
            if (minValue==min5){
                min5Index++;
            }
        }
        return res.get(index-1);
    }

        @Test
    public void  test(){
        System.out.println(GetUglyNumber_Solution_2(7));
    }
}
