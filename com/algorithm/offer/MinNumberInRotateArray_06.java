package com.algorithm.offer;

/**
 *
 题目描述
 把一个数组最开始的若干个元素搬到数组的末尾，我们称之为数组的旋转。
 输入一个非递减排序的数组的一个旋转，输出旋转数组的最小元素。
 NOTE：给出的所有元素都大于0，若数组大小为0，请返回0。

 输入
 [3,4,5,1,2]
 返回值
 1
 */
public class MinNumberInRotateArray_06 {
    /**
     要素：非递减排序的数组
     */
    public int minNumberInRotateArray(int [] array) {
        int length=array.length;
        if (length==0){
            return 0;
        }

        if (length==1){
            return array[0];
        }
        int left=0;
        int right=array.length-1;

        while (left<right){
            int mid= left +(right-left)/2;
            if (array[mid]>array[right]){
                left=mid+1;
            }else if (array[mid]==array[right]){
                right--;
            }else {
                right=mid;
            }
        }
        return array[left];


    }
}
