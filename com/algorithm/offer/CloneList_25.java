package com.algorithm.offer;


/**
 * 输入一个复杂链表（每个节点中有节点值，以及两个指针，一个指向下一个节点，
 * 另一个特殊指针random指向一个随机节点），请对此链表进行深拷贝，并返回拷贝后的头结点。
 * （注意，输出结果中请不要返回参数中的节点引用，否则判题程序会直接返回空)
 */
public class CloneList_25 {
    public RandomListNode Clone(RandomListNode pHead) {
        if (pHead==null){
            return null;
        }

        //1.复制每个结点到原结点之后
        RandomListNode currNode=pHead;
        while (currNode!=null){
            RandomListNode currNodeClon=new RandomListNode(currNode.label);
            RandomListNode nextNode=currNode.next;
            currNode.next=currNodeClon;
            currNodeClon.next=nextNode;
            currNode =nextNode;
        }

        //赋值后的结点树是偶数个
        //重新遍历链表将老结点的随机结点的拷贝份 赋值给克隆结点随机结点
        currNode=pHead;
        while (currNode!=null){
            //注意为什么这里currNode.next.random=currNode.random.next而不是currNode.next.random=currNode.random
            //因为我们要让拷贝结点的随机结点指向也指向拷贝份
            currNode.next.random=currNode.random==null?null:currNode.random.next;
            currNode=currNode.next.next;
        }

        //拆分链表,拆除新旧链表
        RandomListNode cloneHead=pHead.next;
        currNode=pHead;
        while (currNode!=null){
            RandomListNode cloneNode=currNode.next;
            currNode.next=cloneNode.next;
            cloneNode.next=cloneNode.next==null?null:cloneNode.next.next;
            currNode=currNode.next;
        }
        return cloneHead;
    }
}
