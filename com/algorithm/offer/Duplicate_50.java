package com.algorithm.offer;

import org.junit.Test;

public class Duplicate_50 {

    /**
     *
     题目描述
     在一个长度为n的数组里的所有数字都在0到n-1的范围内。
     数组中某些数字是重复的，但不知道有几个数字是重复的。也不知道每个数字重复几次。
     请找出数组中任一一个重复的数字。 例如，如果输入长度为7的数组[2,3,1,0,2,5,3]，那么对应的输出是2或者3。存在不合法的输入的话输出-1

     示例1
     输入
     [2,3,1,0,2,5,3]
     返回值
     2
     说明
     2或3都是对的
     */

    public int duplicate (int[] numbers) {
        // write code here
        int[] count=new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            count[numbers[i]]++;
            if (count[numbers[i]]==2){
                return numbers[i];
            }
        }
        return -1;
    }

    @Test
    public void  test(){
        System.out.println(duplicate(new int[]{2,3,1,0,2,5,3}));

    }
}
