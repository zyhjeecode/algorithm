package com.algorithm.offer;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;

/**
 * @description: 数字在升序数组中出现的次数
 * @author: zyh
 * @create: 2021-04-26 21:59
 **/
public class GetNumberOfK_37 {
    /**
     * 题目描述
     * 统计一个数字在升序数组中出现的次数。
     * 示例1
     * 输入
     * [1,2,3,3,3,3,4,5],3
     * 返回值
     * 4
     */
    public int GetNumberOfK(int[] array, int k) {
        if (array == null || array.length < 1) {
            return 0;
        }
        return binarySearch(array, k + 0.5) - binarySearch(array, k - 0.5);
    }

    private int binarySearch(int[] array, double k) {
        int low = 0, high = array.length - 1;
        while (low <= high) {
            int mid = getMidIndex(low, high);
            if (k < array[mid]) {
                high = mid - 1;
            } else if (k > array[mid]) {
                low = mid + 1;
            }
        }
        return low;
    }

    public int getMidIndex(int left, int right) {
        return left + (right - left) / 2;
    }

    @Test
    public void test() {
        System.out.println(GetNumberOfK(new int[]{1, 2, 3, 3, 3, 3, 4, 5}, 3));
    }


    //统计一个数字在排序数组中出现的次数
    public int GetNumberOfK_2(int[] array, int k) {
        if (array == null) {
            return 0;
        }
        if (array.length == 1 && array[0] == k) {
            return 1;
        }
        int firstKIndex = getFirstKIndex(array, k);
        int lastKIndex = getLastKIndex(array, k);
        return firstKIndex == lastKIndex ? 0 : lastKIndex - firstKIndex + 1;
    }

    public int getFirstKIndex(int[] array, int k) {//得到第一个k---右结点向左移动
        int left = 0, right = array.length - 1;
        int mid = getMidIndex(left, right);
        while (left <= right) {
            if (array[mid] == k && mid - 1 >= 0 && array[mid - 1] == k) {
                right = mid - 1;
            } else if (array[mid] > k) {
                right = mid - 1;
            } else if (array[mid] < k) {
                left = mid + 1;
            } else {
                return mid;
            }
            mid = getMidIndex(left, right);
        }
        return -1;
    }

    public int getLastKIndex(int[] array, int k) {//得到第一个k---左结点向右移动
        int left = 0, right = array.length - 1;
        int mid = getMidIndex(left, right);
        while (left <= right) {
            if (array[mid] == k && mid + 1 <= array.length - 1 && array[mid + 1] == k) {
                left = mid + 1;
            } else if (array[mid] > k) {
                right = mid - 1;
            } else if (array[mid] < k) {
                left = mid + 1;
            } else {
                return mid;
            }
            mid = getMidIndex(left, right);
        }
        return -1;
    }

}
