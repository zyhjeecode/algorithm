package com.algorithm.offer;

/**
 * @description: 数组中超过一半的数字
 * @author: zyh
 * @create: 2021-04-20 20:42
 **/

import org.junit.Test;

/**
 * 题目描述
 * 数组中有一个数字出现的次数超过数组长度的一半，请找出这个数字。例如输入一个长度为9的数组{1,2,3,2,2,2,5,4,2}。由于数字2在数组中出现了5次，超过数组长度的一半，因此输出2。如果不存在则输出0。
 *
 * 输入
 * [1,2,3,2,2,2,5,4,2]
 * 返回值
 * 2
 */
public class MoreThanHalfNum_Solution_28 {

    public int MoreThanHalfNum_Solution(int [] array) {
        if (array.length==1){
            return array[0];
        }

        int survivor = array[0];
        int surCount=1;
        for (int i = 0; i < array.length; i++) {
            if (surCount==0){
                survivor=array[i];
                surCount++;
            }else if (survivor!=array[i]) {
                surCount--;
            }else  if (survivor==array[i]){
                surCount++;
            }
        }

        int count=0;
        for(int i=0;i<array.length;i++){
            if (array[i]==survivor){
                count++;
            }
        }

        if (count>array.length/2){
            return survivor;
        }else {
            return 0;
        }
    }

    @Test
    public void  test(){
        int[] arr= new int[]{1,2,3,2,2,2,5,4,2};
        System.out.print(MoreThanHalfNum_Solution(arr));
    }
}
