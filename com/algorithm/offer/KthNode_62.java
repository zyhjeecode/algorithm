package com.algorithm.offer;

import org.junit.Test;

import java.util.Stack;

/**
 * @description:
 * @author: zyh
 * @create: 2021-05-25 19:34
 **/
public class KthNode_62 {
    /**
     * 题目描述
     * 给定一棵二叉搜索树，请找出其中的第k小的TreeNode结点。
     * 示例1
     * 输入
     * {5,3,7,2,4,6,8},3
     * 返回值
     * {4}
     * 说明
     * 按结点数值大小顺序第三小结点的值为4
     */
    //当前遍历的是数字第count小的数据
    int count = 1;

    TreeNode KthNode(TreeNode pRoot, int k) {
        if (pRoot == null || k < 1) {
            return null;
        }

        TreeNode res = midSearch(pRoot, k);
        return res;
    }

    //proot不为null
    private TreeNode midSearch(TreeNode pRoot, int k) {
        Stack<TreeNode> stack = new Stack<>();
        while (!stack.isEmpty() || pRoot != null) {
            if (pRoot != null) {
                stack.push(pRoot);
                pRoot = pRoot.left;
            } else {
                pRoot = stack.pop();
                if (count == k) {
                    return pRoot;
                } else {
                    pRoot= pRoot.right;
                }
                count++;
            }
        }
        return null;
    }

    @Test
    public void test(){
        TreeNode head = new TreeNode(5);
        head.left = new TreeNode(3);
        head.right = new TreeNode(8);
        head.left.left = new TreeNode(2);
        head.left.right = new TreeNode(4);
        head.left.left.left =new TreeNode(1);
        head.right.left = new TreeNode(7);
        head.right.left.left =new TreeNode(6);
        head.right.right = new TreeNode(10);
        head.right.right.left =new TreeNode(9);
        head.right.right.right =new TreeNode(11);
        final TreeNode treeNode = KthNode(head, 4);
        System.out.println(treeNode.val);
    }
}



