package com.algorithm.offer;


import java.util.ArrayList;

/**
 * 从上往下打印出二叉树的每个节点，同层节点从左至右打印。
 */
public class PrintFromTopToBottom_22 {
    public ArrayList<Integer> PrintFromTopToBottom(TreeNode root) {
        ArrayList<Integer> res=new ArrayList<>();
        if (root==null){
            return res;
        }

        ArrayList<TreeNode> queue=new ArrayList<>();

        queue.add(root);

        while (queue.size()!=0){
            TreeNode currNode= queue.remove(0);
            res.add(currNode.val);

            if (currNode.left!=null){
                queue.add(currNode.left);
            }
            if (currNode.right!=null){
                queue.add(currNode.right);
            }
        }
        return res;
    }

}
