package com.algorithm.offer;

import org.junit.Test;

import java.util.ArrayList;

/**
 * @description: 顺时针打印矩阵
 * @author: zyh
 * @create: 2021-04-09 21:33
 **/
//题目描述
// 输入一个矩阵，按照从外向里以顺时针的顺序依次打印出每一个数字，
// 例如，如果输入如下4 X 4矩阵：
// 1    2   3  4
// 5    6   7  8
// 9   10  11 12
// 13  14  15 16
// 则依次打印出数字
// 1,2,3,4,8,12,16,15,14,13,9,5,6,7,11,10.
// 1 2 3 4 8 12 16 15 14 13 9 5 1 6 7 11 10 6
public class PrintMatrix_19 {
    public ArrayList<Integer> printMatrix(int [][] matrix) {
        ArrayList res=new ArrayList<Integer>();

        //如果是单列或者单行直接返回
        if (matrix==null||matrix.length==0||matrix[0].length==0){
            return res;
        }

        //思路:每次打印一个长形的一周
        // 正方形由左上顶点和右下顶点进行确定

        //顶点xy值一样
        int lX=0;
        int lY=0;
        int rX=matrix.length-1;
        int rY=matrix[0].length-1;
        while (lX<=rX&&lY<=rY){
            addCircleNum(lX,lY,rX,rY,matrix,res);
            lX++;
            lY++;
            rX--;
            rY--;
        }

        return res;
    }


    private void addCircleNum(int lX, int lY,int rX, int rY,int [][] matrix, ArrayList res) {
        //如果数据是一行
        if (lX==rX){
            for (int i = lY; i <=rY ; i++) {
                res.add(matrix[rX][i]);
            }
            return;
        }else if (lY==rY){
            for (int i = lX; i <=rX ; i++) {
                res.add(matrix[i][rY]);
            }
            return;
        }

        //上边数据
        for (int i = lY; i <=rY ; i++) {
            res.add(matrix[lX][i]);
        }

        //右边数据
        for (int i = lX+1; i <=rX ; i++) {
            res.add(matrix[i][rY]);
        }

        //下边数据
        for (int i = rY-1; i >= lY; i--) {
            res.add(matrix[rX][i]);
        }

        //左边数据
        for (int i = rX-1; i >lX; i--) {
            res.add(matrix[i][lY]);
        }
    }

    @Test
    public void  test(){
        int[][] data=new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        ArrayList<Integer> res = printMatrix(data);
        res.forEach(item->{
            System.out.print(item+" ");
        });
    }
}
