package com.algorithm.offer;

import org.junit.Test;

/**
 * 二维数组中的查找
 *
 *题目描述
 * 在一个二维数组中（每个一维数组的长度相同），
 * 每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。
 * 请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
 *
 * 输入
 * 7,
 * [
 * [1,2,8,9],
 * [2,4,9,12],
 * [4,7,10,13],
 * [6,8,11,15]]
 * 返回值
 * true
 */
public class Find_01 {
    public boolean Find(int target, int [][] array) {
        //以右上角为起点检索
        int height=array.length;
        int width=array[0].length;
        if (array==null||height<1||width<1){
            return false;
        }
        int x=0,y=width-1;
        int currNum;
        while (x<=height-1&&y>=0){
            currNum=array[x][y];
            if (currNum==target){
                return true;
            }else if (currNum>target){
                y--;
            }else if (currNum<target){
                x++;
            }
        }
        return false;
    }

    @Test
    public void  test(){
        int[][] array={{1,2,8,9},{2,4,9,12},{4,7,10,13}, {6,8,11,15}};
        boolean res = Find(1, array);
        System.out.println(res);
    }

}
