package com.algorithm.offer;

import org.junit.Test;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * @description:
 * @author: zyh
 * @create: 2021-05-25 20:29
 **/
public class GetMedian_63 {
    /**
     * 题目描述
     * 如何得到一个数据流中的中位数？如果从数据流中读出奇数个数值，那么中位数就是所有数值排序之后位于中间的数值。
     * 如果从数据流中读出偶数个数值，那么中位数就是所有数值排序之后中间两个数的平均值。
     * 我们使用Insert()方法读取数据流，使用GetMedian()方法获取当前读取数据的中位数。
     */
    int count=0;
    PriorityQueue<Integer> minHeap=new PriorityQueue<>();
    PriorityQueue<Integer> maxHeap=new PriorityQueue<>(new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
            return o2-o1;
        }
    });

    public void Insert(Integer num) {
        //如果是奇数
        if ((count&1)==0){
            minHeap.offer(num);
            maxHeap.offer(minHeap.poll());
        }else{
            maxHeap.offer(num);
            minHeap.offer(maxHeap.poll());
        }
        count++;
    }

    public Double GetMedian() {
        //如果是奇数
        if ((count&1)!=0){
            return new Double(maxHeap.peek());
        }else {
            return new Double((maxHeap.peek()+minHeap.peek()))/2;
        }
    }

    @Test
    public void  test(){

    }

}
