package com.algorithm.offer;


import org.junit.Test;

/**
 * @description: 平衡二叉树
 * @author: zyh
 * @create: 2021-04-29 22:04
 **/
public class IsBalanced_Solution_39 {

    /**
     * 题目描述
     * 输入一棵二叉树，判断该二叉树是否是平衡二叉树。
     * 在这里，我们只需要考虑其平衡性，不需要考虑其是不是排序二叉树
     * 平衡二叉树（Balanced Binary Tree），具有以下性质：它是一棵空树或它的左右两个子树的高度差的绝对值不超过1，并且左右两个子树都是一棵平衡二叉树。
     */
    public boolean IsBalanced_Solution(TreeNode root) {
        if (root==null){
            return true;
        }

        return Math.abs(treeDepth(root.left)-treeDepth(root.right))<=1?IsBalanced_Solution(root.left)&&IsBalanced_Solution(root.right):false;
    }


    //返回树深度
    private int treeDepth(TreeNode node) {
        if(node==null){
            return 0;
        }

        return Math.max(treeDepth(node.left),treeDepth(node.right))+1;
    }

    @Test
    public void test(){

    }
}
