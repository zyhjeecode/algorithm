package com.algorithm.offer;

import org.junit.Test;

public class Merge_16 {
    //合并两个有序的链表
    public ListNode Merge(ListNode list1,ListNode list2) {
        if (list1==null){
            return list2;
        }
        if (list2==null){
            return list1;
        }

        ListNode head=null;
        ListNode curr=null;
        while (list1!=null&&list2!=null){
            if (list1.val<= list2.val){
                if (head==null){
                    head=list1;
                    curr=head;
                }else {
                    curr.next=list1;
                    curr=list1;
                }
                list1=list1.next;
            }else {
                if (head==null){
                    head=list2;
                    curr=head;
                }else {
                    curr.next=list2;
                    curr=list2;
                }
                list2=list2.next;
            }
        }
        if (list1==null){
            curr.next=list2;
        }else {
            curr.next=list1;
        }
        return head;
    }

    @Test
    public void  test(){
        ListNode listNode= new ListNode(1) ;
        listNode.next=new ListNode(2);
        listNode.next.next=new ListNode(8);
        listNode.next.next.next=new ListNode(9);
        listNode.next.next.next.next=new ListNode(10);

        ListNode listNode1= new ListNode(2) ;
        listNode1.next=new ListNode(5);
        listNode1.next.next=new ListNode(5);
        listNode1.next.next.next=new ListNode(7);
        listNode1.next.next.next.next=new ListNode(8);

        ListNode merge = Merge(listNode, listNode1);
        while (merge!=null){
            System.out.print(merge.val+"  ");
            merge=merge.next;
        }
        System.out.println(merge);
    }
}
