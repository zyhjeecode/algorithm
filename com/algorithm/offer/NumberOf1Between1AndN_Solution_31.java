package com.algorithm.offer;

import org.junit.Test;

/**
 * 从1 到 n的数字中1 出现的次数
 */
public class NumberOf1Between1AndN_Solution_31 {
    /**
     * 第一种方法
     * @param n
     * @return
     */
    public int NumberOf1Between1AndN_Solution(int n) {
        int count = 0;

        //m为个位， 十位 百位 千位....
        for (long m = 1; m <= n; m *= 10) {
            //n除以基数后的数
            long a = n / m;
            //n在基础上的数
            long b = n % m;
            //a%10表示上一位是否为1，为0?后一位为几就加几:0
            count+=(a+8)/10*m+(a%10==1?b+1:0);
        }
        return count;
    }

    public int NumberOf1Between1AndN_Solution_2(int n) {
        int count=0;
        for (int i = 1; i <=n; i*=10) {
            //更高位的((数字
            int high=n/i*10;
            //更低位数字
            int low=n%i;
            //当前位数字
            int curr=n/i%10;
            if (curr==0){
                count+=high*10;
            }else if (curr==1){
               count+=high*i+(low+1);
            }else {
               count+=(high+1)*i;
            }
        }
        return count;
    }

    @Test
    public void  test(){
        System.out.println(852%10);
    }
}
