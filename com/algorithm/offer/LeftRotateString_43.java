package com.algorithm.offer;

import org.junit.Test;

public class LeftRotateString_43 {
    /**
     * 题目描述
     * 汇编语言中有一种移位指令叫做循环左移（ROL），现在有个简单的任务，就是用字符串模拟这个指令的运算结果。
     * 对于一个给定的字符序列S，请你把其循环左移K位后的序列输出。
     * 例如，字符序列S=”abcXYZdef”,要求输出循环左移3位后的结果，即“XYZdefabc”。是不是很简单？OK，搞定它！
     * 示例1
     * 输入
     * "abcXYZdef",3
     *
     * 返回值
     * "XYZdefabc"
     */
    public String LeftRotateString(String str,int n) {
        //左移位数确定
        int length=str.length();
        if (length==n||n==0||length<1){
            return str;
        }
        n=n%length;

        //保留第n个字符到末尾再加上前n个字符
        str+=str;
        return str.substring(n,length+n);
    }

    @Test
    public void  test(){
        System.out.println(LeftRotateString("abcXYZdef",3));
    }


}
