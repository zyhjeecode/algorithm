package com.algorithm.offer;

import java.util.ArrayList;

/**
 * 输入一个链表，按链表从尾到头的顺序返回一个ArrayList。
 * 输入
 * {67,0,24,58}
 * 返回值
 * [58,24,0,67]
 */

public class PrintListFromTailToHead_03 {

    public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
        ArrayList<Integer> arrayList=new ArrayList<>();
        while (listNode!=null){
            arrayList.add(0,listNode.val);
            listNode=listNode.next;
        }
        return  arrayList;
    }




}
