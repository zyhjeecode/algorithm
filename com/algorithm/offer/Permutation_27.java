package com.algorithm.offer;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 题目描述
 * 输入一个字符串,按字典序打印出该字符串中字符的所有排列。
 * 例如输入字符串abc,则按字典序打印出由字符a,b,c所能排列出来的所有字符串abc,acb,bac,bca,cab和cba。
 * 输入描述:
 * 输入一个字符串,长度不超过9(可能有字符重复),字符只包括大小写字母。
 */
public class Permutation_27 {
    @Test
    public void  test(){
        String s="abc";
        ArrayList<String> permutation = Permutation(s);
        permutation.forEach(item->{
            System.out.print(item+" ");
        });
    }
    public ArrayList<String> Permutation(String str) {
        List<String> res = new ArrayList<>();

        if (str == null || str.length() <= 1) {
            res.add(str);
            return (ArrayList<String>) res;
        }

        permutationHelper(str.toCharArray(), 0, res);
        Collections.sort(res);
        return (ArrayList<String>) res ;
    }


    //回溯法其实是在构造一棵生成树。对于"abc"，第一个位置有三种取值，第二个位置有两种取值，第三个位置有一种取值。
    private void permutationHelper(char[] arr, int index, List<String> res) {
        if (index == arr.length - 1 && !res.contains(String.valueOf(arr))) {
            res.add(String.valueOf(arr));
        }

        for (int i = index; i < arr.length; i++) {
            swap(arr,i,index);
            permutationHelper(arr,index+1,res);
            swap(arr,i,index);
        }
    }

    private void swap(char[] arr, int a, int b) {
        char temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }
}
