package com.algorithm.offer;

/**
 * @description:
 * @author: zyh
 * @create: 2021-05-21 11:29
 **/
public class FirstNoRepetition_54 {
//    题目描述
//    请实现一个函数用来找出字符流中第一个只出现一次的字符。例如，当从字符流中只读出前两个字符"go"时，第一个只出现一次的字符是"g"。
//    当从该字符流中读出前六个字符“google"时，第一个只出现一次的字符是"l"。

//    后台会用以下方式调用Insert 和 FirstAppearingOnce 函数
//    string caseout = "";
//    1.读入测试用例字符串casein
//    2.如果对应语言有Init()函数的话，执行Init() 函数
//    3.循环遍历字符串里的每一个字符ch {
//            Insert(ch);
//            caseout += FirstAppearingOnce()
//        }
//    2. 输出caseout，进行比较。

//    返回值描述:
//    如果当前字符流没有存在出现一次的字符，返回#字符。
//    public void Insert(char ch)
//    {
//
//    }
//    public char FirstAppearingOnce()
//    {
//
//    }
}
