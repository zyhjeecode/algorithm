package com.algorithm.offer;

/**
 * @description: 矩形覆盖
 * @author: zyh
 * @create: 2021-03-11 21:13
 **/

/**
 * 题目描述
 * 我们可以用2*1的小矩形横着或者竖着去覆盖更大的矩形。请问用n个2*1的小矩形无重叠地覆盖一个2*n的大矩形，总共有多少种方法？
 * <p>
 * 比如n=3时，2*3的矩形块有3种覆盖方法
 * <p>
 * 0 1 2 3 4 5 6 7
 * <p>
 * 0 1 2 3 5 8
 */
public class RectCover_10 {
    public int rectCover(int target) {
        if (target < 2) {
            return target;
        }

        int sum = 2;
        int one = 1;
        for (int i = 3; i <= target; i++) {
            sum=sum+one;
            one=sum-one;
        }
        return sum;
    }
}
