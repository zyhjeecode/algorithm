package com.algorithm.offer;

import org.junit.Test;

/**
 * 题目描述
 * 输入一棵二叉树，求该树的深度。
 * 从根结点到叶结点依次经过的结点（含根、叶结点）形成树的一条路径，最长路径的长度为树的深度。
 * 示例1
 * 输入
 * {1,2,3,4,5,#,6,#,#,7}
 * 返回值
 * 4
 */
public class TreeDepth_38 {
    int depth = 0;

    public int TreeDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        deepSearch(root, 1);
        return depth;
    }

    private void deepSearch(TreeNode root, int i) {
        if (root.left == null && root.right == null && i > depth) {
            depth = i;
            return;
        }
        if (root.left != null) {
            deepSearch(root.left, i + 1);
        }
        if (root.right != null) {
            deepSearch(root.right, i + 1);
        }
    }

    public int TreeDepth_2(TreeNode root){
        if (root==null){
            return 0;
        }
        int depth=0;
        int leftDepth = TreeDepth_2(root.left);
        int rightDepth=TreeDepth_2(root.right);
        return depth=leftDepth>rightDepth?leftDepth+1:rightDepth+1;
    }

    @Test
    public void test() {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        System.out.println(TreeDepth_2(root));
    }

}
