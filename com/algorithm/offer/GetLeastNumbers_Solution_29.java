package com.algorithm.offer;

import org.junit.Test;

import java.util.ArrayList;

/**
 * @description: 获取最大的k个数
 * @author: zyh
 * @create: 2021-04-22 19:18
 **/

/**
 * 题目描述
 * 给定一个数组，找出其中最小的K个数。例如数组元素是4,5,1,6,2,7,3,8这8个数字，则最小的4个数字是1,2,3,4。
 * 如果K>数组的长度，那么返回一个空的数组
 */
public class GetLeastNumbers_Solution_29 {
    public ArrayList<Integer> GetLeastNumbers_Solution(int[] input, int k) {
        ArrayList<Integer> res = new ArrayList<>();

        if (k>input.length){
            return res;
        }

        //插入实现大根堆
        for (int i = 0; i < input.length; i++) {
            heapInsert(input, i);
        }

        for (int i = 0; i < k; i++) {
            res.add(input[0]);
            swap(input,0,input.length-1-i);
            heapify(input,0,input.length-i-1);
        }
        return res;
    }

    @Test
    public void test(){
        ArrayList<Integer> integers = GetLeastNumbers_Solution(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 10);
        integers.forEach(item->{
            System.out.print(item+" ");
        });

    }

    //插入实现大根堆
    public void heapInsert(int[] heapArr, int currIndex) {
        while (heapArr[currIndex] > heapArr[parentIndex(currIndex)]) {
            swap(heapArr, currIndex, parentIndex(currIndex));
            currIndex = parentIndex(currIndex);
        }
    }

    /**
     * 堆平衡
     * 当某个节点发送变化了,那么其子树就需要重新维持平衡
     * param 堆,修改位置,堆数组大小
     */
    public void heapify(int[] heapArr, int index, int heapArrSize) {
        //如果存在左孩子节点
        while (leftChild(index) < heapArrSize) {
            //左右孩子节点最大的值;
            int largestIndex = rightChild(index) < heapArrSize && heapArr[rightChild(index)] > heapArr[leftChild(index)] ?
                    rightChild(index) : leftChild(index);

            if (heapArr[largestIndex] > heapArr[index]) {
                swap(heapArr, index, largestIndex);
                index = largestIndex;
            } else {
                break;
            }
        }
    }

    private void swap(int[] heapArr, int currIndex, int parentIndex) {
        int temp = heapArr[currIndex];
        heapArr[currIndex] = heapArr[parentIndex];
        heapArr[parentIndex] = temp;
    }

    public int parentIndex(int childIndex) {
        return (childIndex - 1) / 2;
    }


    public int leftChild(int parentIndex) {
        return 2 * parentIndex + 1;
    }

    public int rightChild(int parentIndex) {
        return 2 * parentIndex + 2;
    }
}
