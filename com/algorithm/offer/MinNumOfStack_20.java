package com.algorithm.offer;

import java.util.Stack;

public class MinNumOfStack_20 {
    //定义栈的数据结构
    //请在该类型中实现一个能够得到栈中所含最小元素的min函数（时间复杂度应为O（1））。

    Stack<Integer> dataStack = new Stack<>();
    Stack<Integer> minValueStack = new Stack<>();

    public void push(int node) {
        dataStack.push(node);
       if (minValueStack.isEmpty()){
           minValueStack.push(node);
       }else {
           minValueStack.push(Math.min(min(), node));
       }
    }

    public void pop() {
        dataStack.pop();
        minValueStack.pop();
    }

    public int top() {
        return dataStack.peek();
    }

    public int min() {
        return minValueStack.peek();
    }
}
