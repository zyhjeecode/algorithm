package com.algorithm.offer;

/**
 * @description:
 * @author: zyh
 * @create: 2021-05-21 11:19
 **/
public class isNumeric_53 {

    /**
     * 题目描述
     * 请实现一个函数用来判断字符串是否表示数值（包括整数和小数）。
     * 例如，字符串"+100","5e2","-123","3.1416"和"-1E-16"都表示数值。 但是"12e","1a3.14","1.2.3","+-5"和"12e+4.3"都不是。
     * <p>
     * 示例1
     * 输入
     * "123.45e+6"
     * <p>
     * 返回值
     * true
     * 示例2
     * 输入
     * "1.2.3"
     * 返回值
     * false
     **/
    public boolean isNumeric(String str) {
        try {
            double x = Double.parseDouble(str);
            return true;
        }catch (NumberFormatException exception){
            return false;
        }
    }
}
