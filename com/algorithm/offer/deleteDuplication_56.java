package com.algorithm.offer;

import org.junit.Test;

public class deleteDuplication_56 {

    /**
     * 题目描述
     * 在一个 排序 的链表中，存在重复的结点，请删除该链表中重复的结点，重复的结点不保留，返回链表头指针。
     * 例如，链表1->2->3->3->4->4->5 处理后为 1->2->5
     * 示例1
     * 输入
     * {1,2,3,3,3,3,4,4,5}
     * 返回值
     * {1,2,5}
     */
    public ListNode deleteDuplication(ListNode pHead) {
        if (pHead == null ) {
            return pHead;
        }
        //辅助头结点(真正头结点前面一个结点)用于避免第一个数字就重读的情况
        ListNode tempHeadPre = new ListNode(0);

        //当前已遍历的最后一个不重复的结点
        ListNode preNoDup = tempHeadPre;

        preNoDup.next = pHead;

        //当前结点的前面一个结点
        ListNode pre = pHead;
        //当前结点
        ListNode curr = pre.next;


        while (curr != null) {
            if (curr.val == pre.val) {
                while (curr!= null && curr.val == pre.val) {
                    curr = curr.next;
                }
                preNoDup.next = curr;
                if (curr == null) {
                    return tempHeadPre.next;
                } else {
                    pre = preNoDup.next;
                    curr = pre.next;
                }
            } else {
                preNoDup = pre;
                pre = curr;
                curr = curr.next;
            }
        }
        return tempHeadPre.next;
    }

    @Test
    public void test() {
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(2);
        l1.next.next = new ListNode(3);
        l1.next.next.next = new ListNode(3);
        l1.next.next.next.next = new ListNode(4);
        l1.next.next.next.next.next = new ListNode(4);
        l1.next.next.next.next.next.next = new ListNode(5);
        ListNode listNode = deleteDuplication(l1);
        while (listNode != null) {
            System.out.print(listNode.val + " ");
            listNode = listNode.next;
        }
    }
}
