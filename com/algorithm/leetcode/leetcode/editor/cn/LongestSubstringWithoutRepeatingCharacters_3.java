package com.algorithm.leetcode.leetcode.editor.cn;
//给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
//
//
//
// 示例 1:
//
//
//输入: s = "abcabcbb"
//输出: 3
//解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
//
//
// 示例 2:
//
//
//输入: s = "bbbbb"
//输出: 1
//解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
//
//
// 示例 3:
//
//
//输入: s = "pwwkew"
//输出: 3
//解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
//     请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
//
//
// 示例 4:
//
//
//输入: s = ""
//输出: 0
//
//
//
//
// 提示：
//
//
// 0 <= s.length <= 5 * 104
// s 由英文字母、数字、符号和空格组成
//
// Related Topics 哈希表 字符串 滑动窗口
// 👍 6815 👎 0


import java.util.HashMap;

public class LongestSubstringWithoutRepeatingCharacters_3 {
    public static void main(String[] args) {
        Solution solution = new LongestSubstringWithoutRepeatingCharacters_3().new Solution();

    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int lengthOfLongestSubstring(String s) {
            if (s.length()<1){
                return s.length();
            }

            HashMap<Character, Integer> charIndex = new HashMap<>();
            //输入: s = "abcabcbb"
            //输出: 3
            //解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。

            int res = Integer.MIN_VALUE;

            for (int start = 0, end = 0; end < s.length(); end++) {

                final char currChar = s.charAt(end);
                //如果之前已经出现过该元素，取重复元素的后一个元素为 新的开始元素
                if (charIndex.containsKey(currChar)) {
                    start = Math.max(charIndex.get(currChar) + 1, start);
                }

                res = Math.max(res, end-start+1);

                //更新元素位置为最后面的位置
                charIndex.put(currChar, end);
            }

            return res;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
