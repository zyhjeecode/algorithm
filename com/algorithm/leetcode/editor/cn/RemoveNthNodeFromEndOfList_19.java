package com.algorithm.leetcode.editor.cn;
//给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。 
//
// 进阶：你能尝试使用一趟扫描实现吗？ 
//
// 
//
// 示例 1： 
//
// 
//输入：head = [1,2,3,4,5], n = 2
//输出：[1,2,3,5]
// 
//
// 示例 2： 
//
// 
//输入：head = [1], n = 1
//输出：[]
// 
//
// 示例 3： 
//
// 
//输入：head = [1,2], n = 1
//输出：[1]
// 
//
// 
//
// 提示： 
//
// 
// 链表中结点的数目为 sz 
// 1 <= sz <= 30 
// 0 <= Node.val <= 100 
// 1 <= n <= sz 
// 
// Related Topics 链表 双指针 
// 👍 1427 👎 0


import java.util.List;

/**
删除链表的倒数第 N 个结点
**/
public class RemoveNthNodeFromEndOfList_19{
    public static void main(String[] args) {
        Solution solution = new RemoveNthNodeFromEndOfList_19().new Solution();
        ListNode head=new ListNode(1);
        head.next=new ListNode(2);
        head.next.next=new ListNode(3);
        head.next.next.next=new ListNode(4);
        head.next.next.next.next=new ListNode(5);
        solution.removeNthFromEnd(head,2);
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    //输入：head = [1,2,3,4,5], n = 2
    //输出：[1,2,3,5]
    public ListNode removeNthFromEnd(ListNode head, int n) {
        //预设指针
        ListNode pre=new ListNode(0,head);
        ListNode last=pre;
        ListNode first=pre;
        //快指针先走n步
        while (first.next!=null){
            first=first.next;
            if (n==0){
                last=last.next;
            }else {
                n--;
            }
        }
        last.next=last.next.next;
        return pre.next;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}