package com.algorithm.leetcode.editor.cn;

import com.algorithm.GenerateStructure.TreeNode;

/**
 * @description: 二叉树展开为链表
 * @author: zyh
 * @create: 2021-10-19 20:42
 **/
public class Flatten_114 {
    public void flatten(TreeNode root) {
        TreeNode curr=root;
        while (curr!=null){
            if (curr.left==null){
                curr=curr.right;
            }else {
                TreeNode leftRight=curr.left;
                //找到左孩子最右边结点,将有孩子挂上去
                while (leftRight.right!=null){
                    leftRight=leftRight.right;
                }

                leftRight.right=curr.right;
                curr.right=curr.left;
                curr.left=null;
                curr=curr.right;
            }
        }
    }
}
