package com.algorithm.leetcode.editor.cn;
//给你一个整数 n ，求恰由 n 个节点组成且节点值从 1 到 n 互不相同的 二叉搜索树 有多少种？返回满足题意的二叉搜索树的种数。 
//
// 
//
// 示例 1： 
//
// 
//输入：n = 3
//输出：5
// 
//
// 示例 2： 
//
// 
//输入：n = 1
//输出：1
// 
//
// 
//
// 提示： 
//
// 
// 1 <= n <= 19 
// 
// Related Topics 树 二叉搜索树 数学 动态规划 二叉树 
// 👍 1362 👎 0


import java.util.ArrayList;
import java.util.List;

/**
不同的二叉搜索树
**/
public class UniqueBinarySearchTrees_96{
    public static void main(String[] args) {
        Solution solution = new UniqueBinarySearchTrees_96().new Solution();
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int numTrees(int n) {
        if (n<2){
            return n;
        }
        int[] gArr=new int[n+1];
        gArr[0]=1;
        gArr[1]=1;

        //G(n)=G(0)*G(n-1)+G(1)*G2(n-2)+....+G(n-1)*G(0)
        for (int i = 2; i <=n ; i++) {
            //可以第二个for循环这里的i当成上面公式里的n
            for (int j = 1; j <=i ; j++) {
                gArr[i]+=gArr[j-1]*gArr[i-j];
            }
        }
        return gArr[n];
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}