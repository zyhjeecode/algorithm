package com.algorithm.leetcode.editor.cn;
//运用你所掌握的数据结构，设计和实现一个 LRU (最近最少使用) 缓存机制 。 
//
// 
// 
// 实现 LRUCache 类： 
//
// 
// LRUCache(int capacity) 以正整数作为容量 capacity 初始化 LRU 缓存 
// int get(int key) 如果关键字 key 存在于缓存中，则返回关键字的值，否则返回 -1 。 
// void put(int key, int value) 如果关键字已经存在，则变更其数据值；如果关键字不存在，则插入该组「关键字-值」。当缓存容量达到上
//限时，它应该在写入新数据之前删除最久未使用的数据值，从而为新的数据值留出空间。 
// 
//
// 
// 
// 
//
// 进阶：你是否可以在 O(1) 时间复杂度内完成这两种操作？ 
//
// 
//
// 示例： 
//
// 
//输入
//["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
//[[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
//输出
//[null, null, null, 1, null, -1, null, -1, 3, 4]
//
//解释
//LRUCache lRUCache = new LRUCache(2);
//lRUCache.put(1, 1); // 缓存是 {1=1}
//lRUCache.put(2, 2); // 缓存是 {1=1, 2=2}
//lRUCache.get(1);    // 返回 1
//lRUCache.put(3, 3); // 该操作会使得关键字 2 作废，缓存是 {1=1, 3=3}
//lRUCache.get(2);    // 返回 -1 (未找到)
//lRUCache.put(4, 4); // 该操作会使得关键字 1 作废，缓存是 {4=4, 3=3}
//lRUCache.get(1);    // 返回 -1 (未找到)
//lRUCache.get(3);    // 返回 3
//lRUCache.get(4);    // 返回 4
// 
//
// 
//
// 提示： 
//
// 
// 1 <= capacity <= 3000 
// 0 <= key <= 10000 
// 0 <= value <= 10⁵ 
// 最多调用 2 * 10⁵ 次 get 和 put 
// 
// Related Topics 设计 哈希表 链表 双向链表 👍 1691 👎 0


import org.omg.CORBA.IRObject;

import java.util.HashMap;
import java.util.Map;

/**
 * LRU 缓存机制
 **/
public class LruCache_146 {


    public static void main(String[] args) {

    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class LRUCache {
        private HashMap<Integer,Node> lruCache;
        private DoubleList doubleList;
        private int capacity;

        public LRUCache(int capacity){
            this.capacity=capacity;
            lruCache=new HashMap<>(capacity);
            doubleList=new DoubleList();
        }

        public int get(int key){
            if (!lruCache.containsKey(key)){
                return -1;
            }

            int value = lruCache.get(key).val;
            //将顺序提前
            put(key,value);
            return value;
        }

        public void put(int key, int value) {
            //创建新结点
            Node node=new Node(key,value);

            if (lruCache.containsKey(key)){
                //删除旧结点
                doubleList.remove(lruCache.get(key));
                //新结点添加
                doubleList.addFirst(node);
                //更新map数据
                lruCache.put(key,node);
            }else {
                if (lruCache.size()==this.capacity){
                    Node last = doubleList.removeLast();
                    lruCache.remove(last.key);
                }
                doubleList.addFirst(node);
                lruCache.put(key,node);
            }

        }


        //构建node结点
        class Node {
            public int key, val;
            public Node next, prev;
            public Node(int k, int v) {
                this.key = k;
                this.val = v;
            }
        }
        //构建双链表
        class DoubleList {
            //头结点
            private Node head;
            //尾结点
            private Node tail;

            private int size=0;

            // 在链表头部添加节点 node，时间 O(1)
            public void addFirst(Node node){
                if (head==null){
                    head=tail=node;
                }else {
                    Node temp= head;
                    temp.prev=node;
                    node.next=temp;
                    head=node;
                }
            }

            // 删除链表中的 node 节点（node 一定存在）
            // 由于是双链表且给的是目标 Node 节点，时间 O(1)
            public void remove(Node node){
                if (node==head&&node==tail){
                    head=tail=null;
                }else if (tail==node){
                    tail.prev.next=null;
                    tail=node.prev;
                }else if (head==node){
                    head.next.prev=null;
                    head=node.next;
                }else{
                    node.prev.next=node.next;
                    node.next.prev=node.prev;
                }
            }

            // 删除链表中最后一个节点，并返回该节点，时间 O(1)
            public Node removeLast(){
                Node temp=tail;
                remove(tail);
                return temp;
            }

            // 返回链表长度，时间 O(1)
            public int size(){
                return size;
            }
        }

    }

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */
//leetcode submit region end(Prohibit modification and deletion)

}