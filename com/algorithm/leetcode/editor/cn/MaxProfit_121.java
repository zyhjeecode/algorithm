package com.algorithm.leetcode.editor.cn;

/**
 * 121. 买卖股票的最佳时机
 * @author zyh
 * @date 2021/10/21
 */
public class MaxProfit_121  {
    /**
     * 输入：[7,1,5,3,6,4]
     * 输出：5
     * 解释：在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
     *      注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格；同时，你不能在买入前卖出股票
     */
    public int maxProfit(int[] prices) {
        int minPrice=prices[0];
        int maxProfit=0;

        for (int i = 1,len=prices.length; i <len ; i++) {
            int currProfit=prices[i]-minPrice;
            if (currProfit>maxProfit){
                maxProfit=currProfit;
            }

            minPrice=minPrice>prices[i]?prices[i]:minPrice;
        }

        return maxProfit;
    }
}
