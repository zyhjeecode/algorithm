package com.algorithm.leetcode.editor.cn;
//给定一个二叉树的根节点 root ，和一个整数 targetSum ，求该二叉树里节点值之和等于 targetSum 的 路径 的数目。 
//
// 路径 不需要从根节点开始，也不需要在叶子节点结束，但是路径方向必须是向下的（只能从父节点到子节点）。 
//
// 
//
// 示例 1： 
//
// 
//
// 
//输入：root = [10,5,-3,3,2,null,11,3,-2,null,1], targetSum = 8
//输出：3
//解释：和等于 8 的路径有 3 条，如图所示。
// 
//
// 示例 2： 
//
// 
//输入：root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
//输出：3
// 
//
// 
//
// 提示: 
//
// 
// 二叉树的节点个数的范围是 [0,1000] 
// -10⁹ <= Node.val <= 10⁹ 
// -1000 <= targetSum <= 1000 
// 
// Related Topics 树 深度优先搜索 二叉树 👍 1176 👎 0


import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
路径总和 III
**/
public class PathSumIii_437{
    public static void main(String[] args) {
        Solution solution = new PathSumIii_437().new Solution();
        //1,-2,-3,1,3,-2,null,-1
        TreeNode root = LeetCodeEntity.listToTree(Stream.of(1,-2,-3,1,3,-2,null,-1).collect(Collectors.toList()));
        System.out.println(solution.pathSum(root,-1));
    }

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {

    public int pathSum(TreeNode root, int targetSum) {
        if (root==null){
            return 0;
        }
        int count =dfs(root,targetSum);
        int countLeft = pathSum(root.left, targetSum);
        int countRight=pathSum(root.right,targetSum);
        return count+countLeft+countRight;
    }

    /**
     * 以curr为起点的连续路径是否有结点和为targetSum的数
     * count当前起点和为tarsum的值个数
     */
    private int dfs(TreeNode root, int targetSum) {
        if (root==null){
            return 0;
        }
        targetSum-=root.val;

        //当前结点是否已满足条件
        int curr=0;
        if (targetSum==0){
            curr=1;
        }

        //这里不能加currVal>targetSum或者root.val>targetSum因为咱注意到里面有负数，人家后面还是有希望翻盘的

        return curr+dfs(root.left,targetSum)+dfs(root.right,targetSum);
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}