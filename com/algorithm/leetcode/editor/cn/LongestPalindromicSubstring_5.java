package com.algorithm.leetcode.editor.cn;
//给你一个字符串 s，找到 s 中最长的回文子串。 
//
// 
//
// 示例 1： 
//
// 
//输入：s = "babad"
//输出："bab"
//解释："aba" 同样是符合题意的答案。
// 
//
// 示例 2： 
//
// 
//输入：s = "cbbd"
//输出："bb"
// 
//
// 示例 3： 
//
// 
//输入：s = "a"
//输出："a"
// 
//
// 示例 4： 
//
// 
//输入：s = "ac"
//输出："a"
// 
//
// 
//
// 提示： 
//
// 
// 1 <= s.length <= 1000 
// s 仅由数字和英文字母（大写和/或小写）组成 
// 
// Related Topics 字符串 动态规划 
// 👍 3738 👎 0


/**
最长回文子串
**/
public class LongestPalindromicSubstring_5{
    public static void main(String[] args) {
        Solution solution = new LongestPalindromicSubstring_5().new Solution();
        String str = solution.longestPalindrome("acbbac");
        System.out.println(str);
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public String longestPalindrome(String s) {
        //用于存放最大长度区间值
        int[] range=new int[2];
        char[] chars = s.toCharArray();
        for (int i = 0,len=chars.length; i <len; i++) {
            // 把回文看成中间的部分全是同一字符，左右部分相对称
            // 找到下一个与当前字符不同的字符，这样可以减少不少重复计算
            i=findLongest(chars,i,range);
        }
        return s.substring(range[0],range[1]+1);
    }

    private int findLongest(char[] chars, int low, int[] range) {
        //初始位置为当前字符位置，找到重复元素的最后一个元素位置
        int high=low;
        while (high+1<chars.length&&chars[high+1]==chars[low]){
            high++;
        }

        //找到重复元素两边对称位置不相同的元素位置
        //保存最后一个和low位置元素的重复元素位置
        int ans=high;
        while (low>0&&high<chars.length-1&&chars[low-1]==chars[high+1]){
            low--;
            high++;
        }

        if (high-low>range[1]-range[0]){
            range[1]=high;
            range[0]=low;
        }
        return ans;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}