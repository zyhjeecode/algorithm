package com.algorithm.leetcode.editor.cn;
//给定一个 24 小时制（小时:分钟 "HH:MM"）的时间列表，找出列表中任意两个时间的最小时间差并以分钟数表示。 
//
// 
//
// 示例 1： 
//
// 
//输入：timePoints = ["23:59","00:00"]
//输出：1
// 
//
// 示例 2： 
//
// 
//输入：timePoints = ["00:00","23:59","00:00"]
//输出：0
// 
//
// 
//
// 提示： 
//
// 
// 2 <= timePoints.length <= 2 * 10⁴ 
// timePoints[i] 格式为 "HH:MM" 
// 
// Related Topics 数组 数学 字符串 排序 👍 181 👎 0


import java.util.Arrays;
import java.util.List;

/**
 * 最小时间差
 **/
public class MinimumTimeDifference_539 {
    public static void main(String[] args) {
        Solution solution = new MinimumTimeDifference_539().new Solution();
        solution.findMinDifference(Arrays.asList("23:59","00:00"));
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int findMinDifference(List<String> timePoints) {
            int size=timePoints.size();
            //仿时间戳做个今明两天的数组
            int[] time=new int[size*2];
            for (int i = 0,idx=0; i <size; idx=idx+2,i++) {
                String[] split = timePoints.get(i).split(":");
                int h = Integer.parseInt(split[0]);
                int m = Integer.parseInt(split[1]);
                //计算当前时间的时间戳
                time[idx]=h*60+m;

                //计算后一天当前时间的时间戳
                //60*24=1440分钟
                time[idx+1]=time[idx]+1440;
            }

            Arrays.sort(time);

            int res=Integer.MAX_VALUE;
            for (int i = 0; i+1 < time.length; i++) {
                res=Math.min((time[i+1]-time[i]),res);
            }
            return res;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}