package com.algorithm.leetcode.editor.cn;
//Trie（发音类似 "try"）或者说 前缀树 是一种树形数据结构，用于高效地存储和检索字符串数据集中的键。这一数据结构有相当多的应用情景，例如自动补完和拼
//写检查。
//
// 请你实现 Trie 类：
//
//
// Trie() 初始化前缀树对象。
// void insert(String word) 向前缀树中插入字符串 word 。
// boolean search(String word) 如果字符串 word 在前缀树中，返回 true（即，在检索之前已经插入）；否则，返回 false
// 。
// boolean startsWith(String prefix) 如果之前已经插入的字符串 word 的前缀之一为 prefix ，返回 true ；否
//则，返回 false 。
//
//
//
//
// 示例：
//
//
//输入
//["Trie", "insert", "search", "search", "startsWith", "insert", "search"]
//[[], ["apple"], ["apple"], ["app"], ["app"], ["app"], ["app"]]
//输出
//[null, null, true, false, true, null, true]
//
//解释
//Trie trie = new Trie();
//trie.insert("apple");
//trie.search("apple");   // 返回 True
//trie.search("app");     // 返回 False
//trie.startsWith("app"); // 返回 True
//trie.insert("app");
//trie.search("app");     // 返回 True
//
//
//
//
// 提示：
//
//
// 1 <= word.length, prefix.length <= 2000
// word 和 prefix 仅由小写英文字母组成
// insert、search 和 startsWith 调用次数 总计 不超过 3 * 104 次
//
// Related Topics 设计 字典树 哈希表 字符串
// 👍 940 👎 0


import java.util.HashMap;

public class ImplementTriePrefixTree_208 {
    public static void main(String[] args) {
        Trie solution = new ImplementTriePrefixTree_208().new Trie();

    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Trie {
        //定义前缀树结点结构
        public class TrieNode {
            //表示当前节点所能链接到其他节点的个数（在删除操作中会用到）
            public int path;
            //表示以当前节点为结尾的单词的个数
            public int end;
            //表示当前节点能链接到的所有节点。
            public HashMap<Character, TrieNode> next;

            public TrieNode() {
                path = 0;
                end = 0;
                next = new HashMap<>();
            }
        }

        //跟结点
        private TrieNode root;

        public Trie() {
           root=new TrieNode();
        }

        public void insert(String word) {
            if(word == null || word.length()==0) {
                return ;
            }
            TrieNode node=root;
            for (int i = 0; i < word.length(); i++) {
                char ch=word.charAt(i);
                if (!node.next.containsKey(ch)){
                    node.next.put(ch,new TrieNode());
                }
                node = node.next.get(ch);
                node.path++;
            }
            node.end++;
        }

        public boolean search(String word) {
            if(word==null||word.length()==0) {
                return false;
            }

            TrieNode node=root;
            for (int i = 0; i < word.length(); i++) {
                char currChar=word.charAt(i);
                if (!node.next.containsKey(currChar)){
                    return false;
                }else{
                    node=node.next.get(currChar);
                }
            }
            //如果这个没有以此为截至结点的就返回false
            if (node.end==0){
                return false;
            }
            return true;
        }

        public boolean startsWith(String prefix) {
            if(prefix==null||prefix.length()==0) {
                return false;
            }

            TrieNode node=root;
            for (int i = 0; i < prefix.length(); i++) {
                char currChar=prefix.charAt(i);
                if (!node.next.containsKey(currChar)){
                    return false;
                }else{
                    node=node.next.get(currChar);
                }
            }
            return true;
        }
    }

/**
 * Your Trie object will be instantiated and called as such:
 * Trie obj = new Trie();
 * obj.insert(word);
 * boolean param_2 = obj.search(word);
 * boolean param_3 = obj.startsWith(prefix);
 */
//leetcode submit region end(Prohibit modification and deletion)

}
