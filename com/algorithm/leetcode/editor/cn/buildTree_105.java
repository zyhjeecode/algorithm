package com.algorithm.leetcode.editor.cn;

import com.algorithm.GenerateStructure.TreeNode;
import org.junit.Test;

import java.util.Arrays;

/**
 * @description: 先序和中序遍历结果重构二叉树
 * @author: zyh
 * @create: 2021-10-15 19:58
 **/
public class buildTree_105 {

    @Test
    public void test(){
        int[] preorder=new int[]{3,9,20,15,7};
        int[] inorder=new int[]{9,3,15,20,7};
        final TreeNode treeNode = buildTree(preorder, inorder);
    }

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder.length==0||inorder.length==0){
            return null;
        }

        TreeNode root=new TreeNode(preorder[0]);

        for (int i = 0,len=inorder.length; i <len ; i++) {
            if (root.val==inorder[i]){
                int[] inorderLeft=Arrays.copyOfRange(inorder,0,i);
                int[] preorderLeft= Arrays.copyOfRange(preorder,1,1+inorderLeft.length);
                root.left=buildTree(preorderLeft,inorderLeft);

                int[] inorderRight=Arrays.copyOfRange(inorder,i+1,inorder.length);
                int[] preorderRight=Arrays.copyOfRange(preorder,1+inorderLeft.length,preorder.length);
                root.right=buildTree(preorderRight,inorderRight);
                break;
            }
        }
        return root;
    }
}
