package com.algorithm.leetcode.editor.cn;
//给你一个整数数组 nums 和一个整数 k ，请你统计并返回该数组中和为 k 的连续子数组的个数。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [1,1,1], k = 2
//输出：2
// 
//
// 示例 2： 
//
// 
//输入：nums = [1,2,3], k = 3
//输出：2
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 2 * 10⁴ 
// -1000 <= nums[i] <= 1000 
// -10⁷ <= k <= 10⁷ 
// 
// Related Topics 数组 哈希表 前缀和 👍 1234 👎 0


import java.util.HashMap;

/**
和为 K 的子数组
**/
public class SubarraySumEqualsK_560{
    public static void main(String[] args) {
        Solution solution = new SubarraySumEqualsK_560().new Solution();
        solution.subarraySum(new int[]{1,1,1},2);
    }

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    int count=0;

    //暴力法
    public int subarraySum0(int[] nums, int k) {

        for (int i = 0; i < nums.length; i++) {
            int sum=0;
            for (int j = i; j < nums.length; j++) {
                sum+=nums[j];
                if (sum==k){
                    count++;
                }
            }
        }
        return count;
    }


    public int subarraySum1(int[] nums, int k) {

        //存储从0~i项的和
        int[] sumArr=new int[nums.length+1];

        //存储值
        for (int i = 1; i < nums.length; i++) {
            sumArr[i]=sumArr[i-1]+nums[i-1];
        }

        for (int left = 0; left < nums.length; left++) {
            //第1~第k项
            for (int right = left+1; right <= nums.length; right++) {
                int sum=sumArr[right]-sumArr[left];
                if (sum==k){
                    count++;
                }
            }
        }
        return count;
    }

    public int subarraySum(int[] nums, int k) {
        //统计
        int count=0;
        //前缀和
        int sum=0;
        HashMap<Integer,Integer> map=new HashMap<>();
        map.put(0,1);
        for (int num : nums) {
            //记录前i项的和sum
            sum+=num;

            //判断sum-k是否为某个位置的前n项和，若是，更新统计量。
            if (map.containsKey(sum-k)){
                count+=map.get(sum-k);
            }

            //记录前缀和为sum的个数;
            if (map.containsKey(sum)){
                map.put(sum,map.get(sum)+1);
            }else {
                map.put(sum,1);
            }
        }
        return count;
    }




}
//leetcode submit region end(Prohibit modification and deletion)

}