package com.algorithm.leetcode.editor.cn;

import java.time.LocalDateTime;

/**
 * @description: 找到环的开始结点
 * @author: zyh
 * @create: 2021-10-22 20:53
 **/
public class DetectCycle_142 {
    public ListNode detectCycle(ListNode head) {
        ListNode meetNode=null;
        if (head==null){
            return null;
        }

        //一个一次走两步的 一个一次走一步的,如果两人能两次相遇(起点,终点),必然存在环
        ListNode fast=head;
        ListNode slow=head;
        int meetCount=0;
        while (slow!=null){
            if (fast==slow){
                meetCount++;
            }

            if (meetCount==2){
                meetNode=slow;
                break;
            }
            //如果过程中快指针出现了null说明没环
            if (fast==null||fast.next==null){
                return null;
            }

            fast=fast.next.next;
            slow=slow.next;
        }

        fast=head;

        while (fast!=meetNode){
            fast=fast.next;
            meetNode=meetNode.next;
        }
        return fast;
    }

    public static void main(String[] args) {
        System.out.println(LocalDateTime.now().plusDays(1));
    }
}
