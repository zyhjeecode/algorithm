package com.algorithm.leetcode.editor.cn;
//给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。 
//
// 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。 
//
// 
//
// 
//
// 示例 1： 
//
// 
//输入：digits = "23"
//输出：["ad","ae","af","bd","be","bf","cd","ce","cf"]
// 
//
// 示例 2： 
//
// 
//输入：digits = ""
//输出：[]
// 
//
// 示例 3： 
//
// 
//输入：digits = "2"
//输出：["a","b","c"]
// 
//
// 
//
// 提示： 
//
// 
// 0 <= digits.length <= 4 
// digits[i] 是范围 ['2', '9'] 的一个数字。 
// 
// Related Topics 哈希表 字符串 回溯 
// 👍 1373 👎 0


import java.util.LinkedList;
import java.util.List;

/**
 * 电话号码的字母组合
 **/
public class LetterCombinationsOfAPhoneNumber_17 {
    public static void main(String[] args) {
        Solution solution = new LetterCombinationsOfAPhoneNumber_17().new Solution();
        List<String> strings = solution.letterCombinations("2");
        strings.forEach(item->{
            System.out.println(item);
        });
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        String[] letter_map = {" ", "*", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

        public List<String> letterCombinations(String digits) {
            List<String> res = new LinkedList<>();
            if (digits.length() < 1) {
                return res;
            }

            char[] chars = digits.toCharArray();
            getAllLetter("", chars, res);
            return res;
        }

        private void getAllLetter(String currStr, char[] chars, List<String> res) {
            if (currStr.length() == chars.length) {
                res.add(currStr);
                return;
            }

            int currNum = chars[currStr.length()] - '0';

            String charStr = letter_map[currNum];

            for (char c : charStr.toCharArray()) {
                getAllLetter(currStr+String.valueOf(c),chars,res);
            }
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}