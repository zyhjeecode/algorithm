package com.algorithm.leetcode.editor.cn;


public class AddTwoNumbers_02 {

    /**
     * 给你两个非空的链表，表示两个非负的整数。它们每位数字都是按照逆序的方式存储的，并且每个节点只能存储一位数字。
     * 请你将两个数相加，并以相同形式返回一个表示和的链表。
     * 你可以假设除了数字0之外，这两个数都不会以0开头。
     * 示例 2：
     * <p>
     * 输入：l1 = [0], l2 = [0]
     * 输出：[0]
     * 示例 3：
     * <p>
     * 输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
     * 输出：[8,9,9,9,0,0,0,1]
     * <p>
     * <p>
     * <p>
     * https://leetcode-cn.com/problems/add-two-numbers/
     */
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //新链表的头的前一个结点
        ListNode pre = new ListNode(0);
        //当前要更新的链表位置
        ListNode curr = pre;
        //进位值
        int carry=0;
        while (l1 != null || l2 != null) {
            int a = l1 == null ? 0 : l1.val;
            int b = l2 == null ? 0 : l2.val;
            int sum=a+b+carry;
            carry=sum/10;
            sum=sum%10;
            curr.next=new ListNode(sum);

            curr=curr.next;
            if (l1!=null){
                l1=l1.next;
            }
            if (l2!=null){
                l2=l2.next;
            }
        }
        //两个链表都遍历完了之后，如果有进位1，则补一个进位
        if (carry==1){
            curr.next=new ListNode(carry);
        }
        return pre.next;
    }
}
