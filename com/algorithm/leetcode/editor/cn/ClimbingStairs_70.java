package com.algorithm.leetcode.editor.cn;
//假设你正在爬楼梯。需要 n 阶你才能到达楼顶。 
//
// 每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？ 
//
// 注意：给定 n 是一个正整数。 
//
// 示例 1： 
//
// 输入： 2
//输出： 2
//解释： 有两种方法可以爬到楼顶。
//1.  1 阶 + 1 阶
//2.  2 阶 
//
// 示例 2： 
//
// 输入： 3
//输出： 3
//解释： 有三种方法可以爬到楼顶。
//1.  1 阶 + 1 阶 + 1 阶
//2.  1 阶 + 2 阶
//3.  2 阶 + 1 阶
// 
// Related Topics 记忆化搜索 数学 动态规划 
// 👍 1768 👎 0


/**
爬楼梯
**/
public class ClimbingStairs_70{
    public static void main(String[] args) {
        Solution solution = new ClimbingStairs_70().new Solution();
        int i = solution.climbStairs(3);
        System.out.println(i);
    }

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int climbStairs(int n) {
        //由于我们现在占的台阶可以从前一步跳上来也可以从前两步跳上来，因此有
        // f(x)=f(x−1)+f(x−2)
        //这种下一步的结果由上一步决定的一般用动态规划都比较快
        // 设r为当前点，q，p为前两个点，则有
        int q = 0, p = 0, r = 1;
        for (int i = 1; i <= n; i++) {
            q = p;
            p = r;
            r = q + p;
        }
        return r;
    }

}
//leetcode submit region end(Prohibit modification and deletion)
class Solution1 {
    int res=0;
    public int climbStairs(int n) {
        getStep(n);
        return res;
    }

    private void getStep(int n) {
        if (n<=1){
            res++;
        }else if (n>=2){
            n-=2;
            getStep(n);
            n+=1;
            getStep(n);
        }
    }
}
}