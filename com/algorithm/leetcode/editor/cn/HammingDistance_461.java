package com.algorithm.leetcode.editor.cn;
//两个整数之间的 汉明距离 指的是这两个数字对应二进制位不同的位置的数目。 
//
// 给你两个整数 x 和 y，计算并返回它们之间的汉明距离。 
//
// 
//
// 示例 1： 
//
// 
//输入：x = 1, y = 4
//输出：2
//解释：
//1   (0 0 0 1)
//4   (0 1 0 0)
//       ↑   ↑
//上面的箭头指出了对应二进制位不同的位置。
// 
//
// 示例 2： 
//
// 
//输入：x = 3, y = 1
//输出：1
// 
//
// 
//
// 提示： 
//
// 
// 0 <= x, y <= 2³¹ - 1 
// 
// Related Topics 位运算 👍 547 👎 0


/**
汉明距离
**/
public class HammingDistance_461{
    public static void main(String[] args) {
        Solution solution = new HammingDistance_461().new Solution();
        System.out.print(solution.hammingDistance(1,4));
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int hammingDistance(int x, int y) {
        //异或，相同为0，相异为1，那么temp中1的位置就是二进制数不同的地方
        int temp=x^y;

        //统计temp里1的个数
        int res=0;

        while (temp!=0){
            //&相同为1，不同为0
            //判断最后一位是否为1
            if ((temp&1)==1){
                res++;
            }
            //右移一位，我们每次仅需要判断最后一位是否为0即可
            temp=temp>>1;
        }
        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}