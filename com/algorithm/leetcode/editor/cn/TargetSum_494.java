package com.algorithm.leetcode.editor.cn;
//给你一个整数数组 nums 和一个整数 target 。 
//
// 向数组中的每个整数前添加 '+' 或 '-' ，然后串联起所有整数，可以构造一个 表达式 ： 
//
// 
// 例如，nums = [2, 1] ，可以在 2 之前添加 '+' ，在 1 之前添加 '-' ，然后串联起来得到表达式 "+2-1" 。 
// 
//
// 返回可以通过上述方法构造的、运算结果等于 target 的不同 表达式 的数目。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [1,1,1,1,1], target = 3
//输出：5
//解释：一共有 5 种方法让最终目标和为 3 。
//-1 + 1 + 1 + 1 + 1 = 3
//+1 - 1 + 1 + 1 + 1 = 3
//+1 + 1 - 1 + 1 + 1 = 3
//+1 + 1 + 1 - 1 + 1 = 3
//+1 + 1 + 1 + 1 - 1 = 3
// 
//
// 示例 2： 
//
// 
//输入：nums = [1], target = 1
//输出：1
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 20 
// 0 <= nums[i] <= 1000 
// 0 <= sum(nums[i]) <= 1000 
// -1000 <= target <= 1000 
// 
// Related Topics 数组 动态规划 回溯 👍 981 👎 0


import java.util.HashMap;

/**
目标和
**/
public class TargetSum_494{
    public static void main(String[] args) {
        Solution solution = new TargetSum_494().new Solution();
    }

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {

    HashMap<String,Integer> cache=new HashMap<>();

    public int findTargetSumWays(int[] nums, int target) {
        return dfs(nums,target,0,0);
    }

    private int dfs(int[] nums, int target, int index, int currSum) {
        String key=index+"_"+currSum;

        if (cache.containsKey(key)){
            return cache.get(key);
        }
        //前面一个数字是正的、负的
        if (index==nums.length){
            //最后结果判断是否可以达成目标，然后在相当于回溯的操作中被使用
            cache.put(key,currSum==target?1:0);
            return cache.get(key);
        }

        int incr=dfs(nums,target,index+1,currSum+nums[index]);
        int decr=dfs(nums,target,index+1,currSum-nums[index]);
        cache.put(key,incr+decr);
        return cache.get(key);
    }
}

    class Solution2 {

        int count=0;
        public int findTargetSumWays(int[] nums, int target) {
            dfs(nums,target,0,0);
            return count;
        }

        private void dfs(int[] nums, int target, int index, int currSum) {
            if (index==nums.length){
                count+=currSum==target?1:0;
                return;
            }
            //前面一个数字是正的、负的
            dfs(nums,target,index+1,currSum+nums[index]);
            dfs(nums,target,index+1,currSum-nums[index]);
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}