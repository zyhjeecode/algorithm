package com.algorithm.leetcode.editor.cn;

import org.junit.Test;

public class FindMedianSortedArrays_04 {


    /**
     * 给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
     * 输入：nums1 = [1,3], nums2 = [2]
     * 输出：2.00000
     * 解释：合并数组 = [1,2,3] ，中位数 2
     *
     * 进阶：你能设计一个时间复杂度为 O(log (m+n)) 的算法解决此问题吗？
     */

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        //num1,num2的游标
        int num1Index=0,num2Index=0;
        int num1Length=nums1.length,num2Length=nums2.length;

        int totalLength= nums1.length+nums2.length;
        int avgIndex= (int) Math.ceil(new Double(totalLength)/2);
        //如果总长度是偶数则取第avgIndex和第avgIndex+1个数的平均数，如果是奇数则取第avgIndex个数的数

        int left=0,right=0;
        for (int i = 0,end=((totalLength&1)==1)?avgIndex:avgIndex+1; i <end ; i++) {
            left=right;
            if (num1Index==num1Length){
                right=nums2[num2Index++];
            }else if (num2Index==num2Length){
                right=nums1[num1Index++];
            }else {
                right=nums1[num1Index]<nums2[num2Index]?nums1[num1Index++]:nums2[num2Index++];
            }
        }

        if ((totalLength&1)==1){
            return right/1.0;
        }else {
            return (right+left)/2.0;
        }
    }

    @Test
    public void  test(){
        int[] ints1 = {1};
        int[] ints2 = {2};
        System.out.println(findMedianSortedArrays(ints1,ints2));
    }
}
