package com.algorithm.leetcode.editor.cn;
//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。 
//
// 有效字符串需满足： 
//
// 
// 左括号必须用相同类型的右括号闭合。 
// 左括号必须以正确的顺序闭合。 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：s = "()"
//输出：true
// 
//
// 示例 2： 
//
// 
//输入：s = "()[]{}"
//输出：true
// 
//
// 示例 3： 
//
// 
//输入：s = "(]"
//输出：false
// 
//
// 示例 4： 
//
// 
//输入：s = "([)]"
//输出：false
// 
//
// 示例 5： 
//
// 
//输入：s = "{[]}"
//输出：true 
//
// 
//
// 提示： 
//
// 
// 1 <= s.length <= 104 
// s 仅由括号 '()[]{}' 组成 
// 
// Related Topics 栈 字符串 
// 👍 2474 👎 0


import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * 有效的括号
 **/
public class ValidParentheses_20 {
    public static void main(String[] args) {
        Solution solution = new ValidParentheses_20().new Solution();
        System.out.printf("" + solution.isValid("{[]}"));
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public boolean isValid(String s) {
            Map<Character,Character> map=new HashMap<Character,Character>(8) {
                {
                    put(')','(');
                    put(']','[');
                    put('}','{');
                }
            };

            //如果长度是奇数
            if ((s.length() & 1) == 1) {
                return false;
            }
            //向栈内加数据相同的取数据
            Stack<Character> stack = new Stack<Character>();
            for (char c : s.toCharArray()) {
                if (stack.isEmpty()) {
                    stack.push(c);
                } else {
                    if (stack.peek().equals(map.get(c))) {
                        stack.pop();
                    } else {
                        stack.push(c);
                    }
                }
            }
            return stack.isEmpty();
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}