package com.algorithm.leetcode.editor.cn;
//实现获取 下一个排列 的函数，算法需要将给定数字序列重新排列成字典序中下一个更大的排列。 
//
// 如果不存在下一个更大的排列，则将数字重新排列成最小的排列（即升序排列）。 
//
// 必须 原地 修改，只允许使用额外常数空间。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [1,2,3]
//输出：[1,3,2]
// 
//
// 示例 2： 
//
// 
//输入：nums = [3,2,1]
//输出：[1,2,3]
// 
//
// 示例 3： 
//
// 
//输入：nums = [1,1,5]
//输出：[1,5,1]
// 
//
// 示例 4： 
//
// 
//输入：nums = [1]
//输出：[1]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 100 
// 0 <= nums[i] <= 100 
// 
// Related Topics 数组 双指针 
// 👍 1202 👎 0


import com.algorithm.future.offer.Joseph;

import java.util.concurrent.ForkJoinPool;

/**
下一个排列
**/
public class NextPermutation_31{
    public static void main(String[] args) {
        Solution solution = new NextPermutation_31().new Solution();
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public void nextPermutation(int[] nums) {
        //如果从尾到头遍历一直是升序的，那么就反转数组
        //如果从尾到头遍历有降序，那么置换他俩
        //1 5 3 2
        int i=nums.length-2;

        //找到一个左边小右边大的停止
        while (i>=0&&nums[i]>=nums[i+1]){
            i--;
        }

        //如果能找到
        if (i>=0){
            int j=nums.length-1;
            //从右开始找，找到第一个大于i的数字交换
            while (j>=0&& nums[i]>=nums[j]){
                j--;
            }
            swap(nums,i,j);
        }
        reverse(nums, i+1);
    }
    public void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }


    //倒置start到最后一个元素直接的数组
    public void reverse(int[] nums, int start) {
        int left = start, right = nums.length - 1;
        while (left < right) {
            swap(nums, left, right);
            left++;
            right--;
        }
    }

}
//leetcode submit region end(Prohibit modification and deletion)

}