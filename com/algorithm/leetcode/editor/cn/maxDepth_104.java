package com.algorithm.leetcode.editor.cn;

import com.algorithm.GenerateStructure.TreeNode;

/**
 * @description: 最大深度
 * @author: zyh
 * @create: 2021-10-15 19:37
 **/
public class maxDepth_104 {



    public int maxDepth(TreeNode root) {
        if (root==null){
            return 0;
        }
        return Math.max(maxDepth(root.left),maxDepth(root.right))+1;
    }

}
