package com.algorithm.leetcode.editor.cn;
//给定两个字符串 s 和 p，找到 s 中所有 p 的 异位词 的子串，返回这些子串的起始索引。不考虑答案输出的顺序。 
//
// 异位词 指由相同字母重排列形成的字符串（包括相同的字符串）。 
//
// 
//
// 示例 1: 
//
// 
//输入: s = "cbaebabacd", p = "abc"
//输出: [0,6]
//解释:
//起始索引等于 0 的子串是 "cba", 它是 "abc" 的异位词。
//起始索引等于 6 的子串是 "bac", 它是 "abc" 的异位词。
// 
//
// 示例 2: 
//
// 
//输入: s = "abab", p = "ab"
//输出: [0,1,2]
//解释:
//起始索引等于 0 的子串是 "ab", 它是 "ab" 的异位词。
//起始索引等于 1 的子串是 "ba", 它是 "ab" 的异位词。
//起始索引等于 2 的子串是 "ab", 它是 "ab" 的异位词。
// 
//
// 
//
// 提示: 
//
// 
// 1 <= s.length, p.length <= 3 * 10⁴ 
// s 和 p 仅包含小写字母 
// 
// Related Topics 哈希表 字符串 滑动窗口 👍 740 👎 0


import sun.security.provider.SHA;

import java.util.*;

/**
 * 找到字符串中所有字母异位词
 **/
public class FindAllAnagramsInAString_438 {
    public static void main(String[] args) {
        Solution solution = new FindAllAnagramsInAString_438().new Solution();
        System.out.println(solution.findAnagrams("cbaebabacd", "abc"));
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public List<Integer> findAnagrams(String s, String p) {
            List<Integer> res=new LinkedList<>();
            int sLen = s.length(),pLen=p.length();
            if (pLen>sLen){
                return res;
            }
            //s链表
            int[] sChar=new int[26];
            //标准窗口（比对项）
            int[] pChar=new int[26];

            //滑动窗口比对项
            for (int i = 0; i < pLen; i++) {
                sChar[s.charAt(i)-'a']++;
                pChar[p.charAt(i)-'a']++;
            }


            if (Arrays.equals(sChar,pChar)){
                res.add(0);
            }

            for (int i = pLen; i < sLen; i++) {
                //将每次滑动后，被移除窗口的那个数字的字符所在位置数量-1
                sChar[s.charAt(i-pLen)-'a']--;
                //当前位置字符数量+1
                sChar[s.charAt(i)-'a']++;
                //窗口比对，check过程
                if (Arrays.equals(sChar,pChar)){
                    res.add(i-pLen+1);
                }
            }
            return res;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}