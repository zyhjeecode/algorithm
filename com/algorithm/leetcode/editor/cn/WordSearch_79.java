package com.algorithm.leetcode.editor.cn;
//给定一个 m x n 二维字符网格 board 和一个字符串单词 word 。如果 word 存在于网格中，返回 true ；否则，返回 false 。 
//
// 单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。 
//
// 
//
// 示例 1： 
//
// 
//输入：board = [["A","B","C","E"],
//              ["S","F","C","S"],
//              ["A","D","E","E"]], word = "ABCCED"
//输出：true
// 
//
// 示例 2： 
//
//
//输入：board = [["A","B","C","E"]
//  ,["S","F","C","S"],["A","D","E","E"]], word = "SE
//E"
//输出：true
// 
//
// 示例 3： 
//
// 
//输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "AB
//CB"
//输出：false
// 
//
// 
//
// 提示： 
//
// 
// m == board.length 
// n = board[i].length 
// 1 <= m, n <= 6 
// 1 <= word.length <= 15 
// board 和 word 仅由大小写英文字母组成 
// 
//
// 
//
// 进阶：你可以使用搜索剪枝的技术来优化解决方案，使其在 board 更大的情况下可以更快解决问题？ 
// Related Topics 数组 回溯 矩阵 
// 👍 1055 👎 0


/**
 * 单词搜索
 **/
public class WordSearch_79 {
    public static void main(String[] args) {
        Solution solution = new WordSearch_79().new Solution();
        char[][] chars = {{'A', 'B', 'C', 'E'},
                {'S', 'F', 'C', 'S'},
                {'A', 'D', 'E', 'E'}};
        boolean abcb = solution.exist(chars, "ABCB");
        System.out.print(abcb);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        private int rows;
        private int cols;
        private int wordLength;
        private boolean[][] hasUse;
        private char[] charArray;
        private char[][] board;

        public boolean exist(char[][] board, String word) {
            rows = board.length;
            if (rows == 0) {
                return false;
            }
            cols = board[0].length;

            wordLength = word.length();
            charArray = word.toCharArray();
            hasUse = new boolean[rows][cols];
            this.board = board;

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    if (dfs(i, j, 0)) {
                        return true;
                    }
                }
            }
            return false;
        }


        private boolean dfs(int i, int j, int currCharIndex) {
            //判断是否已使用，是否点在区域内
            if (!isArea(i, j)||hasUse[i][j]){
                return false;
            }

            //结束条件
            if (currCharIndex == wordLength - 1) {
                return board[i][j] == charArray[currCharIndex];
            }

            if (board[i][j] == charArray[currCharIndex]) {
                //上下左右的尝试
                hasUse[i][j] = true;
                boolean res = dfs(i + 1, j, currCharIndex + 1) || dfs(i - 1, j, currCharIndex + 1)
                        || dfs(i, j + 1, currCharIndex + 1) || dfs(i, j - 1, currCharIndex + 1);

                if (res) {
                    return true;
                }
                hasUse[i][j] = false;
            }

            return false;

        }

        private boolean isArea(int i, int j) {
            return i >= 0 && i < rows && j >= 0 && j < cols;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}