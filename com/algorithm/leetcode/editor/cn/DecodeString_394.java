package com.algorithm.leetcode.editor.cn;
//给定一个经过编码的字符串，返回它解码后的字符串。 
//
// 编码规则为: k[encoded_string]，表示其中方括号内部的 encoded_string 正好重复 k 次。注意 k 保证为正整数。 
//
// 你可以认为输入字符串总是有效的；输入字符串中没有额外的空格，且输入的方括号总是符合格式要求的。 
//
// 此外，你可以认为原始数据不包含数字，所有的数字只表示重复的次数 k ，例如不会出现像 3a 或 2[4] 的输入。 
//
// 
//
// 示例 1： 
//
// 输入：s = "3[a]2[bc]"
//输出："aaabcbc"
// 
//
// 示例 2： 
//
// 输入：s = "3[a2[c]]"
//输出："accaccacc"
// 
//
// 示例 3： 
//
// 输入：s = "2[abc]3[cd]ef"
//输出："abcabccdcdcdef"
// 
//
// 示例 4： 
//
// 输入：s = "abc3[cd]xyz"
//输出："abccdcdcdxyz"
// 
// Related Topics 栈 递归 字符串 👍 960 👎 0


import java.util.LinkedList;
import java.util.Stack;

/**
 * 字符串解码
 **/
public class DecodeString_394 {
    public static void main(String[] args) {
        Solution solution = new DecodeString_394().new Solution();
        System.out.println(solution.decodeString("3[a]2[bc]"));

    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public String decodeString(String s) {
            //括号前的数量，用于做倍数计算
            LinkedList<Integer> countStack=new LinkedList<>();
            //字符串栈
            LinkedList<String> strStack=new LinkedList<>();
            StringBuilder sb=new StringBuilder();

            int count=0;
            //eg：输入：s = "3[a]2[bc]"
            for (char c : s.toCharArray()) {
                if (c>='0'&&c<='9'){
                    count=count*10+(c-'0');
                }else if (c>='a'&&c<='z'){
                    sb.append(c);
                }else if (c=='['){
                    countStack.push(count);
                    strStack.push(sb.toString());

                    sb=new StringBuilder();
                    count=0;
                }else {
                    //c==]
                    StringBuilder currSb=new StringBuilder();
                    currSb.append(strStack.pop());
                    Integer currCount = countStack.pop();
                    for (int i = 0; i < currCount; i++) {
                        currSb.append(sb);
                    }
                    sb=currSb;
                }
            }

        return sb.toString();
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}