package com.algorithm.leetcode.editor.cn;
//给你一个整数数组 nums ，你需要找出一个 连续子数组 ，如果对这个子数组进行升序排序，那么整个数组都会变为升序排序。 
//
// 请你找出符合题意的 最短 子数组，并输出它的长度。 
//
// 
//
// 
// 
// 示例 1： 
//
// 
//输入：nums = [2,6,4,8,10,9,15]
//输出：5
//解释：你只需要对 [6, 4, 8, 10, 9] 进行升序排序，那么整个表都会变为升序排序。
// 
//
// 示例 2： 
//
// 
//输入：nums = [1,2,3,4]
//输出：0
// 
//
// 示例 3： 
//
// 
//输入：nums = [1]
//输出：0
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 10⁴ 
// -10⁵ <= nums[i] <= 10⁵ 
// 
//
// 
//
// 进阶：你可以设计一个时间复杂度为 O(n) 的解决方案吗？ 
// 
// 
// Related Topics 栈 贪心 数组 双指针 排序 单调栈 👍 755 👎 0


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
最短无序连续子数组
**/
public class ShortestUnsortedContinuousSubarray_581{
    public static void main(String[] args) {
        Solution solution = new ShortestUnsortedContinuousSubarray_581().new Solution();
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
        //这个子数组有个特征
        // 子数组前面的数都是升序的，且最后一个数小于子数组里的任意一个数
        // 子数组后面的数都是升序的，且第一个数大于子数组里的任意一个数
    public int findUnsortedSubarray(int[] nums) {
        int len=nums.length;
        int max=nums[0];
        int min=nums[len-1];
        //这里right定义为-1，是为了避免数组本身有序的情况
        int left=0,right=-1;
        for (int i = 0; i < len; i++) {
            //寻找右边届
            if (max>nums[i]){
                right=i;
            }else {
                max=nums[i];
            }

            //寻找左边界
            if (min<nums[len-1-i]){
                left=len-1-i;
            }else {
                min=nums[len-1-i];
            }
        }
        return right-left+1;
    }

    public int findUnsortedSubarray1(int[] nums) {
        //拷贝份
        int[] clone = nums.clone();
        Arrays.sort(clone);

        //子数组左边界
        int left=0;
        //子数组右边届
        int right=nums.length-1;
        while (left<nums.length){
            if (nums[left]==clone[left]){
                left++;
            }else {
                break;
            }
        }

        while (right>=0){
            if (nums[right]==clone[right]){
                right--;
            }else {
                break;
            }
        }
        return right>left?right-left+1:0;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}