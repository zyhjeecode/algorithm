package com.algorithm.leetcode.editor.cn;
//给你链表的头结点 head ，请将其按 升序 排列并返回 排序后的链表 。 
//
// 进阶： 
//
// 
// 你可以在 O(n log n) 时间复杂度和常数级空间复杂度下，对链表进行排序吗？ 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：head = [4,2,1,3]
//输出：[1,2,3,4]
// 
//
// 示例 2： 
//
// 
//输入：head = [-1,5,3,4,0]
//输出：[-1,0,3,4,5]
// 
//
// 示例 3： 
//
// 
//输入：head = []
//输出：[]
// 
//
// 
//
// 提示： 
//
// 
// 链表中节点的数目在范围 [0, 5 * 10⁴] 内 
// -10⁵ <= Node.val <= 10⁵ 
// 
// Related Topics 链表 双指针 分治 排序 归并排序 👍 1344 👎 0


/**
 * 排序链表
 **/
public class SortList_148 {
    public static void main(String[] args) {
        Solution solution = new SortList_148().new Solution();
    }

//leetcode submit region begin(Prohibit modification and deletion)

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode() {}
     * ListNode(int val) { this.val = val; }
     * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     * }
     */
    class Solution {
        public ListNode sortList(ListNode head) {
            return sortList(head, null);
        }

        //对收尾为head和tail的链表进行排序
        public ListNode sortList(ListNode head, ListNode tail) {
            if (head == null) {
                return null;
            }

            //区间范围，前闭后开 [mid, tail)，tail不包含在内。
            //如果只有一个结点了就不需要再拆了
            if (head.next == tail) {
                head.next=null;
                return head;
            }

            //找到中间结点
            //快指针一次走两步，慢指针一次走一步，快指针到链表末尾时候
            //慢指针指向链表中间位置
            ListNode quick=head,slow=head;
            while (quick!=tail){
                quick=quick.next;
                slow=slow.next;
                //防null,直到quick走到最后一个结点
                if (quick!=tail){
                    quick=quick.next;
                }
            }

            ListNode listNode1 = sortList(head, slow);
            ListNode listNode2 = sortList(slow, tail);
            ListNode res=merge(listNode1,listNode2);
            return res;
        }


        //合并两个有序链表
        private ListNode merge(ListNode listNode1, ListNode listNode2) {
            ListNode pre=new ListNode(0);
            ListNode curr=pre,temp1=listNode1,temp2=listNode2;
            while (temp1!=null&&temp2!=null){
                if (temp1.val<temp2.val){
                    curr.next=temp1;
                    temp1=temp1.next;
                }else {
                    curr.next=temp2;
                    temp2=temp2.next;
                }
                curr=curr.next;
            }

            if (temp1==null){
                curr.next=temp2;
            }
            if (temp2==null){
                curr.next=temp1;
            }
            return pre.next;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}