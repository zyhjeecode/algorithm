package com.algorithm.leetcode.editor.cn;

import org.junit.Test;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @description: 单词拆分
 * @author: zyh
 * @create: 2021-10-21 17:23
 **/
public class WordBreak_139 {

    /**
     * 给定一个非空字符串 s 和一个包含非空单词的列表 wordDict，判定 s 是否可以被空格拆分为一个或多个在字典中出现的单词。
     * 说明：
     * 拆分时可以重复使用字典中的单词。
     * 你可以假设字典中没有重复的单词。
     * <p>
     * 示例 1：
     * 输入: s = "leetcode", wordDict = ["leet", "code"]
     * 输出: true
     * 解释: 返回 true 因为 "leetcode" 可以被拆分成 "leet code"。
     * <p>
     * 示例 2：
     * 输入: s = "applepenapple", wordDict = ["apple", "pen"]
     * 输出: true
     * 解释: 返回 true 因为 "applepenapple" 可以被拆分成 "apple pen apple"。
     * 注意你可以重复使用字典中的单词。
     */
    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> words = new HashSet<>(wordDict);

        boolean[] hasCheck = new boolean[s.length() + 1];

        hasCheck[0] = true;

        for (int i = 1; i <= s.length(); i++) {
            for (int j = i - 1; j >= 0; j--) {
                if (!hasCheck[j]) {
                    continue;
                }

                if (hasCheck[j] && words.contains(s.substring(j, i ))) {
                    hasCheck[i] = true;
                    break;
                }
            }
        }

        return hasCheck[s.length()];
    }

    @Test
    public void test() {
        String s = "leetcode";
        List<String> list = new LinkedList<>();
        list.add("leet");
        list.add("code");
        final boolean res = wordBreak(s, list);
        System.out.println(res);
    }

}
