package com.algorithm.leetcode.editor.cn;
//给你一个整数数组 nums ，找到其中最长严格递增子序列的长度。
//
// 子序列是由数组派生而来的序列，删除（或不删除）数组中的元素而不改变其余元素的顺序。例如，[3,6,2,7] 是数组 [0,3,1,6,2,2,7] 的子序
//列。
//
//
// 示例 1：
//
//
//输入：nums = [10,9,2,5,3,7,101,18]
//输出：4
//解释：最长递增子序列是 [2,3,7,101]，因此长度为 4 。
//
//
// 示例 2：
//
//
//输入：nums = [0,1,0,3,2,3]
//输出：4
//
//
// 示例 3：
//
//
//输入：nums = [7,7,7,7,7,7,7]
//输出：1
//
//
//
//
// 提示：
//
//
// 1 <= nums.length <= 2500
// -104 <= nums[i] <= 104
//
//
//
//
// 进阶：
//
//
// 你可以设计时间复杂度为 O(n2) 的解决方案吗？
// 你能将算法的时间复杂度降低到 O(n log(n)) 吗?
//
// Related Topics 数组 二分查找 动态规划
// 👍 2021 👎 0


public class LongestIncreasingSubsequence_300{
    public static void main(String[] args) {
        Solution  solution = new LongestIncreasingSubsequence_300().new Solution();
        final int i = solution.lengthOfLIS(new int[]{10, 9, 2, 5, 3, 7, 101, 18});
        System.out.print(i);
    }

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int lengthOfLIS(int[] nums) {
        if (nums.length<=1){
            return nums.length;
        }


        //存放长度为i+1的上升序列尾数尾部值,第0位存放长度位1的上升序列尾部值
        int[] tails=new int[nums.length];
        int res=0;

        for (int num : nums) {
            int i = 0, j = res;
            while(i < j) {
                int m = (i + j) / 2;
                if(tails[m] < num) {
                    i = m + 1;
                } else {
                    j = m;
                }
            }

            tails[i] = num;
            if(res == j) {
                res++;
            }
        }

        return res;

    }


    /**
     * 未优化的动态优化情况
     * @author zyh
     * @date 2021/11/17
     */
    public int lengthOfLIS2(int[] nums) {
        if (nums.length<=1){
            return nums.length;
        }
        //存放当前位置左侧小于等于当前值的数量
        int[] dp=new int[nums.length];
        dp[0]=1;

        int res=0;
        for (int i = 1; i < nums.length; i++) {
            dp[i]=1;
            for (int j = 0; j < i; j++) {
                if (nums[j]<nums[i]){
                    dp[i]=Math.max(dp[i],dp[j]+1);
                }
            }
            res=Math.max(res,dp[i]);
        }
        return res;

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}
