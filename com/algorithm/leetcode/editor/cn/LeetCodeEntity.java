package com.algorithm.leetcode.editor.cn;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LeetCodeEntity {
    public static TreeNode listToTree(List<Integer> nodes){
        int size = nodes.size();
        if (size ==0){
            return null;
        }

        int index=0;
        Queue<TreeNode> queue=new LinkedList<>();
        queue.add(new TreeNode(nodes.get(index++)));
        TreeNode root=null;
        while (!queue.isEmpty()){
            TreeNode curr = queue.poll();
            if (root==null){
                root=curr;
            }

            if (index<size){
                if (nodes.get(index)==null){
                    curr.left=null;
                }else {
                    curr.left=new TreeNode(nodes.get(index));
                    queue.add(curr.left);
                }
                index++;

                if (index>=size){
                    break;

                }
                if (nodes.get(index)==null){
                    curr.right=null;
                }else {
                    curr.right=new TreeNode(nodes.get(index));
                    queue.add(curr.right);
                }
                index++;
            }
        }
        return root;
    }

    @Test
    public void  test(){
        List<Integer> collect = Stream.of(1, -2, -3, 1, 3, -2, null, -1).collect(Collectors.toList());
        TreeNode root = listToTree(collect);
        System.out.print(root.val);

    }


}
class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
