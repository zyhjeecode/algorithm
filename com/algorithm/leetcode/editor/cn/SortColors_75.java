package com.algorithm.leetcode.editor.cn;
//给定一个包含红色、白色和蓝色，一共 n 个元素的数组，原地对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排列。 
//
// 此题中，我们使用整数 0、 1 和 2 分别表示红色、白色和蓝色。 
//
// 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [2,0,2,1,1,0]
//输出：[0,0,1,1,2,2]
// 
//
// 示例 2： 
//
// 
//输入：nums = [2,0,1]
//输出：[0,1,2]
// 
//
// 示例 3： 
//
// 
//输入：nums = [0]
//输出：[0]
// 
//
// 示例 4： 
//
// 
//输入：nums = [1]
//输出：[1]
// 
//
// 
//
// 提示： 
//
// 
// n == nums.length 
// 1 <= n <= 300 
// nums[i] 为 0、1 或 2 
// 
//
// 
//
// 进阶： 
//
// 
// 你可以不使用代码库中的排序函数来解决这道题吗？ 
// 你能想出一个仅使用常数空间的一趟扫描算法吗？ 
// 
// Related Topics 数组 双指针 排序 
// 👍 971 👎 0


/**
 * 颜色分类
 **/
public class SortColors_75 {
    public static void main(String[] args) {
        Solution solution = new SortColors_75().new Solution();
        int[] arr=new int[]{2,0,2,1,1,0};
        solution.sortColors1(arr);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public void sortColors1(int[] nums) {
            int len = nums.length;
            if (nums.length < 2) {
                return;
            }
            //按照0，1，2顺序排序
            int zero = -1;
            int two = len;
            int i = 0;
            while (i<two){
                if (nums[i]==0){
                    //之前的数字要么是0，要么是1，不会是2，因此不需要再考虑当前数字要不要再次交换问题，因此直接i++
                    swap(nums,++zero,i++);
                }else if (nums[i]==2){
                    //由于从后面交换到前面的数字，情况无法保证是否需要再次交换，因此需要再次遍历
                    swap(nums,--two,i);
                }else {
                    i++;
                }
            }
        }

        public void sortColors(int[] nums) {
            //0的数量，小于等于1的数量
            int count0=0,count1=0;
            for (int i = 0; i < nums.length; i++) {
                int curr=nums[i];
                nums[i]=2;
                if (curr<2){
                    nums[count1++]=1;
                }
                if (curr<1){
                    nums[count0++]=0;
                }
            }
        }

        private void swap(int[] nums, int index1, int index2) {
            int temp = nums[index1];
            nums[index1] = nums[index2];
            nums[index2] = temp;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}