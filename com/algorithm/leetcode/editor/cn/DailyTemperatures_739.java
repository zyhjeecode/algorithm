package com.algorithm.leetcode.editor.cn;
//请根据每日 气温 列表 temperatures ，请计算在每一天需要等几天才会有更高的温度。如果气温在这之后都不会升高，请在该位置用 0 来代替。 
//
// 示例 1: 
//
// 
//输入: temperatures = [73,74,75,71,69,72,76,73]
//输出: [1,1,4,2,1,1,0,0]
// 
//
// 示例 2: 
//
// 
//输入: temperatures = [30,40,50,60]
//输出: [1,1,1,0]
// 
//
// 示例 3: 
//
// 
//输入: temperatures = [30,60,90]
//输出: [1,1,0] 
//
// 
//
// 提示： 
//
// 
// 1 <= temperatures.length <= 10⁵ 
// 30 <= temperatures[i] <= 100 
// 
// Related Topics 栈 数组 单调栈 👍 976 👎 0


import java.util.Arrays;
import java.util.Stack;

/**
每日温度
**/
public class DailyTemperatures_739{
    public static void main(String[] args) {
        Solution solution = new DailyTemperatures_739().new Solution();
        int[] ints = solution.dailyTemperatures(new int[]{78, 72, 77});
        for (int anInt : ints) {
            System.out.print(anInt+"  ");
        }
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    /**
     * 单调栈法
     * @param temperatures
     * @return
     */
    public int[] dailyTemperatures(int[] temperatures) {
        //存储没找到更高温度的下标
        Stack<Integer> stack=new Stack<>();
        //结果集
        int[] res=new int[temperatures.length];

        for (int i = 0; i < temperatures.length; i++) {
            while (!stack.isEmpty()&&temperatures[stack.peek()]<temperatures[i]){
                Integer index = stack.pop();
                res[index]=i-index;
            }
            stack.push(i);
        }

        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}