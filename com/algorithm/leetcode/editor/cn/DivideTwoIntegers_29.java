package com.algorithm.leetcode.editor.cn;
//给定两个整数，被除数 dividend 和除数 divisor。将两数相除，要求不使用乘法、除法和 mod 运算符。 
//
// 返回被除数 dividend 除以除数 divisor 得到的商。 
//
// 整数除法的结果应当截去（truncate）其小数部分，例如：truncate(8.345) = 8 以及 truncate(-2.7335) = -2 
//
// 
//
// 示例 1: 
//
// 输入: dividend = 10, divisor = 3
//输出: 3
//解释: 10/3 = truncate(3.33333..) = truncate(3) = 3 
//
// 示例 2: 
//
// 输入: dividend = 7, divisor = -3
//输出: -2
//解释: 7/-3 = truncate(-2.33333..) = -2 
//
// 
//
// 提示： 
//
// 
// 被除数和除数均为 32 位有符号整数。 
// 除数不为 0。 
// 假设我们的环境只能存储 32 位有符号整数，其数值范围是 [−2³¹, 231 − 1]。本题中，如果除法结果溢出，则返回 231 − 1。 
// 
// Related Topics 位运算 数学 👍 806 👎 0


import com.algorithm.future.designPattern.状态模式.Yellow;

/**
两数相除
**/
public class DivideTwoIntegers_29{
    public static void main(String[] args) {
        Solution solution = new DivideTwoIntegers_29().new Solution();
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int divide(int dividend, int divisor) {
        if (dividend==Integer.MIN_VALUE&&divisor==-1){
            return Integer.MAX_VALUE;
        }

        //判断符号是否一致
        //这里不能用两者的乘法做运算，因为可能会移除变负数;
        boolean same= (dividend>0&&divisor>0||dividend<0&&divisor<0)?true:false;

        // 将被除数和除数都转成正数或负数进行计算
        // 由于在Java中，当t=Integer.MIN_VALUE时（t取相反数依旧是它本身）此时可能存在越界问题，因此都用负数进行计算
        dividend=-Math.abs(dividend);
        divisor=-Math.abs(divisor);
        int res=0;
        while (dividend<=divisor){
            int temp=divisor;
            //记录有多少个divisor
            int count=1;
            //dividend每次减去2^n个divisor（尽可能多），同时reslut每次加2^n
            while (dividend-temp<=temp){
                //temp*2,count*2;
                temp=temp<<1;
                count=count<<1;
            }
            //减去2的n次方个divisor
            dividend=dividend-temp;
            res=res+count;
        }
        return same? res:-res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}