package com.algorithm.leetcode.editor.cn;
//给你一个 只包含正整数 的 非空 数组 nums 。请你判断是否可以将这个数组分割成两个子集，使得两个子集的元素和相等。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [1,5,11,5]
//输出：true
//解释：数组可以分割成 [1, 5, 5] 和 [11] 。 
//
// 示例 2： 
//
// 
//输入：nums = [1,2,3,5]
//输出：false
//解释：数组不能分割成两个元素和相等的子集。
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 200 
// 1 <= nums[i] <= 100 
// 
// Related Topics 数组 动态规划 👍 1051 👎 0


/**
 * 分割等和子集
 **/
public class PartitionEqualSubsetSum_416 {
    public static void main(String[] args) {
        Solution solution = new PartitionEqualSubsetSum_416().new Solution();
        System.out.println(solution.canPartition(new int[]{1, 5, 11, 5}));
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public boolean canPartition(int[] nums) {
            //数组的和
            int sum = 0, max = 0;
            for (int num : nums) {
                sum += num;
                max = Math.max(num, max);
            }

            //目标值，这里其实就转换为是否能从nums里任选n个数字，其和恰好为target了
            int target = sum / 2;

            //如果是奇数或者数组里有数字大于其一半的值就直接返回
            if ((sum & 1) == 1 || max > target) {
                return false;
            }

            int length = nums.length;
            //dp[i][j] 是否i个数里任选若干个数字可以组成j
            //可能情况：
            //  当前物品nums[i]不选,看看能否组成j，即判断dp[i-1][j]=true;
            //  当前物品nums[i]本身就等于j, 即判断nums[i]=j
            //  当前物品nums[i]要选才能组成j,也就是其他物品要组成j-nums[i],即判断 dp[i-1][j-nums[i]]=true,但是这里要注意j需要大于nums[i]
            boolean[][] dp = new boolean[length][target + 1];
            //初始化(组成重量为0的数)
            dp[0][0] = false;

            for (int i = 1; i < length; i++) {
                for (int j = 0; j <= target; j++) {
                    //先按第一种情况进行赋值，后面再修改
                    dp[i][j] = dp[i - 1][j];

                    if (nums[i] == j) {
                        dp[i][j] = true;
                        continue;
                    }

                    if (j > nums[i]) {
                        dp[i][j] = dp[i - 1][j] || dp[i - 1][j - nums[i]];
                    }
                }
            }

            return dp[length - 1][target];
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}