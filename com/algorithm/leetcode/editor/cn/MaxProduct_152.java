package com.algorithm.leetcode.editor.cn;

import org.junit.Test;

/**
 * @description: 152. 乘积最大子数组
 * @author: zyh
 * @create: 2021-11-02 18:00
 **/
public class MaxProduct_152 {
    //给你一个整数数组 nums ，请你找出数组中乘积最大的连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。

    /**
     输入: [2,3,-2,4]
     输出: 6
     解释: 子数组 [2,3] 有最大乘积 6。

     输入: [-2,0,-1]
     输出: 0
     解释: 结果不能为 2, 因为 [-2,-1] 不是子数组。
     **/
    public int maxProduct(int[] nums) {
        if (nums.length==1){
            return nums[0];
        }

        int max=nums[0],itemMax=nums[0],itemMin=nums[0];
        //max 存储所有连续子数组最大值
        //itemMax,itemMin存储当前子数组最大值和最小值
        //其中需要itemMin存最小值主要因为数组里可能有负数,负数最小值再乘以一个整数就是最大值
        for (int i = 1; i < nums.length; i++) {
            if (nums[i]<0){
                int temp=itemMin;
                itemMin=itemMax;
                itemMax=temp;
            }

            itemMax=Math.max(itemMax*nums[i],nums[i]);
            itemMin=Math.min(itemMin*nums[i],nums[i]);

            max=Math.max(max,itemMax);
        }
        return max;
    }

    @Test
    public void test(){
        final int[] ints = {2, -1, 3,0, 4};
        final int i = maxProduct(ints);
        System.out.println(i);
    }
}
