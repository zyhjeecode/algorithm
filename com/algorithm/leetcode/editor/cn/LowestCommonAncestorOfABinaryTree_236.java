package com.algorithm.leetcode.editor.cn;
//给定一个二叉树, 找到该树中两个指定节点的最近公共祖先。
//
// 百度百科中最近公共祖先的定义为：“对于有根树 T 的两个节点 p、q，最近公共祖先表示为一个节点 x，满足 x 是 p、q 的祖先且 x 的深度尽可能大（
//一个节点也可以是它自己的祖先）。”
//
//
//
// 示例 1：
//
//
//输入：root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
//输出：3
//解释：节点 5 和节点 1 的最近公共祖先是节点 3 。
//
//
// 示例 2：
//
//
//输入：root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
//输出：5
//解释：节点 5 和节点 4 的最近公共祖先是节点 5 。因为根据定义最近公共祖先节点可以为节点本身。
//
//
// 示例 3：
//
//
//输入：root = [1,2], p = 1, q = 2
//输出：1
//
//
//
//
// 提示：
//
//
// 树中节点数目在范围 [2, 105] 内。
// -109 <= Node.val <= 109
// 所有 Node.val 互不相同 。
// p != q
// p 和 q 均存在于给定的二叉树中。
//
// Related Topics 树 深度优先搜索 二叉树
// 👍 1379 👎 0


import com.algorithm.GenerateStructure.TreeNode;

public class LowestCommonAncestorOfABinaryTree_236{
    public static void main(String[] args) {
        Solution  solution = new LowestCommonAncestorOfABinaryTree_236().new Solution();

    }

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    /**
     * 分析root有以下三种情况
     * p 和 q 在 root 的子树中，且分列 root 的 异侧（即分别在左、右子树中）；
     * p = root p=root ，且 q 在 root 的左或右子树中；
     * q = root q=root ，且 p 在 root 的左或右子树中；
     *
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {

        //如果root是p或者q,则root就是最近公共祖先
        if (root.val==p.val||root.val==q.val){
            return root;
        }

        //如果两个结点都在左子树里,就在左子树里继续找
        if (find(root.left,p)&&find(root.left,q)){
            return lowestCommonAncestor(root.left,p,q);
        }

        //如果两个结点都在右子树里,就在右子树里继续找
        if (find(root.right,p)&&find(root.right,q)){
            return lowestCommonAncestor(root.right,p,q);
        }

        //如果没同时在左子树也没同时在结点的右子树里,那本结点就是最近公共祖先
        return root;
    }


    /**
     * 判断node子树里是否含有find结点
     * @author zyh
     * @date 2021/11/13
     */
    public boolean find(TreeNode node,TreeNode find){
        if (node==null){
            return false;
        }

        if (node.val==find.val){
            return true;
        }

        return find(node.left,find)||find(node.right,find);
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}
