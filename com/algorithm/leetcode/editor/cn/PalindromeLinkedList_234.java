package com.algorithm.leetcode.editor.cn;
//给你一个单链表的头节点 head ，请你判断该链表是否为回文链表。如果是，返回 true ；否则，返回 false 。
//
//
//
// 示例 1：
//
//
//输入：head = [1,2,3,2,1]
//输出：true
//
//
// 示例 2：
//
//
//输入：head = [1,2]
//输出：false
//
//
//
//
// 提示：
//
//
// 链表中节点数目在范围[1, 105] 内
// 0 <= Node.val <= 9
//
//
//
//
// 进阶：你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？
// Related Topics 栈 递归 链表 双指针
// 👍 1168 👎 0


public class PalindromeLinkedList_234 {
    public static void main(String[] args) {
        Solution solution = new PalindromeLinkedList_234().new Solution();
        ListNode root=new ListNode(1);
        root.next=new ListNode(2);
        root.next.next=new ListNode(3);
        root.next.next.next=new ListNode(2);
        System.out.println(solution.isPalindrome(root));;
    }

//leetcode submit region begin(Prohibit modification and deletion)

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode() {}
     * ListNode(int val) { this.val = val; }
     * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     * }
     */
    class Solution {
        public boolean isPalindrome(ListNode head) {
            if (head==null||head.next==null){
                return true;
            }

            //定义快慢指针
            ListNode slow=head,fast=head;

            //寻找中间结点
            while (fast!=null&&fast.next!=null){
                slow=slow.next;
                fast=fast.next.next;
            }
            //如果fast不为空，说明链表的长度是奇数个
            if (fast != null) {
                slow = slow.next;
            }

            ListNode lastHead = reverseNode(slow);

            while (lastHead!=null){
                if (lastHead.val!=head.val){
                    return false;
                }
                head=head.next;
                lastHead=lastHead.next;
            }


            return true;
        }

        private ListNode reverseNode(ListNode slow) {
            ListNode pre=null;
            ListNode curr=slow;
            while (curr!=null){
                ListNode temp=curr.next;
                curr.next=pre;
                pre=curr;
                curr=temp;
            }
            return pre;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
