package com.algorithm.leetcode.editor.cn;
//给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。
//
// 你可以假设数组是非空的，并且给定的数组总是存在多数元素。
//
//
//
// 示例 1：
//
//
//输入：[3,2,3]
//输出：3
//
// 示例 2：
//
//
//输入：[2,2,1,1,1,2,2]
//输出：2
//
//
//
//
// 进阶：
//
//
// 尝试设计时间复杂度为 O(n)、空间复杂度为 O(1) 的算法解决此问题。
//
// Related Topics 数组 哈希表 分治 计数 排序
// 👍 1185 👎 0


public class MajorityElement_169 {
    public static void main(String[] args) {
        Solution solution = new MajorityElement_169().new Solution();
        final int[] ints = {3,2,3};
        System.out.print(solution.majorityElement(ints));

    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        /**
         * 守阵地法,每个不同的数字都是一个部队,遇到自己人进行阵营人数++,堆到敌人就自爆带走
         *
         * @author zyh
         * @date 2021/11/3
         */
        public int majorityElement(int[] nums) {
            int count = 0;
            Integer currNum=null;
            for (int i = 0; i < nums.length; i++) {
                if (currNum==null){
                    count=1;
                    currNum=nums[i];
                }else{
                    if (nums[i]==currNum){
                        count++;
                    }else{
                        count--;
                        if (count==0){
                            currNum=null;
                        }
                    }
                }
            }
            return currNum;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
