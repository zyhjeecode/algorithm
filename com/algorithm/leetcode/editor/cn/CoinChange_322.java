package com.algorithm.leetcode.editor.cn;
//给你一个整数数组 coins ，表示不同面额的硬币；以及一个整数 amount ，表示总金额。 
//
// 计算并返回可以凑成总金额所需的 最少的硬币个数 。如果没有任何一种硬币组合能组成总金额，返回 -1 。 
//
// 你可以认为每种硬币的数量是无限的。 
//
// 
//
// 示例 1： 
//
// 
//输入：coins = [1, 2, 5], amount = 11
//输出：3 
//解释：11 = 5 + 5 + 1 
//
// 示例 2： 
//
// 
//输入：coins = [2], amount = 3
//输出：-1 
//
// 示例 3： 
//
// 
//输入：coins = [1], amount = 0
//输出：0
// 
//
// 示例 4： 
//
// 
//输入：coins = [1], amount = 1
//输出：1
// 
//
// 示例 5： 
//
// 
//输入：coins = [1], amount = 2
//输出：2
// 
//
// 
//
// 提示： 
//
// 
// 1 <= coins.length <= 12 
// 1 <= coins[i] <= 2³¹ - 1 
// 0 <= amount <= 10⁴ 
// 
// Related Topics 广度优先搜索 数组 动态规划 👍 1599 👎 0


import java.util.Arrays;

/**
 * 零钱兑换
 **/
public class CoinChange_322 {
    public static void main(String[] args) {
        Solution solution = new CoinChange_322().new Solution();
        int res = solution.coinChange(new int[]{3, 5, 7}, 11);
        System.out.println(res);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        //拆分coins =[1, 2, 5]，amount=11
        //面值为11的最小可能和为以下可能的最小值
        //面值为1的硬币最小可能组合数（1），加面值为（11-1）=10的最小可能组合数量
        //面值为2的硬币最小可能组合数，加面值为9的最小可能组合数量
        //面值为5的硬币最小可能组合数，加面值为6的最小可能组合数量
        //设dp[amount]为金额为amount的最小可能组合
        //这里dp[11]=min(1+dp[11-coins[0],1+coins[11-coins[1],1+coins[11-coins[2])
        //即dp[amount] = min(dp[amount], 1 + dp[amount - coins[i]]) for i in [0, len - 1] if coins[i] <= amount
        public int coinChange(int[] coins, int amount) {
            int[] dp = new int[amount + 1];

            //这里我们要求最小组合数，因此这里可以填充最大值
            Arrays.fill(dp, Integer.MAX_VALUE);

            dp[0] = 0;

            for (int i = 1; i <= amount; i++) {
                //遍历每种可能
                for (int coin : coins) {
                    //1. i - coin >= 0 意思当前硬币要小于等于我们要凑的钱数
                    //2. dp[i - coin] != Integer.MAX_VALUE 意思我们剩下的钱也是要能凑齐的才行；
                    //   不是所有的dp都有意义，有可能这个组合就是不存在，因此这里也可以理解为只有除掉不可拆分的硬币之外剩余的钱必须能被凑齐才有意义做为比较值；
                    if (i - coin >= 0 && dp[i - coin] != Integer.MAX_VALUE) {
                        dp[i] = Math.min(dp[i - coin] + 1, dp[i]);
                    }
                }
            }

            return dp[amount] == Integer.MAX_VALUE ? -1 : dp[amount];
        }


    }
//leetcode submit region end(Prohibit modification and deletion)

}