package com.algorithm.leetcode.editor.cn;
//给定一个无重复元素的数组 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。 
//
// candidates 中的数字可以无限制重复被选取。 
//
// 说明： 
//
// 
// 所有数字（包括 target）都是正整数。 
// 解集不能包含重复的组合。 
// 
//
// 示例 1： 
//
// 输入：candidates = [2,3,6,7], target = 7,
//所求解集为：
//[
//  [7],
//  [2,2,3]
//]
// 
//
// 示例 2： 
//
// 输入：candidates = [2,3,5], target = 8,
//所求解集为：
//[
//  [2,2,2,2],
//  [2,3,3],
//  [3,5]
//] 
//
// 
//
// 提示： 
//
// 
// 1 <= candidates.length <= 30 
// 1 <= candidates[i] <= 200 
// candidate 中的每个元素都是独一无二的。 
// 1 <= target <= 500 
// 
// Related Topics 数组 回溯 
// 👍 1429 👎 0


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 组合总和
 **/
public class CombinationSum_39 {
    public static void main(String[] args) {
        Solution solution = new CombinationSum_39().new Solution();
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {

        List<List<Integer>> res = new LinkedList<>();

        public List<List<Integer>> combinationSum(int[] candidates, int target) {
            //排序方便剪枝
            Arrays.sort(candidates);
            searchRes(candidates, 0, target, new LinkedList<>());
            return res;
        }

        /**
         * path其实就是taget每次减的数字记录的值，如果我们target减为0的，那么该路径就是其和的路径了
         * 随着path变深，
         */
        public void searchRes(int[] candidates, int start, int target, LinkedList<Integer> path) {
            //结束循环
            if (target < 0) {
                return;
            }
            if (target == 0) {
                res.add(new LinkedList<>(path));
                return;
            }

            //每次从自己和自己后面的数字遍历即可减少很多重复计算
            for (int i = start; i < candidates.length; i++) {
                // 如果target<当前数，则跳过
                // 因为如果target小于当前值也就说当前值已不能构成path的加数了，不然总值会大于taget
                if (target < candidates[i]) {
                    break;
                }
                ;
                path.add(candidates[i]);
                searchRes(candidates, i, target - candidates[i], path);
                //回撤
                path.removeLast();
            }


        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}