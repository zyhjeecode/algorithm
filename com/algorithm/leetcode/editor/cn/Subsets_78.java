package com.algorithm.leetcode.editor.cn;
//给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。 
//
// 解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [1,2,3]
//输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
// 
//
// 示例 2： 
//
// 
//输入：nums = [0]
//输出：[[],[0]]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 10 
// -10 <= nums[i] <= 10 
// nums 中的所有元素 互不相同 
// 
// Related Topics 位运算 数组 回溯 
// 👍 1283 👎 0


import java.util.LinkedList;
import java.util.List;

/**
子集
**/
public class Subsets_78{
    public static void main(String[] args) {
        Solution solution = new Subsets_78().new Solution();
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res=new LinkedList<>();
        getAll(0,nums,new LinkedList<Integer>(),res);
        return res;
    }

    private void getAll(int i, int[] nums, LinkedList<Integer> curr, List<List<Integer>> res) {
        res.add(new LinkedList<>(curr));
        //排序第i和元素参与排序的情况，这样每次其实我们是排除了一种已有元素排序的情况
        for (int j = i; j < nums.length; j++) {
            if (!curr.contains(nums[j])){
                curr.add(nums[j]);
                getAll(j+1,nums,curr,res);
                curr.removeLast();
            }
        }
    }

}
//leetcode submit region end(Prohibit modification and deletion)

}