package com.algorithm.leetcode.editor.cn;
//给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [1,2,3]
//输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
// 
//
// 示例 2： 
//
// 
//输入：nums = [0,1]
//输出：[[0,1],[1,0]]
// 
//
// 示例 3： 
//
// 
//输入：nums = [1]
//输出：[[1]]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 6 
// -10 <= nums[i] <= 10 
// nums 中的所有整数 互不相同 
// 
// Related Topics 数组 回溯 
// 👍 1452 👎 0


import java.util.LinkedList;
import java.util.List;

/**
全排列
**/
public class Permutations_46{
    public static void main(String[] args) {
        Solution solution = new Permutations_46().new Solution();
        List<List<Integer>> res = solution.permute(new int[]{1});
        res.forEach(System.out::println);
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public List<List<Integer>> permute(int[] nums) {
        LinkedList<List<Integer>> res = new LinkedList<>();
        getRes(nums,res, new LinkedList<Integer>(),nums.length);
        return res;
    }

    private void getRes(int[] nums,LinkedList<List<Integer>> lists, LinkedList<Integer> path,int len) {
        if (path.size()==len){
            lists.add(new LinkedList<>(path));
            return;
        }

        for (int i = 0; i < len; i++) {
            if (path.contains(nums[i])){
                continue;
            }else {
                path.add(nums[i]);
                getRes(nums,lists,path,len);
                path.removeLast();
            }
        }
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}