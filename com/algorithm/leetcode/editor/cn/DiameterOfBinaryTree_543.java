package com.algorithm.leetcode.editor.cn;
//给定一棵二叉树，你需要计算它的直径长度。一棵二叉树的直径长度是任意两个结点路径长度中的最大值。这条路径可能穿过也可能不穿过根结点。 
//
// 
//
// 示例 : 
//给定二叉树 
//
//           1
//         / \
//        2   3
//       / \     
//      4   5    
// 
//
// 返回 3, 它的长度是路径 [4,2,1,3] 或者 [5,2,1,3]。 
//
// 
//
// 注意：两结点之间的路径长度是以它们之间边的数目表示。 
// Related Topics 树 深度优先搜索 二叉树 👍 856 👎 0


import com.sun.xml.internal.bind.v2.model.core.ID;

/**
二叉树的直径
**/
public class DiameterOfBinaryTree_543{
    public static void main(String[] args) {
        Solution solution = new DiameterOfBinaryTree_543().new Solution();
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    int max;
    public int diameterOfBinaryTree(TreeNode root) {
        //分析下，二叉树的最长路径某个结点的左孩子最大深度加右孩子的最大深度
        max=0;
        dfs(root);
        return max;
    }

    /**
     * 返回树的深度
     */
    private int dfs(TreeNode root) {
        if (root==null){
            return 0;
        }
        //左子树的深度
        int lDeep= dfs(root.left);
        //右子树的深度
        int rDeep= dfs(root.right);

        max=Math.max(max,lDeep+rDeep);
        return Math.max(lDeep,rDeep)+1;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}