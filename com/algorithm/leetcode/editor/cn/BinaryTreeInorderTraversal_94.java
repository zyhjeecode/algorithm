package com.algorithm.leetcode.editor.cn;
//给定一个二叉树的根节点 root ，返回它的 中序 遍历。 
//
// 
//
// 示例 1： 
//
// 
//输入：root = [1,null,2,3]
//输出：[1,3,2]
// 
//
// 示例 2： 
//
// 
//输入：root = []
//输出：[]
// 
//
// 示例 3： 
//
// 
//输入：root = [1]
//输出：[1]
// 
//
// 示例 4： 
//
// 
//输入：root = [1,2]
//输出：[2,1]
// 
//
// 示例 5： 
//
// 
//输入：root = [1,null,2]
//输出：[1,2]
// 
//
// 
//
// 提示： 
//
// 
// 树中节点数目在范围 [0, 100] 内 
// -100 <= Node.val <= 100 
// 
//
// 
//
// 进阶: 递归算法很简单，你可以通过迭代算法完成吗？ 
// Related Topics 栈 树 深度优先搜索 二叉树 
// 👍 1121 👎 0


import com.algorithm.GenerateStructure.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
二叉树的中序遍历
**/
public class BinaryTreeInorderTraversal_94{
    public static void main(String[] args) {
        Solution solution = new BinaryTreeInorderTraversal_94().new Solution();
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> res= new LinkedList<>();
        Stack<TreeNode> stack=new Stack<>();

        while (root!=null||stack.size()>0){
            //直到不能继续向左再停止
            if (root!=null){
                stack.add(root);
                root=root.left;
                //如果root为null说明stack最新的一个结点无左节点，
                // 因此直接先打印本节点，再迭代调用B结点
            }else {
                TreeNode pop = stack.pop();
                res.add(pop.val);
                root=pop.right;
            }
        }
        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}