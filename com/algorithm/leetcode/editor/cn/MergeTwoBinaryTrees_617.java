package com.algorithm.leetcode.editor.cn;
//给定两个二叉树，想象当你将它们中的一个覆盖到另一个上时，两个二叉树的一些节点便会重叠。 
//
// 你需要将他们合并为一个新的二叉树。合并的规则是如果两个节点重叠，那么将他们的值相加作为节点合并后的新值，否则不为 NULL 的节点将直接作为新二叉树的节点
//。 
//
// 示例 1: 
//
// 
//输入: 
//	Tree 1                     Tree 2                  
//          1                         2                             
//         / \                       / \                            
//        3   2                     1   3                        
//       /                           \   \                      
//      5                             4   7                  
//输出: 
//合并后的树:
//	     3
//	    / \
//	   4   5
//	  / \   \ 
//	 5   4   7
// 
//
// 注意: 合并必须从两个树的根节点开始。 
// Related Topics 树 深度优先搜索 广度优先搜索 二叉树 👍 834 👎 0


import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
合并二叉树
**/
public class MergeTwoBinaryTrees_617{
    public static void main(String[] args) {
        Solution solution = new MergeTwoBinaryTrees_617().new Solution();
        TreeNode tree1 = LeetCodeEntity.listToTree(Stream.of(1, 3, 2, 5).collect(Collectors.toList()));
        TreeNode tree2 = LeetCodeEntity.listToTree(Stream.of(2,1,3,null,4,null,7).collect(Collectors.toList()));
        solution.dfs(tree1,tree2);

    }
    
//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
        TreeNode res = dfs(root1, root2);
        return res;
    }

    //找出合并后的结点
    private TreeNode dfs(TreeNode root1, TreeNode root2) {
        if (root1==null){
            return root2;
        }
        if (root2==null){
            return root1;
        }

        TreeNode newNode = new TreeNode(root1.val + root2.val);
        newNode.left=dfs(root1.left,root2.left);
        newNode.right=dfs(root1.right,root2.right);
        return newNode;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}