package com.algorithm.leetcode.editor.cn;

/**
 * @description: 判断链表中是否有环
 * @author: zyh
 * @create: 2021-10-22 20:18
 **/
public class HasCycle_141 {
    // 3 → 2 → 0 → -4
    //     ↑        ↓
    //     ↑← ← ← ← ←
    public boolean hasCycle(ListNode head) {
        if (head==null){
            return false;
        }

        //一个一次走两步的 一个一次走一步的,如果两人能两次相遇(起点,终点),必然存在环
        ListNode fast=head;
        ListNode slow=head;
        int meetCount=0;
        while (slow!=null){
            if (fast==slow){
                meetCount++;
            }

            if (meetCount==2){
                return true;
            }
            //如果过程中快指针出现了null说明没环
            if (fast==null||fast.next==null){
                return false;
            }

            fast=fast.next.next;
            slow=slow.next;
        }
        return false;
    }
}
