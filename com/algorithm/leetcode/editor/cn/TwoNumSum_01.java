package com.algorithm.leetcode.editor.cn;

/**
 * @description: 两数之和
 * @author: zyh
 * @create: 2021-04-14 20:49
 **/

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 给定一个整数数组 nums和一个整数目标值 target,请你在该数组中找出和为目标值target的那两个整数，并返回它们的数组下标。
 你可以假设每种输入只会对应一个答案。
 但是，数组中同一个元素在答案里不能重复出现。你可以按任意顺序返回答案。
 *
 *
 示例 1：
 输入：nums = [2,7,11,15], target = 9
 输出：[0,1]
 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。

 示例 2：
 输入：nums = [3,2,4], target = 6
 输出：[1,2]

 示例 3：
 输入：nums = [3,3], target = 6
 输出：[0,1]
 */
public class TwoNumSum_01 {
    public int[] twoSum(int[] nums, int target) {
        int[] res= new int[2];
        if (nums.length<2){
            return res;
        }
        Map<Integer,Integer> keyIndex=new HashMap<>(nums.length);
        for (int i = 0; i < nums.length; i++) {
            if (keyIndex.containsKey(target-nums[i])){
                res[0]=keyIndex.get(target-nums[i]);
                res[1]=i;
                return res;
            }else {
                keyIndex.put(nums[i],i);
            }
        }
        return res;
    }

    @Test
    public void  test(){
        int[] ints = {3,2,4};
        int[] res = twoSum(ints, 6);
        for (int i = 0,len=res.length; i <len ; i++) {
            System.out.print(res[i]+" ");
        }
    }
}
