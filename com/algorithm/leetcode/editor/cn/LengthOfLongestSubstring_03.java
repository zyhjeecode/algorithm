package com.algorithm.leetcode.editor.cn;

import java.util.HashMap;
import java.util.Map;

public class LengthOfLongestSubstring_03 {
    /**
     给定一个字符串，请你找出其中不含有重复字符的最长子串的长度。
     示例1：
     输入：s="abcabcbb'
     输出：3
     解释：因为无重复字符的最长子串是"abc"，所以其长度为3。
     示例2：
     输入：s="bbbbb"
     输出：1
     解释：因为无重复字符的最长子串是"b"，所以其长度为1。
     示例3：
     输入：s="pwwkew"
     输出：3
     解释：因为无重复字符的最长子串是"wke"，所以其长度为3。
     请注意，你的答案必须是子串的长度，"pwke"是一个子序列，不是子串。
     示例4：
     输入：S="输出：0
     */
    public int lengthOfLongestSubstring(String s) {
        int length=s.length();
        int max=0;
        //存放字符以及
        Map<Character,Integer> map =new HashMap<>();
        for (int start = 0,end=0; end <length ; end++) {
            char element=s.charAt(end);
            if (map.containsKey(element)){
                //为了防止连续重复字符，这里要进行一次判断
                //+1表示该元素后一个元素才是不重复字符串的开始
                start=Math.max(map.get(element)+1,start);
            }
            max=Math.max(max,end-start+1);
            //保存最后一个该结点的位置；
            map.put(element,end);
        }
        return max;
    }
}
