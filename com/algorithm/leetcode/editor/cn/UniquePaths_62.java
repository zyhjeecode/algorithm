package com.algorithm.leetcode.editor.cn;
//一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为 “Start” ）。 
//
// 机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。 
//
// 问总共有多少条不同的路径？ 
//
// 
//
// 示例 1： 
//
// 
//输入：m = 3, n = 7
//输出：28 
//
// 示例 2： 
//
// 
//输入：m = 3, n = 2
//输出：3
//解释：
//从左上角开始，总共有 3 条路径可以到达右下角。
//1. 向右 -> 向下 -> 向下
//2. 向下 -> 向下 -> 向右
//3. 向下 -> 向右 -> 向下
// 
//
// 示例 3： 
//
// 
//输入：m = 7, n = 3
//输出：28
// 
//
// 示例 4： 
//
// 
//输入：m = 3, n = 3
//输出：6 
//
// 
//
// 提示： 
//
// 
// 1 <= m, n <= 100 
// 题目数据保证答案小于等于 2 * 109 
// 
// Related Topics 数学 动态规划 组合数学 
// 👍 1057 👎 0


/**
 * 不同路径
 **/
public class UniquePaths_62 {
    public static void main(String[] args) {
        Solution solution = new UniquePaths_62().new Solution();
        int i = solution.uniquePaths(51, 9);
        System.out.println(i);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        int res = 0;

        public int uniquePaths(int m, int n) {
            int[][] data=new int[m][n];
            for (int i = 0; i < m; ++i) {
                data[i][0]=1;
            }

            for (int i = 0; i < n; ++i) {
                data[0][i]=1;
            }
            for (int i = 1; i < m; i++) {
                for (int j = 1; j < n; j++) {
                    data[i][j]=data[i-1][j]+data[i][j-1];
                }
            }

            return data[m-1][n-1];
        }
    }
//leetcode submit region end(Prohibit modification and deletion)


    class Solution2 {
        int res = 0;

        public int uniquePaths(int m, int n) {
            //x,y 0 0 xmac m ymax x
            getCount(0, 0, m, n);
            return res;
        }

        private void getCount(int x, int y, int m, int n) {
            if (x >= m || y >= n) {
                return;
            }

            if (x == m - 1 || y == n - 1) {
                res++;
                return;
            }

            //向右
            if (x < m - 1) {
                x++;
                getCount(x, y, m, n);
                x--;
            }


            //向下
            if (y < n - 1) {
                y++;
                getCount(x, y, m, n);
                y--;
            }

        }
    }
}