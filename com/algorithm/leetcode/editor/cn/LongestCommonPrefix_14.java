package com.algorithm.leetcode.editor.cn;
//编写一个函数来查找字符串数组中的最长公共前缀。 
//
// 如果不存在公共前缀，返回空字符串 ""。 
//
// 
//
// 示例 1： 
//
// 
//输入：strs = ["flower","flow","flight"]
//输出："fl"
// 
//
// 示例 2： 
//
// 
//输入：strs = ["dog","racecar","car"]
//输出：""
//解释：输入不存在公共前缀。 
//
// 
//
// 提示： 
//
// 
// 1 <= strs.length <= 200 
// 0 <= strs[i].length <= 200 
// strs[i] 仅由小写英文字母组成 
// 
// Related Topics 字符串 👍 1950 👎 0


/**
最长公共前缀
**/
public class LongestCommonPrefix_14{
    public static void main(String[] args) {
        Solution solution = new LongestCommonPrefix_14().new Solution();
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public String longestCommonPrefix(String[] strs) {
        if (strs.length==0){
            return "";
        }
        //公共前缀比所有字符串都短，随便选一个先
        String commonPre = strs[0];
        //比较每个字符串，目标是找到一个所有字符串的公共子串，这个子串肯定长度是最短的
        for (String curr : strs) {
            while (!curr.startsWith(commonPre)){
                //如果公共子串变成空串就没必要继续比较了
                if (commonPre.length()==0){
                    return "";
                }else {
                    //如果不匹配就让公共串继续变短
                    commonPre=commonPre.substring(0,commonPre.length()-1);
                }
            }
        }
        return commonPre;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}