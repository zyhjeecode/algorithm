package com.algorithm.leetcode.editor.cn;
//以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [starti, endi] 。请你合并所有重叠的区间，并返
//回一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间。 
//
// 
//
// 示例 1： 
//
// 
//输入：intervals = [[1,3],[2,6],[8,10],[15,18]]
//输出：[[1,6],[8,10],[15,18]]
//解释：区间 [1,3] 和 [2,6] 重叠, 将它们合并为 [1,6].
// 
//
// 示例 2： 
//
// 
//输入：intervals = [[1,4],[4,5]]
//输出：[[1,5]]
//解释：区间 [1,4] 和 [4,5] 可被视为重叠区间。 
//
// 
//
// 提示： 
//
// 
// 1 <= intervals.length <= 104 
// intervals[i].length == 2 
// 0 <= starti <= endi <= 104 
// 
// Related Topics 数组 排序 
// 👍 1023 👎 0


import java.util.Arrays;
import java.util.Comparator;

/**
合并区间
**/
public class MergeIntervals_56{
    public static void main(String[] args) {
        Solution solution = new MergeIntervals_56().new Solution();
    }
    
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[][] merge(int[][] intervals) {
        //按照起始值排序
        Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));
        //存储结果，先按最大长度存着
        int [][] res=new int[intervals.length][2];
        //记录已经安排的res位置
        int setResIndex=-1;
        for (int[] interval : intervals) {
            //如果未存储值或者目前元素的左便捷大于上个已安排元素的右边界
            //则存入新值
            if (setResIndex==-1||interval[0]>res[setResIndex][1]){
                res[++setResIndex]=interval;
            }else {
                //其他情况就是当前元素左边界在上个元素区间内，这里决定要不要扩充上个元素的右边届
                res[setResIndex][1]=Math.max(res[setResIndex][1],interval[1]);
            }
        }
        return Arrays.copyOf(res,setResIndex+1);
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}