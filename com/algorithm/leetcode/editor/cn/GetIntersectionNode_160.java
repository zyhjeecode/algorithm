package com.algorithm.leetcode.editor.cn;


/**
 * @description: 160. 相交链表
 * @author: zyh
 * @create: 2021-11-02 20:47
 **/
public class GetIntersectionNode_160 {
    /**
     *
     给你两个单链表的头节点 headA 和 headB ，请你找出并返回两个单链表相交的起始节点。如果两个链表没有交点，返回 null 。

     * @author zyh
     * @date 2021/11/2
     */
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA==null||headB==null){
            return null;
        }

        ListNode startA=headA,startB=headB;
        while (startA!=startB){
            startA=startA==null?headB:startA.next;
            startB=startB==null?headA:startB.next;
        }
        return startA;
    }
}
