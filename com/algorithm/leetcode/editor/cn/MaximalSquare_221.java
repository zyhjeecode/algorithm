package com.algorithm.leetcode.editor.cn;
//在一个由 '0' 和 '1' 组成的二维矩阵内，找到只包含 '1' 的最大正方形，并返回其面积。
//
//
//
// 示例 1：
//
//
//输入：matrix =
// [
// ["1","0","1","0","0"],
// ["1","0","1","1","1"],
// ["1","1","1","1","1"]
//,["1","0","0","1","0"]]
//输出：4
//
//
// 示例 2：
//
//
//输入：matrix =
// [
// ["0","1"],
// ["1","0"]]
//输出：1
//
//
// 示例 3：
//
//
//输入：matrix = [["0"]]
//输出：0
//
//
//
//
// 提示：
//
//
// m == matrix.length
// n == matrix[i].length
// 1 <= m, n <= 300
// matrix[i][j] 为 '0' 或 '1'
//
// Related Topics 数组 动态规划 矩阵
// 👍 915 👎 0


public class MaximalSquare_221 {
    public static void main(String[] args) {
        Solution solution = new MaximalSquare_221().new Solution();
        char[][] chars = new char[][]{
                {'0', '1', '1', '1', '0'},
                {'1', '1', '1', '1', '0'},
                {'0', '1', '1', '1', '1'},
                {'0', '1', '1', '1', '1'}
        };
        System.out.println(solution.maximalSquare(chars));

    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        //初始值问题:如果matrix[i][j]=0,则dp[i][j]=0
        //边界问题:如果matrix[i][j]=1情况下i=0||j=0,则
        //递归问题 dp[i][j]=min(dp[i-1][j],dp[i][j-1],dp[i-1][j-1])
        public int maximalSquare(char[][] matrix) {
            //max存储最大边长
            int max=0;
            if (matrix==null||matrix.length==0||matrix[0].length==0){
                return max;
            }


            //初始化dp
            int[][] dp=new int[matrix.length][matrix[0].length];
            int rows=matrix.length;
            int columns=matrix[0].length;
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    //这里不用写等于0的代码,参见1,而基础类型默认值就是0,因此代码不用写
                    if (matrix[i][j]=='1'){
                        if (i==0||j==0){
                            dp[i][j]=1;
                        }else {
                            dp[i][j]=Math.min(Math.min(dp[i-1][j],dp[i][j-1]),dp[i-1][j-1])+1;
                        }
                        max=Math.max(dp[i][j],max);
                    }
                }
            }

            return max*max;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
