package com.algorithm.test;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: zyh
 * @create: 2021-03-23 15:09
 **/
public class StrTest {

    @Test
    public void  test() {
        user user1=new user("zyh",18,1);
        user user2=new user("zmy",20,1);
        user user3=new user("zyh",23,-1);
        user user4=new user("zhh",15,-1);
        List<user> list=new LinkedList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        final List<user> filterList = list.stream().filter(item -> item.getName().equals("aaaa")).collect(Collectors.toList());
        System.out.print(filterList);
        filterList.forEach(item->{
            System.out.println(item.toString());
        });
        System.out.println("------------------------------------------------------");
        list.forEach(item->{
            System.out.println(item.toString());
        });
    }

    class user{
        private String name;
        private Integer age;
        private int status;

        @Override
        public String toString() {
            return "user{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    ", status=" + status +
                    '}';
        }

        public user(String name, Integer age, int status) {
            this.name = name;
            this.age = age;
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
