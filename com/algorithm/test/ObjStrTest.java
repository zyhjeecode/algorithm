package com.algorithm.test;

import org.junit.Test;

import java.util.LinkedList;

public class ObjStrTest {
    @Test
    public void  test() throws InterruptedException {
        final Thread aaa = new Thread(() -> {
            System.out.println("aaa");
        });
        aaa.start();
        aaa.setUncaughtExceptionHandler((t, ext) -> {

        });
        aaa.join();
    }

    /**
     * 我们用1,2,4,8,16,32,64分别表示周一到周七
     * 先给出sum表示求某任取几个元素的和,求其是由每周那几天组成的
     *
     * @return 返回从小达到的顺序hashset
     * @author zyh
     * @date 2021/10/29
     */
    public static LinkedList<Integer> getDayPartition(Integer sum) {
        final String s = Integer.toBinaryString(sum);
        LinkedList<Integer> res = new LinkedList<>();
        final char[] chars = s.toCharArray();
        for (int len = chars.length, curr = len - 1; curr >= 0; curr--) {
            if (chars[curr] == '1') {
                res.add(len - curr);
            }
        }
        return res;
    }
}
