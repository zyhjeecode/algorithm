package com.algorithm.test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommonDemo {
    public static void main(String[] args) {
        final long l = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        System.out.println(l);
        System.out.println(LocalDateTime.now());
    }


    class LRUCache<K, V> extends LinkedHashMap<K, V> {
        private final int CACHE_SIZE;

        public LRUCache(int size) {
            //true代表顺序存放,最新的放头部,最老的放尾部
            super((int) Math.ceil(size / 0.75) + 1, 0.75f, true);
            CACHE_SIZE = size;
        }
        /**
         * 定义移除最老的数据的规则
         *
         * @author zyh
         * @date 2021/8/4
         **/
        @Override
        protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
            return size() > CACHE_SIZE;
        }
    }
}



