package com.algorithm.test;

import org.junit.Test;

public class 快速幂测试 {
    //求a的b次方的最后三位
    public long lastThird(long base, long power){
        long res=1;
        while (power>0){
            if ((power&1)==1){
                res=res*base%1000;
            }
            power=power>>1;
            base=(base*base)%1000;
        }
        return res;
    }

    @Test
    public void  test(){
        System.out.println(lastThird(2,1000000000));
    }
}
