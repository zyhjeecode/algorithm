package com.algorithm.test;

import java.util.LinkedList;
import java.util.List;

public class test {

    private final static String first = "first";
    private final static String content = "{{first.DATA}}\n" +
            "取货密码：{{keyword1.DATA}}\n" +
            "手机号码：{{keyword2.DATA}}\n" +
            "收件人姓名：{{keyword3.DATA}}\n" +
            "快递单号：{{keyword4.DATA}}\n" +
            "{{remark.DATA}}";


    public static void main(String[] args) {

        final List<TemplateContent> dealContent = getDealContent(content);

        dealContent.stream().forEach(System.out::println);

    }



    private static List<TemplateContent> getDealContent(String content) {
        List<TemplateContent> res = new LinkedList<>();

        final String[] split = content.split("\n");
        for (String s : split) {
            final String[] kidNames = s.split("：");
            TemplateContent tc = new TemplateContent();
            if (kidNames.length > 1) {
                tc.setName(kidNames[0]);
                tc.setKid(getKid(kidNames[1]));
            } else {
                tc.setKid(getKid(kidNames[0]));
                tc.setName(first.equals(tc.getKid()) ? "消息头部" : "消息尾部");
            }
            res.add(tc);
        }

        return res;
    }

    private static String getKid(String str) {
        return str.substring(2, str.indexOf('.'));
    }
















    public static class TemplateContent {
        private String kid;
        private String name;


        public String getKid() {
            return kid;
        }

        public void setKid(String kid) {
            this.kid = kid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "TemplateContent{" +
                    "kid='" + kid + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

}
