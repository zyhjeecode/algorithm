package com.algorithm.objtest;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @description: stream流测试
 * @author: zyh
 * @create: 2022-01-10 11:52
 **/
public class StreamTest {


    /**
     * map从总实体里提取某些值
     *
     * @author zyh
     * @date 2022/1/10
     */
    @Test
    public void mapTest() {
        final List<People> mockList = People.getMockList();
        final List<String> collect = mockList.stream().map(People::getName).collect(Collectors.toList());
        System.out.println(collect);

        final List<String> collect1 = mockList
                .stream()
                .map(str -> str.getName().split(""))
                .flatMap(item -> Arrays.stream(item))
                .collect(Collectors.toList());
        System.out.println(collect1);


        List<Integer> inner1 = Arrays.asList(1, 2);
        List<Integer> inner2 = Arrays.asList(3, 4);
        List<Integer> inner3 = Collections.singletonList(5);
        List<Integer> inner4 = Collections.singletonList(6);
        List<Integer> inner5 = Arrays.asList(7,8,9);
        List<List<Integer>> outer = Arrays.asList(inner1,inner2,inner3,inner4,inner5);
        List<Integer> result = outer.stream().flatMap(Collection::stream).collect(Collectors.toList());
        System.out.println(result);

        Integer res=null;
        System.out.println(Optional.ofNullable(res).orElse(0));
    }
}
