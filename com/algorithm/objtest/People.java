package com.algorithm.objtest;

import java.util.LinkedList;
import java.util.List;

public class People {
    int age;
    String name;

    public People(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "people{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }


    static List<People> getMockList(){
        List<People> mocks=new LinkedList<>();
        mocks.add(new People(17,"zyh"));
        mocks.add(new People(18,"zyh2"));
        mocks.add(new People(19,"zyh3"));
        return mocks;
    }
}
