package com.algorithm.future.java;

import java.util.ArrayList;
import java.util.Stack;

public class PreOrderPrintTree {
    public class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }
    public ArrayList<Integer> preorderTraversal(TreeNode root) {
        ArrayList<Integer> list=new ArrayList<>();
        if (root==null){
            return list;
        }
        Stack<TreeNode> stack=new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()){
            TreeNode curr = stack.pop();
            list.add(curr.val);
            if (curr.right!=null){
                stack.push(curr.right);
            }
            if (curr.left!=null){
                stack.push(curr.left);
            }
        }
        return list;
    }
}
