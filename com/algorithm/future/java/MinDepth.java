package com.algorithm.future.java;

import org.junit.Test;

import java.util.PriorityQueue;

public class MinDepth {
    public class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }
   //题目描述
   //求给定二叉树的最小深度。
   //最小深度是指树的根结点到最近叶子结点的最短路径上结点的数量。
    public int run(TreeNode root) {
        if (root==null){
            return 0;
        }
        PriorityQueue<Integer> queue =new PriorityQueue<>() ;
        MinDepth(root,0,queue);
        return queue.poll();
    }

    public void  MinDepth(TreeNode root, int count,PriorityQueue<Integer> queue) {
                if (root==null){
                    return;
                }
                count=count+1;
                if (root.left==null&&root.right==null){//如果当前结点是叶节点
                    queue.add(count);
                    return;
                }
                //当前结点不是叶子结点
                MinDepth(root.left,count,queue);
                MinDepth(root.right,count,queue);
        }

    @Test
    public void test() {
        TreeNode treeNode=new TreeNode(0);
        treeNode.left=new TreeNode(2);
        int run = run(treeNode);
        System.out.print(run);

    }

}
