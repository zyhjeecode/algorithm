package com.algorithm.future.java;

public class insertionSortList {
    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
            next = null;
        }
    }

    public ListNode insertionSortList(ListNode head) {
        ListNode newl=new ListNode(Integer.MIN_VALUE); //新链表的头
        ListNode curr=head;//当前要添加旧链表的哪个结点
        ListNode pre=newl;//遍历新链表的指针
        while (curr!=null){//插入链表的位置
            //保存当前节点下一个节点,防止数据丢失
            ListNode next = curr.next;
            pre=newl;//遍历新链表的指针

            //寻找当前节点正确位置的一个节点
            while (pre.next != null && pre.next.val < curr.val) {
                pre = pre.next;
            }
            //当前结点比pre下一个结点大了
            //讲curr插到pre下一个结点之前
            curr.next=pre.next;
            pre.next=curr;
            curr=next;
        }
        return newl.next;
    }
}
