package com.algorithm.future.java;

import java.util.ArrayList;
import java.util.Stack;

public class InorderTraversal {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public ArrayList<Integer> inOrderTraversal(TreeNode root) {
        ArrayList<Integer> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        Stack<TreeNode> stack=new Stack<>();
        TreeNode head=root;
        while (!stack.isEmpty()||head!=null)
        {
            if (head!=null){
                stack.push(head);
                head=head.left;
            }else {
                head = stack.pop();
                list.add(head.val);
                head=head.right;
            }
        }
        return list;
    }
}
