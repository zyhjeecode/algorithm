package com.algorithm.future.java;


import org.junit.Test;

import java.util.Stack;

public class evalRPN {
    //计算逆波兰式（后缀表达式）的值
    //运算符仅包含"+","-","*"和"/"，被操作数可能是整数或其他表达式
    //例如：
    //["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9↵
    // ["4", "13", "5", "/", "+"] -> (4 + (13 / 5)) -> 6
    public int evalRPN(String[] tokens) {
        if (tokens.length==1){
            return Integer.parseInt(tokens[0]);
        }
        String str="+-*/";
        Stack<Integer> stack=new Stack<>();
        for(int i=0;i<=tokens.length-1;i++){
            if (!str.contains(tokens[i])){//非运算符
                stack.push(Integer.parseInt(tokens[i]));
            }else {
                int n1=stack.pop();
                int n2=stack.pop();
                if (tokens[i].equals("+")){
                    stack.push(n1+n2);
                }else if (tokens[i].equals("-")){
                    stack.push(n2-n1);
                }else if (tokens[i].equals("*")){
                    stack.push(n2*n1);
                }else if (tokens[i].equals("/")){
                    stack.push(n2/n1);
                }
            }
        }
        return stack.pop();
    }

    @Test
    public void test() {
        String[] strs=new String[]{"0","3","/"};
        int res = evalRPN(strs);
        System.out.print(res);
    }

}
