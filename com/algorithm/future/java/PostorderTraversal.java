package com.algorithm.future.java;

import java.util.ArrayList;
import java.util.Stack;

public class PostorderTraversal {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public ArrayList<Integer> postorderTraversal(TreeNode root) {
        /*
        求给定的二叉树的后序遍历。
        例如：
        给定的二叉树为{1,#,2,3},
*/
        ArrayList<Integer> list=new ArrayList<>();
        if (root==null){
            return list;
        }
        //非递归
        Stack<TreeNode> stack1 = new Stack<>();
        Stack<TreeNode> stack2 = new Stack<>();
            stack1.push(root);
            while (!stack1.isEmpty()) {
                TreeNode curr = stack1.pop();
                stack2.push(curr);
                if (curr.left!=null){
                    stack1.push(curr.left);
                }
                if (curr.right!=null){
                    stack1.push(curr.right);
                }
            }
        while (!stack2.isEmpty())
        {
           list.add(stack2.pop().val);
        }
        return list;
    }
}
