package com.algorithm.future.test;

public class BubbleSort {
        public static void  bubbleSort(int[] arr){
            if (arr==null||arr.length<1){
                return;
            }
            for(int i=arr.length-1;i>0;i--){
                for(int j=0;j+1<=i;j++){
                     if (arr[j]>arr[j+1]){
                         swap(arr,j,j+1);
                     }
                }
            }

        }
    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public static void main(String[] args){
        int[] a={4,1,2,8,6,7,222,33,4,5,55,22,424};
        bubbleSort(a);
        for (int i = 0; i <a.length; i++) {
            System.out.print(a[i]+" ");
        }
    }
}
