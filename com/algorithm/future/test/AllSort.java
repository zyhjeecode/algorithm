package com.algorithm.future.test;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AllSort {
    public List<String> allSort(char[] arr){
        //全排列问题
        // a b c
        // a b c //a a
        // b a c //a b
        // a b c //a c
        int index=0;//当前要交换的位置
        List<String> list =new ArrayList<>();
        getAll(arr,0,list);
        return list;
    }

    private void getAll(char[] arr, int i, List<String> list) {
        if (i==arr.length-1){
            if (!list.contains(new String(arr))){
                list.add(new String(arr));
            }
            return;
        }else {
        for(int index=i;index<=arr.length-1;index++){
            swap(arr,i,index);
            getAll(arr,i+1,list);
            swap(arr,i,index);
        }
        }
    }
    public void  swap(char[] arr,int a,int b){
        char temp=arr[a];
        arr[a]=arr[b];
        arr[b]=temp;
    }
    
    @Test
    public void test() {
        char[] chars=new char[]{'c','a','b'};
        List<String> list = allSort(chars);
        Collections.sort(list);
        for (String s:list){ 
        System.out.print(s+" ");
        }
    }
    
}
