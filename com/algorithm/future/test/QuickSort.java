package com.algorithm.future.test;

import org.junit.Test;

import java.util.Stack;

public class QuickSort {
    public void  quickSort(int[] arr){
        if (arr==null||arr.length<1){
            return;
        }
        Stack<Integer> stack=new Stack<>();
        stack.push(arr.length-1);
        stack.push(0);
        while (!stack.isEmpty()){
            Integer l = stack.pop();
            Integer r=stack.pop();
            int[] partition = partition(arr, l, r);
            if (partition[1]<r){
                stack.push(r);
                stack.push(partition[1]);
            }
            if (partition[0]>l){
                stack.push(partition[0]);
                stack.push(l);
            }
        }

    }

    private int[] partition(int[] arr, int L, int R) {
        int less=L-1;
        int more=R;
        int curr=L;
        while (curr<more){
             if (arr[curr]<arr[R]){
                 swap(arr,++less,curr++);
             }else if (arr[curr]>arr[R]){
                 swap(arr,--more,curr);
             }else {
                 curr++;
             }
        }
        swap(arr,curr,R);
        return new int[]{less,curr};
    }

    private void swap(int[] arr, int i, int curr) {
        int temp=arr[i];
        arr[i]=arr[curr];
        arr[curr]=temp;
    }
    
    @Test
    public void test() {
        int[] arr=new int[]{1,999,55,12,53,48,54};
        quickSort(arr);
        for (Integer num: arr){
        System.out.print(num+" ");
        }
        
    }
    
}

