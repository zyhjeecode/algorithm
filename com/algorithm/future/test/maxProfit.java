package com.algorithm.future.test;

import org.junit.Test;

public class maxProfit {
    public int maxProfit(int[] prices) {
        int sold = 0, rest = 0, hold = Integer.MIN_VALUE;
        for (int p : prices) {
            int pre_hold = sold;
            sold = hold + p;
            hold = Math.max(hold, rest - p);
            rest = Math.max(rest, pre_hold);
        }
        return Math.max(rest, sold);
    }


}
