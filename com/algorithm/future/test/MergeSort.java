package com.algorithm.future.test;

import org.junit.Test;
public class MergeSort {
    public void mergeSort(int[] arr,int l,int r){
        if (l == r) {
            return;
        }
        int mid=(l+r)/2;
        mergeSort(arr,l,mid);
        mergeSort(arr,mid+1,r);
        merge(arr,l,mid,r);
    }

    private void merge(int[] arr, int l, int mid, int r) {
        //定义两个指针,一个从左半边的起始位置开始,一个从右半边的开始位置开始
        int[] newarr=new int[r-l+1];
        int d=0;
        int lIndex=l;
        int rIndex=mid+1;
        while (lIndex<=mid&&rIndex<=r){
            newarr[d++]=arr[lIndex]<arr[rIndex]?arr[lIndex++]:arr[rIndex++];
        }
        while (lIndex<=mid){
            newarr[d++]=arr[lIndex++];
        }
        while (rIndex<=r){
            newarr[d++]=arr[rIndex++];
        }
        d=0;
        while (d<newarr.length){
            arr[l+d]=newarr[d];
            d++;
        }
    }

    @Test
    public void test() {
        int[] arr=new int[]{2,1,3,5,7,4,5 };
        mergeSort(arr,0,arr.length-1);
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+ " ");
        }
    }

}
