package com.algorithm.future.test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
    static Pattern  mobilePattern = Pattern.compile("^[1]\\d{10}$");

    public static void main(String[] args){
        String mobile="SH-002001";
        mobile=mobile.replace("SH-","");
        mobile=mobile.replace("SZ-","");
        System.out.print(mobile);
    }
}
