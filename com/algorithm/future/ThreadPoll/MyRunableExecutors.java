package com.algorithm.future.ThreadPoll;

public class MyRunableExecutors {
    public static void main(String[] args) throws InterruptedException {
        MyRunable m1= new MyRunable();
        MyRunable m2= new MyRunable();
        Thread thread1 = new Thread(m1);
        Thread thread2 = new Thread(m2);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println(m1.num1+m2.num1);
    }
}
