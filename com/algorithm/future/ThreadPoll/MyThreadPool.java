package com.algorithm.future.ThreadPoll;


import java.util.concurrent.*;

public class MyThreadPool  extends ThreadPoolExecutor{
    public MyThreadPool(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
    }


    /**
     * 线程异常捕获
     * @param r
     * @param t
     */
    @Override
    public void afterExecute(Runnable r, Throwable t) {
        if(t != null) {
            System.out.println("打印异常日志：" + t);
        }
    }

    public static void main(String[] args){
        ExecutorService threadPool=  //自定义线程池
                new ThreadPoolExecutor(2,
                        5,
                        1,
                        TimeUnit.SECONDS,
                        new LinkedBlockingQueue<Runnable>(3),//任务队列容量设为3
                        Executors.defaultThreadFactory()
                        ,new ThreadPoolExecutor.CallerRunsPolicy());
        for(int i=1;i<=10;i++){
            int d=i;
            threadPool.execute(()->{
                System.out.println(Thread.currentThread().getName()+"办理业务"+d);
            });
        }

    }
}
