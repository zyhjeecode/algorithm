package com.algorithm.future.ThreadPoll;


import java.util.concurrent.*;

public class MyCallableFutureExecutor {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<Integer> task1 = new FutureTask<>(new MyCallable(5));
        FutureTask<Integer> task2 = new FutureTask<>(new MyCallable(4));
        ExecutorService exe = Executors.newCachedThreadPool();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        exe.execute(task2);
        exe.execute(task1);
        while (true) {
            if (task1.isDone() && task2.isDone()) {
                countDownLatch.countDown();
                break;
            }
        }
        countDownLatch.await();
        System.out.println(task1.get() + task2.get());
        exe.shutdown();
    }
}
