package com.algorithm.future.ThreadPoll;

import java.util.concurrent.*;

public class MyCallable implements Callable<Integer> {
    int res;

    public MyCallable(int res) {
        this.res = res;
    }
    @Override
    public Integer call() throws Exception {
        return res;
    }
}
