package com.algorithm.future.ThreadPoll;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class test {
    public static void main(String[] args){
        ExecutorService threaPoll= Executors.newFixedThreadPool(5);//一池五线程(模拟一个银行五个窗口)
        // ExecutorService threaPoll=Executors.newCachedThreadPool();//一池N个线程,根据请求的并发程度增加减少处理线程
        //ExecutorService threaPoll=Executors.newSingleThreadExecutor();//一池一个线程
        try{
            for(int i=0;i<10;i++){
                threaPoll.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"办理业务");
                });
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            threaPoll.shutdown();
        }
    }
}
