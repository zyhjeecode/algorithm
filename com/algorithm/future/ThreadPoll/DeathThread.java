package com.algorithm.future.ThreadPoll;

import java.util.concurrent.TimeUnit;

class HoldLockThread implements Runnable{
    private  String ziyuanA;
    private  String ziyuanB;

    public HoldLockThread(String lockA, String lockB) {
        this.ziyuanA = lockA;
        this.ziyuanB = lockB;
    }


    @Override
    public void run() {
        synchronized (ziyuanA){
            System.out.println(Thread.currentThread().getName()+"持有"+ ziyuanA +"尝试获得"+ziyuanB);
            //暂停线程
            try{ TimeUnit.SECONDS.sleep(5);} catch(InterruptedException e){ e.printStackTrace();}

            synchronized (ziyuanB){
                System.out.println(Thread.currentThread().getName()+"持有"+ziyuanB+"尝试获得"+ ziyuanA);
            }

        }
    }
}

public class DeathThread {
    public static void main(String[] args){  
        String lockA="zylockAA";
        String lockB="zylockBB";
        new Thread(new HoldLockThread(lockA, lockB),"线程a").start();
        new Thread(new HoldLockThread(lockB, lockA),"线程b").start();
    }
}
