package com.algorithm.future.ThreadPoll;

import java.util.concurrent.TimeUnit;

public class StringTest {
    public static void main(String[] args){  
        String s1=new String("s11");
        String s2="s11";
        String s3="s11";
        String s4=s1.intern();
        String s5=s2.intern();
        String s6=new String("s11");
        System.out.println(s1==s2);
        System.out.println(s2==s3);
        System.out.println(s1==s4);
        System.out.println(s2==s4);
        System.out.println(s2==s5);
        System.out.println(s1.intern()==s6.intern());

    }
}
