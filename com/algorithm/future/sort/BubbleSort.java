package com.algorithm.future.sort;

public class BubbleSort {

    public static void bubbleSort(int[] arr){
        if (arr==null||arr.length<2){
            return;
        }
        //每次把大值上浮
        for(int end=arr.length-1;end>0;end--){
            for(int i=0;i<end;i++){
                if (arr[i]>arr[i+1]){
                    swap(arr,i,i+1);
                }

            }
        }
    }

    //交换数组array上,ab两个位置的值
    public static void swap(int [] array,int a,int b){ //a,b为数组下标
        int temp=array[a];
        array[a]=array[b];
        array[b]=temp;
    }
    public static void main(String[] args){
        int[] a={4,1,2,8,6,7};
        bubbleSort(a);
        for (int i = 0; i <a.length; i++) {
            System.out.print(a[i]+" ");
        }
    }
}
