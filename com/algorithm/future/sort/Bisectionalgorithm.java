package com.algorithm.future.sort;

/*二分算法*/
public class Bisectionalgorithm {
    public static void main(String[] args){
        int[] array={1,2,4,6};
        int num=3;
        int returnIndex = findAndReturnIndex(array, num);
        System.out.println(returnIndex);
    }

    public static int findAndReturnIndex(int[] array,int num){
        int low=0,high=array.length-1,mid=(low+high)/2;
        while (low<=high){
            if (num==array[mid]){
                return mid;
            }else if(num<array[mid]){
                high=mid-1;
                mid=(low+high)/2;
            }else if(num>array[mid]){
                low=mid+1;
                mid=(low+high)/2;
            }
        }
        return -1;
    }

}

