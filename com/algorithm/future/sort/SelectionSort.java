package com.algorithm.future.sort;

public class SelectionSort {

    public static void selectionSort(int[] arr){
        if (arr==null||arr.length<2){ //如果数组为空或者长度为小于等于1
            return;
        }
        int minCurr;
        for(int i=0;i<arr.length;i++){
            minCurr=i;
            for(int j=i+1;j<arr.length;j++){//每次遍历后得到最小的索引,并交换
                if (arr[j]<arr[minCurr]){
                    minCurr=j;
                }
            }
            swap(arr,i,minCurr);
        }
    }
    //交换数组array上,ab两个位置的值
    public static void swap(int [] array,int a,int b){ //a,b为数组下标
        int temp=array[a];
        array[a]=array[b];
        array[b]=temp;
    }
    public static void main(String[] args){
        int[] a={4,1,2,8,6,7};
        selectionSort(a);
        for (int i = 0; i <a.length; i++) {
            System.out.print(a[i]+" ");
        }
    }
}
