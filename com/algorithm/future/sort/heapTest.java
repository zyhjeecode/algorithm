package com.algorithm.future.sort;

public class heapTest {
    public static void main(String[] args){
        int [] iarr={5,4,1,2,8,6};
        heapSort(iarr);
        printArray(iarr);
    }
    // for com.algorithm.com.algorithm.leetcode.leetcode.test 打印数组
    public static void printArray(int[] arr) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
    //堆排序
    public static void heapSort(int[] arr) {
        if (arr==null||arr.length<2) { //如果数组为空或者长度为小于等于1
            return;
        }
        //堆化数组
        for(int i=0;i<arr.length;i++){
            creatHeap(arr,i);
        }
        int end=arr.length-1;
        while (end!=0){
            swap(arr,0,end--);//将头结点和尾结点交换,把最大值放最后
            heapify(arr,0,end);//保持树化
        }
    }

    //插入逐步实现大根堆
    public static void  creatHeap(int[] arr, int curr){
        while (arr[curr]>arr[parent(curr)]){
            swap(arr,curr,parent(curr));
            curr=parent(curr);
        }
    }

    //维持堆化
    public static void heapify(int[] arr, int index, int end) {
        int lagerestIndex;
        while (left(index)<=end){
         lagerestIndex= right(index)<=end&&arr[right(index)]>arr[left(index)]?right(index):left(index);
         lagerestIndex=arr[lagerestIndex]>arr[index]?lagerestIndex:index;

         if (index==lagerestIndex){
             return;
         }
         swap(arr,index,lagerestIndex);
         index=lagerestIndex;
        }
    }


    //交换
    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
    //求父结点
    public static int parent(int i) {
        return (i-1)/2;
    }
    //求左孩子
    public static int left(int i) {
        return 2*i+1;
    }
    //求右孩子
    public static int right(int i) {
        return 2*i+2;
    }
}
