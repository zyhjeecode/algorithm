package com.algorithm.future.sort;

import org.junit.Test;

import java.util.Stack;

public class NonRecursiveQuickSort {
    public void NonRecursiveQuickSort(int[] arr){
        Stack<Integer> stack=new Stack<>();
        stack.push(arr.length-1);
        stack.push(0);
        while (!stack.isEmpty()){
            int l = stack.pop();//左边界
            int r=stack.pop();//右边界
            int[] partition = partition(arr, l, r);//根据左右边界分区
            if (partition[0]>l){
                stack.push(partition[0]);
                stack.push(l);
            }
            if (partition[1]<r){
                stack.push(r);
                stack.push(partition[1]);
            }
        }

    }
    public int[] partition(int[] arr, int L, int R){
        int less= L -1;
        int more=R;
        int curr= L;
        while (curr<more){
            if (arr[curr]<arr[R]){
                swap(arr,++less,curr++);
            }else if (arr[curr]>arr[R]){
                swap(arr,--more,curr);
            }else {
                curr++;
            }
        }
        swap(arr,curr,R);//让最后一个比较的基准数和more的第一个数交换
        return new int[]{less,curr};//返回小于区最后一个数位置和大于区第一个数位置
    }

    //交换数组array上,ab两个位置的值
    public static void swap(int [] array,int a,int b){ //a,b为数组下标
        int temp=array[a];
        array[a]=array[b];
        array[b]=temp;
    }

}
