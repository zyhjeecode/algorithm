package com.algorithm.future.sort;

public class InsertionSort {

    public static  void  insertionSort(int[] arr){
        if (arr==null||arr.length<2){ //如果数组为空或者长度为小于等于1
            return;
        }
        for(int i=1;i<arr.length;i++){
            for(int j=i-1;j>=0&&arr[j]>arr[j+1];j--){
                    swap(arr,j,j+1);
            }
        }

    }

    //交换数组array上,ab两个位置的值
    public static void swap(int [] array,int a,int b){ //a,b为数组下标
        int temp=array[a];
        array[a]=array[b];
        array[b]=temp;
    }

    public static void main(String[] args){
        int[] a={42,20,-23};
        insertionSort(a);
        for (int i = 0; i <a.length; i++) {
            System.out.print(a[i]+" ");
        }

    }
}

