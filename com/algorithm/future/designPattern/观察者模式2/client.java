package com.algorithm.future.designPattern.观察者模式2;

public class client {
	public static void main(String[] args) {
		biganimal 老虎=new biganimal();
		animal animal1=new animal("小猴子", 老虎);
		animal animal2=new animal("小猪", 老虎);
		animal animal3=new animal("小鹿", 老虎);
		老虎.add(animal1);
		老虎.add(animal2);
		老虎.add(animal3);
		老虎.setactive("老虎来了");
		老虎.change();
	}
}
