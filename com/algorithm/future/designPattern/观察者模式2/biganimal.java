package com.algorithm.future.designPattern.观察者模式2;

import java.util.Vector;

public class biganimal {
	private String active;
	private Vector<animal> animals=new Vector();
	public String getactive() {
		return active;
	}
	public void setactive(String active) {
		this.active = active;
	}
	public void add(animal s) {
		animals.add(s);
	}
	public void change() {
		for(animal a :animals)
		{
			a.respond();
		}
	}
	
}
