package com.algorithm.future.designPattern.类适配器模式;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class XiaoMi {
    public static void main(String[] args){
        /*
        题目描述：
        小米之家有很多米粉喜欢的产品，产品种类很多，价格也不同。比如某签字笔1元，某充电宝79元，某电池1元，某电视1999元等
        假设库存不限，小明去小米之家买东西，要用光N元预算的钱，请问他最少能买几件产品？
        输入
        第1行为产品种类数
        接下来的每行为每种产品的价格
        最后一行为预算金额
        输出
        能买到的最少的产品的件数，无法没有匹配的返回-1
        样例输入
        2
        500
        1
        1000
        * */
        Scanner scanner=new Scanner(System.in);
        int num = scanner.nextInt();
        List<Integer> priceList=new ArrayList<>();
        for(int i=0;i<num;i++){
        int price = scanner.nextInt();
            priceList.add(price);
        }
        int money;
        money=scanner.nextInt();
        sort(priceList);
        int count = getCount(priceList, money);
        System.out.println(count);
    }

    private static int getCount(List<Integer> priceList, int money) {
        int count = 0;
        while (true){
            int countNum=0;
            int monnyCurr=priceList.get(countNum);
            if (money-monnyCurr>=0){
            money=money-monnyCurr ;
            }else {
             count++;
             countNum++;
                break;
            }
        }
        return count;
    }

    private static void sort(List<Integer> priceList) {
        for(int i=0;i<priceList.size();i++){
            for(int j=i+1;j<priceList.size();j++){
                if (priceList.get(i) < priceList.get(j)){
                    int mone=priceList.get(i);
                    priceList.set(i,priceList.get(j));
                    priceList.set(j,mone);
                }
            }
        }
    }
}
