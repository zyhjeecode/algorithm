package com.algorithm.future.designPattern.类适配器模式;

public class Adaptee {
	public void show() {
		System.out.println("被适配者，有特殊功能");
	}

}
