package com.algorithm.future.designPattern.策略模式;

public class context {
	private double oneprice = 0;
	private double totalprice = 0;
	private realprice end = new common();

	public void moneynum(double a) {
		this.oneprice = a;
		totalprice += oneprice;
		if (totalprice <= 1000) {
			System.out.println("您应付的价格为：" + end.realprice(oneprice));
		} else if (totalprice >= 1000 && totalprice <= 2000) {
			end = new vip();
			System.out.println("您应付的价格为：" + end.realprice(oneprice));
		} else if (totalprice >= 2000 && totalprice <= 3000) {
			end = new goldvip();
			System.out.println("您应付的价格为：" + end.realprice(oneprice));
		}
	}
}
