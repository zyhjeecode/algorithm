package com.algorithm.future.designPattern.单例模式;

public class Singleton {
    private   static volatile Singleton singleton;

    private Singleton() {
    }

    public static Singleton getSingleton(){
        if (singleton!=null){
            synchronized (Singleton.class){
                singleton=new Singleton();
            }
        }
        return singleton;
    }

}
