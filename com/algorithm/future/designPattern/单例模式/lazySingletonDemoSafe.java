package com.algorithm.future.designPattern.单例模式;

public class lazySingletonDemoSafe {
        //懒汉式之加锁 synchronized          线程不安全

        /*
        是否 Lazy 初始化：是
        是否多线程安全：是
        实现难度：易
        描述：这种方式具备很好的 lazy loading，能够在多线程中很好的工作，但是，效率很低，99% 情况下不需要同步。
        优点：第一次调用才初始化，避免内存浪费。
        缺点：必须加锁 synchronized 才能保证单例，但加锁会影响效率。
        getInstance() 的性能对应用程序不是很关键（该方法使用不太频繁）。
         */
        private static lazySingletonDemoSafe instance;
        private lazySingletonDemoSafe (){

        }
        //加锁 synchronized 使synchronized在多线程下只执行一次
        public static synchronized lazySingletonDemoSafe getInstance() {
                if (instance == null) {
                        instance = new lazySingletonDemoSafe();
                }
                return instance;
        }

        public static void main(String[] args) {
//                System.out.println(Math.round(512.51));

        }


}
