package com.algorithm.future.designPattern.观察者模式;

public class student {
	private String name;
	private cartoon cartoon;
	public student(String name,cartoon cartoon) {
		this.cartoon=cartoon;
		this.name=name;
	}
	public void respond() {
		System.out.println(cartoon.getActive()+","+name+" 请去观看哦");
	}
}
