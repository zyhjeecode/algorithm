package com.algorithm.future.designPattern.观察者模式;

public class client {
	public static void main(String[] args) {
		cartoon 葫芦娃=new cartoon();
		student student1=new student("小明", 葫芦娃);
		student student2=new student("小红", 葫芦娃);
		葫芦娃.add(student1);
		葫芦娃.add(student2);
		葫芦娃.setActive("葫芦娃更新了");
		葫芦娃.update();
	}
}
