package com.algorithm.future.designPattern.观察者模式;

import java.util.Vector;

public class cartoon {
	private Vector<student> studentlist=new Vector();  //定义学生数组
	private String active;     //定义活动
	
	public String getActive() {       //获取活动
		return active;
	}

	public void setActive(String active) {  //设置活动
		this.active = active;
	}

	public void add(student student) {
		studentlist.add(student);        //增添学生
	}

	public void update(){
		for(student s:studentlist){
			s.respond();
		}
	}
}
