package com.algorithm.future.designPattern.对象适配器模式;

public class adapter implements target {
	private  adaptee adaptee;
	
	public adapter( adaptee adaptee){
		this.adaptee=adaptee;
	}
	public void get() {
		adaptee.show();
		
	}

}
