package com.algorithm.future.designPattern.抽象工厂方法模式;

public class client {
	public static void main(String[] args) {
		factor Boss=new factorA();
		productphone productA=Boss.creatphone();
		productcomputer productcomputer=Boss.creatcoumputer();
		System.out.println("工厂老大生产的手机为");
		productA.phoneproduct();
		System.out.println("工厂老大生产的电脑为");
		productcomputer.productcomputer();
}
}
