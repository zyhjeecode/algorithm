package com.algorithm.future.designPattern.职责链模式;

public class Client {
	public static void main(String[] args) {
		Leverone leverone=new Leverone("主管");
		Levertwo levertwo=new Levertwo("经理");
		Leverthree leverthree=new Leverthree("总经理");
		
		leverone.setsuperior(levertwo);
		levertwo.setsuperior(leverthree);
		leverone.dealrequdst("加薪200");

	}
}
