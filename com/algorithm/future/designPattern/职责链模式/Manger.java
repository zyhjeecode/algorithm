package com.algorithm.future.designPattern.职责链模式;

public abstract class Manger {
		protected Manger superior;  //上级
		protected String postion;   //职位
		public Manger(String position) { 	//构造方法设置
			this.postion=position;         //你的职位
		}
		public void setsuperior(Manger superior) {
			this.superior=superior;
		}                      //设置上级
		public abstract void dealrequdst(String request) ;
}
