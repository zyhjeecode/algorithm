package com.algorithm.future.designPattern.状态模式;

public class Green extends AccountState{
	public Green(AccountState accountState) {
	this.balance=accountState.balance;
	this.account=accountState.account;
	}
	public Green(double d, Account account) {
		this.account=account;
		this.balance=d;
		}
	
	@Override
	public void qukuan(double a) {
		balance-=a;
		check();
	}

	@Override
	public void cunkuan(double a) {
		balance+=a;
		check();
	}

	@Override
	public void check() {
		if (balance<-1000) {
			account.setstate(new Red(this));
			
		}else if (balance<0) {
			account.setstate(new Yellow(this));
			
		}
	}

}
