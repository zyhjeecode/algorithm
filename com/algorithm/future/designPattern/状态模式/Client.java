package com.algorithm.future.designPattern.状态模式;

public class Client {
	public static void main(String[] args) {
		Account account=new Account("皇上", 5.0);
		account.quqian(5200);
		account.quqian(200);
		account.cunqian(5000);
		account.cunqian(200.0);
	}
}
