package com.algorithm.future.designPattern.状态模式;

public class Account {
	/**用户名*/
	private String owner;
	/**账户操作类-含账号信息以及账号操作*/
	private AccountState state;

	public void setstate(AccountState state) {
		this.state = state;
	}

	public Account(String string, Double d) {
		this.owner = string;
		this.state = new Green(d, this);
		System.out.println(owner + ":已开户       账号余额为" + state.balance);
		System.out.println("其账户状态为:" + state.getClass().getName());
		System.out.println("——————————————————————————————————————————————————————————");
	}

	public void quqian(double d) {
		state.qukuan(d);
		System.out.println("取款：" + d + "  账户余额为：" + state.balance);
		System.out.println("其账户状态为:" + state.getClass().getName());
		System.out.println("——————————————————————————————————————————————————————————");
	}
	public void cunqian(double d) {
		state.cunkuan(d);
		System.out.println("存款：" + d + "  账户余额为：" + state.balance);
		System.out.println("其账户状态为:" + state.getClass().getName());
		System.out.println("——————————————————————————————————————————————————————————");
	}
}
