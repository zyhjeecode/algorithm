package com.algorithm.future.designPattern.状态模式;

public class Red extends AccountState{
	public Red(AccountState accountState) {
		this.balance=accountState.balance;
		this.account=accountState.account;
		}
		public Red(double d, Account account) {
			this.account=account;
			this.balance=d;
			}
	@Override
	public void qukuan(double a) {
	System.out.println("您为红色状态，禁止取款");
	}

	@Override
	public void cunkuan(double a) {
		balance+=a;
		check();
	}

	@Override
	public void check() {
		if (balance>=0) {
			account.setstate(new Green(this));
			
		}else if (balance>-1000) {
			account.setstate(new Yellow(this));
			
		}
	}

}
