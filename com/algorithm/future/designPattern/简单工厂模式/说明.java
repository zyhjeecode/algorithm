package com.algorithm.future.designPattern.简单工厂模式;

public class 说明 {

	/*
	模式结构
	简单工厂模式包含如下角色：
	Factory：工厂角色
	Product：抽象产品角色
	ConcreteProduct：具体产品角色


	工厂角色（Creator）
	是简单工厂模式的核心，它负责实现创建所有具体产品类的实例。工厂类可以被外界直接调用，创建所需的产品对象。

	抽象产品角色（Product）
	是所有具体产品角色的父类，它负责描述所有实例所共有的公共接口。


	具体产品角色（Concrete Product）
	继承自抽象产品角色，一般为多个，是简单工厂模式的创建目标。工厂类返回的都是该角色的某一具体产品。
	*/
}
