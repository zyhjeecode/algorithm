package com.algorithm.future.datetools;

import org.junit.Test;

public class MyDateTools {
    //获取当前时间int
    public static int getNowInt() {
        return millis2Int(System.currentTimeMillis());
    }


    //时间戳转int
    public static int millis2Int(Long time) {
        return (int) (time / 1000);
    }

    @Test
    public void test(){
        System.out.println(getNowInt());
    }
}
