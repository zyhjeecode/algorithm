package com.algorithm.future.utils;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * @description: 拆分等比数列的积组成的数字
 * @author: zyh
 * @create: 2021-10-29 19:39
 **/
public class getIntegers {

    /**
     * nums是由1,2,4,8,16,32,64这样的等比数列组成的积,每个数字只出现一次,求出其组成元素
     * @author zyh
     * @date 2021/10/29
     */
    private static Set<Integer> getIntegers(Integer nums) {
        final String s = Integer.toBinaryString(nums);
        Set<Integer> res=new HashSet<>();
        final char[] chars = s.toCharArray();
        for (int curr = 0,len=chars.length; curr <len ; curr++) {
            if (chars[curr]=='1'){
                res.add(len-curr);
            }
        }
        return res;
    }


    /**
     * 获取下一天
     * integer里是一个起始值为1,公比为2的等比数列,一次代表周一 周二...周7
     * nowDayOfWeek 表示当天是周几,获取距离nowDayOfWeek最近的一天距离当前还有几天
     *
     * @author zyh
     * @date 2021/11/1
     */
    public static int getPlusDay(LinkedList<Integer> integers, int nowDayOfWeek) {
        if (nowDayOfWeek<0||nowDayOfWeek>7){

        }
        for (Integer integer : integers) {
            if (integer == nowDayOfWeek) {
                return 0;
            } else if (integer > nowDayOfWeek) {
                return integer - nowDayOfWeek;
            }
        }

        return 7-nowDayOfWeek+integers.getFirst();
    }


    public static void main(String[] args) {
        final LocalTime localTime = LocalDateTime.now().toLocalTime();
        final LocalTime localTime1 = LocalDateTime.now().toLocalTime();
        System.out.println(localTime.isBefore(localTime1)||localTime.equals(localTime1));
    }
}
