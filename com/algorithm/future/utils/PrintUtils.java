package com.algorithm.future.utils;

/**
 * @description: 用于打印的工具类
 * @author: zyh
 * @create: 2021-01-08 22:06
 **/
public class PrintUtils {

    /**
     * 用于接收任意类型数组进行打印
     * @param arr
     * @param <T>
     */
    public static <T> void arrPrint(T[] arr){
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
    }
}
