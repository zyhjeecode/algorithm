package com.algorithm.future.leetcode.middle;

import com.algorithm.future.utils.PrintUtils;
import org.junit.Test;

/**
 * @description: 选转数组
 * @author: zyh
 * @create: 2021-01-08 16:06
 **/
public class Rotate_189_m2 {
    @Test
    public void test(){
        Integer[] nums=new Integer[]{1,2};
        rotate(nums,3);
        PrintUtils.arrPrint(nums);
    }
    public void rotate(Integer[] nums, int k) {
        if (k%nums.length==0) return;
        k%=nums.length;
        int length = nums.length;
        reverse(nums,0,length-1);
        reverse(nums,0,k-1);
        reverse(nums,k, length -1);
    }

    public void reverse(Integer[] arr,int start,int end){
        if (start==end) return;
        int temp;
        while (start<end){
            temp=arr[end];
            arr[end]=arr[start];
            arr[start]=temp;
            start++;
            end--;
        }
    }
}


/**
 * 给定一个数组，将数组中的元素向右移动 k 个位置，其中 k 是非负数。
 *
 * 示例 1:
 *
 * 输入: [1,2,3,4,5,6,7] 和 k = 3
 * 输出: [5,6,7,1,2,3,4]
 * 解释:
 * 向右旋转 1 步: [7,1,2,3,4,5,6]
 * 向右旋转 2 步: [6,7,1,2,3,4,5]
 * 向右旋转 3 步: [5,6,7,1,2,3,4]
 * 示例 2:
 *
 * 输入: [-1,-100,3,99] 和 k = 2
 * 输出: [3,99,-1,-100]
 * 解释:
 * 向右旋转 1 步: [99,-1,-100,3]
 * 向右旋转 2 步: [3,99,-1,-100]
 * 说明:
 *
 * 尽可能想出更多的解决方案，至少有三种不同的方法可以解决这个问题。
 * 要求使用空间复杂度为 O(1) 的 原地 算法。
 *
 * */
