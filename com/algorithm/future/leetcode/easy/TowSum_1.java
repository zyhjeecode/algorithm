package com.algorithm.future.leetcode.easy;

/**
 * @description: 两数之和
 * @author: zyh
 * @create: 2020-12-30 21:11
 **/

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
 *
 * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。
 *
 *  
 *
 * 示例:
 *
 * 给定 nums = [2, 7, 11, 15], target = 9
 *
 * 因为 nums[0] + nums[1] = 2 + 7 = 9
 * 所以返回 [0, 1]
 */
public class TowSum_1 {
    @Test
    public void  test(){
        int[] nums=new int[]{3,2,3,1,5,67};
        twoSum(nums,6);

    }

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> valueIndex=new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int currValue = nums[i];
            if (valueIndex.containsKey(target- currValue)){
                return new int[] {valueIndex.get(target- currValue),i};
            }
            valueIndex.put(currValue,i);
        }
        throw new IllegalArgumentException("没有发现");
    }
}
