package com.algorithm.future.leetcode.easy;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * @description: 830. 较大分组的位置
 * @author: zyh
 * @create: 2021-01-05 21:20
 **/
public class largeGroupPositions_830 {

    @Test
    public void test(){
        // 输入：s = "abcdddeeeeaabbbcd"
        // 输出：[[3,5],[6,9],[12,14]]
        String  s = "bababbabaa";
        List<List<Integer>> lists = largeGroupPositions(s);
        for (int i = 0; i < lists.size(); i++) {
            System.out.println(lists.get(i));
        }

    }

    public List<List<Integer>> largeGroupPositions(String s) {
        List<List<Integer>> res=new LinkedList<>();
        if (s.length()<3){
            return res;
        }
        List<Integer> tempList;
        //记录起点位置
        int left=-1,right=-1;

        char tempChar='1';
        for (int i = 0; i < s.length(); i++) {
            //初始化left
            if (left==-1){
                left=i;
                right=i;
                tempChar=s.charAt(i);
            }else if (tempChar==s.charAt(i)){
                right=i;
                //如果与上一个元素相同
                if (right==s.length()-1&&(right-left)>=2){
                    tempList=new LinkedList<>();
                    tempList.add(left);
                    tempList.add(right);
                    res.add(tempList);
                }
            }else {
                //如果当前与上个元素不同
                //如果已经是已经是较大分组
                if (right!=-1&&left!=-1&&(right-left)>=2){
                    tempList=new LinkedList<>();
                    tempList.add(left);
                    tempList.add(right);
                    res.add(tempList);
                }
                //如果不是较大分组
                left=i;right=i;
                tempChar=s.charAt(i);
            }
        }
        return res;
    }
}


/**在一个由小写字母构成的字符串 s 中，包含由一些连续的相同字符所构成的分组。

 例如，在字符串 s = "abbxxxxzyy" 中，就含有 "a", "bb", "xxxx", "z" 和 "yy" 这样的一些分组。

 分组可以用区间 [start, end] 表示，其中 start 和 end 分别表示该分组的起始和终止位置的下标。上例中的 "xxxx" 分组用区间表示为 [3,6] 。

 我们称所有包含大于或等于三个连续字符的分组为 较大分组 。

 找到每一个 较大分组 的区间，按起始位置下标递增顺序排序后，返回结果。

  

 示例 1：

 输入：s = "abbxxxxzzy"
 输出：[[3,6]]
 解释："xxxx" 是一个起始于 3 且终止于 6 的较大分组。
 示例 2：

 输入：s = "abc"
 输出：[]
 解释："a","b" 和 "c" 均不是符合要求的较大分组。
 示例 3：

 输入：s = "abcdddeeeeaabbbcd"
 输出：[[3,5],[6,9],[12,14]]
 解释：较大分组为 "ddd", "eeee" 和 "bbb"
 示例 4：

 输入：s = "aba"
 输出：[]
  
 提示：

 1 <= s.length <= 1000
 s 仅含小写英文字母

 **/
