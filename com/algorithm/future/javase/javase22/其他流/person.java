package com.algorithm.future.javase.javase22.其他流;

import java.io.Serializable;

public class person implements Serializable{      //继承Serializable接口，将person定义为可序列化的（理解为游戏中可存档取档的）
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;  //person类的版本号
	private String name;
	private int age;
	public person() {
		super();
		
	}
	public person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	@Override
	public String toString() {
		return "person [name=" + name + ", age=" + age + "]";
	}
	

}
