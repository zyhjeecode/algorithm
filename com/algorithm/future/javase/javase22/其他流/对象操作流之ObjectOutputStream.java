package com.algorithm.future.javase.javase22.其他流;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class 对象操作流之ObjectOutputStream {
	//对象的写入操作（序列化）//理解为游戏中的存档操作
//	（序列化）
	public static void main(String[] args) throws IOException, IOException {
		person p1=new person("小明",19);
		person p2=new person("小黄",19);
		person p3=new person("小嘿",19);
		person p4=new person("小蓝",19);
		ArrayList<person> alist=new ArrayList<>(); //将对象都存入alist数组中
		alist.add(p1);
		alist.add(p2);
		alist.add(p3);
		alist.add(p4);
		
		ObjectOutputStream ojps=new ObjectOutputStream(new FileOutputStream("dangan.txt")); //利用ObjectOutputStream将对象数组存入out.txt
		ojps.writeObject(alist);
		ojps.close();
	}

}
