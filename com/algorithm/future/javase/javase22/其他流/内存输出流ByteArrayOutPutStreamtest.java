package com.algorithm.future.javase.javase22.其他流;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class 内存输出流ByteArrayOutPutStreamtest {
	public static void main(String[] args) throws IOException {
		FileInputStream fis=new FileInputStream("aaa.txt");
		ByteArrayOutputStream baos=new ByteArrayOutputStream(); 
		//此类实现了一个输出流，其中的数据被写入一个字节数组。
		//缓冲区会随着数据的不断写入而自动增长。可使用 toByteArray() 和 toString() 检索数据。创建一个带缓存的字节输出流对象（继承OutputS）
		int b;
		while ((b=fis.read())!=-1) {
			baos.write(b);
		}
		
//		方法一读取数据
		byte [] baosbyte=baos.toByteArray();  //将缓存区的数据全部存入byte数组里          //可以通过一定方法用指定码表读取 
		System.out.println(new String(baosbyte));
		System.out.println("——————————————————————————————————————————————————");
//		方法二读取数据
		System.out.println(baos.toString());  //将缓存区的数据转换为字符换 		 	//用平台默认码表读取，不能指定码表
//		方法二ByteArrayOutputStream里可以省略调用tostring，平台默认调用		
		System.out.println(baos);
		fis.close();
		//无需关闭ByteArrayOutputStream，
//		内存读写流，不同于指向硬盘的流，它内部是使用字节数组读内存的，这个字节数组是它的成员变量
//		当这个数组不再使用变成垃圾的时候，Java的垃圾回收机制会将它回收。所以不需要关流。
		
		
	}

}
