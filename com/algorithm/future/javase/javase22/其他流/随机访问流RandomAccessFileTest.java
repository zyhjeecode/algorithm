package com.algorithm.future.javase.javase22.其他流;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class 随机访问流RandomAccessFileTest {

	public static void main(String[] args) throws IOException {
		RandomAccessFile raf =new RandomAccessFile("RandomAccessFileTest.txt", "rw"); //将raf关联RandomAccessFileTest.txt并且设置为可读可写的
//		raf.write('s');
//		System.out.println((char)raf.read());
		raf.seek(5); 		//将指针位置设置为5
		raf.write('w');
		raf.write('w');
		raf.write('w');      //write将覆盖之后的元素
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.write('w');
		raf.close();
		
	}
}
