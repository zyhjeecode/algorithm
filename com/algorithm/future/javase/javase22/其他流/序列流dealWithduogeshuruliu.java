package com.algorithm.future.javase.javase22.其他流;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.SequenceInputStream;
import java.util.Enumeration;
import java.util.Vector;

public class 序列流dealWithduogeshuruliu {
	public static void main(String[] args) throws IOException {
		// 序列流 整合多个输入流
		FileInputStream fis =new FileInputStream("aaa.txt");
		FileInputStream fis2=new FileInputStream("ddd.txt");
		FileInputStream fis3=new FileInputStream("bbbb.txt");
		Vector<FileInputStream> v=new Vector<>();  //返回一个装有FileInputStream的集合对象
		v.add(fis);
		v.add(fis3);
		v.add(fis2);
		Enumeration<FileInputStream> en=v.elements();   //返回对一个装有FileInputStream的集合对象的枚举对象
		SequenceInputStream sqis=new SequenceInputStream(en);//整合整个枚举对象里所有的输入流D
		FileOutputStream fos=new FileOutputStream("ww.txt");
		int b;
		while ((b=sqis.read())!=-1) {
			fos.write(b);
		}
		sqis.close(); //直接关整合的序列流就可以
		fos.close();
		
	}

}
