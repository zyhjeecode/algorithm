package com.algorithm.future.javase.javase22.其他流;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class 数据输入输出流 {
	public static void main(String[] args) throws IOException {
		DataOutputStream dops=new DataOutputStream(new FileOutputStream("bbbb.txt"));
		dops.writeChars("小明一家人来了");
		dops.close();
		DataInputStream dips=new DataInputStream(new FileInputStream("bbbb.txt"));
		int s;
		while ((s=dips.readInt())!=-1) {
			System.out.print((char)s);
		}
		dips.close();
	}

}
