package com.algorithm.future.javase.javase22.其他流;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class 对象操作流之ObjectInputStream {

	public static void main(String[] args) throws IOException, IOException, ClassNotFoundException {
		//对象操作流之读取操作 （反序列化）//理解为游戏的读档操作
//		（反序列化）
		ObjectInputStream ois=new ObjectInputStream(new FileInputStream("dangan.txt"));
		ArrayList<person> aList=(ArrayList<person>) ois.readObject(); //将dangan.txt里存有person信息的arraylist取出
		for (person person : aList) {
			System.out.println(person);
		}
		ois.close();

	}

}
