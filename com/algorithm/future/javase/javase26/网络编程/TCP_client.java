package com.algorithm.future.javase.javase26.网络编程;

import java.awt.print.Printable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
/*
1.客户端

- 创建Socket连接服务端(指定ip地址,端口号)通过ip地址找对应的服务器
- 调用Socket的getInputStream()和getOutputStream()方法获取和服务端相连的IO流
- 输入流可以读取服务端输出流写出的数据
- 输出流可以写出数据到服务端的输入流*/

public class TCP_client {
	public static void main(String[] args) throws IOException, IOException {
		Socket socket=new Socket("127.0.0.1", 12345);
		BufferedReader br=new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintStream ps=new PrintStream(socket.getOutputStream());
		System.out.println(br.readLine());
		ps.println("我在家，干嘛？");
		System.out.println(br.readLine());
		ps.println("好吧，可以");
		socket.close();
		
	}

}
