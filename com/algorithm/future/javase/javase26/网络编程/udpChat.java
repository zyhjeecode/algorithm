package com.algorithm.future.javase.javase26.网络编程;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;

public class udpChat extends Frame{
	private Button send;
	private Button login;
	private Button clear;
	private Button shark;
	private TextArea viewtext;
	private TextArea sendtext;
	private TextField textField;
	private DatagramSocket socket;
	private BufferedWriter bw;
	public static void main(String[] args) throws IOException {
		new udpChat();
		
	}
	public  udpChat() throws IOException {
		setframe();
		southpanel();
		centenpanel();
		Event();
		
	}
	
	public void setframe() throws IOException {
		new receive().start();
		socket = new DatagramSocket();
		bw=new BufferedWriter(new FileWriter("config.txt",true));
		this.setSize(500,500);	
		this.setLocation(700,200);
		this.setTitle("UDP聊天");
		this.setVisible(true);
		
		
	}
	public  void Event() {
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				socket.close();//关闭窗口前关闭传入包
				try {
					bw.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				System.exit(0);

			}
		}); //设置窗体X
		send.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					send();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

		});
		login.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					 loginfile();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

		});
		clear.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				viewtext.setText("");
			}
		});
		shark.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					send(new byte[]{-1},textField.getText());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

		});
		sendtext.addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				try {
					send();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		});
	}
	public void shark() {
		int x=this.getLocation().x;
		int y=this.getLocation().y;
		for(int i = 0; i < 5; i++) {
			try {
				this.setLocation(x + 20, y + 20);
				Thread.sleep(20);
				this.setLocation(x + 20, y - 20);
				Thread.sleep(20);
				this.setLocation(x - 20, y + 20);
				Thread.sleep(20);
				this.setLocation(x - 20, y - 20);
				Thread.sleep(20);
				this.setLocation(x, y);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
	}
	private void loginfile() throws IOException{
		bw.flush();
		FileInputStream fs=new FileInputStream("config.txt");
		ByteArrayOutputStream bo=new ByteArrayOutputStream();//创建内存缓冲区，可以一下全读出去
		int len;
		byte [] bs=new byte[8192];
		while ((len=fs.read(bs))!=-1) {
			bo.write(bs,0,len);
		}
		fs.close();
		String string=bo.toString();
		viewtext.setText(string);
	}
	public void send(byte[] arr,String ip) throws IOException {
		DatagramPacket packet1=new DatagramPacket(arr, arr.length, InetAddress.getByName(ip), 6666);
		/*	DatagramPacket(byte[] buf, int length, InetAddress address, int port) 构造数据报包，用来将长度为 length 的包发送到指定主机上的指定端口号。
	        InetAddress.getByName(ip)返回此 InetAddress 对象的原始 IP 地址	*/
			socket.send(packet1);
	}
	private void send() throws IOException {
		String ip=textField.getText();
		ip=ip.trim().length()==0 ?"255.255.255.255" :ip; //比较ip去掉首位空格之后的长度以此判断是否为空
		String sendnn=sendtext.getText();
		send(sendnn.getBytes(),ip);
		String time=gettime();
		String str = time+":我对"+(ip=="255.255.255.255"?"所有人":ip)+"说\r\n"+sendnn+"\r\n"; //alt +shift+ l 抽取局部变量
		viewtext.append(str);
		bw.write(str);
		sendtext.setText("");
	}
	private String gettime() {
		Date date=new Date();
		SimpleDateFormat sid=new SimpleDateFormat("yyyy年-MM月-dd天-HH-mm-ss");
		return sid.format(date);
	}
	public  void centenpanel() {
		Panel centerpael=new Panel();
		viewtext = new TextArea();
		sendtext = new TextArea(5, 5);
		viewtext.setEditable(false);//设置为不可编辑
		viewtext.setBackground(Color.WHITE);//设计背景为白色
		centerpael.setLayout(new BorderLayout()); //设置为边界布局
		centerpael.add(viewtext,BorderLayout.CENTER);
		centerpael.add(sendtext,BorderLayout.SOUTH);
		this.add(centerpael,BorderLayout.CENTER);
		sendtext.setFont(new Font("s", Font.PLAIN, 20));
		viewtext.setFont(new Font("s", Font.PLAIN, 20 ));
	}
	public void southpanel() {
		Panel southpanel=new Panel();
		textField = new TextField(20);
		textField.setText("127.0.0.1");
		send = new Button("发送");
		login = new Button("记录");
		clear = new Button("清屏");
		shark = new Button("震动");
		southpanel.add(textField);
		southpanel.add(send);
		southpanel.add(login);
		southpanel.add(clear);
		southpanel.add(shark);
		this.add(southpanel,BorderLayout.SOUTH);
		
	}
	public class receive extends Thread{
		public void  run() {
			try {
				DatagramSocket socket2 = new DatagramSocket(6666);
				DatagramPacket packet2=new DatagramPacket(new byte [8192], 8192);
				while (true) {
					socket2.receive(packet2);
					byte [] bs=packet2.getData();
					int length=packet2.getLength();     //获取有效的字节数据
					if(bs[0] == -1 && length == 1) {	//如果发过来的数组第一个存储的值是-1,并且数组长度是1
						shark();						//调用震动方法
						continue;						//终止本次循环,继续下次循环,因为震动后不需要执行下面的代码
					}
					String string2=new String(bs, 0, length);
					String time=gettime();
					String ip=packet2.getAddress().getHostAddress();
					String str = time + " " + ip + " 对我说:\r\n" + string2 + "\r\n";
					viewtext.append(str);
					bw.write(str);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		}

	}
}


