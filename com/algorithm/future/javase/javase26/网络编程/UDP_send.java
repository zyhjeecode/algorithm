package com.algorithm.future.javase.javase26.网络编程;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

public class UDP_send {
	/*
	 * 1.发送Send - 创建DatagramSocket, 随机端口号 - 创建DatagramPacket, 指定数据, 长度, 地址, 端口 -
	 * 使用DatagramSocket发送DatagramPacket - 关闭DatagramSocket
	 */
	public static void main(String[] args) throws IOException {
		DatagramSocket socket = new DatagramSocket();
		Scanner SC = new Scanner(System.in);
		System.out.println("请输出你想发出的话：");
		while (true) {
			String s = SC.nextLine();
			if (s.equals("quit")) {
				break;
			}
			DatagramPacket packet = new DatagramPacket(s.getBytes(), s.getBytes().length,
			InetAddress.getByName("127.0.0.1"), 6666);
			socket.send(packet);
		}
		socket.close();
	}

}
