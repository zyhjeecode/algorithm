package com.algorithm.future.javase.javase26.网络编程;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class twoUDP {
	public static void main(String[] args) {

			new recive().start();
			new send().start();
	}
}
	class send extends Thread{
		public void run() {
			try {
				DatagramSocket socket = new DatagramSocket();
				Scanner SC = new Scanner(System.in);
				System.out.println("请输出你想发出的话：");
				while (true) {
					String s = SC.nextLine();
					if (s.equals("quit")) {
						break;
					}
					DatagramPacket packet = new DatagramPacket(s.getBytes(), s.getBytes().length,InetAddress.getByName("127.0.0.1"), 6666);
					socket.send(packet);
				}
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
class recive extends Thread{
	public void run() {
		try {
			DatagramSocket socket=new DatagramSocket(6666);
			DatagramPacket packet=new DatagramPacket(new byte [1024], 1024);
			while(true){
			socket.receive(packet);
			byte [] b=packet.getData();
			int len=b.length;
			String string=new String(b, 0,len);
			String ipString=packet.getAddress().getHostAddress(); //获取ip地址
			int pint=packet.getPort();//获取端口号
			System.out.println(ipString+pint+"： "+string);
}
		} catch (IOException e) {
			e.printStackTrace();
		}
		}
}
