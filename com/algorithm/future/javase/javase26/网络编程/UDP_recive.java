package com.algorithm.future.javase.javase26.网络编程;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UDP_recive {
	/*
	* 接收Receive
	- 创建DatagramSocket, 指定端口号
	- 创建DatagramPacket, 指定数组, 长度
	- 使用DatagramSocket接收DatagramPacket
	- 关闭DatagramSocket
	- 从DatagramPacket中获取数据
	 */
	public static void main(String[] args) throws IOException {
		DatagramSocket socket=new DatagramSocket(6666);
		DatagramPacket packet=new DatagramPacket(new byte [1024], 1024);
		while(true){
		socket.receive(packet);
		byte [] b=packet.getData();
		int len=b.length;
		String string=new String(b, 0,len);
		String ipString=packet.getAddress().getHostAddress(); //获取ip地址
		int pint=packet.getPort();//获取端口号
		System.out.println(ipString+pint+string);
	}
	}
}
