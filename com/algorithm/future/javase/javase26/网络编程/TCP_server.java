package com.algorithm.future.javase.javase26.网络编程;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
/*服务端

- 创建ServerSocket(需要指定端口号)
- 调用ServerSocket的accept()方法接收一个客户端请求，得到一个Socket
- 调用Socket的getInputStream()和getOutputStream()方法获取和客户端相连的IO流
- 输入流可以读取客户端输出流写出的数据
- 输出流可以写出数据到客户端的输入流
*/
public class TCP_server {
	public static void main(String[] args) throws IOException {
		ServerSocket server=new ServerSocket(12345);
		Socket socket=server.accept();
		BufferedReader br=new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintStream ps=new PrintStream(socket.getOutputStream());
		ps.println("小黄你在家吗？");
		System.out.println(br.readLine());
		ps.println("周六周末出去玩");
		
		socket.close();
	}

}
