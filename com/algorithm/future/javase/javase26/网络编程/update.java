package com.algorithm.future.javase.javase26.网络编程;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class update {
	public static void main(String[] args) {
		serve serve = new serve();
		serve.start();
		client client = new client();
		client.start();
	}

}

class client extends Thread {
	public void run() {
		File file = getfile();
		try {
			send(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static File getfile() {
		System.out.println("请输入一个要复制的文件:");
		Scanner sc = new Scanner(System.in);
		String path = sc.nextLine();
		File f = new File(path);
		while (true) {
			if (!f.exists()) {
				System.out.println("该文件路径不存在，请重新输入");
			} else if (f.isDirectory()) {
				System.out.println("您输入的是文件夹，请重新输入");
			} else {
				return f;
			} 
		}
	}

	public void send(File f) throws IOException, IOException {
		Socket socket = new Socket("127.0.0.1", 55612);
		BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintStream ps = new PrintStream(socket.getOutputStream());
		ps.println(f.getName());
		String sreflect = br.readLine();
		if (sreflect.equals("已存在")) {
			System.out.println("文件已经存在，无需传送");
			socket.close();
			return;
		} else {

			FileInputStream fis = new FileInputStream(f);
			byte[] b = new byte[8192];
			int len;
			while ((len = fis.read(b)) != -1) {
				ps.write(b, 0, len);

			}

		}

	}
}

class serve extends Thread {
	public void run() {
		try {
			creat();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void creat() throws IOException  {
		ServerSocket server=new ServerSocket(55612);
		System.out.println("服务器启动绑定55612号端口");
		while(true){
		Socket socket=server.accept();
		new Thread(){
			
			public void run() {
				try {
					InputStream is=socket.getInputStream();
					BufferedReader br=new BufferedReader(new InputStreamReader(is));
					PrintStream ps=new PrintStream(socket.getOutputStream());
					String string=br.readLine();
					File dir=new File("update");
					dir.mkdir();
					File newfile =new File(dir,string);
					if (newfile.exists()) {
						ps.println("已存在");
					}else {
						ps.println(" 不存在");
						FileOutputStream fos=new FileOutputStream(newfile);
						byte [] barr=new byte[8192];
						int len;
						while ((len=is.read(barr))!=-1) {
							fos.write(barr,0,len);
						}
						fos.close();
						socket.close();
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		
		}.start();

	}	
	}}
