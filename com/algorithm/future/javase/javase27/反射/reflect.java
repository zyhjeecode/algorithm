package com.algorithm.future.javase.javase27.反射;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.algorithm.future.javase.javase14.duixiang.Date;

public class reflect {
	public static void main(String[] args) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		BufferedReader br=new BufferedReader(new FileReader("config.properties"));
		Class clazz=Class.forName(br.readLine()); //通过类的链接得到类
		Date t=(Date) clazz.newInstance(); //实例化创建类
		t.main(args); //调用主类方法
		
}

}
