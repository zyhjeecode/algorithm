package com.algorithm.future.javase.javase27.反射;

public class jadk1_8 {
	public static void main(String[] args) {
		
		p.pr(); //静态调用静态
		
		
		demo demo=new demo();//创建子类实现
		demo.pring();
		demo.ss();
		

	}
}

interface p {
	public default void pring() {
		System.out.println("你好");
	}

	public static void pr() {
		System.out.println("我哈");
	}
}

class demo implements p {
	public void ss() {
		String name = "Zyh";     //jdk1.8之前 内部类引用本地变量必须是最终变量即加final 
								//jdk1.8之后 内部类调用本地变量默认给本地变量加final
		class d {
			public void name() {
				System.out.println("我的名字是" + name);
			
			}
		}
//		name="sd"; 
		d ddemo=new d();
		ddemo.name();
	}
	

}
