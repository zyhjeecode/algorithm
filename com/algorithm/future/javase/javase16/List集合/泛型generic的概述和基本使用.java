package com.algorithm.future.javase.javase16.List集合;

import java.util.ArrayList;
import java.util.Iterator;

import com.algorithm.future.javase.javase15.collections.Student;

public class 泛型generic的概述和基本使用 {
	public static void main(String[] args) {
		ArrayList<Student> a=new ArrayList<>(); //约束了本数组必须存储的元素为Student类型数据
		a.add(new Student("张三", 99));  
		a.add(new Student("张AA", 99));  
		a.add(new Student("张BB", 99));  
		a.add(new Student("张CC", 99));  
//		System.out.println(a);
		Iterator<Student> SI =a.iterator();
		while (SI.hasNext()) {
			Student student = SI.next();		//集合中的每一个元素用student记录
			System.out.print(student.getName()+"  "+student.getAge()+" ");
		}
		
		System.out.println();
		ArrayList<String> b=new ArrayList<>(); //创建集合对象 约束了本数组必须存储的元素为String类型数据
		b.add("a");
		b.add("b");
		b.add("c");
		b.add("d");
		Iterator<String> iterator =b.iterator();
		while (iterator.hasNext()) {
			String s =  iterator.next();      //集合中的每一个元素用s记录
			System.out.print(s+" ");
		}
	}

}
