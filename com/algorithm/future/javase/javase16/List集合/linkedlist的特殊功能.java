package com.algorithm.future.javase.javase16.List集合;

import java.util.LinkedList;

public class linkedlist的特殊功能 {
public static void main(String[] args) {
	LinkedList ls=new LinkedList<>();
	ls.add("a");
	ls.add("b");
	ls.add("c");               //添加元素
	ls.add("d");
	System.out.println(ls);
	ls.addFirst("e");       //在头加
	System.out.println(ls);
	ls.addLast("f");//在尾加
	System.out.println(ls);
//	ls.get(index),ls.getFirst(),ls.getLast();             获取元素
//	ls.remove(index),ls.remove(Object)					删除元素
	System.out.println(ls.getFirst());
	System.out.println(ls.getLast());
}
}
