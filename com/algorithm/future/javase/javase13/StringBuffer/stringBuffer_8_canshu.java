package com.algorithm.future.javase.javase13.StringBuffer;

public class stringBuffer_8_canshu {

	public static void main(String[] args) {
		String string="我是大魔王";
		change(string);
		System.out.println(string );           //为啥这里没有改变，是因为string作为参数传递时候相当于基本数据类型而不是引用数据类型
		changere(string);
		System.out.println(string );  
		StringBuffer sBuffer=new StringBuffer("我是大魔王");
		change(sBuffer);
		System.out.println(sBuffer);
		

	}

	public static void change(StringBuffer sBuffer) {
		sBuffer.append("张耀辉");
	}

	public static void change(String string) {
		string+="张耀辉";
	}
	public static String changere(String string) {
		return string+"张耀辉";
	}
}
