package com.algorithm.future.javase.javase13.StringBuffer;
import java.util.Arrays;

public class stringBuffer_12_Arrays {

	public static void main(String[] args) {
		int [] a={132,122,323,44,255,616,277,868};
		String string=Arrays.toString(a);              //Arrays 自带的将数组转换为字符串
		System.out.println(string);
		Arrays.sort(a); 								//Arrays 自带的排序
		System.out.println(Arrays.toString(a));
		System.out.println(Arrays.binarySearch(a, 6222));//Arrays 自带的二分法查找  如果搜索元素不在数组中则返回   （-插入点-1）  //此处插入点应该是在有序序列中的位置
	}

}
