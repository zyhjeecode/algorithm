package com.algorithm.future.javase.javase13.StringBuffer;

public class stringBuffer_2_add {
	public static void main(String[] args) {
		StringBuffer s1=new StringBuffer();
		StringBuffer s2=s1.append("我是张耀辉");
		StringBuffer s3=s1.append(true);
		StringBuffer s4=s1.append(1231);       //append（）可以添加任意类型数据 	
		
		System.out.println(s1);       //结果一致的原因是stringBuffer是字符串缓冲区，当new的时候是在堆内存创建了一个对象，底层是一个长度为16的字符
		System.out.println(s2);		//当调用添加方法时候，不会再创建对象，只是向最开始创建的s1中添加数据
		System.out.println(s3);		//因此s1 s2 s3 s4返回的都是同一个对象，故结果一致 
		System.out.println(s4);
		
		
		s4.insert(2, "大魔王");                  //在指定位置插入任意数据
		System.out.println(s1);                
	
	}
}
