package com.algorithm.future.javase.javase13.StringBuffer;

public class stringBuffer_6_stringBuffer_change_string {

	public static void main(String[] args) {
		StringBuffer sBuffer=new StringBuffer("我是大魔王");          
		
		
		String s=new String(sBuffer);							  //方法一 通过构造
		System.out.println(s);   
		
		
		String s1=sBuffer.toString();							//方法二  通过tostring方法（常用）				
		System.out.println(s1);     
		
		String s2=sBuffer.substring(0,sBuffer.length());           //方法三 通过substring提取
		System.out.println(s2);  
	}		
}
