package com.algorithm.future.javase.javase13.StringBuffer;

public class StringBuffer_4_replace_and_reverse {

	public static void main(String[] args) {
		StringBuffer s=new StringBuffer("我是大魔王张耀辉");
		s.replace(2, 5, "大魔神");        //替换
		System.out.println(s);
		s.reverse();                  //反转
		System.out.println(s);
	}

}
