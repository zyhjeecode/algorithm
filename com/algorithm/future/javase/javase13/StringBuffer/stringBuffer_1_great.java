package com.algorithm.future.javase.javase13.StringBuffer;

public class stringBuffer_1_great {

	public static void main(String[] args) {
		StringBuffer sb=new StringBuffer();
		System.out.println(sb.length());               //sb的实际大小
		System.out.println(sb.capacity());		 //sb的理论大小（容器值）
		
		StringBuffer sb1=new StringBuffer(10);
		System.out.println(sb1.length());               //sb的实际大小
		System.out.println(sb1.capacity());		 //sb的理论大小（容器值）
		
		StringBuffer sb3=new StringBuffer("1245546");
		System.out.println(sb3.length());               //sb的实际大小
		System.out.println(sb3.capacity());		 //sb的理论大小（容器值）//初始容量+字符串长度

	}

}
