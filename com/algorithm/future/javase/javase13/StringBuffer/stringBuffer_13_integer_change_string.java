package com.algorithm.future.javase.javase13.StringBuffer;

public class stringBuffer_13_integer_change_string {

	public static void main(String[] args) {
		int i =156984;
		String string=String.valueOf(i);            //int 到字符串    方法一	   开发推荐！	
		String string2=Integer.toString(i);			//int 到字符串    方法二		
		String string3=i+"";						//int 到字符串    方法三        开发推荐！
		System.out.println(string);
		System.out.println("————————————————————————————————");

		
		Integer iiInteger=Integer.valueOf(string);					//字符串 到int 方法一
		Integer iiIntege2r=new Integer(string);					//字符串 到int  方法二
		int d=iiInteger;
		System.out.println(d);	
		System.out.println("————————————————————————————————");

		//上面两步等同于
		int dd=Integer.parseInt(string);				//开发推荐！
		System.out.println(dd);
		//基本类型包装类（基本数据类型首字符变大写）有八种其中除了character以外都有praseXXX的方法可以把这其中类型的字符串表现形式转换为基本数据类型
		//eg
		boolean b=Boolean.parseBoolean("ture");
		System.out.println(b);
		byte bb=Byte.parseByte("2");
		System.out.println(bb);

	}

}
