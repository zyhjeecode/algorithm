package com.algorithm.future.javase.javase13.StringBuffer;

public class stringBuffer_13_integer {

	public static void main(String[] args) {
		Integer integer=new Integer(2); //int 到integer     
		//实际上jdk1.5以上有基本数据类型和基本数据类型包装类的自动装箱自动拆箱功能，两者之间可以随意转换
		//其自动装箱和自动拆箱功能底层也是调用其包装类的XXXvalue();方法（拆箱）和new XXXX（）方法转换为对象（装箱）
		//eg
		int i1=2;
		Integer integer1=i1;
		
		System.out.println(integer); 
		Integer s=new Integer("123");  //String 到 intege  //只能纯数字字符串到integer
		System.out.println(s);
		
		
		//面试题
		Integer i3=127;                           
		Integer i4=127;
		System.out.println(i3==i4);               //如果其在byte 的取值范围（-128~127）之内将不再创建对象直接从常量池中取
		Integer i5=128;
		Integer i6=128;
		System.out.println(i5==i6);				 //如果其超出了byte 的取值范围（-128~127）将创建新的对象，其地址值不再相同
	}

}
