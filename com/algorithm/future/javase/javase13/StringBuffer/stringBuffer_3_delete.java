package com.algorithm.future.javase.javase13.StringBuffer;

public class stringBuffer_3_delete {

	public static void main(String[] args) {
		StringBuffer s=new StringBuffer("我是大魔王张耀辉");
		System.out.println(s);
		s.deleteCharAt(2);       //删除指定索引的数据         //左闭右开
		System.out.println(s);
		s.delete(2, 4);  //删除区域的数据         //左闭右开
		System.out.println(s);
		s.delete(0, s.length());  //删除所有数据
		System.out.println("清空后的串为："+s);
	}

}
