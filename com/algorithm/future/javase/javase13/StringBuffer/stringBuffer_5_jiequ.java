package com.algorithm.future.javase.javase13.StringBuffer;

public class stringBuffer_5_jiequ {

	public static void main(String[] args) {
		StringBuffer sd=new StringBuffer("我是大魔王");
		String string=sd.substring(2, 5);         //注意stringBuffer类型数据截取之后返回的数据类型不再是stringbuffer 而是string
		System.out.println(string);
		String s="小明回家了";
		String substring = s.substring(2, s.length());
		System.out.println(substring);

	}

}
