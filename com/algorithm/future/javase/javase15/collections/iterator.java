package com.algorithm.future.javase.javase15.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class iterator {
public static void main(String[] args) {
	Collection c=new ArrayList();
	c.add("小明");
	c.add("小黄");
	c.add("从");
	c.add("小撒旦");
	c.add(new Student("小红",18));
	Iterator i =c.iterator();      //获取c的迭代器
	while (i.hasNext()) {  //如果下一个元素不为空
		System.out.print(i.next()+" "); //返回下一个元素
		
	}
	
}
}
