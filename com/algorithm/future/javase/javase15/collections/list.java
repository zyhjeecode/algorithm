package com.algorithm.future.javase.javase15.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class list {
public static void main(String[] args) {
	List l =new ArrayList<>();
	l.add("a");
	l.add("b");
	l.add("c");
	l.add("d");
	System.out.println(l);
	System.out.println(l.get(2));
	l.remove("a"); //删除指定字符
	System.out.println(l);
	l.remove(0);//删除指定位置字符
	System.out.println(l);
	l.set(0, "改");//修改指定位置元素值
	System.out.println(l);
	
	ListIterator lI =l.listIterator();//list集合专有的迭代器 本身含有add方法
	while (lI.hasNext()) {
		String string=(String)lI.next();
		if (string.equals("改")) {
			lI.add("增");
		}
		
	}
	System.out.print(l);
}
}
