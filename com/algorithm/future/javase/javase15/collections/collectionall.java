package com.algorithm.future.javase.javase15.collections;

import java.util.ArrayList;
import java.util.Collection;
@SuppressWarnings("rawtypes")

public class collectionall {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Collection C1=new ArrayList();
		C1.add("a");
		C1.add("b");
		C1.add("c");
		C1.add("d");
		Collection C2=new ArrayList();
		C2.add("e");
		C2.add("f");
		C1.addAll(C2);
		System.out.println(C1);//将集合C2里的元素添加到 c1里
		C1.removeAll(C2);      //去除C1中与C2相交的元素
		System.out.println(C1);
		System.out.println(C1.contains(C2));//判断调入的集合是否包含传入的集合
		Collection C3=new ArrayList();
		C3.add("c");
		C3.add("f");
		C1.retainAll(C3);     //取C1 C3的交集
		System.out.println(C1);
		
	}
}
