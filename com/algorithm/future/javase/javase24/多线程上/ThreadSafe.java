package com.algorithm.future.javase.javase24.多线程上;

public class ThreadSafe {
	public static void main(String[] args) {
		new pa().start();
		new pa().start();
		new pa().start();
		new pa().start();
	}

}

class pa extends Thread {
	private static int num = 100;

	public void run() {
		while (true) {
			synchronized(pa.class){

			if (num==0) {
				break;
			}
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(Thread.currentThread().getName()+"	这是第 " + num-- + "个票");
		}
		}
	}
}