package com.algorithm.future.javase.javase24.多线程上;

public class ThreadSetAndGetName {
	public static void main(String[] args) {
		new Thread("小黄"){  //第一种方法设置线程名 ,通过构造方法给name赋值
			public void run() {
				System.out.println("线程名---"+this.getName());	
				}
		}.start();
		
		new Thread(){  //第二种方法设置线程名
			public void run() {
				this.setName("小红");
				System.out.println("线程名---"+this.getName());	
				}
		}.start();
		
		Thread t=new Thread(){   //第三种通过 父类引用实现子类对象
			public void run() {
				System.out.println("线程名---"+this.getName());
			}
		};
		t.setName("小蓝");
		t.start();
		
		
	}

}
