package com.algorithm.future.javase.javase24.多线程上;

public class getTheObjectOfThread {
	public static void main(String[] args) {
		
	new Thread(new Runnable() {   
			@Override
			public void run() { 
				//通过实现runable接口创建线程不可以直接使用Thred的方法
				//Thread.currentThread()获取当前线程对象
				Thread.currentThread().setName("我设置的线程名为小明");
					System.out.println(Thread.currentThread().getName());
			}
		}).start();
		
	}

}
