package com.algorithm.future.javase.javase24.多线程上;

public class synchronizedMothed {
	//同步方法
	public static void main(String[] args) {
		p p=new p();
		new Thread() {
			public void run() {
				while (true) {
				p.p1();
				//p.p3();
				}
			}
		}.start();
		
		//12对比知道非静态同步方法锁对象为this   
		//34对比知道静态 方法锁对象为该类字节码
/*		new Thread() {
			public void run() {
				while (true) {
//					p.p2();
					p.p4();
				}
			}
		}.start();*/
		
	}


}
class p {
public  void p1(){ //非静态方法锁对象为this
	System.out.print("我");
	System.out.print("是");
	System.out.print("你");
	System.out.print("爸");
	System.out.println();
}
public void p2() {
	synchronized (this) {
		System.out.print("小"); 
		System.out.print("明");
		System.out.print("在");
		System.out.print("家");
		System.out.println();
	}
}
public static synchronized void p3(){ //静态方法锁对象为该类的字节码对象
	System.out.print("我");
	System.out.print("是");
	System.out.print("你");
	System.out.print("爸");
	System.out.println();
}
public void p4() {
	synchronized (p.class) { // p.class为该类的字节码对象
		System.out.print("小"); 
		System.out.print("明");
		System.out.print("在");
		System.out.print("家");
		System.out.println();
	}
}
}
