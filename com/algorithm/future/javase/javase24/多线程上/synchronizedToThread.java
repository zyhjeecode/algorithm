package com.algorithm.future.javase.javase24.多线程上;
//同步代码块
public class synchronizedToThread {
	public static void main(String[] args) {
		prientt p = new prientt();
		new Thread() {
			public void run() {
				while (true) {
					p.p1();
				}
			}
		}.start();
		new Thread() {
			public void run() {
				while (true) {
					p.p2();
				}
			}

		}.start();
	}
}
//同步代码块，被synchronized (d){} 锁括住的要整个被执行完才可以被解锁
//锁对象可以是任意对象,但是被锁的代码需要保证是同一把锁,不能用匿名对象（因为每个匿名对象都是独一无二的）
//为两端代码块加同一把锁可以报这个两个代码块是同步的
//两段代码是同步的, 那么同一时间只能执行一段, 在一段代码没执行结束之前, 不会执行另外一段代码.
class prientt {
	demo d = new demo();
	public void p1() {
		synchronized (d) {  //synchronized (d)里的对象是任意的
			System.out.print("我");
			System.out.print("是");
			System.out.print("你");
			System.out.print("爸");
			System.out.println();

		}
	}

	public void p2() {
		synchronized (d) {
			System.out.print("小"); 
			System.out.print("明");
			System.out.print("在");
			System.out.print("家");
			System.out.println();
		}
	}
}

class demo {

}
