package com.algorithm.future.javase.javase24.多线程上;

public class ThreadJoin {
	public static void main(String[] args) {

		Thread t1 = new Thread("线程一") {
			public void run() {
				for (int i = 0; i < 10; i++)
					System.out.println(this.getName() + "...www");
			}
		};

		Thread t2 = new Thread("线程二") {
			public void run() {
				for (int i = 0; i < 10; i++) {
					if (i == 2) {
						try {
							t1.join();// 插入全部，等t1执行完毕才会执行本线程
//							t1.join(100);// 插入100ms
						} catch (InterruptedException e) {
							e.printStackTrace();
						} 
					}
					System.out.println(this.getName() + "...aaaa");
				}
			}
		};
		t1.start();
		t2.start();

	}

}
