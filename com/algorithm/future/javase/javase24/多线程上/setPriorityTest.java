package com.algorithm.future.javase.javase24.多线程上;

public class setPriorityTest {
	public static void main(String[] args) {
		Thread t1=new Thread("线程一"){
			public void run() {
				System.out.println(this.getName()+"111111111111111");
			}
		};
		
		Thread t2=new Thread("线程二"){
			public void run() {
				System.out.println(this.getName()+"222222222222222");
			}
		};
		
		t1.setPriority(4); //优先级在1~10之间
		t2.setPriority(7);
		t1.start();
		t2.start();
		
		
	}

}
