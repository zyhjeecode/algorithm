package com.algorithm.future.javase.javase24.多线程上;

public class DaemonThread {
	public static void main(String[] args) {
		Thread t1=new Thread(){
			public void run() {
				for(int i=0;i<2;i++)
					System.out.println(this.getName()+"aaaa");
			}
		};
		
		Thread t2=new Thread(){
			public void run() {
				for(int i=0;i<=50;i++)
					System.out.println(this.getName()+"sssssss");
			}
		};
		t2.setDaemon(true);
		t1.start();
		t2.start();
		
	}

}
