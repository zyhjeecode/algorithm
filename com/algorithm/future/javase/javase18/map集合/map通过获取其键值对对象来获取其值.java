package com.algorithm.future.javase.javase18.map集合;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class map通过获取其键值对对象来获取其值 {

	public static void main(String[] args) {
		Map<String, Integer> map=new HashMap<String, Integer>();
		add(map);
		System.out.println("————————————————————通过迭代器遍历map集合————————————————————————");
		Set<Map.Entry<String, Integer>> mapset=map.entrySet(); //获取每一个map对象
		Iterator<Map.Entry<String, Integer>>   imap=mapset.iterator();   //获取存有每个对象的迭代器
		while (imap.hasNext()) {
			Map.Entry<String, Integer> en=imap.next(); //获取每一个对象
			System.out.print(en.getKey()+"   ");        //Map.Entry<K, V>中存有两个方法可以得到其值还有一个setvalue()可以用指定值与此项替换
			System.out.println(en.getValue());
			
		}
		System.out.println("————————————————————通过增强for结构遍历map集合——————— ————————————");
		for(Map.Entry<String, Integer>   en : map.entrySet()){
			System.out.print(en.getKey()+"   ");        //Map.Entry<K, V>中存有两个方法可以得到其值还有一个setvalue()可以用指定值与此项替换
			System.out.println(en.getValue());
		}
	}
	private static void add(Map<String, Integer> map) {
		map.put("小明请",23);
		map.put("小明撒",21);
		map.put("小明人",22);
		map.put("小明给",25);
		map.put("小明和",24);
	}
}
