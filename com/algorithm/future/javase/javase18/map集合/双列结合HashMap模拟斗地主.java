package com.algorithm.future.javase.javase18.map集合;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class 双列结合HashMap模拟斗地主 {
	public static void main(String[] args) {
		// 制作扑克牌
		String[] a = { "3", "4", "5", "6", "7", "8", "9", "10", "J", "P", "Q", "K", "A", "2" };
		String[] b = { "黑桃", "红桃", "方块", "梅花", };
		int index = 0;
		HashMap<Integer, String> poker = new HashMap<>();
		ArrayList<Integer> pokernum = new ArrayList<>();
		for (String s1 : a) {
			for (String s2 : b) {
				poker.put(index, s2.concat(s1));
				pokernum.add(index);
				index++;

			}
		}
		poker.put(index, "小王");
		pokernum.add(index);
		index++;
		poker.put(index, "大王");
		pokernum.add(index);
		// 选定打牌人
		ArrayList<Integer> xiaoming = new ArrayList<>();
		ArrayList<Integer> xiaohuang = new ArrayList<>();
		ArrayList<Integer> xiaohong = new ArrayList<>();
		ArrayList<Integer> xiaohei = new ArrayList<>();
		ArrayList<Integer> dipai = new ArrayList<>();
		// 洗牌
		Collections.shuffle(pokernum);
		// 发牌
		for (int i=0;i<pokernum.size();i++) {
			if (i > index - 4) {
				dipai.add(pokernum.get(i));
			}
			if (i % 5 == 0) {
				xiaoming.add(pokernum.get(i));

			}
			if (i % 5 == 1) {
				xiaohuang.add(pokernum.get(i));
			}
			if (i % 5 == 2) {
				xiaohong.add(pokernum.get(i));
			}
			if (i % 5 == 3) {
				xiaohei.add(pokernum.get(i));
			}

		}
		//看牌
		lookpoker(poker, xiaoming, "小明");
		lookpoker(poker, xiaohuang, "小黄");
		lookpoker(poker, xiaohong, "小红");
		lookpoker(poker, xiaohei, "小黑");
		lookpoker(poker, dipai, "底牌");
		
		
		
	}
	public static  void lookpoker(HashMap<Integer, String> HM,ArrayList<Integer> al,String name){
		System.out.print(name+"的 牌是：  ");
		for (Integer integer : al) {
			System.out.print(HM.get(integer)+" ");
		}
		System.out.println();
		
	}
	

}
