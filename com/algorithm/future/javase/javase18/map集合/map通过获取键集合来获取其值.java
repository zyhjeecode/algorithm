package com.algorithm.future.javase.javase18.map集合;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class map通过获取键集合来获取其值 {

	public static void main(String[] args) {
		Map<String, Integer> map=new HashMap<String, Integer>();
		add(map);
		Set<String> kSet =map.keySet(); //通过map.keySet()获取map集合所有的键 并存在Set集合中
		System.out.println(kSet);
		System.out.println("————————————————————通过迭代器遍历map集合————————————————————————");
		Iterator<String> iterator=kSet.iterator();   //获取迭代器，其存的泛型限制为String
		while (iterator.hasNext()) {
			String key = (String) iterator.next();   //获取每个键
			int value =map.get(key);
			System.out.println("key="+key+"     value="+value);
			
		}
		
		System.out.println("————————————————————通过增强for结构遍历map集合——————— ————————————");
		for (String key : map.keySet()) {
			System.out.println("key="+key+"     value="+map.get(key));
			
		}
		
	}

	private static void add(Map<String, Integer> map) {
		map.put("小明请",23);
		map.put("小明撒",21);
		map.put("小明人",22);
		map.put("小明给",25);
		map.put("小明和",24);
	}

}
