package com.algorithm.future.javase.javase18.map集合;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class collection中的常见方法 {
	public static void main(String[] args) {
//		方法摘要 
//		 boolean add(E o) 
//		          确保此 collections 包含指定的元素（可选操作）。
//		 boolean addAll(Collection<? extends E> c) 
//		          将指定 collections 中的所有元素都添加到此 collections 中（可选操作）。
//		 void clear() 
//		          移除此 collections 中的所有元素（可选操作）。
//		 boolean contains(Object o) 
//		          如果此 collections 包含指定的元素，则返回 true。
//		 boolean containsAll(Collection<?> c) 
//		          如果此 collections 包含指定 collections 中的所有元素，则返回 true。
//		 boolean equals(Object o) 
//		          比较此 collections 与指定对象是否相等。
//		 int hashCode() 
//		          返回此 collections 的哈希码值。
//		 boolean isEmpty() 
//		          如果此 collections 不包含元素，则返回 true。
//		 Iterator<E> iterator() 
//		          返回在此 collections 的元素上进行迭代的迭代器。
//		 boolean remove(Object o) 
//		          从此 collections 中移除指定元素的单个实例，如果存在的话（可选操作）。
//		 boolean removeAll(Collection<?> c) 
//		          移除此 collections 中那些也包含在指定 collections 中的所有元素（可选操作）。
//		 boolean retainAll(Collection<?> c) 
//		          仅保留此 collections 中那些也包含在指定 collections 的元素（可选操作）。
//		 int size() 
//		          返回此 collections 中的元素数。
//		 Object[] toArray() 
//		          返回包含此 collections 中所有元素的数组。
//		<T> T[] 
//		 toArray(T[] a) 
//		          返回包含此 collections 中所有元素的数组；返回数组的运行时类型与指定数组的运行时类型相同
		
		 ArrayList<String> al =new ArrayList<>();
		 al.add("a");
		 al.add("x");
		 al.add("f");
		 al.add("w");
		 System.out.println(al);
		 Collections.sort(al); //排序
		 System.out.println(al);
		 System.out.println(Collections.max(al)); //获取最大值（根据默认排序结果）
		 System.out.println(Collections.min(al)); //获取最小值（根据默认排序结果）
		 Collections.reverse(al); //反转al
		 System.out.println(al);
		 Collections.shuffle(al); //随机改变集合中元素的位置，可以用来洗牌
		 System.out.println(al);
}
}