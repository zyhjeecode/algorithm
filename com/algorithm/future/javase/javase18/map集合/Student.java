package com.algorithm.future.javase.javase18.map集合;

public class Student implements Comparable<Student>{      //Comparable 是存入的数据比较方法确定顺序
	private String name;
	private int age;
	public Student() {
		super();
		
	}
	public Student(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	@Override
	public String toString() {
		return "Studet [name=" + name + ", age=" + age + "]";
	}
	@Override
	public int compareTo(Student o) {
		int num=this.age-o.age; //以年龄为主要条件
		return num==0?this.name.compareTo(o.name):num;
	}
	
	

}
