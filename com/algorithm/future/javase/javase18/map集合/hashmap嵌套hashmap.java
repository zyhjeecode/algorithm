package com.algorithm.future.javase.javase18.map集合;

import java.util.HashMap;

public class hashmap嵌套hashmap {
	public static void main(String[] args) {
//		定义hm1班
		HashMap<Student, String> hm1=new HashMap<>();
		hm1.put(new Student("小名", 11), "第一名");
		hm1.put(new Student("小三大", 15), "第二名");
		hm1.put(new Student("小发", 12), "第三名");
		
//		定义hm2班
		HashMap<Student, String> hm2=new HashMap<>();
		hm2.put(new Student("小名", 11), "第一名");
		hm2.put(new Student("小三大", 15), "第二名");
		hm2.put(new Student("小发", 12), "第三名");
		
		HashMap<HashMap<Student, String>, String> hm =new HashMap<>(); //定义hm年级，键为班级，值为班级号
		hm.put(hm1, "hm一班");
		hm.put(hm2, "hm二班");
		
		//遍历双列集合
		for (HashMap<Student, String> k : hm.keySet()) {
			System.out.println(hm.get(k));     //得到班级名
			for (Student s : k.keySet()) {     //得到该班级的学生信息
				System.out.println(s+"名次为 ："+k.get(s));
			}
			
		}
		
	}
	
}
