
package com.algorithm.future.javase.javase18.map集合;

import java.util.HashMap;
import java.util.Map;

public class Map功能测试 {
	public static void main(String[] args) {
		Map<String, Integer> map=new HashMap<>();
		map.put("小明", 22);
		map.put("小发生", 23);  //添加
		map.put("小生", 23);
		System.out.println(map);
		map.remove("小明");  //根据键移除其值
		boolean s1=map.containsKey("小明");//判断是否包含此键
		boolean s2=map.containsValue(23);//判断是否包含此值
		System.out.println(" s1 ="+s1+" s2 ="+s2);
		System.out.println(map.size()); //得到其长度
		System.out.println(map.isEmpty()); //判断是否为空
		System.out.println(map.values());//得到集合中的所有值   collections c=map.values（）
		Integer intege=map.get("小明");
		System.out.println(intege);
	}
}
