package com.algorithm.future.javase.javase18.map集合;

import java.util.HashMap;

public class 统计字符串每个字符出现的次数 {
	public static void main(String[] args) {
		String string="sadqwhqwqw         ：“{}{：：{fjqwfbqjaasjkda";
		char [] cs=string.toCharArray();
		HashMap<Character, Integer> hc=new HashMap<>();
		for (char c : cs) {
			hc.put(c, hc.containsKey(c)?hc.get(c)+1:1);
		}
		System.out.println(hc);
		
		System.out.println("____________________________________________");
		for (char key : hc.keySet()) {        //hc.keySet()获取所有的键集合
			System.out.print (key+"="+hc.get(key)      +"  ");
		}
	}

}
