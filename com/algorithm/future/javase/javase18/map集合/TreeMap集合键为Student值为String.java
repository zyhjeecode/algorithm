package com.algorithm.future.javase.javase18.map集合;

import java.util.TreeMap;

public class TreeMap集合键为Student值为String {

	public static void main(String[] args) {
		TreeMap<Student, String> tm =new TreeMap<>();
		tm.put(new Student("小明", 12), "上海");
		tm.put(new Student("小明", 16), "北海");
		tm.put(new Student("小明", 14), "驻京");
		tm.put(new Student("小明", 17), "驻京");
		tm.put(new Student("小明", 14), "驻京");
		tm.put(new Student("小明", 12), "驻京");
		tm.put(new Student("小明", 11), "驻京");
		tm.put(new Student("小明", 18), "安徽");
		System.out.println(tm);
	}

}
