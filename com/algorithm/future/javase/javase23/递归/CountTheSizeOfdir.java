package com.algorithm.future.javase.javase23.递归;

import java.io.File;
import java.util.Scanner;

public class CountTheSizeOfdir {
	public static void main(String[] args) {
		File file=getdir();
		long size=getsize(file);
		System.out.println(size);
	}
	public static File getdir() {
		System.out.println("请输入一个文件夹路径：");
		String s;
		Scanner sc=new Scanner(System.in);
		s=sc.nextLine();
		File file=new File(s);
		while(true){
		if (!file.exists()) {
			System.out.println("文件夹不存在：请重新输入");
		}else if (file.isFile()) {
			System.out.println("您输入的是文件路径，请重新输入");
		}else {
			return file;
		}
		}
	}
	
	public static long getsize(File f){
		File [] flist=f.listFiles();
		long size=0;
		for (File file : flist) {
			if (file.isFile()) {
				size=size+file.length();
			}else {
				size=size+getsize(file);
			}
		
	}
		return size;
	}
}
