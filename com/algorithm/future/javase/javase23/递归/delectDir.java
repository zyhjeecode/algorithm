package com.algorithm.future.javase.javase23.递归;

import java.io.File;

public class delectDir {
	public static void main(String[] args) {
		File file=CountTheSizeOfdir.getdir();
		delectdir(file);
	}

	public static void delectdir(File file) {
		File[] flist = file.listFiles();
		for (File file2 : flist) {
			if (file2.isFile()) {
				file2.delete();
			} else {
				delectdir(file2);
			}
		}
		file.delete(); //只有空文件夹才可以删除
	}

}
