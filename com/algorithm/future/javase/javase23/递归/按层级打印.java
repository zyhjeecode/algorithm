package com.algorithm.future.javase.javase23.递归;

import java.io.File;
import java.util.logging.Level;

public class 按层级打印 {
	public static void main(String[] args) {
		File f=CountTheSizeOfdir.getdir();//得到文件夹
		printlev(f,0);                   //测试目录E:\maven
	}
	public static void printlev(File f,int r) {
		File [] flist=f.listFiles();
		for (File file : flist) {
			for(int i=0;i<=r;i++){
				System.out.print("\t");             
			}
			System.out.println(file.getName());
			if (file.isDirectory()) {
				printlev(file,r+1);
			}
		}
	}
	
}
