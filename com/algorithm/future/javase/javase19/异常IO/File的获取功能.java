package com.algorithm.future.javase.javase19.异常IO;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class File的获取功能 {
	public static void main(String[] args) {
		demo1();
		File f3=new File("aaa");
		demo2(f3);
		System.out.println();
		demo3(f3);
	}

	private static void demo3(File f3) {
		File [] f3Files=f3.listFiles();  //获取文件夹下所有文件引用的数组
		for (File file : f3Files) {
			System.out.println(file.getName());
		}
	}

	private static void demo2(File f3) {
		String[] f3name=f3.list();//获取f3下所有文件的名称数组
		for (String string : f3name) {
			System.out.print(string+"  ");
		}
	}

	private static void demo1() {
		File file=new File("aaa.txt");
		File file2=new File("C:\\Users\\Zyh\\workspace\\heima_study\\aaa.txt");
		System.out.println(file.getAbsolutePath());//获取绝对路径
		System.out.println(file2.getAbsolutePath());
		System.out.println(file.getPath()); //获取构造方法中传入的路径
		System.out.println(file.getName()); //获取文件或者文件夹的名字
		System.out.println(file2.getName()); //不论是绝对路径还是相对路径都是获取文件或者文件夹的名字
		System.out.println(file.length()); //获取文件中含有的字节数
		System.out.println(file.lastModified());//获取文件的最后修改时间 	
		SimpleDateFormat sim=new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");  //将毫秒值转换为我们常用格式
		System.out.println(sim.format(file.lastModified()));
	}

}
