package com.algorithm.future.javase.javase19.异常IO;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Scanner;

public class test异常嵌套 {
	public static void main(String[] args) {
		System.out.println("请输入一个整数：");
		Scanner sc=new Scanner(System.in);
		while (true) {
			String s =sc.nextLine();
			try {
				int a=Integer.parseInt(s);
				System.out.println(Integer.toBinaryString(a));
				break;
			} catch (Exception e) {
				
				try {
					new BigInteger(s);                //如果可以被大数转换接收则代表数字只是过大没有其他异常，程序可以进入下一步
					System.out.println("输入数字过大，请重新输入"); //如果有异常则new BigInteger会出错，下一步不会执行，直接去catch里继续执行
				} catch (Exception e1) {
					try {
						new BigDecimal(s); //如果可以被小鼠转换接收则代表数字只是含有小鼠，程序可以进入下一步的小数错误提示
						System.out.println("输入数字为带小数数字，请重新输入");  
					} catch (Exception e2) {
						System.out.println("输入数字为非法字符，请重新输入");//其他的则是非法字符
					}
				}
			}
		}
		
	}
}
