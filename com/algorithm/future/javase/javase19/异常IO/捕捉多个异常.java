package com.algorithm.future.javase.javase19.异常IO;

public class 捕捉多个异常 {
	public static void main(String[] args) {
		Character[] aCharacters = { 'C', 'A', 'W' };
		try { 
			System.out.println(aCharacters[10]);

		}catch (ArithmeticException e) {      // 多个catch这样可以并存
			System.out.println("除数不能为0");
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("索引越界异常");
		}catch (Exception e) {
			System.out.println("出错了");
		}
	}

}
