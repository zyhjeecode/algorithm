package com.algorithm.future.javase.javase19.异常IO;

import java.io.File;

public class 输出指定目录下指定后缀的文件名 {

	public static void main(String[] args) {
		File f =new File("E:\\2018年传智播客JavaEE49期全套视频下载\\01-JavaSE知识(学习27天)\\day20(IO(字节流))\\day20(IO(字节流))\\day20(IO(字节流))\\day20_video");
		method1(f);
		System.out.println("___________________________________________________________");
		method2(f);
	}

	private static void method2(File f) { //方法二
		File [] ffFiles=f.listFiles();
		for (File file : ffFiles) {
			if (file.isFile()&&file.getName().endsWith(".avi")) {
				System.out.println(file.getName());
			}
		}
	}

	private static void method1(File f) {//方法一
		String [] fname=f.list();
		for (String string : fname) {
			if (string.endsWith(".avi")) {
				System.out.println(string+" ");
			}
		}
	}

}
