package com.algorithm.future.javase.javase19.异常IO;

public class Throwable的几种常见方法 {
	public static void main(String[] args) {
		int a=10,n=0;
		try {
			System.out.println(a/n);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());//获取异常信息
			System.out.println();
			
			System.out.println(e.toString());//打印异常类名和返回异常信息
			System.out.println();
			
			e.printStackTrace();//获取异常类名异常信息 还有异常出现的位置
		}
	}

}
