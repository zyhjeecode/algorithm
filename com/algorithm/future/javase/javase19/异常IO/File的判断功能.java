package com.algorithm.future.javase.javase19.异常IO;

import java.io.File;
import java.io.IOException;

public class File的判断功能 {
	public static void main(String[] args) throws IOException {
		File file=new File("aaa.txt");
//		System.out.println(file.isDirectory());//判断是否是目录
//		System.out.println(file.isFile());//判断是否是文件
//		System.out.println(file.exists());//判断是否存在
		file.setReadable(false);
		System.out.println(file.canRead()); //windows系统下认为所有文件都是可读的
		file.setWritable(false);//设置文件是不可读的
		System.out.println(file.canWrite()); //windows系统可以设置文件不可写
		System.out.println(file.isHidden());//判断文件是否是隐藏的
		 
	
	}
}
