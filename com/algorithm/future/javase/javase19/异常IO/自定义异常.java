package com.algorithm.future.javase.javase19.异常IO;

public class 自定义异常 {
	public static void main(String[] args) throws Exception {
//		age a=new age();
//		a.setAge(111);
//		System.out.println(a);

	}

	class age {
		private int age;

		public age() {
			super();

		}

		public age(int age) {
			super();
			this.age = age;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) throws ageoverboundException {
			if (age < 50 && age > 0) {

				this.age = age;
			} else {
				throw new ageoverboundException("年龄非法");
			}
		}

	}



}
