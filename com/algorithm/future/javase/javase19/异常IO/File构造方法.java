package com.algorithm.future.javase.javase19.异常IO;

import java.io.File;

public class File构造方法 {
	public static void main(String[] args) {

/*构造方法摘要

File(File parent, String child) 
           根据 parent 抽象路径名和 child 路径名字符串创建一个新 File 实例。 
File(String pathname) 
           通过将给定路径名字符串转换成抽象路径名来创建一个新 File 实例。 
File(String parent, String child) 
           根据 parent 路径名字符串和 child 路径名字符串创建一个新 File 实例。 
*/
		
		File_String_pathname();
		System.out.println("__________________________________________________");
		File_String_parent_String_child();
		System.out.println("__________________________________________________");
		File_File_parent_String_child();
		

	}

	private static void File_File_parent_String_child() {
		File parent=new File("C:\\Users\\Zyh\\Desktop\\笔记\\黑马笔记\\day19异常和IO");
		String child="File\\File类的概述和构造方法.png";
		File newfile=new File(parent, child);
		System.out.println(newfile.exists());
	}

	private static void File_String_parent_String_child() {
		String s1="C:\\Users\\Zyh\\Desktop\\笔记\\黑马笔记\\day19异常和IO";
		String s2="File\\File类的概述和构造方法.png";
		File file=new File(s1, s2);
		System.out.println(file.exists());
	}

	private static void File_String_pathname() {
		File f=new File("C:\\Users\\Zyh\\Desktop\\笔记\\黑马笔记\\day19异常和IO\\File\\File类的概述和构造方法.png");
		System.out.println(f.exists());  //判断路径是否存在的方法
		
		File f1=new File("aaa.txt"); //相对路径，aaa.txt在heima_study文件夹下
		System.out.println(f1.exists());

		File f2=new File("aaa.asdadtxt");
		System.out.println(f2.exists());
	}

}
