package com.algorithm.future.javase.javase19.异常IO;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Scanner;

public class 文件名过滤器的使用 {

	public static void main(String[] args) {
		File file=new File("E:\\2018年传智播客JavaEE49期全套视频下载\\01-JavaSE知识(学习27天)\\day20(IO(字节流))\\day20(IO(字节流))\\day20(IO(字节流))\\day20_video");
		String [] fname=file.list(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				File f2=new File(file, name);
				return (f2.isFile()&&f2.getName().endsWith(".avi"))==true?true:false;
			}
		});
		
		for (String string : fname) {
			System.out.println(string+" ");
		}
	}
}
