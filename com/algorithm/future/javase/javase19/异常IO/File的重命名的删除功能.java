package com.algorithm.future.javase.javase19.异常IO;

import java.io.File;
import java.io.IOException;

public class File的重命名的删除功能 {
	public static void main(String[] args) throws IOException {
	/*	boolean renameTo(File dest)  重新命名此抽象路径名表示的文件。 
		boolean delete()     删除此抽象路径名表示的文件或目录。 
		A 重命名注意事项
		如果路径名相同就改名
		如果路径名不同就改名+剪贴
		B 删除注意事项
		JAVA中的删除不走回收站
		要删除文件夹，该文件夹不能包含文件或者文件夹
	*/
		renameTo();
		delete();
	}

	private static void delete() {
		File F3=new File("odo.txt");
		F3.delete();
	}

	private static void renameTo() throws IOException {
		File F1=new File("ooo.txt");
		F1.createNewFile();
		
		File F2=new File("odo.txt");
		F1.renameTo(F2);
	}

}
