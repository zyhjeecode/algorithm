package com.algorithm.future.javase.javase19.异常IO;

import java.io.File;
import java.io.IOException;

public class File的创建功能 {
	public static void main(String[] args) throws IOException {
		createNewFile();
		System.out.println("_______________________________________");
		mkdirs创建文件夹();
	}

	private static void mkdirs创建文件夹() {
		File f1=new File("waww.aaa"); //这样写也可以，文件夹也可以有后缀
		System.out.println(f1.mkdir());
		File f2=new File("wwww//cccs");
		System.out.println(f2.mkdir());    
		File f3=new File("zyh//cccs");
		System.out.println(f3.mkdirs());    //File.mkdirs()创建多级文件夹
	}

	private static void createNewFile() throws IOException {
		File f1=new File("www.txt");
		f1.createNewFile(); //创建文件夹，该方法返回值为boolean类型，如果不存在，支持创建返回true 否则返回false
		
		File f2=new File("ww");
		System.out.println(f2.createNewFile()); //创建文件夹，该方法返回值为boolean类型，如果不存在，支持创建返回true 否则返回false
	}

}
