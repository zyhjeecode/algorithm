package com.algorithm.future.javase.javase14.duixiang;

import java.util.Calendar;

public class calendarStudy {
	public static void main(String[] args) {
		Calendar cl=Calendar.getInstance();  //父类引用指向子类对象
		System.out.println(cl.get(Calendar.YEAR));        //通过字段获取年
		System.out.println(cl.get((Calendar.MONTH))+1); //通过字段获取月；月从0开始编号
		System.out.println(cl.get(Calendar.DAY_OF_MONTH));  //一个月中的第几天
		System.out.println("今天是 "+cl.get(Calendar.YEAR)+"年"+(cl.get((Calendar.MONTH))+1)+"月"+cl.get(Calendar.DAY_OF_MONTH)+"日");
		cl.add(cl.YEAR, -10); //对指定字段进行向前减或者向后加
		System.out.println("今天是 "+cl.get(Calendar.YEAR)+"年"+(cl.get((Calendar.MONTH))+1)+"月"+cl.get(Calendar.DAY_OF_MONTH)+"日");
		cl.set(cl.YEAR, 2088); //对指定字段进行修改
		System.out.println("今天是 "+cl.get(Calendar.YEAR)+"年"+(cl.get((Calendar.MONTH))+1)+"月"+cl.get(Calendar.DAY_OF_MONTH)+"日");
		cl.set(2088, 10, 12);; //对多个字段进行修改
		System.out.println("今天是 "+cl.get(Calendar.YEAR)+"年"+(cl.get((Calendar.MONTH))+1)+"月"+cl.get(Calendar.DAY_OF_MONTH)+"日");
	}

}
