package com.algorithm.future.javase.javase14.duixiang;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class regexfenzu {
	public static void main(String[] args) {
		String regex1="(.)\\1(.)\\2";           //每个括号代表一组
		String regex2="(.)(.)\\1\\2";           //每个括号代表一组
		String regex3="(.)(.)\\2\\1";           //每个括号代表一组
		System.out.println("高高兴兴".matches(regex1));
		System.out.println("高兴高兴".matches(regex1));
		System.out.println("高兴高兴".matches(regex2));
		System.out.println("高兴兴高".matches(regex2));
		System.out.println("高兴兴高".matches(regex3));
		System.err.println("——————————————————————————————————————————————————————————————");
		String string="我我我在在在在安安师大大上上学学学";
		System.out.println(string.replaceAll("(.)\\1+", "$1"));             //$1指的是第一组的内容
		System.err.println("——————————————————————————————————————————————————————————————");
		String string2="dasda17775480850aasd17548122217775482117775480857488414sada17775480852";
		String regex4="1[35798]\\d{9}";
		Pattern pattern=Pattern.compile(regex4);       //获取正则表达式模板
		Matcher matcher=pattern.matcher(string2);     //利用模板获取针对String2的匹配器
		while(matcher.find())							//用匹配器来找出满足正则表达式的式子
		System.out.println(matcher.group());			//返回找到的值
	}
}
