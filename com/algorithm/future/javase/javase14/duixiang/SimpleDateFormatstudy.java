package com.algorithm.future.javase.javase14.duixiang;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;

public class SimpleDateFormatstudy {
	public static void main(String[] args) throws ParseException {
		// SimpleDateFormat实现时期和字符串的相互转换

		// SimpleDateFormat 是一个以与语言环境相关的方式来格式化和分析日期的具体类。它允许进行格式化（日期 -> 文本）、分析（文本
		// -> 日期）和规范化。
		//
		// SimpleDateFormat 使得可以选择任何用户定义的日期-时间格式的模式。但是，仍然建议通过 DateFormat 中的
		// getTimeInstance、getDateInstance 或 getDateTimeInstance
		// 来新的创建日期-时间格式化程序。每一个这样的类方法都能够返回一个以默认格式模式初始化的日期/时间格式化程序。可以根据需要使用
		// applyPattern 方法来修改格式模式。
		//
		// 日期和时间模式 结果
		// "yyyy.MM.dd G 'at' HH:mm:ss z" 2001.07.04 AD at 12:08:56 PDT
		// "EEE, MMM d, ''yy" Wed, Jul 4, '01
		// "h:mm a" 12:08 PM
		// "hh 'o''clock' a, zzzz" 12 o'clock PM, Pacific Daylight Time
		// "K:mm a, z" 0:08 PM, PDT
		// "yyyyy.MMMMM.dd GGG hh:mm aaa" 02001.July.04 AD 12:08 PM
		// "EEE, d MMM yyyy HH:mm:ss Z" Wed, 4 Jul 2001 12:08:56 -0700
		// "yyMMddHHmmssZ" 010704120856-0700
		// "yyyy-MM-dd'T'HH:mm:ss.SSSZ" 2001-07-04T12:08:56.235-0700

		datetostring();// Date对象转换为时间字符串
		dateStringToDate();		// 时间字符串转换为Date对象

	}

	private static void dateStringToDate() throws ParseException {
		// 时间字符串转换为Date对象
		String st= "2019年02月01日 15:24:47";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
		Date date=sdf.parse(st);
		System.out.println(date);
		System.out.println(sdf.format(new Date()));
	}

	private static void datetostring() {
		// Date对象转换为时间字符串
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
		System.out.println(sdf.format(d));
	}

}
