package com.algorithm.future.javase.javase14.duixiang;

public class regextext {

	public static void main(String[] args) {
      String regex="[1~9]\\d{4,14}";             //正则表达式    这时第一位是1~9 然后后面有4~14个数字
      System.out.println("012345".matches(regex));
      System.out.println("2345".matches(regex));
      System.out.println("12345".matches(regex));
      System.out.println("1ascaf".matches(regex));
	}

}
