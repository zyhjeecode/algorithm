package com.algorithm.future.javase.javase14.duixiang;

import java.util.Random;

public class mathstudy {
	public static void main(String[] args) {
			System.out.println(Math.PI);
			System.out.println(Math.abs(-111));  //取绝对值
			System.out.println(Math.ceil(12.1));  //向上取整
			System.out.println(Math.floor(12.1));  //向下取整          返回值为double值
			System.out.println(Math.round(12.4));      //四舍五入法
			System.out.println(Math.max(15, 555));   //取俩个数字中的最大值
			System.out.println(Math.min(15, 555));   //取俩个数字中的最小值
			System.out.println(Math.pow(2, 3));    //前面数字为底数，后面数字为指数	
			System.out.println(Math.sqrt(2));  //对2开平方
			System.out.println(Math.random());    //生成0.0到1.0之间的小数，不包含1.0
			Random   random=new Random();
			System.out.println(random.nextInt(100)); //返回一个0~100之间的随机数  包括0 不包括100
	}

}
