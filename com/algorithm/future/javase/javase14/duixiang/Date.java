package com.algorithm.future.javase.javase14.duixiang;

import java.text.SimpleDateFormat;

public class Date {

	public static void main(String[] args) {
		SimpleDateFormat smd=new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		System.out.println(smd.format(new java.util.Date()));
		System.out.println(new java.util.Date());
	}

}
