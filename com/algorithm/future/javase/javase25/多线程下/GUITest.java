package com.algorithm.future.javase.javase25.多线程下;

import java.awt.Button;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GUITest {
	public static void main(String[] args) {
		Frame frame = new Frame("我的第一个窗口"); // 创建一个隐藏的窗口
		frame.setSize(500, 500); // 设置窗口大小
		frame.setLocation(700, 200);// 设置窗口位置(x,y) 以左上角为坐标轴
		// frame.setIconImage(Toolkit.getDefaultToolkit().createImage("1.png"));
		Button b1 = new Button("buttonOne");
		Button b2 = new Button("按钮二");
		frame.add(b1); // 添加按钮
		frame.add(b2); // 添加按钮
		frame.setLayout(new FlowLayout());// 设置窗口为流式
		frame.addWindowListener(new WindowAdapter() { // 设置窗体监听
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		b1.addMouseListener(new MouseAdapter() {// 创建B1按钮的鼠标监听
			@Override
			public void mouseReleased(MouseEvent e) {// 点击并释放操作
				System.exit(0); // 退出
			}
		});
		b1.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent w) { // KeyEvent为键盘事件
				if (w.getKeyCode() == KeyEvent.VK_SPACE) { // 得到键盘事件内容
					System.exit(0);
				} else if (w.getKeyCode() == 49) {
					System.out.println("按下1");
				}
				System.out.println(w.getKeyCode());

			}
		});
		b2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		frame.setVisible(true);
	}

}
