package com.algorithm.future.javase.javase25.多线程下;

public class TwoThread {
	public static void main(String[] args) {
		 prienterw  p=new prienterw();
		new Thread(){
			public void run() {
				try {
					while(true){
					p.p1();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
		new Thread(){
			public void run() {
				try {
					while(true){
						p.p2();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

}

class prienterw {
	private int flag = 1;

	public void p1() throws Exception {
		synchronized (this) {
			if (flag != 1) {          //只要flag！=1 线程进入睡眠
				this.wait();
			}
			System.out.print("我");
			System.out.print("是");
			System.out.print("你");
			System.out.print("爸");
			System.out.println();
			flag = 2;               //如果flag=1，线程执行到此处改变flag值且随机唤醒其他在睡眠线程
			this.notify();

		}
	}

	public void p2() throws InterruptedException {
		synchronized (this) {
			if (flag != 2) {
				this.wait();
			}
			System.out.print("小");
			System.out.print("明");
			System.out.print("在");
			System.out.print("家");
			System.out.println();
			flag = 1;
			this.notify();
		}
	}
}