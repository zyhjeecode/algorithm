package com.algorithm.future.javase.javase25.多线程下;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class creatThreadPool {
	public static void main(String[] args) {
		ExecutorService epool=Executors.newFixedThreadPool(2);
		epool.submit(new ta1());
		epool.submit(new ta1());
//		epool.submit(new ta1()); 只能放两个新线程，再放也不能存进去了
		epool.shutdown();    //待线程池内程序执行完毕，关闭线程池

	}
}
class ta1 extends Thread{
	public void run() {
		for(int i=0;i<=100;i++)
			System.out.println(this.getName()+"     "+i);
	}
}

