package com.algorithm.future.javase.javase25.多线程下;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockTest {
	public static void main(String[] args) {
		
	
	priente1 p=new priente1();
	new Thread(){
		public void run() {
			try {
				while(true){
					p.p1();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}.start();
	
	new Thread(){
		public void run() {
			try {
				while(true){
					p.p2();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}.start();
	
	new Thread(){
		public void run() {
			try {
				while(true){
					p.p3();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}.start();

}
}
class priente1 {
	private int flag = 1;
	private ReentrantLock rtl = new ReentrantLock();
	private Condition c1 = rtl.newCondition();
	private Condition c2 = rtl.newCondition();
	private Condition c3 = rtl.newCondition();

	public void p1() throws Exception {
		rtl.lock();
		if (flag != 1) { // 只要flag！=1 线程进入睡眠
			c1.await();
		}
		System.out.print("我");
		System.out.print("是");
		System.out.print("你");
		System.out.print("爸");
		System.out.println();
		flag = 2; 
		c2.signal(); //唤醒指定线程c2
		rtl.unlock();
	}

	public void p2() throws InterruptedException {
		rtl.lock();
		if (flag != 2) {
			c2.await();
		}
		System.out.print("小");
		System.out.print("明");
		System.out.print("在");
		System.out.print("家");
		System.out.println();
		flag = 3;
		c3.signal();//唤醒指定线程c3
		rtl.unlock();
	}

	public void p3() throws InterruptedException {
		rtl.lock();
		if (flag != 3) {
			c3.await();
		}
		System.out.print("开");
		System.out.print("门");
		System.out.print("见");
		System.out.print("喜");
		System.out.println();
		flag = 1;
		c1.signal();//唤醒指定线程c1
		rtl.unlock();
	}
}