package com.algorithm.future.javase.javase17.set集合;

import java.util.HashSet;
import java.util.Random;

public class 编写一个程序获得十个不重复的随机数 {
	public static void main(String[] args) {
		/*分析
		 * 1、用random类创建随机数对象
		 * 2、需要十个随机数而且不能重复，我们用hashset集合
		 * 3、如果hashset长度小于10就持续存储，大于10就停止存入
		 * 4、用random中的nextint（n）获取1~20之间的随机数*/
		
		Random random=new Random();
		HashSet<Integer> hs=new HashSet<>();
		while (hs.size()<10) {
			hs.add(random.nextInt(20)+1);   //    random.nextInt(n)代表随机0~n-1的数
		}
		System.out.println(hs);
		for (Integer integer : hs) {
			System.out.print(integer +"  ");
		}
	}
}
