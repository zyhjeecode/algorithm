package com.algorithm.future.javase.javase17.set集合;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class scanner录入字符保存不重复的 {

	public static void main(String[] args) {
		System.out.println("请输入一段字符串:");
		Scanner sc = new Scanner(System.in);
		String string = sc.next();
		char[] s1 = string.toCharArray();
		t1(s1);
		t2(s1);
	}
	static  void  t1(char[] s1){
		HashSet<Character> d=new HashSet<>();
		for (char c : s1) {
			d.add(c);
		}

		System.out.println(d);
	}
	static 	void  t2(char[] s1){
		LinkedHashSet<Character> d=new LinkedHashSet<>();
		for (char c : s1) {
			d.add(c);
		}

		System.out.println(d);
	}




}