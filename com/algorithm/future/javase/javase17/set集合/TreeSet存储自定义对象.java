package com.algorithm.future.javase.javase17.set集合;

import java.util.TreeSet;

public class TreeSet存储自定义对象 {
	public static void main(String[] args) {
		//需要实现comparable类并重写自定义对象里的compareTo方法 从而让TreeSet集合知道以什么顺序存储自定义对象
		 /*	当compareTo返回的值为0时候treeset只存储进去一个值，
		 	当返回值为正数时怎么存就怎么取
		       当返回值为负数时，顺序为倒序
		 */
		TreeSet<people> ts =new TreeSet<>();
		ts.add(new people("法师", 16));
		ts.add(new people("夏末", 16));
		ts.add(new people("大苏打", 16));
		ts.add(new people("大苏打", 16));
		ts.add(new people("大苏打", 16));
		ts.add(new people("法师", 16));
		System.out.println(ts);
	}

}
