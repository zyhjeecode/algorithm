package com.algorithm.future.javase.javase17.set集合;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

public class 将集合中的元素进行排序且不去除重复元素 {
	public static void main(String[] args) {
		ArrayList<String> aList=new ArrayList<>();
		aList.add("a");
		aList.add("a");
		aList.add("s");
		aList.add("s");
		aList.add("b");
		aList.add("c");
		TreeSet<String> tSet=new TreeSet<>(new competor());
		System.out.println(aList);
		tSet.addAll(aList);
		aList.clear();
		aList.addAll(tSet);
		System.out.println(tSet);
		
		}

	public static class competor implements Comparator<String>{       // 实现compartor重写了其存入的时候的比较方法 改变其规则

		@Override
		public int compare(String o1, String o2) {
			int flag=o1.compareTo(o2);
			return flag==0?1:flag;
		}
		
	}


}
