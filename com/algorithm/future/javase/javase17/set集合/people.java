package com.algorithm.future.javase.javase17.set集合;

public class people implements Comparable<people> {
	private String name;
	private int age;

	public people() {
		super();

	}

	public people(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "people [name=" + name + ", age=" + age + "]";
	}

	
	//将自定义对象存入HashSet必须重写hashCode()  equals(）两个方法从而保证HashSet的唯一性
	@Override           //保证了hashSet的唯一性 
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override //保证了hashSet的唯一性
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		people other = (people) obj;
		if (age != other.age)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override                          //   TreeSet每次存数据时候调用compareTo方法,compareTo决定了排序的路径和取数据的顺序
	public int compareTo(people o) {   /*当compareTo返回的值为0时候treeset只存储进去一个值，
	 									 当返回值为正数时怎么存就怎么取
  										 当返回值为负数时，顺序为倒序*/
		return 1;
	}

}
