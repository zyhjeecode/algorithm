package com.algorithm.future.javase.javase12.String;

public class string_judge {

	public static void main(String[] args) {
		String s1 = "zyh";
		String s2 = "zyh";
		String s3 = "Zyh";
		System.out.println(s1.equals(s2));
		System.out.println(s1.equals(s3));// 比较两个字符串内容————区分大小写
		System.out.println(s1.equalsIgnoreCase(s3));// 比较两个字符串内容————不区分大小写
		System.out.println("——————————————————————————————————————————————————————————————————");
		String s4 = "yh";
		System.out.println(s1.contains(s4));// 判断S1是否包含S4//区分大小写
		System.out.println("——————————————————————————————————————————————————————————————————");
		String s5 = "zy";
		String s6 = "yh";
		System.out.println(s1.startsWith(s5));// 判断S1是否以S5开头
		System.out.println(s1.startsWith(s6));// 判断S1是否以S6开头
		System.out.println("——————————————————————————————————————————————————————————————————");
		System.out.println(s1.endsWith(s5));// 判断S1是否以S5结尾
		System.out.println(s1.endsWith(s6));// 判断S1是否以S6结尾
		System.out.println("——————————————————————————————————————————————————————————————————");
		String s7 = "";
		String s8=null;
		System.out.println(s7.isEmpty());//判断S7是否为空
		System.out.println(s1.isEmpty());//判断S1是否为空
		//system.out.println(s8.isEmpty());会报错，因为null为空常量不是一个对象，不能调用方法，如果调用的话将出现java.lang.NULLPointExceptiong异常

	}

}
