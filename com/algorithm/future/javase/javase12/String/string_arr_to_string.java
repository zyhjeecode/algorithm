package com.algorithm.future.javase.javase12.String;

public class string_arr_to_string {

    public static void main(String[] args) {
        byte[] a = {1, 2, 5, 3, 5, 6, 7, 8, 9};
        String s = a.toString();
        System.out.println(s);
        String s1 = new String(a);
        System.out.println(s1);
        String s2 = new String(a, 2, 4);
        System.out.println(s2);
        System.out.println("———————————————————————上面是byte数组到字符串，下面是char数组到字符串——————————————————————————————————————");
        char[] b = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
        String s4 = new String(b);
        String s3 = new String(b, 2, 3);
        System.out.println(s3);
        System.out.println(s4);
        byte[] bytes = s4.getBytes();
        for (int i = 0; i < bytes.length; i++) {
            System.out.println((char) bytes[i]);
        }
    }

}
