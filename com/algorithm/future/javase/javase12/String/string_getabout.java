package com.algorithm.future.javase.javase12.String;

public class string_getabout {

	public static void main(String[] args) {
		String s1 = "aaazyh";
		System.out.println("length是一个方法获取的是每个字符的个数" + s1.length());
		System.out.println("——————————————————————————————————————————————————————————————————————————");

		System.out.println("返回第一次出现”z“的索引" + s1.indexOf("z"));
		System.out.println("如果不存在返回-1" + s1.indexOf("w"));
		System.out.println("如果有这个字串，返回其第一个字符所在位置" + s1.indexOf("zyh"));
		System.out.println("返回从1之后包括1位置后第一次出现a的索引" + s1.indexOf("a", 1));
		System.out.println("——————————————————————————————————————————————————————————————————————————");

		char c1 = s1.charAt(3);
		System.out.println("根据索引获取对应位置的字符" + c1);
		System.out.println("找最后一个出现a字符的位置"+s1.lastIndexOf("a"));
		System.out.println("返回从2之前包括2位置前第一次出现a的索引"+s1.lastIndexOf("a",2));
		
		System.out.println("——————————————————————————————————————————————————————————————————————————");
		System.out.println("截取3之后的字符串"+s1.substring(3));//左闭右开，不包含右边界
		System.out.println("截取3~5索引的字符串"+s1.substring(3, 5));//左闭右开，不包含右边界

	}

}
