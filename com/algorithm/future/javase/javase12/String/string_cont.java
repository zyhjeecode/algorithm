package com.algorithm.future.javase.javase12.String;

public class string_cont {
	public static void main(String[] args) {
		String s1 = "嗯~想你想你想你想我";
		String s2 = "想你";
		System.out.println("—————————————————————方法一———————————————————————");
		int count = 0;
		for (int i = 0; i < s1.length() - 1; i++) {
			String string = s1.substring(i, i + s2.length());
			if (string.equals(s2)) {
				count++;
			}

		}
		System.out.println(s1 + "中包含" + s2 + "的个数为： " + count);
		System.out.println("—————————————————————方法二———————————————————————");
		int count2 = 0;
		String s1c=s1;    //不损伤原来的字符串
		while (s1c.indexOf(s2) >= 0) {
			count2++;
			s1c = s1c.substring(s1c.indexOf(s2) + s2.length());
		}
		System.out.println(s1 + "中包含" + s2 + "的个数为： " +count2);
	}

}
