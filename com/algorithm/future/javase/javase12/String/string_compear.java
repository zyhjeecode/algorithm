package com.algorithm.future.javase.javase12.String;

public class string_compear {

	public static void main(String[] args) {
		String s1="abc";                 //进入了常量池，这一步一共创建了两个对象，一个在常量池中一个在堆中
		String s2="abc";				//由于上一步abc已经进入了常量池，故S2直接从常量池中取
		System.out.println(s1==s2);  
		System.out.println(s1.equals(s2));
		
		
		
		String s3=new String("abc");  
		System.out.println(s2==s3);  //S3记录的是堆中的地址值,S1记录的是常量池中的地址值、
		
		
		
		String s4="a"+"b"+"c";       //在编译时候就变成了“abc”，然后把“abc”复制给了s4——————常量优化机制
		System.out.println(s1==s4);//java中的常量优化机制，直接从现有的取的
		
		String s5="ab";
		//括在一对双引号之内的字符序列或转义字符序列称为字符串常量
        String s6=s5+"c";//s5+"c"是一个字符串变量+字符串常量     不属于常量优化机制  S6取得是堆中一个新建对象的值
        System.out.println(s1==s6);
	}

}
