package com.algorithm.future.javase.javase12.String;

public class string_replace {

	public static void main(String[] args) {
		String string = "Zyh_asdazyh";
		String snew = string.replace("a", "DD"); // 用DD代替字符串中的a
		System.out.println(snew);
		System.out.println("——————————————————————————————————————————————————");
		String skongge = "      zyh        123456      ";
		String squkongge = skongge.trim();
		System.out.println("squkongge去除两端空格前:" + skongge);
		System.out.println("squkongge去除两端空格后:" + squkongge);

	}

}
