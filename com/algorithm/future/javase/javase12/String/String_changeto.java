package com.algorithm.future.javase.javase12.String;

public class String_changeto {

	public static void main(String[] args) {
		String s1 = "asSDASDAdas";
		byte[] a = s1.getBytes(); // 通过gbk码表将字符串转换为字节数组
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
		System.out.println();
		System.out.println("——————————————————————————————————————————————————————————————————");
		char[] c = s1.toCharArray();
		for (int i = 0; i < c.length; i++) { // 将字符串转换为字符数组
			System.out.print(c[i] + " ");
		}
		System.out.println();
		System.out.println("——————————————————————————————————————————————————————————————————");
		int a2=51515;
		String s2=String.valueOf(a2);             //String.valueOf();将任意类型数据转换为字符串 //底层还是String类的构造方法new String（）来完成
		System.out.println(s2);
		System.out.println("——————————————————————————————————————————————————————————————————");
		
		
	}
}
