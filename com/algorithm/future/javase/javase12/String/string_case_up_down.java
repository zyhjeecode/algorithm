package com.algorithm.future.javase.javase12.String;

public class string_case_up_down {

	public static void main(String[] args) {
		String s="asdASDAaSDA123123";
		String sup=s.toUpperCase();
		String sdown=s.toLowerCase();
		System.out.println("大写状态为："+sup+"小写状态为："+sdown);
		String ssum=sup.concat(sdown);
		System.out.println("两个字符串拼接后："+ssum);
		System.out.println("更加强大的拼接方式，任意两种类型都可以拼接："+sup+sdown);
		System.out.println("——————————————————————————————————————————————————————————————————");
		String string="DxxXXXXx";
		String  string2=string.substring(0, 1).toUpperCase()+string.substring(1).toLowerCase();
		System.out.println("首字母大写，其他小写: "+string2);
		
		
		
	}
}
