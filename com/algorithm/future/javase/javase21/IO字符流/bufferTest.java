package com.algorithm.future.javase.javase21.IO字符流;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class bufferTest {
	public static void main(String[] args) throws IOException {
		BufferedReader bfr =new BufferedReader(new FileReader("aaa.txt"));
		BufferedWriter bfw =new BufferedWriter(new FileWriter("buffercopyaaa.txt"));
		int b;
		while ((b=bfr.read())!=-1) {
			bfw.write(b);
		}
		bfr.close();
		bfw.close();
		
	}

}
