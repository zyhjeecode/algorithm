package com.algorithm.future.javase.javase21.IO字符流;

import java.io.FileWriter;
import java.io.IOException;

public class FileWriteTest {
	public static void main(String[] args) throws IOException {
		FileWriter fw=new FileWriter("aaa.txt",true);
		fw.write("就这样!!"); //write中有一个两K的缓冲区 如果不关流就会将内容写到缓冲区内，关流会将缓冲区的内容刷新出并关闭
 		fw.close();
	}

}
