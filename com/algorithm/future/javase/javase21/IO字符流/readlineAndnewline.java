package com.algorithm.future.javase.javase21.IO字符流;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class readlineAndnewline {
	public static void main(String[] args) throws IOException {/*
		- BufferedReader的readLine()方法可以读取一行字符(不包含换行符号)
		- BufferedWriter的newLine()可以输出一个跨平台的换行符号"\r\n"*/
//		readlineTest();
		BufferedReader br = new BufferedReader(new FileReader("bbb.txt"));
		BufferedWriter bw =new BufferedWriter(new FileWriter("bbbline.txt"));
		String sa;
		while ((sa=br.readLine())!=null) {
			bw.write(sa);
			bw.newLine();//每读出一行写入一行就录入一个回车换行符
		}
		br.close();
		bw.close();
		
	}

	private static void readlineTest() throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new FileReader("aaa.txt"));
		String s;
		while (!(s=br.readLine()).equals(null)) {
			System.out.println(s);
		}
		br.close();
	}
}
