package com.algorithm.future.javase.javase21.IO字符流;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class linenumberReaderTest {
	public static void main(String[] args) throws IOException {
		LineNumberReader lnr=new LineNumberReader(new FileReader("aaa.txt"));
		
		String s;
//		lnr.setLineNumber(n);  设置第一行行号n+1,如果不设置默认n为0 第一行行号为1
		while ((s=lnr.readLine())!=null) {
			System.out.println(lnr.getLineNumber()+":"+s);
		}
	}

}
