package com.algorithm.future.javase.javase21.IO字符流;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
/*-
 * io字符流只能拷贝纯文本文件 
 * 不可以拷贝非纯文本的文件
 * 因为在读的时候会将字节转换为字符,在转换过程中,可能找不到对应的字符,就会用?代替,写出的时候会将字符转换成字节写出去
 * 如果是?,直接写出,这样写出之后的文件就乱了,看不了了  
*/
public class IOcharCopy {

	public static void main(String[] args) throws IOException {
		FileReader fr=new FileReader("aaa.txt");
		FileWriter fw=new FileWriter("CharCopyaaa.txt");
		
		int b;
		while ((b=fr.read())!=-1) {
			fw.write(b);
		}
		fr.close();
		fw.close();
	}
}
