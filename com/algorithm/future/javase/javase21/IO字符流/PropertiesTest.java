package com.algorithm.future.javase.javase21.IO字符流;

import java.util.Enumeration;
import java.util.Properties;

public class PropertiesTest {
public static void main(String[] args) {
	Properties pt=new Properties(); //双列集合
	pt.put("name","小明" );
	pt.put("age", "19");
	pt.put("性别", "男");
	Enumeration<String> pts=(Enumeration<String>) pt.propertyNames();//获取pt中所有的键的枚举
	while (pts.hasMoreElements()) {
		String s1=pts.nextElement();//获取键名
		String s2=pt.getProperty(s1);
		System.out.println(s1+"="+s2);
	}
	pt.setProperty("age", "20");  //如果找到键的话就修改，没找到的话就添加
	System.out.println(pt);
	pt.setProperty("tell", "175");
	System.out.println(pt);
}	

	
	
	//	TeshuMothen();

private static void TeshuMothen() {
	Properties pt=new Properties(); //双列集合
	pt.setProperty("name", "小黄");
	pt.setProperty("age", "25");
	pt.setProperty("local", "ahnu");
	
	Enumeration<String> pts=(Enumeration<String>) pt.propertyNames();//获取pt中所有的键的枚举
	while (pts.hasMoreElements()) {
		String s1=pts.nextElement();//获取键名
		String s2=pt.getProperty(s1);
		System.out.println(s1+"="+s2);
	}
}
}
