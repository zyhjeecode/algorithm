package com.algorithm.future.javase.javase21.IO字符流;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class InputStreamReadAndOutPutStreamWrite {
//	InputStreamReadAndOutPutStreamWrite 相当于字节转换读取中间通道
	public static void main(String[] args) throws IOException, FileNotFoundException {
		//用buffer可以能高效的读，更高效的写
		BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream("aaa.txt"), "utf-8"));//指定码表读字节成字符
		BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(new FileOutputStream("bbb.txt"),"gbk"));//指定码表写字节为字符
		
		int len;
		while ((len=br.read())!=-1) {
			bw.write(len);
		}
		br.close();
		bw.close();
		
	}
}
