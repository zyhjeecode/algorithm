package com.algorithm.future.javase.javase21.IO字符流;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class getThetimesofcharinTxt {
	public static void main(String[] args) throws IOException {
		FileReader fr=new FileReader("aaa.txt");
		BufferedWriter fw=new BufferedWriter(new FileWriter("times.txt"));
		HashMap<Character, Integer> hm=new HashMap<>();
		int d;
		while ((d=fr.read())!=-1) {
			char c=(char)d;
			hm.put(c, hm.containsKey(c)?hm.get(c)+1:1);
		}
		fr.close();
		FileWriter fW=new FileWriter("times.txt");
		for (char c : hm.keySet()) {
			fw.write(c+"="+hm.get(c));
			fw.newLine();
		}
		fw.close();
	}

}
