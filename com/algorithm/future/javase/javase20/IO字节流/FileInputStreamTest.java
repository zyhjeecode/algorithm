package com.algorithm.future.javase.javase20.IO字节流;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamTest {
public static void main(String[] args) throws IOException {
		File file =new File("asa");
	FileInputStream fis =new FileInputStream(file);
	/*
	int i1=fis.read();
	int i2=fis.read(); //read一次向后读一次
	*/
	
	//第一种方法 读取完所有的file中的元素
	//methord1(file, fis);

	//第二种方法 读取完所有的file中的元素
	method2(fis);
	fis.close();
}

private static void method2(FileInputStream fis) throws IOException {
	int b;
	while ((b=fis.read())!=-1) {
		System.out.print((char)b);
	}
}

private static void methord1(File file, FileInputStream fis) throws IOException {
	int i=1;
	while (i<=file.length()) {
		System.out.print((char)fis.read());
		i++;
	}
}
}
