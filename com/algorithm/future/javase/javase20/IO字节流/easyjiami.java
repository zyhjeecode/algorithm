package com.algorithm.future.javase.javase20.IO字节流;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


//一个数异或两次另一个数就得到其本身
public class easyjiami {
	public static void main(String[] args) throws IOException {
//		JpgAddMMCopy();  //加密
		
		JpgDeletMMcopy();//解密
	}

	private static void JpgDeletMMcopy() throws FileNotFoundException, IOException {
		BufferedInputStream bis2=new BufferedInputStream(new FileInputStream("11JiaMibuffercopy.jpg"));
		BufferedOutputStream bos3=new BufferedOutputStream(new FileOutputStream("11JieMibuffercopy.jpg"));
		
		int b;
		while ((b=bis2.read())!=-1) {
			bos3.write(b^123);   //再次异或 得到原来的值
		}
		bis2.close();
		bos3.close();
	}

	private static void JpgAddMMCopy() throws FileNotFoundException, IOException {
		BufferedInputStream bis=new BufferedInputStream(new FileInputStream("11.jpg"));
		BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream("11JiaMibuffercopy.jpg"));
		
		int len;
		while ((len=bis.read())!=-1) {
			bos.write(len^123);  //存的是len的异或
		}
		
		bis.close();
		bos.close();
	}

}
