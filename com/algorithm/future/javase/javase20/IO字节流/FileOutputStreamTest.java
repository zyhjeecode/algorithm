package com.algorithm.future.javase.javase20.IO字节流;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
/*
FileOutputStream在创建对象时候（FileOutputStream fop=new FileOutputStream("ddd.txt");）
如果没有这个文件会帮我们创建出来
如果有这个文件则会清空这个文件内容

如果想要续写
则可以采用FileOutputStream fop=new FileOutputStream("ddd.txt"，true);格式 续写，系统不清空
*/

public class FileOutputStreamTest {
	public static void main(String[] args) throws IOException {
		不续写();
	//	续写();
		
	}

	private static void 续写() throws FileNotFoundException, IOException {
		FileOutputStream fop=new FileOutputStream("ddd.txt",true);
		fop.write(555);
	}

	private static void 不续写() throws FileNotFoundException, IOException {
		FileOutputStream fop=new FileOutputStream("ddd.txt");//创建字节输出流对象，如果没有就自动创建一个
		char a='p';
		fop.write(99);     //虽然写入的是一个int数  但是到文件上是一个字节，会自动去除前三个八位
		fop.write(a);
		fop.write('a');
		fop.close();
	}
}
