package com.algorithm.future.javase.javase20.IO字节流;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class 优化copy方法 {
	public static void main(String[] args) throws IOException {
//		优化copy之小数组();
//		优化copy之Buffer包装(); //在缓存中存取
		

	}

	private static void 优化copy之Buffer包装() throws FileNotFoundException, IOException {
		BufferedInputStream bis=new BufferedInputStream(new FileInputStream("11.jpg"));
		BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream("11buffercopy.jpg"));
		
		int len;
		while ((len=bis.read())!=-1) {
			bos.write(len);
		}
		
		bis.close();
		bos.close();
	}

	private static void 优化copy之小数组() throws FileNotFoundException, IOException {
		FileInputStream fis =new FileInputStream("11.jpg");
		FileOutputStream fos =new FileOutputStream("11优化copy.jpg");
		
		byte[] cb=new byte[1024];  //一般定义为1024或者 1024的N个8倍
		int len;
		while ((len=fis.read(cb))!=-1) { //如果read（）括号里忘了加cb数组则返回值不是读取的字节个数而是字节的码表值
			fos.write(cb,0,len);         //从cb数组的第0位开始读，读len个字节 
		}
		fis.close();
		fos.close();
	}

}
