package com.algorithm.future.javase.javase20.IO字节流;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class 拷贝图片 {
	public static void main(String[] args) throws IOException {
		FileInputStream fis=new FileInputStream("11.jpg"); //创建输入流，关联11.jpg
		FileOutputStream fos=new FileOutputStream("copy11.jpg");//创建输出流，关联copy11.jpg 
		
		int b;
		while ((b=fis.read())!=-1) {
			fos.write(b);
		}
		
		fis.close();
		fos.close();
		
	}

}
