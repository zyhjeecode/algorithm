package com.algorithm.future.javase.javase20.IO字节流;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class CopyToPoint {
	public static void main(String[] args) throws IOException {
		File file=getfile();
		BufferedInputStream bis =new BufferedInputStream(new FileInputStream(file));
		BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream(file.getName()));
		int d;
		while ((d=bis.read())!=-1) {
			bos.write(d);
		}
		bis.close();
		bos.close();
		
	}
	public static File getfile() {
		Scanner sc =new Scanner(System.in);
		System.out.println("请输入一个文件路径");
		while (true) {
			String s=sc.nextLine();
			File file=new File(s);
			if (!file.exists()) {
				System.out.println("您输入的路径不存在，请重新输入");
			}else if (file.mkdirs()) {
				System.out.println("您输入的是文件夹，请重新输入");
			} else {
				return file;
			}
			
		}
	}
	

}
