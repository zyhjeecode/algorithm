package com.algorithm.future.jucTest;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

//用信号量来实现线程的交替执行
public class SemaphoreTest {
    private static volatile AtomicInteger flag=new AtomicInteger(1);

    public static void main(String[] args) {
        //定义两个车位
        Semaphore semaphore = new Semaphore(1);
        //定义了六个车
        for (int i = 1; i <= 100; i++) {
            new Thread(() -> {
                try {
                    //这里车子会请求一个车位,车位减1
                    semaphore.acquire();
                    if ((flag.get()&1)==1){
                        System.out.println(Thread.currentThread().getName() + "抢到车位");
                        flag.incrementAndGet();
                    }else {
                        Thread.sleep(1);
                    }
                    //占领这个车位3秒钟
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    //这里车子会释放一个车位,车位加1
                    semaphore.release();

                }
            },"A").start();

            new Thread(() -> {
                try {
                    //这里车子会请求一个车位,车位减1
                    semaphore.acquire();
                    if ((flag.get()&1)==0){
                        System.out.println(Thread.currentThread().getName() + "抢到车位");
                        flag.incrementAndGet();
                    }else {
                        Thread.sleep(1);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    //这里车子会释放一个车位,车位加1
                    semaphore.release();

                }
            },"B").start();


        }
    }
}
