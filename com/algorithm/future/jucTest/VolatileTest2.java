package com.algorithm.future.jucTest;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class VolatileTest2 {
    public static AtomicInteger a=new AtomicInteger(0);
    public static void main(String[] args){
        for(int i=0;i<100000;i++){
            new Thread(()->{
            a.incrementAndGet();
            }, String.valueOf(i)).start();
        }
        //暂停线程
        try{ TimeUnit.SECONDS.sleep(10);} catch(InterruptedException e){ e.printStackTrace();}
        
        System.out.println(a);
    }
}
