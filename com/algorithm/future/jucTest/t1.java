package com.algorithm.future.jucTest;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
 *  Lock+condition 循环打印
 * 需求循环打印首先打印A,A唤醒B打印B,B唤醒C打印C,C再唤醒A打印A*/
public class t1 {
    public static void main(String[] args){
        Zy1 zy1 =new Zy1();

        new Thread(()->{
                zy1.printA();
        }, "A").start();

        new Thread(()->{
                zy1.printB();
        }, "B").start();


        new Thread(()->{
                zy1.printC();
        }, "C").start();

    }
}

class Zy1 {
    volatile int num=1; //为ABC设置标记 A:1 B:2 C:3
    Lock lock=new ReentrantLock();
    Condition conditionA=lock.newCondition();
    Condition conditionB=lock.newCondition();
    Condition conditionC=lock.newCondition();
    public void printA(){
        lock.lock();
        try {
            while (num!=1)
            {
                conditionA.await();
            }
            System.out.println("AA");
            num=2;
            conditionB.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public void printB(){
        lock.lock();
        try {
            while (num!=2)
            {
                conditionA.await();
            }
            System.out.println("BB");
            num=3;
            conditionC.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public void printC(){
        lock.lock();
        try {
            while (num!=3)
            {
                conditionA.await();
            }
            System.out.println("CC");
            num=1;
            conditionA.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
}
