package com.algorithm.future.jucTest;


public enum EnumDemo {
    one(1,"key"),two(2,"key2"),three(3,"key3");

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private int key;
    private String value;

    EnumDemo(int key, String value) {
        this.key = key;
        this.value = value;
    }
    public static EnumDemo getEnum(int index){
        EnumDemo[] el = EnumDemo.values();
        for (EnumDemo e: el
        ) {
            if (index==e.key)
                return e;
        }
        return null;
    }


}

