package com.algorithm.future.jucTest;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

class  CallAbleReource implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println("**进callable了  啦啦啦啦");
        return 1024;
    }
}

public class CallAble {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<Integer> futureTask= new FutureTask<Integer>(new CallAbleReource());
        Thread thread=new Thread(futureTask,"FutureTask测试");
        thread.start();
        System.out.println("FutureTesk运行得到"+futureTask.get());


    }
}
