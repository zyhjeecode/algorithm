package com.algorithm.future.jucTest;

import java.util.WeakHashMap;

public class WeakHashMapTest {
    public static void main(String[] args) {
        WeakHashMap<Integer, String> weakHashMap=new WeakHashMap<>();
        Integer key=Integer.valueOf(5);
        String value="WeakHashMap";
        weakHashMap.put(key,value);
        System.out.println(weakHashMap);
        System.out.println("_________");
        key=null;
        System.out.println(weakHashMap);
        System.out.println("_________");
        System.gc();
        System.out.println(weakHashMap);

    }
}
