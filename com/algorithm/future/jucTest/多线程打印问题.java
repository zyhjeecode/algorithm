package com.algorithm.future.jucTest;

import org.junit.Test;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author: zyh
 * @create: 2021-05-27 13:55
 **/
public class 多线程打印问题 {
    /**
     * 三个线程循环打印
     */
    volatile int currIndex=1;

    @Test
    public void  test() throws InterruptedException {
        TimeUnit unit;
        BlockingQueue workQueue;
        ThreadPoolExecutor pool=new ThreadPoolExecutor(3,3 ,100 , TimeUnit.MINUTES, new LinkedBlockingDeque<>());
        int time=0;
        pool.execute(()->{
            while (true){
                if (currIndex==1){
                    System.out.println("A");
                    currIndex=2;
                }
            }
        });
        pool.execute(()->{
            while (true){
                if (currIndex==2){
                    System.out.println("B");
                    currIndex=3;
                }
            }
        });
        pool.execute(()->{
            while (true){
                if (currIndex==3){
                    System.out.println("C");
                    currIndex=1;
                }
            }
        });

    Thread.sleep(100);
    }

}
