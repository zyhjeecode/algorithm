package com.algorithm.future.jucTest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPollTest {
    static volatile  int a=0;
    public static void main(String[] args){
        ExecutorService service = Executors.newSingleThreadExecutor();
        service.execute(()->{
            a++;
            System.out.println(Thread.currentThread().getName()+"hahaha");
        });
        service.shutdown();
    }
}
