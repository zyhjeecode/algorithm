package com.algorithm.future.jucTest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ConcurrentArraylist {
    public static void main(String[] args){
        //arrayListConcurrentTest();
       }

    private static void arrayListConcurrentTest() {
        List<String> a=new ArrayList<>();
        //Set<String> a=new HashSet<>();
        //暂停线程
        for(int i=0;i<30;i++){
            new Thread(()->{
                a.add(UUID.randomUUID().toString());
                System.out.println(a);
            }, String.valueOf(i)).start();
        }
    }
}
