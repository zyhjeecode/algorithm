package com.algorithm.future.jucTest;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
 *  Lock+condition 循环打印
 * 需求循环打印首先打印A,A唤醒B打印B,B唤醒C打印C,C再唤醒A打印A*/
public class yyyy {
    public static void main(String[] args){
        zy11 zy1 =new zy11();

        new Thread(()->{
            zy1.printA();
        }, "A").start();

        new Thread(()->{
            zy1.printB();
        }, "B").start();


        new Thread(()->{
            zy1.printC();
        }, "C").start();

    }
}

class zy11 {
    volatile int num=1; //为ABC设置标记 A:1 B:2 C:3
    Lock lock=new ReentrantLock();
    Condition conditionA=lock.newCondition();
    Condition conditionB=lock.newCondition();
    Condition conditionC=lock.newCondition();
    public void printA(){
        lock.lock();
        try {
            while (num!=1)
            {
                conditionA.await();
            }
            System.out.println("AA");
            num=2;
            conditionB.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public void printB(){
        lock.lock();
        try {
            while (num!=2)
            {
                conditionB.await();
            }
            System.out.println("BB");
            num=3;
            conditionC.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public void printC(){
        lock.lock();
        try {
            while (num!=3)
            {
                conditionC.await();
            }
            System.out.println("CC");
            num=1;
            conditionA.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
}
