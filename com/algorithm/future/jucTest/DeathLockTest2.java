package com.algorithm.future.jucTest;

public class DeathLockTest2 {
    public static void main(String[] args){  
        String s="key";
        new Thread(()->{
            synchronized (s){
            System.out.println(Thread.currentThread().getName()+"得到了s");
            //暂停线程
            Thread.yield();
            }
        },"LockA").start(); new Thread(()->{
            synchronized (s){
            System.out.println(Thread.currentThread().getName()+"得到了s");
            }
        },"LockB").start();
    }
}
