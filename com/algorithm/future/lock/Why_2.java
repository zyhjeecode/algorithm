package com.algorithm.future.lock;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @description: 为什么要有锁
 * @author: zyh
 * @create: 2021-01-27 21:59
 **/
public class Why_2 {
    volatile AtomicInteger seeCount =new AtomicInteger(0);

    ExecutorService threadPool = new ThreadPool().threadPool;


    /**
     * @desc 单线程下跑
     * @author zyh
     * @date 2021/1/28
     */
    @Test
    public void singlePrint(){
        AtomicInteger count=new AtomicInteger(0);
        for (int i = 0; i < 100; i++) {
                count.incrementAndGet();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(count.get());
        }
    }


    /**
     * @desc 多线程下跑
     * @author zyh
     * @date 2021/1/28
     */
    @Test
    public void unsafePrint(){

        AtomicInteger count=new AtomicInteger(0);
        for (int i = 0; i < 100; i++) {
            threadPool.execute(()->{
                    count.incrementAndGet();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(count.get());
            });
        }
        while (count.get()!=100){
            Thread.yield();
        }
    }

    /**
     * @desc 加synchronized跑
     * @author zyh
     * @date 2021/1/28
     */
    @Test
    public void safePrint(){

        AtomicInteger count=new AtomicInteger(0);
        for (int i = 0; i < 100; i++) {
            threadPool.execute(()->{
                synchronized (this){
                    count.incrementAndGet();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(count.get());
                }
            });
        }
        while (count.get()!=100){
            Thread.yield();
        }

    }

    /**
     * @desc 使用volatile修饰的变量跑
     * @author zyh
     * @date 2021/1/28
     */
    @Test
    public void safePrint2(){
        for (int i = 0; i < 100; i++) {
            threadPool.execute(()->{
                    seeCount.incrementAndGet();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(seeCount.get());
            });
        }
        while (seeCount.get()!=100){
            Thread.yield();
        }
    }


}
