package com.algorithm.future.lock;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class SemaphoreTest {
    public static void main(String[] args){
        //定义两个车位
        Semaphore semaphore=new Semaphore(2);
        //定义了六个车
        for(int i=1;i<=6;i++){
                    new Thread(()->{
                        try {
                            //这里车子会请求一个车位,车位减1
                            semaphore.acquire();
                            System.out.println(Thread.currentThread().getName()+"抢到车位");
                            //占领这个车位3秒钟
                            try{ TimeUnit.SECONDS.sleep(3);} catch(InterruptedException e){ e.printStackTrace();}
                            System.out.println(Thread.currentThread().getName()+"释放车位");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }finally {
                            //这里车子会释放一个车位,车位加1
                            semaphore.release();

                        }


                    }, String.valueOf(i)).start();
                }
    }
}
