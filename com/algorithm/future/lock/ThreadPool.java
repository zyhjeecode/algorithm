package com.algorithm.future.lock;

import java.util.concurrent.*;

/**
 * @description: 线程池创建
 * @author: zyh
 * @create: 2021-01-27 22:04
 **/
public class ThreadPool {
    ExecutorService threadPool =null;

    public ThreadPool() {
        this.threadPool = new ThreadPoolExecutor(
                        2,
                        5,
                        1,
                        TimeUnit.SECONDS,
                        new LinkedBlockingQueue<Runnable>(3),
                        Executors.defaultThreadFactory()
                        , new ThreadPoolExecutor.CallerRunsPolicy());;
    }
}
