package com.algorithm.future.lock;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @description: 不安全对象
 * @author: zyh
 * @create: 2021-01-28 23:12
 **/
public class Why_1 {

    public static void main(String[] args) {
        List list=new ArrayList();
        for (int i = 0; i < 400; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            },String.valueOf(i)).start();
        }
    }
}
