package com.algorithm.future.lock;

import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {
    public static void main(String[] args){

        CyclicBarrier cb=new CyclicBarrier(7,()->{
            System.out.println("七颗龙珠到达"+"召唤神龙");
        });

        for(int i=1;i<=7;i++){
                    new Thread(()->{
                        System.out.println("收集到第"+Thread.currentThread().getName()+"颗龙珠");
                        try {
                            cb.await();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        System.out.print("第"+Thread.currentThread().getName()+"颗龙珠消耗了  ");
                    }, String.valueOf(i)).start();
                }
        }
}
