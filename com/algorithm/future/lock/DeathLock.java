package com.algorithm.future.lock;

import java.util.concurrent.TimeUnit;

/**
 * @description: 死锁模拟
 * @author: zyh
 * @create: 2021-01-28 20:54
 **/
class DeathLock implements Runnable{
    private  String ziyuanA;
    private  String ziyuanB;

    public DeathLock(String lockA, String lockB) {
        this.ziyuanA = lockA;
        this.ziyuanB = lockB;
    }


    @Override
    public void run() {
        synchronized (ziyuanA){
            System.out.println(Thread.currentThread().getName()+"持有"+ ziyuanA +"尝试获得"+ziyuanB);
            //暂停线程//给另外一个线程时间去锁定另一个资源//若不锁定可能
            try{
                TimeUnit.SECONDS.sleep(5);
            }catch(InterruptedException e){
                e.printStackTrace();
            }

            //若这里获得不到ziyuanB将会进入等待
            synchronized (ziyuanB){
                System.out.println(Thread.currentThread().getName()+"持有"+ziyuanB+"尝试获得"+ ziyuanA);
            }

        }
    }
}

class DeathThread {
    public static void main(String[] args){
        String lockA="zylockAA";
        String lockB="zylockBB";
        new Thread(new DeathLock(lockA, lockB),"线程a").start();
        new Thread(new DeathLock(lockB, lockA),"线程b").start();
    }
}

/**
 * 死锁是指两个或两个以上的进程在执行过程中，因争夺资源而造成的一种互相等待的现象，
 * 若无外力干涉那它们都将无法推进下去，如果系统资源充足，进程的资源请求都能够得到满足，
 * 死锁出现的可能性就很低，否则就会因争夺有限的资源而陷入死锁。
 */

/**
 造成死锁的原因
 - 系统资源不足
 - 进程运行推进的顺序不合适
 - 资源分配不当
 */
