package com.algorithm.future.lock;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @description:
 * @author: zyh
 * @create: 2021-08-31 20:20
 **/
public class NotifyTest {
    public static SimpleDateFormat matter = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

    public static void main(String[] args) {
        final long timeMillis = System.currentTimeMillis();
        final Date date = new Date();

        for (int i = 0; i < 122; i++) {
            try {
                new Thread(()->{
                    System.out.println(matter.format(date));
                }).start();
            }catch (Exception exce){
                System.out.println(exce);
            }
        }
    }
}
