package com.algorithm.future.lock;

import java.util.concurrent.CountDownLatch;

public class CountDownLauchTest {
    public static void main(String[] args){

        CountDownLatch countDownLatch=new CountDownLatch(7);

        for(int i=1;i<=7;i++){
            new Thread(()->{
                System.out.println("国家"+Thread.currentThread().getName()+"   " +"灭了");
                countDownLatch.countDown();
                }, String.valueOf(i)).start();
        }

        new Thread(()->{
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"统一天下"); },"秦国").start();
    }

}
