package com.algorithm.future.format;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @description: 时间格式转换bug复现
 * @author: zyh
 * @create: 2021-12-30 14:52
 **/
public class FormatBug2 {
    public static void main(String[] args) {
        final Date strDate = new Date();
        System.out.println("当前时间: " + strDate);
        DateFormat formatUpperCase = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("yyyy-MM-dd 格式: " + formatUpperCase.format(strDate));
        formatUpperCase = new SimpleDateFormat("YYYY-MM-dd");
        System.out.println("YYYY/MM/dd 格式: " + formatUpperCase.format(strDate));
    }
}
