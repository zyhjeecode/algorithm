package com.algorithm.future.format;

//import org.apache.commons.lang3.text.StrSubstitutor;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: 字符串占位符替换
 * @author: zyh
 * @create: 2022-01-12 16:28
 **/
public class StrRepeat {
    public static void main(String[] args) {
        Map<String, Object> values = new HashMap<>();
        values.put("changeTime", LocalDateTime.now());
        values.put("changeScore", 10);
        values.put("changeRemark", "测试积分变动");
        //这里要引入阿帕奇lang3包哟
//        StrSubstitutor sub = new StrSubstitutor(values, "{{", "}}");
//        String result = sub.replace("消费积分：\n" +
//                "当前积分值：{{curScore}}\n  积分变动时间：{{changeTime}}\n 变动积分值：{{changeScore}}\n   积分类型：{{scoreType}}\n   积分变更备注：{{changeRemark}}\n  消费金额：{{money}}\n     消费商户：{{shop}}\n   3.cn/102r0n-iD3.cn/1-02r0m2q");
//        System.out.println(result);
    }
}
