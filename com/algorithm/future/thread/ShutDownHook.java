package com.algorithm.future.thread;

/**
 * @description: 异常钩子函数
 * @author: zyh
 * @create: 2021-08-26 20:40
 **/
public class ShutDownHook {
    ShutDownHook(){
        System.out.println("初始化一个钩子函数,用于服务宕机后执行一些服务保障措施");

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("数据库宕机了兄弟,还原数据");
            System.out.println("记录操作日志");
        }));
    }
}
