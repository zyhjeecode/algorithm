package com.algorithm.future.thread;


import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

public class ManyTheadAdd {
      static   int a=0;
    public static void main(String[] args) throws InterruptedException {
        CyclicBarrier cb=new CyclicBarrier(4000);
        for(int i=1;i<=4000;i++){
            new Thread(()->{
                a++;
                System.out.print(a);
            }).start();
        }
        //暂停线程
        try{ TimeUnit.SECONDS.sleep(2);} catch(InterruptedException e){ e.printStackTrace();}
        System.out.println();
        System.out.println(a);
    }
}
