package com.algorithm.future.thread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @description: 多线程循环打印问题
 * @author: zyh
 * @create: 2021-09-07 11:38
 **/
public class ThreadsPrintOrder_m2 {
    static int count=10;
    public static void main(String[] args) {
        LockOrderPrint lockOrder=new LockOrderPrint();
        new Thread(()->{
            for (int i = 0; i <count ; i++) {
                lockOrder.printA();
            }
        }).start();

        new Thread(()->{
            for (int i = 0; i <count ; i++) {
                lockOrder.printB();
            }
        }).start();

        new Thread(()->{
            for (int i = 0; i <count ; i++) {
                lockOrder.printC();
            }
        }).start();
    }
}

class LockOrderPrint {
    private static Integer flag = 0;
    private static String[] strs = new String[]{"A", "B", "C"};
    private static int count = 10;

    Lock lock=new ReentrantLock();
    Condition conditionA=lock.newCondition();
    Condition conditionB=lock.newCondition();
    Condition conditionC=lock.newCondition();

    public void printA(){
        lock.lock();
        try {
            if (flag%3!=0){
                try {
                    conditionA.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("A");
            flag++;
            conditionB.signal();

        }catch (Exception exce){
            exce.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public void printB(){
        lock.lock();
        try {
            if (flag%3!=1){
                try {
                    conditionB.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("B");
            flag++;
            conditionC.signal();
        }catch (Exception exce){
            exce.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public void printC(){
        lock.lock();
        try {
            if (flag%3!=2){
                try {
                    conditionC.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("C");
            flag++;
            conditionA.signal();
        }catch (Exception exce){
            exce.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

}
