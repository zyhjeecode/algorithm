package com.algorithm.future.thread;

import java.util.concurrent.TimeUnit;

public class DeathLock {
    static String s1="key1";
    static String s2="key2";
    public static void main(String[] args){
        new Thread(()->{
            synchronized (s1){
                //暂停线程
                try{ TimeUnit.SECONDS.sleep(1);} catch(InterruptedException e){ e.printStackTrace();}
                System.out.println(Thread.currentThread().getName()+"尝试获得锁2...");
                synchronized (s2){
                }
            }
        }).start();
        new Thread(()->{
            synchronized (s2){
                try{ TimeUnit.SECONDS.sleep(3);} catch(InterruptedException e){ e.printStackTrace();}
                System.out.println(Thread.currentThread().getName()+"尝试获得锁1...");
                synchronized (s1){
                }
            }
        }).start();

    }
}
