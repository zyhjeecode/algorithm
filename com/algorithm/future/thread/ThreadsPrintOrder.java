package com.algorithm.future.thread;

/**
 * @description: 多线程循环打印问题
 * @author: zyh
 * @create: 2021-09-07 11:38
 **/
public class ThreadsPrintOrder {
    private static Integer curr = 0;
    private static String[] strs = new String[]{"A", "B", "C"};
    private static int count = 10;
    static  Object lock=new Object();

    public static void main(String[] args) {
        final Thread a = getThread("A");
        final Thread b = getThread("B");
        final Thread c = getThread("C");

        a.start();
        b.start();
        c.start();
    }

    private static Thread getThread(String threadName) {
        return new Thread(() -> {
            synchronized (lock) {
                for (int i = 0; i < count; i++) {
                    while (!strs[curr%strs.length].equals(threadName)) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(Thread.currentThread().getName());
                    curr++;
                    lock.notifyAll();
                }
            }
        }, threadName);
    }
}
