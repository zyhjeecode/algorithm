package com.algorithm.future.thread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

public class TwoThreadCount {
    static volatile int  a=1;
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CountDownLatch  countDownLatch=new CountDownLatch(a);
//        ExecutorService threadPool = Executors.newCachedThreadPool();
        int o1,o2;
        {

//            o1 = (int) threadPool.submit(new callThread(20)).get();
//            o2 = (int) threadPool.submit(new callThread(25)).get();
            countDownLatch.countDown();
        }
        countDownLatch.await();
//        System.out.println(o1+o2);
    }
}
