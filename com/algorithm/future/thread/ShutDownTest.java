package com.algorithm.future.thread;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * @description: 宕机函数
 * @author: zyh
 * @create: 2021-08-26 20:42
 **/
public class ShutDownTest {
    public static void main(String[] args) {
        List<Integer> res = getIntegers(4094);
//        res.stream().forEach(System.out::println);

        System.out.println(LocalDateTime.now());
    }

    private static List<Integer> getIntegers(Integer nums) {
        final String s = Integer.toBinaryString(nums);
        List<Integer> res=new LinkedList<>();
        final char[] chars = s.toCharArray();
        for (int curr = 0,len=chars.length; curr <len ; curr++) {
            if (chars[curr]=='1'){
                res.add(((Double)Math.pow(2, len-curr-1)).intValue());
            }
        }
        return res;
    }

    public void doSomething() {
        System.out.println("做一些业务操作");
    }
}
