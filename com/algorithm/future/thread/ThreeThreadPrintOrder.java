package com.algorithm.future.thread;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: 三个线程顺序打印问题
 * @author: zyh
 * @create: 2021-07-24 15:30
 **/
public class ThreeThreadPrintOrder {
    //打印次数
    public static void main(String[] args) {
        final ThreadLocal threadLocal = new ThreadLocal<>();
        Map<String, Object> map=new HashMap<>();
        map.put("name","zyh");
        map.put("age",19);
        threadLocal.set(map);

        final Map<String, Object> res = (Map<String, Object>) threadLocal.get();
        res.entrySet().forEach(item->{
        });
    }
}

