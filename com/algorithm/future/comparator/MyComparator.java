package com.algorithm.future.comparator;

import java.util.Comparator;

public class MyComparator implements Comparator<people> {
    @Override
    public int compare(people p1, people p2) {
        return p1.age-p2.age;
    }
}
