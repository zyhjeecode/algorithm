package com.algorithm.future.offer;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class EntryNodeOfLoop {
    public class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }
    }

    //题目描述
    //给一个链表，若其中包含环，请找出该链表的环的入口结点，否则，输出null。
    public ListNode EntryNodeOfLoop(ListNode pHead) {
        Set<ListNode> set = new HashSet<>();
        ListNode curr = pHead;
        while (curr != null) {
            if (set.contains(curr))
                return curr;
            set.add(curr);
            curr = curr.next;
        }
        return null;
    }

    public ListNode EntryNodeOfLoop2(ListNode pHead) {
        if (pHead == null || pHead.next == null) {
            return null;
        }
        //找出环中相遇的点
        ListNode p1 = pHead; //每次走一步的结点
        ListNode p2 = pHead;//每次走两步的结点
        ListNode pmeet = null;
        while (p2 != null && p2.next != null) { //p2比p1跑的快,如果没有环的话p2先挂掉,所以只判断p2就可以了
            p1 = p1.next;
            p2 = p2.next.next;
            if (p1 == p2) {
                pmeet = p1;
                break;
            }
        }
        if (pmeet == null) {//没环,没有相遇
            return null;
        }
        //如果有环.p1从头走,p2从相遇结点走,下一次相遇就是那个环结点.
        p1 = pHead;
        p2 = pmeet;
        while (p1 != p2) {
            p1 = p1.next;
            p2 = p2.next;
        }
        return p1;
    }

    @Test
    public void test() {
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(2);
        l1.next.next = new ListNode(3);
        l1.next.next.next = new ListNode(4);
        l1.next.next.next.next = new ListNode(5);
        l1.next.next.next.next.next = l1.next.next;
        System.out.println(EntryNodeOfLoop(l1).val);
        System.out.println(EntryNodeOfLoop2(l1).val);
    }

}
