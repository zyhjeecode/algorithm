package com.algorithm.future.offer;


public class HuanQian {
    public static void main(String[] args){  
        //一百块钱换 1,2,5,10一共有多少换法?
        //动态规划+全排列问题,每次都可以取任意一张,如果最后值大于100结束,等于100,加1
        int res=getCount();
        System.out.println(res);
    }

    private static int  getCount() {
        int res=0;
        for(int i10=0;i10<=10;i10++){
            for(int i5=0;i5<=20;i5++){
                for(int i2=0;i2<=50;i2++){
                    for(int i1=0;i2<=100;i1++){
                    if (i10*10+i5*5+i2*2+i1==100){
                        res++;
                    }
                    }
                }
            }
        }
      return res;
    }
}
