package com.algorithm.future.offer;

import org.junit.Test;

import java.util.ArrayList;

//num1,num2分别为长度为1的数组。传出参数
//将num1[0],num2[0]设置为返回结果
public class FindNumsAppearOnce {
    public void FindNumsAppearOnce(int [] array,int num1[] , int num2[]) {
        if (array==null||array.length==0){
            return;
        }
        ArrayList<String> list=new ArrayList<>();
        for(int i=0;i<array.length;i++){
            if (!list.contains(array[i]+"")){
                list.add(array[i]+"");
            }else {
                list.remove(array[i]+"");
            }
        }
        num1[0]= Integer.parseInt(list.get(0));
        num2[0]=Integer.parseInt(list.get(1));
    }


    @Test
    public void test(){
        int[] arr=new  int[]{1,1,2,5,2,8,7,8,7,6};
        int[] num1=new int[1];
        int[] num2=new int[1];
        FindNumsAppearOnce(arr,num1,num2);
        System.out.println(num1[0]+"  "+num2[0]);
    }

}
