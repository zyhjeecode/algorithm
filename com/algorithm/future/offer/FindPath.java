package com.algorithm.future.offer;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FindPath {
    public class TreeNode {
        int val = 0;
        TreeNode left = null;
        TreeNode right = null;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public ArrayList<ArrayList<Integer>> FindPath(TreeNode root, int target) {
        ArrayList<ArrayList<Integer>> arrs = new ArrayList<>();
        ArrayList<Integer> arr = new ArrayList<>();
        addPath(root, target, 0, arr, arrs);
        Collections.sort(arrs, new Comparator<ArrayList<Integer>>() {
            @Override
            public int compare(ArrayList<Integer> arr1, ArrayList<Integer> arr2) {
                return arr2.size()-arr1.size();
            }
        });
        return arrs;
    }


    public void addPath(TreeNode CurrNode, int target, int sum, ArrayList<Integer> arr, ArrayList<ArrayList<Integer>> arrs) {
        if (CurrNode==null){
            return;
        }
        sum+=CurrNode.val;
        arr.add(CurrNode.val);
        if (CurrNode.left == null&&CurrNode.right==null){
            if (sum==target){
                arrs.add(new ArrayList<>(arr));
                arr.remove(arr.size()-1);
            }else {
                arr.remove(arr.size()-1);
                return;
            }
        }else {
            addPath(CurrNode.left, target, sum, arr, arrs);
            addPath(CurrNode.right, target, sum, arr, arrs);
            arr.remove(arr.size()-1);
        }

    }

    @Test
    public void test() {
        TreeNode head=new TreeNode(5);
        head.left=new TreeNode(4);
        head.left.left=new TreeNode(3);
        head.left.left.left=new TreeNode(2);
        head.left.left.left.left=new TreeNode(1);
        ArrayList<ArrayList<Integer>> arrs = FindPath(head, 15);
        for (ArrayList<Integer> arr: arrs
             ) {
            for (Integer i:arr
                 ) {
                System.out.print(i+" ");
            }
            System.out.println();
        }
    }
    



}
