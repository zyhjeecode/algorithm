package com.algorithm.future.offer;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TwoThreadAdd {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Future<Integer> submit1 = executorService.submit(() -> 1 + 2);
        Future<Integer> submit2 = executorService.submit(() -> 1 + 3);
        Integer integer1 = submit1.get();
        Integer integer2 = submit2.get();
        System.out.println(integer1+integer2);
    }
}
