package com.algorithm.future.offer;

import org.junit.Test;

public class GetNEqualAAddB {
    public void printNequalAB(int[] arr,int N){
        //数组A由1000W个随机正整数(int)组成，设计算法，给定整数n，
        // 在A中找出符合如下等式:n=a+b的a和b，说明算法思路以及时间复杂度是多少?
        if (arr==null||N<0){
            return;
        }

        int[] temp=new int[N+1]; //可以放0~N上闭区间的数
        for(int i=0;i<arr.length;i++){
            if (arr[i]<=N){
                if (temp[arr[i]]!=1){
                    temp[arr[i]]=1;
                }
            }
        }//向temp里注入小于N的数,以其数字大小找到其位置


        for(int i=0;i<temp.length;i++){
            if (temp[i]==1&&temp[N-i]==1&&(i<N-i)){//(i<N-i) 去重处理
                System.out.println(i+" "+(N-i));
            }
        }
    }

    @Test
    public void test() {
        int[] arr=new int[]{2,5,1,4,7,6,8,4};
        printNequalAB(arr,10);
    }

}
