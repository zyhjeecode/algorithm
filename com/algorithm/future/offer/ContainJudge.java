package com.algorithm.future.offer;

import org.junit.Test;

public class ContainJudge {
    //给定字符串s1=aboo，s2=awiedobao 判断s2是否包含s1(用例返回true)，能否再优化？
    // 提示使用滑动窗口
    public boolean judge(String s1,String s2){
        boolean res=false;
        if (s1==null||s2==null||s1.length()>s2.length()){
            return res;
        }
        //s2=awied obao  9  s1=aboo 4
        for(int i=0;i<=s2.length()-s1.length();i++){
            int index=i;
            for(int j=0;j<s1.length();j++){
                if (s2.charAt(index)==s1.charAt(j)){
                    index++;
                }else {
                    break;
                }
            }
            if (index==i+4){
                return true;
            }
        }

        return res;
    }

    @Test
    public void test() {
        String s1="iedo",s2="awiedobao";
        boolean judge = judge(s1, s2);
        System.out.println(judge);
    }

}
