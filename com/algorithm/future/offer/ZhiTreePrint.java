package com.algorithm.future.offer;

import org.junit.Test;

import java.util.*;

public class ZhiTreePrint {
    public class TreeNode {
        int val = 0;
        TreeNode left = null;
        TreeNode right = null;
        public TreeNode(int val) {
            this.val = val;
        }
    }

        public ArrayList<ArrayList<Integer>> Print(TreeNode pRoot) {
            ArrayList<ArrayList<Integer>> datas=new ArrayList<>();
            if (pRoot==null){
                return datas;
            }
            Stack<TreeNode> stack1=new Stack<>(); //存放奇数层结点
            stack1.push(pRoot);
            Stack<TreeNode> stack2=new Stack<>();//存放偶数层结点
            boolean leftToRight=true;//奇数层--从左到右
            while (!stack1.isEmpty()||!stack2.isEmpty()){  //只要树没有遍历完
                ArrayList<Integer> data=new ArrayList<>();
                if (leftToRight==true){ //如果是奇数层
                    //将结点从左到右加入偶数栈
                    while (!stack1.isEmpty())
                    {
                        TreeNode node = stack1.pop(); //取队栈顶元素
                        if (node.left!=null){
                            stack2.push(node.left);
                        }
                        if (node.right!=null){
                            stack2.push(node.right);
                        }
                        data.add(node.val);
                    }
                    leftToRight = false;
                }else {
                    while (!stack2.isEmpty())
                    {
                        TreeNode node = stack2.pop(); //取队栈顶元素
                        if (node.right!=null){
                            stack1.push(node.right);
                        }
                        if (node.left!=null){
                            stack1.push(node.left);
                        }
                        data.add(node.val);
                    }
                    leftToRight=true;
                }
                datas.add(data);
            }
            return datas;
    }

        @Test
        public void test(){
            TreeNode head = new TreeNode(5);
            head.left = new TreeNode(3);
            head.right = new TreeNode(8);
            head.left.left = new TreeNode(2);
            head.left.right = new TreeNode(4);
            head.left.left.left = new TreeNode(1);
            head.right.left = new TreeNode(7);
            head.right.left.left = new TreeNode(6);
            head.right.left.left.left = new TreeNode(1151);
            head.right.right = new TreeNode(10);
            head.right.right.left = new TreeNode(9);
            head.right.right.right = new TreeNode(11);
            Print(head);
        }


}
