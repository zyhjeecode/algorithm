package com.algorithm.future.offer;

import org.junit.Test;

public class JumpFloorII {
    //题目描述
    //一只青蛙一次可以跳上1级台阶，也可以跳上2级……它也可以跳上n级。求该青蛙跳上一个n级的台阶总共有多少种跳法。
    public int jumpFloorII(int target) {
        if (target <= 0) {
            return 1;
        }else {
        int res = 0;
        for (int i = 0; i <= target - 1; i++) {//可能从0~target-1上任一位置跳过来
            res += jumpFloorII(i);
        }
        return res;
        }
    }

    
    @Test
    public void test() {
        System.out.print(jumpFloorII(3));
        
    }
    

}