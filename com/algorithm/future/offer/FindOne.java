package com.algorithm.future.offer;

import org.junit.Test;

public class FindOne {

    public static int find1From2(int[] a){
        int len = a.length, res = 0;
        for(int i = 0; i < len; i++){
            res = res ^ a[i];
        }
        return res;
    }

    @Test
    public void test(){
        int[] arr=new int[]{2 ,1 ,3 ,2 ,1};
        int from2 = find1From2(arr);
        System.out.println(0^5^1^1);
    }


}
