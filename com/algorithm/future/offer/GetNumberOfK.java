package com.algorithm.future.offer;

import org.junit.Test;

public class GetNumberOfK {
    //题目描述
    //统计一个数字在排序数组中出现的次数
    public int GetNumberOfK(int[] array, int k) {
        if (array == null) {
            return 0;
        }
        if (array.length==1&&array[0]==k){
            return 1;
        }
        int firstKIndex = getFirstKIndex(array, k);
        int lastKIndex = getLastKIndex(array, k);
        return firstKIndex==lastKIndex?0:lastKIndex-firstKIndex+1;
    }
    public int getFirstKIndex(int[] array, int k){//得到第一个k---右结点向左移动
        int left = 0, right = array.length - 1;
        int mid = getMid(left, right);
        while (left <= right) {
            if (array[mid] == k&&mid - 1 >= 0 && array[mid - 1] == k) {
                right=mid-1;
            } else if (array[mid]> k) {
                right = mid-1;
            } else if (array[mid] < k) {
                left = mid+1;
            }else {
                return mid;
            }
            mid = getMid(left, right);
        }
        return -1;
    }
    public int getLastKIndex(int[] array, int k){//得到第一个k---右结点向左移动
        int left = 0, right = array.length - 1;
        int mid = getMid(left, right);
        while (left <= right) {
            if (array[mid] == k&&mid + 1<=array.length-1&& array[mid + 1] == k) {
                left=mid+1;
            } else if (array[mid]> k) {
                right = mid-1;
            } else if (array[mid] < k) {
                left = mid+1;
            }else {
                return mid;
            }
            mid = getMid(left, right);
        }
        return -1;
    }

    public int getMid(int left, int right) {
        return (left + right) / 2;
    }

    @Test
    public void test() {
        int[] arr = new int[]{3};
        int i = GetNumberOfK(arr, 3);
        System.out.println(i);
    }

}
