package com.algorithm.future.offer;

public class NumberOf1 {
    public  static int numberOf1(int num){
        int count=0;
        while (num!=0){
            count++;
            num=num&(num-1);
        }
        return count;
    }

    public  static int method2(int num){
        int count=0;
        char[] chars = Integer.toBinaryString(num).toCharArray();
        for (char c:chars ) {
            if (c=='1'){
                count++;
            }
        }
        return count;
    }

    // for com.algorithm.com.algorithm.leetcode.leetcode.test  产生随机数组
    public static int[] generateRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random());
        }
        return arr;
    }

    // for com.algorithm.com.algorithm.leetcode.leetcode.test 拷贝得到一样的数组
    public static int[] copyArray(int[] arr) {
        if (arr == null) {
            return null;
        }
        int[] res = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            res[i] = arr[i];
        }
        return res;
    }

    // for com.algorithm.com.algorithm.leetcode.leetcode.test 比较两个数组的值是否完全一致
    public static boolean isEqual(int[] arr1, int[] arr2) {
        if ((arr1 == null && arr2 != null) || (arr1 != null && arr2 == null)) {
            return false;
        }
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1.length != arr2.length) {
            return false;
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    // for com.algorithm.com.algorithm.leetcode.leetcode.test 打印数组
    public static void printArray(int[] arr) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    /*// for com.algorithm.com.algorithm.leetcode.leetcode.test
    public static void main(String[] args) {
        int testTime = 500000;  //设置比较验证次数
        int maxSize = 20;//设置测试随机数组最大长度
        int maxValue = 100;//设置数组内值的最大值
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = generateRandomArray(maxSize, maxValue);//得到随机数组
            int[] arr2 = copyArray(arr1); //得到随机数组拷贝份
            InsertionSort.InsertionSort(arr1); //用自己的排序算法排序
            com.algorithm.com.algorithm.leetcode.leetcode.comparator(arr2);//用绝对正确的方法排序
            if (!isEqual(arr1, arr2)) { //验证自己的算法和绝对正确的算法得到的结果是否相同
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");

        int[] arr = generateRandomArray(maxSize, maxValue);
        printArray(arr);//排序前打印
        InsertionSort.InsertionSort(arr);//排序
        printArray(arr);//打印排序后结果
    }*/
    //交换数组array上,ab两个位置的值
    public static void swap(int [] array,int a,int b){ //a,b为数组下标
        int temp=array[a];
        array[a]=array[b];
        array[b]=temp;
    }
    public static void main(String[] args){
        int[] array = generateRandomArray(100, 200);
        int[] arrry2=new int[array.length];
        int[] arrry3=new int[array.length];
        for(int i=0;i<array.length;i++){
            int i1 = numberOf1(array[i]);
            int i2 = method2(array[i]);
            arrry2[i]=i1;
            arrry3[i]=i2;
        }
        boolean equal = isEqual(arrry2, arrry3);
        System.out.println(equal);
    }
}
