package com.algorithm.future.offer;


import com.sun.deploy.cache.MemoryCache;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Joseph {
    public int joseph(int n, int m) {
        //约瑟夫问题是一个非常著名的趣题，即由n个人坐成一圈，按顺时针由1开始给他们编号。
        // 然后由第一个人开始报数，数到m的人出局。现在需要求的是最后一个出局的人的编号。
        //给定两个int n和m，代表游戏的人数。请返回最后一个出局的人的编号。保证n和m小于等于1000。
        //测试样例：
        //5 3
        //返回：4
        if (n <= 0||m<=0) {
            return -1;
        }
        List<Integer> list=new ArrayList<>();
        for(int i=1;i<=n;i++){
            list.add(i);
        }
        //2,1,4,5,6
        int curr=0;//当前位置
        while (list.size()>1){
            int delIndex=(curr+m-1)%list.size();
            list.remove(delIndex);
            curr=delIndex% list.size();
        }
        return list.remove(0);
    }
    
    @Test
    public void test() {
        int n=5;
        int m=3;
        System.out.println(joseph(5,3));
    }
    
}
