package com.algorithm.future.offer;


import org.junit.Test;

public class deleteDuplication {
    public class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }
    }

    // 链表1->2->3->3->4->4->5 处-理后为 1->2->3->4->5
    public ListNode deleteDuplication(ListNode pHead) //保留一个重复元素
    {
        if (pHead == null) {
            return pHead;
        }
        ListNode pre = pHead;
        ListNode curr = pHead.next;
        while (curr != null) {
            if (curr.val == pre.val) {//如果当前结点的值和前一结点重复
                pre.next = curr.next;
                curr = pre.next;
            } else {
                pre = curr;
                curr = curr.next;
            }
        }
        return pHead;
    }

    //{1,2,3,3,4,4,5} 处-理后为 {1,2,5}
    public ListNode deleteDuplication2(ListNode pHead) //重复元素一个都不保留
    {
        if (pHead == null) {
            return pHead;
        }
        ListNode headpre=new ListNode(0);//设headpre
        headpre.next=pHead;
        //{0,1,2,3,3,4,4,5}
        ListNode preNotParall = headpre;//记录上一个不重复的结点
        ListNode pre = pHead;
        ListNode curr = pHead.next;
        while (curr != null) {
            if (curr.val == pre.val) {//如果当前结点的值和前一结点重复
                //继续往下找,直到当前结点和前一结点值不同
                while (curr!=null&&curr.val == pre.val){
                    curr=curr.next;
                }
               preNotParall.next=curr;
               if (curr!=null){
                   pre=curr;
                   curr=pre.next;
               }else {
                   return headpre.next;
               }
            } else {
                preNotParall.next=pre;
                preNotParall=pre;
                pre = curr;
                curr = curr.next;
            }
        }
        return headpre.next;
    }

    @Test
    public void tt() {
        //{1,2,3,3,4,4,5}---{1,2,5}
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(3);
        head.next.next.next.next = new ListNode(4);
        head.next.next.next.next.next = new ListNode(4);
        head.next.next.next.next.next.next = new ListNode(5);
        ListNode curr = deleteDuplication2(head);
        while (curr != null) {
            System.out.print(curr.val + " ");
            curr = curr.next;
        }

    }


}
