package com.algorithm.future.offer;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class MoreThanHalfNum_Solution {
    //数组中有一个数字出现的次数超过数组长度的一半，请找出这个数字。
    // 例如输入一个长度为9的数组{1,2,3,2,2,2,5,4,2}。
    // 由于数字2在数组中出现了5次，超过数组长度的一半，因此输出2。如果不存在则输出0。
    public int MoreThanHalfNum_Solution(int [] array) {
        int survivor=array[0];
        int count=1;
        for(int i=1;i<array.length;i++){
            if (array[i]==survivor)
                count++;
            else if (array[i]!=survivor){
                count--;
            }
          //当count=0，保存下一个数
            if ( count == 0 ){
                survivor = array [i];
                count = 1 ;
            }
        }
        count=0;
        for(int i=0;i<array.length;i++){
            if (array[i]==survivor){
             count++;
            }
        }
        if (count>array.length/2){
            return survivor;
        }else {
            return 0;
        }
    }
    @Test
    public void test() {
        int[] arr=new int[]{1,2,2,1,3};
        int i = MoreThanHalfNum_Solution(arr);
        System.out.println(i);
    }
    
}
