package com.algorithm.future.offer;

import org.junit.Test;

public class reOrderArray {
    //题目描述
    //输入一个整数数组，实现一个函数来调整该数组中数字的顺序，
    //使得所有的奇数位于数组的前半部分，所有的偶数位于数组的后半部分。
    // 并保证奇数和奇数，偶数和偶数之间的相对位置不变。
    public void reOrderArray(int [] array) {
        int index=-1;
        //从前面进行奇数的插入排序
        for(int i=1;i<array.length;i++){
            int j=i;
            while ((array[j]&1)!=0&&(array[j-1]&1)==0){
                swap(array,j,j-1);
                j--;
            }
        }
    }

    private void swap(int[] array, int index, int i) {
        int temp=array[index];
        array[index]=array[i];
        array[i]=temp;
    }

    @Test
    public void test() {
        int[] arr=new int[]{2,4,3,2,5,7,8,9};
        reOrderArray(arr);
        for (int i:arr
             ) {
            System.out.print(i+" ");
        }
    }

}
