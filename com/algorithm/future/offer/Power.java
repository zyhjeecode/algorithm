package com.algorithm.future.offer;

public class Power {
    public static double Power(double base, int exponent) {
        double res = 1.0;
        if (base == 0)
            return 0;
        if (exponent == 0)
            return 1;
        if (exponent < 0)
            base = 1.0 / base;
        while (exponent!=0){
            res *= base;
            exponent = (exponent>0) ? (--exponent) : (++exponent);
        }
        return res;
    }
    public static void main(String[] args){
        System.out.println(Power(1.12,3));
    }
}
