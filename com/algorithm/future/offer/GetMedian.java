package com.algorithm.future.offer;

import org.junit.Test;

import java.util.Comparator;
import java.util.PriorityQueue;

public class GetMedian {
    public class myComperator implements Comparator<Integer>{
        @Override //从大到小排序
        public int compare(Integer o1, Integer o2) {
            return o2-o1;
        }
    }
   //          ~~~~~~~~Maxheap   minheap~~~~~
    PriorityQueue<Integer> minHeap=new PriorityQueue<>();//根最小
    PriorityQueue<Integer> MaxHeap=new PriorityQueue<>(new myComperator());//根最大
    int count=0;
    public void Insert(Integer num) {
        if ((count&1)==0){//偶数,放小根堆
            minHeap.offer(num);
            MaxHeap.offer(minHeap.poll());
        }else {
            MaxHeap.offer(num);
            minHeap.offer(MaxHeap.poll());
        }
        count++;
    }

    public Double GetMedian() {
        return (count&1)==0?new Double((minHeap.peek() + MaxHeap.peek())+"")/2:new Double(MaxHeap.peek()+"");
    }


    
}
