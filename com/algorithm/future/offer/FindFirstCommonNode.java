package com.algorithm.future.offer;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class FindFirstCommonNode {
    public class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }
    }
    public ListNode FindFirstCommonNode(ListNode pHead1, ListNode pHead2) {
        if (pHead1==null||pHead2==null){
            return null;
        }
        ListNode node1=pHead1;
        Set<ListNode> set=new HashSet<>();
        while (node1!=null){
            set.add(node1);
            node1=node1.next;
        }
        ListNode node2=pHead2;
        while (node2!=null){
            if (set.contains(node2)){
                return node2;
            }
                node2=node2.next;
        }
    return null;
    }


    public ListNode FindFirstCommonNode2(ListNode pHead1, ListNode pHead2) {
        ListNode p1=pHead1;
        ListNode p2=pHead2;
        while(p1!=p2){
            p1= p1==null?pHead2:p1.next;
            p2= p2==null?pHead1:p2.next;
        }
        return p1;
    }

    @Test
    public void test(){
        ListNode n1=new ListNode(1);
        ListNode n2=new ListNode(2);
        ListNode n3=new ListNode(3);
        ListNode n4=new ListNode(4);
        ListNode n5=new ListNode(5);
        n1.next=n2;
        n2.next=n3;
        n4.next=n2;
        n5.next=n4;
        ListNode node = FindFirstCommonNode2(n1, n5);
        System.out.println(node.val);

    }

}
