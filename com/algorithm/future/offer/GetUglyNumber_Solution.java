package com.algorithm.future.offer;

import org.junit.Test;

import java.util.ArrayList;

public class GetUglyNumber_Solution {
    //题目描述
    //把只包含质因子2、3和5的数称作丑数（Ugly Number）。
    //例如6、8都是丑数，但14不是，因为它包含质因子7。 习惯上我们把1当做是第一个丑数。求按从小到大的顺序的第N个丑数。
    public int GetUglyNumber_Solution(int index) {
        if(index<=0)return 0;
        ArrayList<Integer> list=new ArrayList<Integer>();
        list.add(1);
        int i2=0,i3=0,i5=0;
        while (list.size()<index){
            int min = Math.min(Math.min(list.get(i2)*2, list.get(i3)*3), list.get(i5)*5);
            list.add(min);
            if (min==list.get(i2)*2){
                i2++;
            }
            if (min==list.get(i3)*3){
                i3++;
            }
            if (min==list.get(i5)*5){
                i5++;
            }
        }
        return list.get(list.size()-1);
    }

    @Test
    public void test() {
        int j = GetUglyNumber_Solution(7);
        System.out.println(j);

    }


}
