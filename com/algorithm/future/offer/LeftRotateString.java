package com.algorithm.future.offer;

import org.junit.Test;

public class LeftRotateString {
    public String LeftRotateString(String str,int n) {
        if (str==null||str.length()<=1||n==str.length()){
            return str;
        }
       int move=n%str.length();//"abcd"
        return str.substring(move)+str.substring(0,move);
    }

    public String LeftRotateString2(String str,int n) {
        if (str==null||str.length()<=1||n==str.length()){
            return str;
        }
        // abcde  5
        // 移1和移6位一样的效果
        int move=n>str.length()?n-str.length():n;
        StringBuffer sb=new StringBuffer();
        for(int i=move;i<str.length();i++){
            sb.append(str.charAt(i));
        }
        for(int i=0;i<move;i++){
            sb.append(str.charAt(i));
        }
        return sb.toString();
    }
    @Test
    public void test(){
        String s = LeftRotateString("abcde", 5);
        System.out.println(s);


    }
    
}
