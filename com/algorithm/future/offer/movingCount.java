package com.algorithm.future.offer;

import org.junit.Test;

public class movingCount {
    /*
    题目描述
    地上有一个m行和n列的方格。一个机器人从坐标0,0的格子开始移动，每一次只能向左，右，上，下四个方向移动一格，
    但是不能进入行坐标和列坐标的数位之和大于k的格子。
    例如，当k为18时，机器人能够进入方格（35,37），因为3+5+3+7 = 18。
    但是，它不能进入方格（35,38），因为3+5+3+8 = 19。
    请问该机器人能够达到多少个格子？
    */
    public int movingCount(int threshold, int rows, int cols) {
        boolean [][] isState= new boolean [rows][cols]; //记录是否走过
        int sum=help( threshold,  rows,  cols,0,0,isState);
        return sum;
    }
    public int help(int k, int m, int n,int r,int c,boolean [][] isState){
        if (numSum(r)+numSum(c)>k){//如果该点数为和大于k
            return 0;
        }
        if (r<0||r>=m||c<0||c>=n){ //如果该点不在方格范围之内
            return 0;
        }
        if (!isState[r][c]){
            isState[r][c]=true;
               return help(k,m,n,r,c+1,isState)
                +help(k,m,n,r,c-1,isState)
                +help(k,m,n,r+1,c,isState)
                +help(k,m,n,r-1,c,isState)
                +1
                ;
        }else {
            return 0;
        }

    }
    public int numSum(int num){
        int res=0;
        while (num!=0){
            res+= num % 10;
            num=num/10;
        }
        return res;
    }
    
    @Test
    public void test() {
        int num = movingCount(15, 4, 6);
        System.out.println(num);

    }
    
}
