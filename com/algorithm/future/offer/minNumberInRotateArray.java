package com.algorithm.future.offer;

public class minNumberInRotateArray {
    public int minNumberInRotateArray(int [] array) {
        int res=0;
        if (array.length==0){
            return res;
        }
        if (array.length==1){
            return array[0];
        }

 //       4~5 1~3
        //最小值在较小的后半段
        int left=0;
        int right=array.length-1;
        while (left<right){
        int mid=left+(right-left)/2;
            if (array[mid]>array[right]){ //中值大于右值
                left=mid;
            }else if (array[mid]==array[right]){//中值等于右值
                right = right - 1;
            }else {//中值小于右值
                right=mid;
            }
        }
        return array[left];
    }
}
