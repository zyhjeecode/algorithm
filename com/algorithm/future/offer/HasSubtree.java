package com.algorithm.future.offer;

import org.junit.Test;

public class HasSubtree {
    /*输入两棵二叉树A，B，判断B是不是A的子结构。*/
    public class TreeNode {
        int val = 0;
        TreeNode left = null;
        TreeNode right = null;

        public TreeNode(int val) {
            this.val = val;
        }
    }
    public boolean HasSubtree(TreeNode root1,TreeNode root2) {
        boolean sign=false;
        if (root2==null||root1==null){
            return false;
        }
        if (root1.val==root2.val){
            sign=judgeChild(root1,root2);
        }
        if (!sign){
           return HasSubtree(root1.left,root2)||HasSubtree(root1.right,root2);
        }
           return sign;
    }

    private boolean judgeChild(TreeNode root1, TreeNode root2) {
        if (root1==null&&root2!=null){
            return false;
        }
        if (root2==null){
            return true;
        }
        if (root1.val!=root2.val){
            return false;
        }
     return judgeChild(root1.left,root2.left)&&judgeChild(root1.right,root2.right);

    }
    
    @Test
    public void test() {
        TreeNode head=new TreeNode(8);
        head.left=new TreeNode(8);
        head.right=new TreeNode(7);
        head.left.left=new TreeNode(9);
        head.left.right=new TreeNode(3);
        TreeNode othreHead=new TreeNode(8);
        othreHead.left=new TreeNode(9);
        othreHead.right=new TreeNode(2);
        boolean res = HasSubtree(head, othreHead);
        System.out.println(res);
    }
    
}
