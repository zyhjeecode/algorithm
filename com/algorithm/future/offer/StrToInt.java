package com.algorithm.future.offer;

import org.junit.Test;

import java.security.interfaces.RSAKey;

public class StrToInt {
    //输入一个字符串,包括数字字母符号,可以为空
    //输出描述:
    //如果是合法的数值表达则返回该数字，否则返回 0
    public int StrToInt(String str) {
        if (str==null||str.length()==0){
            return 0;
        }
        int sign=1;
        long res=0;
       if (str.charAt(0)=='+'||str.charAt(0)=='-'){//首位判断
           sign=str.charAt(0)=='+'?1:-1;
           int j=1;
           while (j<str.length()){
               if (str.charAt(j)>48&&str.charAt(j)<=57){
                   res=res*10+(str.charAt(j)-48);
                   j++;
               }else {
                   return 0;
               }
           }
       }else if (str.charAt(0)>48&&str.charAt(0)<=57){//0的acsii码十进制为48 9为57
           res=str.charAt(0)-48;
           int i=1;
           while (i<str.length()){
               if (str.charAt(i)>48&&str.charAt(i)<=57){
                   res=res*10+str.charAt(i)-48;
                   i++;
               }else {
                   return 0;
               }
           }
       }
       if (sign==-1){
           res=-res;
       }
       if(res>Integer.MAX_VALUE||res<Integer.MIN_VALUE){
           return 0;
       }else {
           return Integer.parseInt(res+"");
       }

     //   return sign*res;
    }
    
    @Test
    public void test(){
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
        System.out.println("_______________________________");
        System.out.println(StrToInt("-2147483648"));
    }
    
}
