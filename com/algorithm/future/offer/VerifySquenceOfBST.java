package com.algorithm.future.offer;

import org.junit.Test;

public class VerifySquenceOfBST {
    /*
    输入一个整数数组，判断该数组是不是某二叉搜索树的后序遍历的结果。
    如果是则输出Yes,否则输出No。假设输入的数组的任意两个数字都互不相同。
    */
    public boolean VerifySquenceOfBST(int [] sequence) {
        if(sequence.length==0)
            return false;
        if(sequence.length==1)
            return true;
        return judge(sequence, 0, sequence.length-1);
    }
    public boolean judge(int [] sequence,int start,int end){
        if (start>=end){
            return true;
        }
        int leftEnd=end-1;
        while (sequence[leftEnd]>sequence[end]&&leftEnd>start)
            leftEnd--;
        /*if (leftEnd==start){
            return true;
        }*/
        for(int i=start;i<=leftEnd;i++){
            if (sequence[i]>sequence[end]){
                return false;
            }
        }
        return judge(sequence,start,leftEnd)&&judge(sequence,leftEnd+1,end-1);
    }

    @Test
    public void test(){
        int[] arr=new int[]{15,16,17,18,41,42,55,58,25};
        System.out.println(VerifySquenceOfBST(arr));
    }
}
