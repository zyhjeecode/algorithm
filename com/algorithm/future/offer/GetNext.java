package com.algorithm.future.offer;

public class GetNext {
    private class TreeLinkNode {
        int val;
        TreeLinkNode left = null;
        TreeLinkNode right = null;
        TreeLinkNode next = null;

        TreeLinkNode(int val) {
            this.val = val;
        }
    }
    public TreeLinkNode GetNext(TreeLinkNode pNode)
    {
        if (pNode==null){//如果结点为空
            return null;
        }
        if (pNode.right!=null)//如果该结点有右孩子树,找到右孩子的最左叶子结点
        {
            TreeLinkNode node=pNode.right;
            while (node.left!=null){
                node=node.left;
            }
            return node;
        }
        //如果没有右孩子
        TreeLinkNode node=pNode;
        while (node.next!=null){
            //若节点是其父节点的左孩子，则其后继就是该节点
            if (node.next.left==pNode){
                return node.next;
            }
            //若节点是其父节点的右孩子，则继续向上找,如果到根了也可以停止
            node=node.next;
        }
        //右子树为空时,依次找父节点，直至根节点
        while(pNode.next != null){
            //若节点是其父节点的左孩子，则其后继就是 父节点
            if(pNode.next.left == pNode){
                return pNode.next;
            }
            pNode = pNode.next;
        }



        return null;
    }
}
