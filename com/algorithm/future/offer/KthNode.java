package com.algorithm.future.offer;

import java.util.LinkedList;
import java.util.Queue;

public class KthNode {
    public class TreeNode {
        int val = 0;
        TreeNode left = null;
        TreeNode right = null;

        public TreeNode(int val) {
            this.val = val;
        }

        //题目描述
        //给定一棵二叉搜索树，请找出其中的第k小的结点。
        //例如，（5，3，7，2，4，6，8） 中，按结点数值大小顺序第三小结点的值为4。
        TreeNode KthNode(TreeNode pRoot, int k) {
            if (pRoot == null || k == 0)
                return null;
            Queue<TreeNode> queue = new LinkedList<>();
            midGet(pRoot, queue);
            int count=0;
            while (!queue.isEmpty()&&count!=k-1){
                count++;
                queue.poll();
            }
            if (queue.isEmpty()){
                return null;
            }
            return queue.poll();
        }

        private void midGet(TreeNode pRoot, Queue<TreeNode> queue) {
            if (pRoot == null) {
                return;
            }
            midGet(pRoot.left,queue);
            queue.offer(pRoot);
            midGet(pRoot.right,queue);
        }
    }
}
