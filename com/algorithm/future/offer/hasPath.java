package com.algorithm.future.offer;

import org.junit.Test;

public class hasPath {
    //题目描述
    // 请设计一个函数，用来判断在一个矩阵中是否存在一条包含某字符串所有字符的路径。
    // 路径可以从矩阵中的任意一个格子开始，每一步可以在矩阵中向左，向右，向上，向下移动一个格子。
    // 如果一条路径经过了矩阵中的某一个格子，则该路径不能再进入该格子。
    // 例如 a b c e s f c s a d e e 矩阵中包含一条字符串"bcced"的路径
    // 但是矩阵中不包含"abcb"路径，因为字符串的第一个字符b占据了矩阵中的第一行第二个格子之后，路径不能再次进入该格子。
    public boolean hasPath(char[] matrix, int rows, int cols, char[] str)
    {
        int[] flags=new int[matrix.length]; //用1标志走过
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                boolean res = helper(matrix, rows, cols, i, j, 0, str, flags);
                if (res) return true;
            }
        }
        return false;
    }
    //k指当前匹对的字串下标
    public boolean helper(char[]matrix,int rows,int cols,int i,int j,int k,char[]str,int[]flag){
        int curr=i*cols+j;
        if (i<0||i>=rows||j<0||j>=cols||flag[curr]==1||matrix[curr]!=str[k]) return false;
        if (k==str.length-1) return true;

        flag[curr]=1;

        if (helper(matrix,rows,cols,i+1,j,k+1,str,flag)||helper(matrix,rows,cols,i-1,j,k+1,str,flag)||
                helper(matrix,rows,cols,i,j+1,k+1,str,flag)||helper(matrix,rows,cols,i,j-1,k+1,str,flag)
        )return true;

        flag[curr]=0;
        return false;
    }
    
    @Test
    public void test() {

        char[] charss=new char[]{'a','b','c','d'};
        char[] chars=new char[]{'a','b','s'};
        boolean b = hasPath("ABCESFCSADEE".toCharArray(),3,4,"ABCCED".toCharArray());
        System.out.print(b);

    }
    
}
