package com.algorithm.future.offer;

public class TreeDepth {
    public static class TreeNode {
        int val = 0;
        TreeNode left = null;
        TreeNode right = null;

        public TreeNode(int val) {
            this.val = val;

        }

    }
    //按层遍历;取层数
    public static int TreeDepth(TreeNode root) {
        if (root==null){
            return 0;
        }
        int depth=0;
        int leftDepth = TreeDepth(root.left);
        int rightDepth=TreeDepth(root.right);
        return depth=leftDepth>rightDepth?leftDepth+1:rightDepth+1;

    }
}