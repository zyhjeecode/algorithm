package com.algorithm.future.offer;

import org.junit.Test;

import java.util.ArrayList;

public class maxInWindows {
    //给定一个数组和滑动窗口的大小，找出所有滑动窗口里数值的最大值
    public ArrayList<Integer> maxInWindows(int [] num, int size)
    {
        ArrayList<Integer> arr=new ArrayList<>();
        if(size==0)
            return arr;
        if (num.length < size) {
            return arr;
        }
        int count=0;
        int max=Integer.MIN_VALUE;
        for(int i=0;i<=num.length-size;i++){
            int j=i;
            while (count!=size){
                count++;
                max=max>num[j]?max:num[j];
                j++;
            }
            arr.add(max);
            max=Integer.MIN_VALUE;
            count=0;
        }
        return arr;
    }
    @Test
    public void test() {
        int[] arr=new int[]{1,3,5,7,9,11,13,15};//[7,9,11,13,15]
        ArrayList<Integer> arrayList = maxInWindows(arr, 4);
        for ( Integer i:arrayList
             ) {
            System.out.print(i+" ");
        }

    }

}
