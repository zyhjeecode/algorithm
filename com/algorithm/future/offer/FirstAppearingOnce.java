package com.algorithm.future.offer;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FirstAppearingOnce {
    //Insert one char from stringstream
    Map<Character,Integer> map=new HashMap<>();
    List<Character> list=new ArrayList<>();
    public void Insert(char ch)
    {
        if (map.containsKey(ch)){
            map.put(ch,map.get(ch)+1);
        }else {
            map.put(ch,1);
        }
        list.add(ch);
    }
    //return the first appearence once char in current stringstream
    public char FirstAppearingOnce()
    {
        for (char c:list
             ) {
            if (map.get(c)==1){
                return c;
            }
        }
        return '#';
    }

}
