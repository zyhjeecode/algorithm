package com.algorithm.future.offer;



        import org.junit.Test;

        import java.util.LinkedList;
        import java.util.Queue;
        import java.util.Stack;

public class SerializeAndDeserializeTree {


    public class TreeNode {
        int val = 0;
        TreeNode left = null;
        TreeNode right = null;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    //序列化时通过 某种符号表示空节点（#）
    // 以 ！ 表示一个结点值的结束（value!）。

    //先序遍历序列化,将遍历结果转换为字符串
    String Serialize(TreeNode root) {
        if (root==null){
            return "#!";
        }
        String res=root.val+"!";
        res+=Serialize(root.left);
        res+=Serialize(root.right);
        return res;
    }
    //先序遍历重建二叉树
    TreeNode Deserialize(String str) {
        String[] nodes = str.split("!");
        if (nodes[0].equals("#")){//如果是空树,直接返回null
            return null;
        }
        Queue<String> queue=new LinkedList<>();
        for (String s:nodes
        ) {
            queue.offer(s);
        }

        return ReBuildPreTree(queue);
    }

    private TreeNode ReBuildPreTree(Queue<String> queue) {
        String nodeVal = queue.poll();
        if (nodeVal.equals("#")){
            return null;
        }
        TreeNode node= new TreeNode(Integer.parseInt(nodeVal));
        node.left=ReBuildPreTree(queue);
        node.right=ReBuildPreTree(queue);
        return node;
    }
    @Test
    public void test(){
        TreeNode head = new TreeNode(1);
        head.left = new TreeNode(2);
        head.right = new TreeNode(3);
        head.left.left = new TreeNode(4);
        head.right.right = new TreeNode(5);
        String serializeTree = Serialize(head);
        TreeNode node = Deserialize(serializeTree);
        preOrderPrint(node);

    }

    public static void  preOrderPrint(TreeNode head){
        if (head!=null){
            Stack<TreeNode> stack=new Stack<>();
            stack.push(head);
            while (!stack.isEmpty()){
                head = stack.pop();
                System.out.print(head.val);
                if (head.right!=null){
                    stack.push(head.right);
                }
                if (head.left!=null){
                    stack.push(head.left);
                }
            }
        }
    }

}
