package com.algorithm.future.offer;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class leverArrsPrintTree {
    public class TreeNode {
        int val = 0;
        TreeNode left = null;
        TreeNode right = null;
        public TreeNode(int val) {
            this.val = val;
        }
    }
    //题目描述
    //从上到下按层打印二叉树，同一层结点从左至右输出。每一层输出一行。

    ArrayList<ArrayList<Integer>> Print(TreeNode pRoot) {
        ArrayList<ArrayList<Integer>> arrs=new ArrayList<>();
        if (pRoot==null){
            return arrs;
        }
        Queue<TreeNode> queue1=new LinkedList<>();
        Queue<TreeNode> queue2=new LinkedList<>();
        queue1.offer(pRoot);
        while (!queue1.isEmpty()||!queue2.isEmpty()){
            ArrayList<Integer> arr=new ArrayList<>();
            if (!queue1.isEmpty())
            {
                while (!queue1.isEmpty()){
                    TreeNode node=queue1.poll();
                    if (node.left!=null){
                        queue2.offer(node.left);
                    }
                    if (node.right!=null){
                        queue2.offer(node.right);
                    }
                    arr.add(node.val);
                }
            }else
            {
                while (!queue2.isEmpty()){
                    TreeNode node=queue2.poll();
                    if (node.left!=null){
                        queue1.offer(node.left);
                    }
                    if (node.right!=null){
                        queue1.offer(node.right);
                    }
                    arr.add(node.val);
                }
            }
            arrs.add(arr);
        }
        return arrs;
    }

    @Test
    public void test(){
        TreeNode head = new TreeNode(5);
        head.left = new TreeNode(3);
        head.right = new TreeNode(8);
        head.left.left = new TreeNode(2);
        head.left.right = new TreeNode(4);
        head.left.left.left = new TreeNode(1);
        head.right.left = new TreeNode(7);
        head.right.left.left = new TreeNode(6);
        head.right.left.left.left = new TreeNode(1151);
        head.right.right = new TreeNode(10);
        head.right.right.left = new TreeNode(9);
        head.right.right.right = new TreeNode(11);
        Print(head);
    }
}
