package com.algorithm.future.offer;

import org.junit.Test;

import java.util.Stack;

public class OtherStack {
    //定义栈的数据结构
    //请在该类型中实现一个能够得到栈中所含最小元素的min函数（时间复杂度应为O（1））。
    int Curmin=Integer.MAX_VALUE;
    Stack<Integer> dataStack=new Stack<>();
    Stack<Integer> minStack=new Stack<>();
    public void push(int node) {
        dataStack.push(node);
        Curmin=Curmin<node?Curmin:node;
        minStack.push(Curmin);
    }
    public void pop() {
        dataStack.pop();
        minStack.pop();
    }

    public int top() {
        minStack.pop();
        return dataStack.peek();
    }

    public int min() {
        return minStack.peek();
    }

    @Test
    public void test(){
        OtherStack otherStack=new OtherStack();
        otherStack.push(5);
        System.out.print(otherStack.min());
        otherStack.push(4);
        System.out.print(otherStack.min());
        otherStack.push(6);
        System.out.print(otherStack.min());
        otherStack.push(3);
        System.out.print(otherStack.min());
        otherStack.push(1);
        System.out.print(otherStack.min());

    }

}
