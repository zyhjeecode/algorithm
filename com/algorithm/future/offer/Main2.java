package com.algorithm.future.offer;

import java.util.*;

public class Main2 {
   static class baozi{
        int index;
        int xiandu;
        int down;

        public baozi(){
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public int getXiandu() {
            return xiandu;
        }

        public void setXiandu(int xiandu) {
            this.xiandu = xiandu;
        }

        public int getDown() {
            return down;
        }

        public void setDown(int down) {
            this.down = down;
        }
    }
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int m=sc.nextInt();
        PriorityQueue<baozi> xianQueue=new PriorityQueue<>(new Comparator<baozi>() {
            @Override
            public int compare(baozi o1, baozi o2) {
                if (o2.xiandu-o1.xiandu==0){
                    return o2.down-o1.down;
                }
                return o2.xiandu-o1.xiandu;
            }
        });
        PriorityQueue<baozi> temp=new PriorityQueue<>(new Comparator<baozi>() {
            @Override
            public int compare(baozi o1, baozi o2) {
                if (o2.xiandu-o1.xiandu==0){
                    return o2.down-o1.down;
                }
                return o2.xiandu-o1.xiandu;
            }
        });
        //包子进来
        int i=0;
        while (i<n){
            baozi baozi=new baozi();
            baozi.setXiandu(sc.nextInt());
            baozi.setDown(sc.nextInt());
            baozi.setIndex(i);
            xianQueue.add(baozi);
            i++;
        }
        //吃包子 (当天也要吃)
        int day=0;
        int canEat=n;//能吃的包子初始值为n;
        while (day<=m){
            int count=0;
            boolean preOver=false;//是否提前结束
            //每天先吃两个
            while (count!=2){
                count++;
                baozi baozi = xianQueue.poll();
                if (baozi.getXiandu()>0){
                    baozi.setXiandu(-1);
                    canEat--;
                    temp.add(baozi);
                }else {
                    temp.add(baozi);
                    //如果每天吃的包子都是过期的或者已经吃过的就没必要过下面的天了.
                    preOver=true;
                    break;
                }
            }
            if(preOver==true||canEat<m){ //若提前结束,直接转移包子,和交给哆啦A梦检查.
                while (!xianQueue.isEmpty())
                {
                    temp.add(xianQueue.poll());
                }
                cheack(temp,canEat,m);
                break;
            }

            //吃剩下的包子要下降鲜度
            while (!xianQueue.isEmpty())
            {
                baozi baozi=xianQueue.poll();
                if (baozi.getXiandu()>0){ //如果没被吃或者没过期
                    int xiandu=baozi.getXiandu()-baozi.getDown()>0?baozi.getXiandu()-baozi.getDown():0;
                    baozi.setXiandu(xiandu);
                }
                if (baozi.getXiandu()<=0){
                    canEat--;
                }
                temp.add(baozi);
            }
            PriorityQueue<baozi> dd=new PriorityQueue<>(new Comparator<baozi>() {
                @Override
                public int compare(baozi o1, baozi o2) {
                    if (o2.xiandu-o1.xiandu==0){
                        return o2.down-o1.down;
                    }
                    return o2.xiandu-o1.xiandu;
                }
            });
            xianQueue=temp;
            temp=dd;

            day++;
        }
    }

    private static void cheack(PriorityQueue<baozi> xianQueue,int canEat,int m) {
       //首先判断是否发怒
        if (canEat<m){
       //创建一个以序号排列的队列,并显示
            PriorityQueue<baozi> pringQueue=new PriorityQueue<>(new Comparator<baozi>() {
                @Override
                public int compare(baozi o1, baozi o2) {
                    return o1.getIndex()-o2.getIndex();
                }
            });

            //装包子
            while (!xianQueue.isEmpty())
            {
                pringQueue.add(xianQueue.poll());
            }
            //打印
            while (!pringQueue.isEmpty())
            {
                System.out.println(pringQueue.poll().xiandu);
            }
        }
        return;
    }
}
