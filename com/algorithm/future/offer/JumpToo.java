package com.algorithm.future.offer;

public class JumpToo {
    //假设还有n阶要跳,那么这一次有n种跳法  1 2 3...n
    public static int getCountJump(int target){
        if (target==0){
            return 1;
        }
        else {
        int sum=0;
        for(int i=1;i<=target;i++){
            sum+=getCountJump(target-i);
        }
        return sum;
        }
    }
    public static void main(String[] args){
        System.out.println(getCountJump(1));
    }
}
