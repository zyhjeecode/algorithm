package com.algorithm.future.offer;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class AllPollStack {
    public void getAll() {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] nums = new int[num];
        for (int i = 0; i < num; i++) {
            nums[i] = sc.nextInt();
        }
        ArrayList<int[]> list = null;
        allSort(nums, 0, list);
        Iterator<int[]> iterator = list.iterator();
        while (iterator.hasNext()){
            for (int i:iterator.next()
                 ) {
                System.out.print(i+" ");
            }
            System.out.println();
        }
    }

    private void allSort(int[] nums, int index, ArrayList<int[]> list) {
        if (index == nums.length - 1) {
            list.add(nums);
            return;
        } else {
            for (int i = index; i < nums.length; i++) {
                swap(nums, index,i );
                allSort(nums,index+1,list);
                swap(nums, index,i );
            }
        }
    }

    public void swap(int[] nums, int indexa, int indexb) {
        int temp = nums[indexa];
        nums[indexa] = nums[indexb];
        nums[indexb] = temp;
    }
    
    public void  judge(ArrayList<int[]> list ){
        
    }

    @Test
    public void test(){
        getAll();
    }

}
