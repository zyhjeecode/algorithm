package com.algorithm.future.offer;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    //给你一根长度为n的绳子，请把绳子剪成整数长的m段（m、n都是整数，n>1并且m>1），每段绳子的长度记为k[0],k[1],...,k[m]。
    //请问k[0]xk[1]x...xk[m]可能的最大乘积是多少？
    public static int cutRope(int target) {
        //对于大于3的数在分割都是可以分为a个2和b个3相加的,比如4=2*2,5=2+3,6=3+3
        if (target < 2)
            return 0;
        if (target==2){//1,1
            return 1;
        }
        if (target==3){//1,2
            return 2;
        }
        if (target%3==2){
            return (int) (Math.pow(3,target/3)*2);//剩一个2
        }
        if (target%3==1){
            return (int) (Math.pow(3,target/3-1)*2*2);//省一个1话,我们而已少算一个3,算两个2相乘,因为3*1<2*2
        }
        return (int)Math.pow(3, target/3);//可以分为全是3的情况
    }

    public static void main(String[] args) throws IOException {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        System.out.println(cutRope(n));
    }

}
