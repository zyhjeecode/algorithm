package com.algorithm.future.offer;

public class Sum_Solution {
    public static int Sum_Solution(int n) {
        if (n<=0){
            return 0;
        }
        if (n==1){
            return 1;
        }
        return n+Sum_Solution(n-1);
    }
    public static void main(String[] args){
        int sum_solution = Sum_Solution(3);
        System.out.println(sum_solution);
    }
}
