package com.algorithm.future.offer;

import org.junit.Test;

public class ReverseSentence {
   // 题目描述
    //牛客最近来了一个新员工Fish，每天早晨总是会拿着一本英文杂志，写些句子在本子上。
   // 同事Cat对Fish写的内容颇感兴趣，有一天他向Fish借来翻看，但却读不懂它的意思。
   // 例如，“student. a am I”。
   // 后来才意识到，这家伙原来把句子单词的顺序翻转了，正确的句子应该是“I am a student.”。
   // Cat对一一的翻转这些单词顺序可不在行，你能帮助他么？
    public String ReverseSentence(String str) {
        if (str==null){
            return null;
        }
        if (str.length()==1){
            return str;
        }
        String[] s = str.split(" ");
        String res="";
        for(int i=s.length-1;i>=0;i--){
            res+=Reverse(s[i]);
            if (i>0){
                res+=" ";
            }
        }
        return res;
    }

    private String Reverse(String s) {
        if (s.charAt(0)<'a'||s.charAt(0)>'z'){
            s=s.substring(1)+s.substring(0,1);
        }
        return s;
    }


    @Test
    public void test() {
        System.out.println(ReverseSentence("student. a am I"));
    }

}
