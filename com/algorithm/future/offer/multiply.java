package com.algorithm.future.offer;

import org.junit.Test;

import java.lang.ref.SoftReference;

public class multiply {
    //题目描述
    //给定一个数组A[0,1,...,n-1],请构建一个数组B[0,1,...,n-1],
    //其中B中的元素B[i]=A[0]*A[1]*...*A[i-1]*A[i+1]*...*A[n-1]。
    // 不能使用除法
    
    //意思意思即B(i)等于A数组中除了A(i)以外的各个数相乘
    public int[] multiply(int[] A) {
        int[] B=new int[A.length];
        if (A==null||A.length==0){
            return B;
        }
        B[0]=1;
        //每个B上的数B(i)分左右俩个部分且每个左部分等于上个数的左部分B(i-1)*A(i-1) 且B[0]左边没数,设为1
        //左部分
        for(int i=1;i<=A.length-1;i++){
            B[i]=B[i-1]*A[i-1];
        }

        //每个B上的数B(i)分左右俩个部分且每个左部分等于上个数的左部分B(i-1)*A(i-1) 且B[0]左边没数,设为1
        //求出右部分以及左部分*右部分
        int temp=1;//初始值为B[n]的右部分
        for(int i=A.length-2;i>=0;i--){
            temp=temp*A[i+1];
            B[i]=B[i]*temp;
        }
        return B;
    }
    
    @Test
    public void test() {
        int[] arr=new int[]{1,2,3,4,5};
        //[120,60,40,30,24]
        int[] multiply = multiply(arr);
        for (Integer i: multiply
             ) {
            System.out.print(i+" ");
        }
    }
    
}
