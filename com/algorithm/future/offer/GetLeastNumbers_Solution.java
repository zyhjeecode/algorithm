package com.algorithm.future.offer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class GetLeastNumbers_Solution {

    //输入n个整数，找出其中最小的K个数。
    // 例如输入4,5,1,6,2,7,3,8这8个数字，则最小的4个数字是1,2,3,4,。
    public static ArrayList<Integer> GetLeastNumbers_Solution(int [] input, int k) {
        ArrayList<Integer> list=new ArrayList<Integer>();
        if (input==null){
            return list;
        }
        if (input.length<k||k==0){
            return list;
        }
        PriorityQueue<Integer> maxHeap=new PriorityQueue<Integer>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });
        //2,41,5,2,5,3,4,23
        for(int i=0;i<input.length;i++){
            if (i<k){
                maxHeap.add(input[i]);
            }
            else  if (maxHeap.peek()>input[i]){
                maxHeap.poll();
                maxHeap.add(input[i]);
                }
            }


        while (!maxHeap.isEmpty())
        {
            list.add(maxHeap.poll());
        }
        return list;
    }

    public static void main(String[] args){
        int[] arr=new  int[]{2,41,5,2,5,3,4,23};
        ArrayList<Integer> integers = GetLeastNumbers_Solution(arr, 4);
        System.out.println(integers.toString());
    }
}
