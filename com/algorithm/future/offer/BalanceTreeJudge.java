package com.algorithm.future.offer;

import org.junit.Test;

public class BalanceTreeJudge {
    public static class TreeNode {
        public int value;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int data) {
            this.value = data;
        }
    }

    public boolean IsBalanced_Solution(TreeNode root) {
        if (root==null){
            return true;
        }
        return Math.abs(getDepth(root.left)-getDepth(root.right))>1?false:true&&IsBalanced_Solution(root.left)&&IsBalanced_Solution(root.right);
    }
    public int getDepth(TreeNode node){
        if (node==null){
            return 0;
        }
        return Math.max(getDepth(node.left),getDepth(node.right))+1;
    }

    
    @Test
    public void te(){
        TreeNode head = new TreeNode(5);
        head.left = new TreeNode(3);
        head.right = new TreeNode(8);
        head.left.left = new TreeNode(2);
        head.left.right = new TreeNode(4);
        head.left.left.left = new TreeNode(1);
        head.right.left = new TreeNode(7);
        head.right.left.left = new TreeNode(6);
        head.right.left.left.left = new TreeNode(1151);
        head.right.left.left.left.right = new TreeNode(1151);
        head.right.right = new TreeNode(10);
        head.right.right.left = new TreeNode(9);
        head.right.right.right = new TreeNode(11);
        System.out.println(IsBalanced_Solution(head));

    }
    
    
}
