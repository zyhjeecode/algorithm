package com.algorithm.future.enumtest;

/**
 * @description:
 * @author: zyh
 * @create: 2022-01-14 14:48
 **/
public class EnumTest {
    public static void main(String[] args) {
        for (int i = 0; i < ArgsSource.values().length; i++) {
            System.out.println(ArgsSource.values()[i]);
        }
    }


    enum ArgsSource {
        Base(1L, new SystemArg[]{SystemArg.MallId}),
        MallBase(2L, new SystemArg[]{SystemArg.MallName, SystemArg.MallPhone}),
        UserBase(3L, new SystemArg[]{SystemArg.UserNickName, SystemArg.UserMobile, SystemArg.Gender, SystemArg.UserName,}),
        memberBase(4L, new SystemArg[]{SystemArg.curCard, SystemArg.Bonus, SystemArg.UserCard, SystemArg.UserCardAfterFour}),
        opBase(5L, new SystemArg[]{SystemArg.GZH});

        /**
         * 枚举值
         */
        private final Long code;
        private final SystemArg[] systemArgs;


        public Long getCode() {
            return code;
        }


        public SystemArg[] getSystemArgs() {
            return systemArgs;
        }

        /**
         * 构造器
         */
        ArgsSource(Long code, SystemArg[] systemArgs) {
            this.code = code;
            this.systemArgs = systemArgs;
        }

        public static ArgsSource getEnum(int code) {
            for (ArgsSource e : ArgsSource.values()) {
                if (e.getCode() == code) {
                    return e;
                }
            }
            throw new IllegalArgumentException();
        }

    }
}
