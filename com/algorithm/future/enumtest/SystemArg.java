package com.algorithm.future.enumtest;


import java.util.Arrays;
import java.util.List;

/**
 * 系统参数枚举类
 *
 * @author zyh
 * @date 2021/7/9
 */
public enum SystemArg {
    /**
     * 商场编号 不对外提供
     */
    MallId(1L, "mallId", "商场编号", "10000", false, Arrays.asList("thing", "number", "character_string")),

    /**
     * 商场名称
     */
    MallName(2L, "mallName", "商场名称", "猫酷体验商场", true, Arrays.asList("thing", "name", "number", "character_string")),

    /**
     * 客服电话
     */
    MallPhone(3L, "mallPhone", "客服电话", "65512321", true, Arrays.asList("thing", "phone_number")),

    /**
     * 公众号名称
     **/
    GZH(4L, "GZH", "公众号名称", "猫酷体验商场", true, Arrays.asList("thing", "character_string", "name", "phrase")),


    /**
     * 用户昵称
     */
    UserNickName(5L, "userNickName", "用户昵称", "王某某", true, Arrays.asList("thing", "name", "phrase","character_string")),

    /**
     * 用户手机号
     **/
    UserMobile(6L, "UserMobile", "用户手机号", "17621232100", true, Arrays.asList("thing", "number", "character_string", "phone_number")),

    /**
     * 先生/女士
     */
    Gender(7L, "Gender", "先生/女士", "先生", true, Arrays.asList("thing", "name", "phrase", "character_string")),

    /**
     * 会员名称
     */
    UserName(8L, "userName", "会员姓名", "陈女士", true, Arrays.asList("thing", "name", "phrase", "character_string")),

    /**
     * 会员卡等级
     **/
    curCard(9L, "curCard", "会员卡等级", "金卡", true, Arrays.asList("thing", "name", "phrase", "character_string")),
    /**
     * 当前积分数
     **/
    Bonus(10L, "Bonus", "当前积分数", "1000", true, Arrays.asList("thing", "character_string","number","amount")),
    /**
     * 卡号
     */
    UserCard(11L, "userCard", "会员卡号", "20210010310375", true, Arrays.asList("thing", "number", "letter", "character_string", "name")),

    /**
     * userCardAfterFour
     */
    UserCardAfterFour(12L, "userCardAfterFour", "会员卡号后四位", "1374", true, Arrays.asList("thing", "number", "letter", "character_string", "name"));

    /**
     * 枚举值
     */
    private final Long code;
    private final String label;
    private final String displayName;
    private final String defaultValue;
    private final Boolean canSelect;
    private final List<String> ruleTypes;


    public Long getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public Boolean getCanSelect() {
        return canSelect;
    }

    public List<String> getRuleTypes() {
        return ruleTypes;
    }

    /**
     * 构造器
     */
    SystemArg(Long code, String label, String displayName, String defaultValue, Boolean canSelect, List<String> ruleTypes) {
        this.code = code;
        this.defaultValue = defaultValue;
        this.label = label;
        this.displayName = displayName;
        this.canSelect = canSelect;
        this.ruleTypes = ruleTypes;
    }


    public static SystemArg getEnum(int code) {
        for (SystemArg e : SystemArg.values()) {
            if (e.getCode() == code) {
                return e;
            }
        }

        throw new IllegalArgumentException();
    }

}
