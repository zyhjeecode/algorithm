package com.algorithm.future.threadlocal;

import org.junit.Test;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @description: 线程池使用本地线程的坑
 * @author: zyh
 * @create: 2022-01-14 19:19
 **/
public class ThreadPoolInfluence {
    /**线程变量*/
    private final ThreadLocal<String> threadLocal=new ThreadLocal<>();

    /**线程池*/
    ThreadPoolExecutor threadPool=new ThreadPoolExecutor(2, 2, 10, TimeUnit.SECONDS, new LinkedBlockingDeque<>());



    @Test
    public void enterTest(){
        for (int i = 0; i < 4; i++) {
            threadPool.execute(()->{
                System.out.println(Thread.currentThread().getName()+"直接拿到的值为:"+threadLocal.get());
                threadLocal.set(Thread.currentThread().getName());
                System.out.println(Thread.currentThread().getName()+"修改后的值为:"+threadLocal.get());
                threadLocal.remove();
            });
        }
    }
}
