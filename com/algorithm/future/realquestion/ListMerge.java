package com.algorithm.future.realquestion;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ListMerge { //蘑菇街
   /* 题目描述
    请编写一段代码，实现两个单向有序链表的合并
    输入描述:
    第一行一个链表，如1 2 3 4 5

    第二行一个链表，如2 3 4 5 6
    输出描述:
    输出：1 2 2 3 3 4 4 5 5 6*/
   public static void main(String[] args) throws IOException {
       BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
       String[] l1 = br.readLine().split(" ");
       String[] l2 = br.readLine().split(" ");
       int index1=0;
       int index2=0;
       while (index1<=l1.length-1&&index2<=l2.length-1){
           int num1=Integer.parseInt(l1[index1]);
           int num2=Integer.parseInt(l2[index2]);
           if (num1<num2){
               System.out.print(num1+" ");
               index1++;
           }else {
               System.out.print(num2+" ");
               index2++;
           }
       }
       while (index1<=l1.length-1){
           System.out.print(Integer.parseInt(l1[index1])+" ");
           index1++;
       }
       while (index2<=l2.length-1){
           System.out.print(Integer.parseInt(l2[index2])+" ");
           index2++;
       }
   }
}
