package com.algorithm.future.practice.matrix;

import java.util.ArrayList;
import java.util.List;

public class RotatingSquareMatrix { //90度旋转方针

    public static void Rotate(int[][] SquareMatrix){
        int tR=0;//矩阵左上角的行坐标
        int tC=0;//矩阵左上角的列左边
        int dR=SquareMatrix.length-1;   //矩阵右下角的行坐标
        int dC=SquareMatrix[0].length-1;//矩阵右下角的列左边
        while (tR<=dR&&tC<=dC){
            RotateEdge(SquareMatrix,tR,tC,dR,dC);
            tR++;tC++;
            dR--;dC--;
        }
    }
    //         {   1, 2,  3,  4   },
    //         {   5, 6,  7,  8   },
    //         {   9, 10, 11, 12  },
    //         {   13,14, 15, 16  }
    //          (tR,tC)
    //
    //
    //
    //                      (dR,dC)
    public static void RotateEdge(int[][] sm,int tR,int tC,int dR,int dC){
        List list=new ArrayList();
        for(int i=0;i<dC-tC;i++){
            int temp=sm[tR][tC+i];//temp存上面点的值
            sm[tR][tC+i]= sm[dR-i][tC];//用左边点的值覆盖上面点√
            sm[dR-i][tC]=sm[dR][dC-i];//用下边点的值覆盖左面点√
            sm[dR][dC-i]=sm[tR+i][dC];//用右边点的值覆盖下面点√
            sm[tR+i][dC]=temp;//用临时存储的上边点的值覆盖右面点√
        }
    }
    public static void main(String[] args){
        int [][] matrix = {
                {   1, 2,  3,  4   },
                {   5, 6,  7,  8   },
                {   9, 10, 11, 12  },
                {   13,14, 15, 16  }
        };
        Rotate(matrix);
        for(int i=0;i<matrix.length;i++){
          for(int j=0;j<matrix.length;j++){
              System.out.print(matrix[i][j]+" ");
          }
              System.out.println();
        }
    }

}
