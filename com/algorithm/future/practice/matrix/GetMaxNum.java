package com.algorithm.future.practice.matrix;

import java.util.PriorityQueue;

public class GetMaxNum {
    public static PriorityQueue<Integer> queue=new PriorityQueue<>();
    int result=0;
    public static  void move(int[][] arrM,int x,int y,int result){
       //行 arrM.length-1
       //列 arrM[0].length-1
       //(x,y)
        if (x==arrM.length-1&&y==arrM[0].length-1){
          queue.add(result+arrM[x][y]);
        }
        else if (x==arrM.length-1&&y<arrM[0].length-1){
            move(arrM,x,y+1,result+arrM[x][y]);
        }
        else if (x<arrM.length-1&&y==arrM[0].length-1){
            move(arrM,x+1,y,result+arrM[x][y]);
        }else if (x<arrM.length-1&&y<arrM[0].length-1){
            move(arrM,x,y+1,result+arrM[x][y]);
            move(arrM,x+1,y,result+arrM[x][y]);
        }
    }

    public static  int moveMin(int[][] arrM,int x,int y){
        //行 arrM.length-1
        //列 arrM[0].length-1
        //(x,y)
        if (x==arrM.length-1&&y==arrM[0].length-1){
            return arrM[x][y];
        }
         if (x==arrM.length-1&&y<arrM[0].length-1){
            moveMin(arrM,x,y+1);
        }
         if (x<arrM.length-1&&y==arrM[0].length-1){
            moveMin(arrM,x+1,y);
        }
       int right=moveMin(arrM,x,y+1);
       int down=moveMin(arrM,x+1,y);
       return Math.min(right,down);
    }
    public static void main(String[] args){
        int[][] m = { { 1, 3, 5, 9 }, { 8, 1, 3, 4 }, { 5, 0, 6, 1 }, { 8, 8, 4, 0 } };
        move(m,0,0,0);
        System.out.println(queue.peek());
        int i = moveMin(m, 0, 0);
        System.out.println(i);
    }
}
