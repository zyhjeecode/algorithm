package com.algorithm.future.practice.matrix;

public class  PrintMatrixZigZag{
   public static void printMatrixZigZag(int[][] matrix) {
        //A点
        int aR=0;
        int aC=0;
        //B点
        int bR=0;
        int bC=0;
        int endR=matrix.length-1;//结束行索引
        int endC=matrix[0].length-1;//结束列索引
       boolean upFlag=true;
        while (aC!=endC+1){
            printSlash(matrix,aR,aC,bR,bC,upFlag);
           //{   1, 2,  3,  4   },
           //{   5, 6,  7,  8   },
           //{   9, 10, 11, 12  },
           //{   13,14, 15, 16  }
            if (aR<endR){
                aR++;
            }else {
                aC++;
            }
            if (bC<endC){
                bC++;
            }else {
                bR++;
            }
            upFlag=!upFlag;
        }

    }
    public static void  printSlash(int[][] matrix,int aR,int aC ,int bR,int bC,boolean upFlag){ //打印AB之间斜线上的数字
        if (upFlag){
            int currR=aR;
            int currC=aC;
            for(int i=aR;i>=bR;i--){
                System.out.print(matrix[currR][currC]+" ");
                currR--;
                currC++;
            }
        }else {
            int currR=bR;
            int currC=bC;
            for(int i=bC;i>=aC;i--){
                System.out.print(matrix[currR][currC]+" ");
                currR++;
                currC--;
            }
        }
    }
    public static void main(String[] args){
        int [][] matrix = {
                {   1, 2,  3,  4   },
                {   5, 6,  7,  8   },
                {   9, 10, 11, 12  },
                {   13,14, 15, 16  }
        };
        printMatrixZigZag(matrix);

    }

}
