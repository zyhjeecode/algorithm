package com.algorithm.future.practice.matrix;


import org.junit.Test;

import java.util.ArrayList;

public class PrintEdge {
      ArrayList<Integer> list=new ArrayList<>();
    public  ArrayList<Integer>  spiralOrderPrint(int[][] matrix){
        //       (tR,tC)
        //
        //
        //
        //                        (dR,dC)
        int tR=0;//矩阵左上角的行坐标
        int tC=0;//矩阵左上角的列左边
        int dR=matrix.length-1;   //矩阵右下角的行坐标
        int dC=matrix[0].length-1;//矩阵右下角的列左边
        while (tR<=dR&&tC<=dC){
        prindEdge(matrix,tR,tC,dR,dC);
        tR++;tC++;
        dR--;dC--;
        }
        return list;
    }
    public  void  prindEdge(int[][] matrix, int tR,int tC,int dR,int dC){
        if (tR==dR&&tC==dC){ //同点
        list.add(matrix[tR][tC]);
        }
        if (tR==dR&&tC!=dC)//同行不同列
        {
            for(int i=tC;i<=dC;i++){
                list.add(matrix[tR][i]);
            }
        }
        if (tC==dC&&tR!=dR)//同列不同行
        {
            for(int i=tR;i<=dR;i++){
                list.add(matrix[i][tC]);
            }
        }
        if (tC!=dC&&tR!=dR){//不同列不同行
            //       (tR,tC)
            //
            //
            //
            //                        (dR,dC)
            int currC=tC;
            int currR=tR;
            while (currC!=dC) { //打印上边框
                list.add(matrix[tR][currC]);
                currC++;
            }
            while (currR!=dR){//打印右边框
                list.add(matrix[currR][dC]);
                currR++;
            }
            while (currC!=tC){//打印下边框
                list.add(matrix[currR][currC]);
                currC--;
            }
            while (currR!=tR){//打印左边框
                list.add(matrix[currR][currC]);
                currR--;
            }
        }
    }
    @Test
    public void test(){
        int [][] matrix = {
                {   1,  2,  3,   4,   5   },
                {   6,  7,  8   ,9,   10  },
                {   11, 12, 13,  14,  15  },
                {   16, 17, 18,  19  ,20  },
                {   21, 22 ,23  ,24,  25  }
        };

        ArrayList<Integer> arrayList = spiralOrderPrint(matrix);
            System.out.println(arrayList.toString());
    }

}
