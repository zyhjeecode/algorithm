package com.algorithm.future.practice.recursion;

public class NRecursion {
    public static long getNR(int N){
        if (N==1){
            return 1l;
        }
        return N*getNR(N-1);
    }
    public static long getN(int N){
        long result=1;
        for(int i=1;i<=N;i++){
            result=result*i;
        }
        return result;
    }
    public static void main(String[] args){
        System.out.println(getNR(5));
        System.out.println(getN(5));
    }
}
