package com.algorithm.future.practice.heap;

import java.util.Comparator;
import java.util.PriorityQueue;

public class PriorityQueueDemo {


	public static class MinheapComparator implements Comparator<Integer> {

		@Override
		public int compare(Integer o1, Integer o2) {
			return o1 - o2; // < 0  o1 < o2  负数
		}

	}

	public static class MaxheapComparator implements Comparator<Integer> {

		@Override
		public int compare(Integer o1, Integer o2) {
			return o2 - o1; // <   o2 < o1
		}

	}

	public static void main(String[] args) {

		int[] arrForHeap = { 3, 5, 2, 7, 0, 1, 6, 4 };

		//不指定比较器,默认是从小到大排序即特殊的小根堆,(对象不指定比较器无法比较)
		PriorityQueue<Integer> minQ1 = new PriorityQueue<>();
		for (int i = 0; i < arrForHeap.length; i++) {//添加数组元素到优先队列
			minQ1.add(arrForHeap[i]);
		}
		while (!minQ1.isEmpty()) {//打印优先队列
			System.out.print(minQ1.poll() + " ");
		}
		System.out.println();

		// min heap use Comparator //指定从小到大排序实现特殊小根堆
		PriorityQueue<Integer> minQ2 = new PriorityQueue<>(new MinheapComparator());
		for (int i = 0; i < arrForHeap.length; i++) {
			minQ2.add(arrForHeap[i]);
		}
		while (!minQ2.isEmpty()) {
			System.out.print(minQ2.poll() + " ");
		}
		System.out.println();

		// max heap use Comparator//指定从大到小排序比较器实现特殊大根堆
		PriorityQueue<Integer> maxQ = new PriorityQueue<>(new MaxheapComparator());
		for (int i = 0; i < arrForHeap.length; i++) {
			maxQ.add(arrForHeap[i]);
		}
		while (!maxQ.isEmpty()) {
			System.out.print(maxQ.poll() + " ");
		}

	}

}
