package com.algorithm.future.practice.heap;

import java.util.PriorityQueue;

public class LessMoney {
    public static int lessMoney(int[] arr){
        if (arr==null||arr.length==0){
            return 0;
        }
        PriorityQueue<Integer> queue=new PriorityQueue<>();
        for(int i=0;i<arr.length;i++){
            queue.add(arr[i]);
        }
        int cos=0;
        while (queue.size()>1){
            int min1=queue.poll();
            int min2=queue.poll();

            cos+=min1+min2;
            queue.add(min1+min2);
        }
        return cos;
    }
    public static void main(String[] args){
        int[] arr = { 6, 7, 8, 9 };//13 17 30
        System.out.println(lessMoney(arr));

        int[] arrForHeap = { 3, 5, 2, 7, 0, 1, 6, 4 };
    }
}
