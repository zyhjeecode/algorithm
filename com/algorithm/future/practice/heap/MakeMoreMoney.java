package com.algorithm.future.practice.heap;

import java.util.Comparator;
import java.util.PriorityQueue;

public class MakeMoreMoney {
    public class Node{
        int cost;
        int getM;
        public Node(int c,int g){
            this.cost=c;
            this.getM=g;
        }
    }
    public class minHeapCompartor implements Comparator<Node>{ //以成本打造小根堆
        @Override
        public int compare(Node n1,Node n2) {
            return n1.cost-n2.cost;
        }
    }
    public class MaxHeapCompartor implements Comparator<Node>{ //以利润打造大根堆
        @Override
        public int compare(Node n1,Node n2) {
            return n2.getM-n1.getM;
        }
    }

    public int makeMoreMoneyint (int k, int W, int[] Profits, int[] Capital){
        //k 可以做项目的次数,  W 资本,  Profits 利润,  Capital 成本
        Node[] progectS=new  Node[Profits.length];
        for(int i=0;i<progectS.length;i++){//添加成本和利润构成项目集
            progectS[i]=new Node(Capital[i],Profits[i]);
        }
        //建造成本小根堆
        PriorityQueue<Node> capitalQueue=new PriorityQueue<>(new minHeapCompartor());//指定小根堆比较器
        for(int i=0;i<progectS.length;i++){
            capitalQueue.add(progectS[i]);
        }
        PriorityQueue<Node> profitsQueue=new PriorityQueue<>(new MaxHeapCompartor());//指定大根堆比较器
        for(int i=0;i<k;i++){
            while (!capitalQueue.isEmpty()&&capitalQueue.peek().cost<=W){
                profitsQueue.add(capitalQueue.poll());
            }
            if (profitsQueue.isEmpty()){
                return W;
            }
           W+=profitsQueue.poll().getM;
        }
        return W;
    }


}
