package com.algorithm.future.practice;

public class Hanoi {                        // 移动方     目标方         中间
    public static void moveHanoi(int N,String left,String right,String middle){
        if (N==1){
            System.out.println("移动1号盘子从"+left+"到"+right);
        }else{
            moveHanoi(N-1,left,middle,right);//1~N-1号盘子从from 移到help
            System.out.println("移动"+N+"号盘子从"+left+"到"+right);//移动N号盘子到help
            moveHanoi(N-1,middle,right,left);//移动1~N-1号盘子从help到from
        }
    }
    public static void main(String[] args)
    {
        moveHanoi(3,"左","右","中");
    }
}
