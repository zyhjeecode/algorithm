package com.algorithm.future.practice.hash;

import java.util.HashMap;

public class RandomPool {
    public HashMap<String,Integer> hashMap1;
    public HashMap<Integer,String> hashMap2;
    public int index;
    public RandomPool(){
     hashMap1=new HashMap<>();
     hashMap2=new HashMap<>();
        index =0;
    }
    public  void  insert(String key){
        if (!hashMap1.containsKey(key)) {
            hashMap1.put(key, index);
            hashMap2.put(index, key);
            index++;
        }}
    public  String  getRandom(){
        if (index==0){
            return null;
        }
       int index1=(int) Math.random()*index;//0~size-1
        return hashMap2.get(index1);
    }
    public  void   delete(String key){
        Integer index = hashMap1.get(key);//要删除的索引位置
        hashMap1.remove(key);//删除hash1上的值
        hashMap2.remove(index);//删除hash2上的值
        String lastValue = hashMap2.get(index);//得到最后一个索引的值
        hashMap1.remove(lastValue); //将最后一个索引的值填到刚刚两个hashmap删除的位置
        hashMap1.put(lastValue,index);
        hashMap2.remove(index);
        hashMap2.put(index,lastValue);
        index--;

    }
}
