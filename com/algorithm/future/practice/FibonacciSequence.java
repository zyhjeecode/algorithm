package com.algorithm.future.practice;

public class FibonacciSequence {
    //请你输出斐波那契数列的第n项（从0开始，第0项为0）。
    public static int printFibonacciSequenceNum(int n){
        if (n==0){
            return 0;
        }
         if (n==1){
            return 1;
        }
       return printFibonacciSequenceNum(n-1)+printFibonacciSequenceNum(n-2);
    }
    public static void main(String[] args){
        int num = printFibonacciSequenceNum(10);
        System.out.println(num);
    }
}


