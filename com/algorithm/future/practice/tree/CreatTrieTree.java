package com.algorithm.future.practice.tree;

import org.omg.PortableInterceptor.INACTIVE;

public class CreatTrieTree {
    public static class TrieTree {
        int path;//略过的次数---有多少字符串包含此结点到根结点的所有字符
        int end;//以该结点为最后一个字符的字符串
        TrieTree[] nodes;

        public TrieTree() {
            path = 0;
            end = 0;
            nodes = new TrieTree[26];
        }
    }

    public static class Trie {
        private TrieTree root;

        public Trie() {
            root = new TrieTree();
        }

        public void insert(String word) {
            if (word == null || "".equals(word)) {
                return;
            }
            TrieTree node = root;
            int index;
            for (char c : word.toCharArray()//遍历字符串中的每一个字符
            ) {
                index = c - 'a';
                if (node.nodes[index] == null) {
                    node.nodes[index] = new TrieTree();
                }
                node = node.nodes[index];
                node.path++;
            }
            node.end++;
        }


public  int search(String str) {
            if (str == null || "".equals(str)) {
                return 0;
            }
            char[] chars = str.toCharArray();
            TrieTree node = root;
            int index;
            for (int i = 0; i < chars.length; i++) {
                index = chars[i] - 'a';
                if (node.nodes[index] == null) {
                    return 0;
                }
                node = node.nodes[index];
            }
            return node.end;
        }

        public void delete(String str) {
            if (search(str)!=0){ //前缀树中有才进行删除
             char[]chars=str.toCharArray();
             int index;
             TrieTree node=root;
             for(int i=0;i<chars.length;i++){
                 index=chars[i]-'a';
                 if (--node.nodes[index].path==0){
                     node.nodes[index]=null;
                     return;
                 }
                 node=node.nodes[index];
             }
             node.end--;
            }
        }

        public int preFixNumber(String str){
            if (str==null||"".equals(str)){
                return 0;
            }
            char[] chars=str.toCharArray();
            TrieTree node=root;
            int index;
            for(int i=0;i<chars.length;i++){
                index=chars[i]-'a';
                if (node.nodes[index]==null){
                    return 0;
                }
                node=node.nodes[index];
            }
            return node.path;
        }
    }
    public static void main(String[] args) {
        Trie trie = new Trie();
        System.out.println(trie.search("zuo"));
        trie.insert("zuo");
        System.out.println(trie.search("zuo"));
        trie.delete("zuo");
        System.out.println(trie.search("zuo"));
        trie.insert("zuo");
        trie.insert("zuo");
        trie.delete("zuo");
        System.out.println(trie.search("zuo"));
        trie.delete("zuo");
        System.out.println(trie.search("zuo"));
        trie.insert("zuoa");
        trie.insert("zuoac");
        trie.insert("zuoab");
        trie.insert("zuoad");
        trie.delete("zuoa");
        System.out.println(trie.search("zuoa"));
        System.out.println(trie.preFixNumber("zuo"));

    }
}