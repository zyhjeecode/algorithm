package com.algorithm.future.practice.tree;
import java.util.Stack;

public class SearchTreeJudge {
    public static class Node {
        public int value;
        public Node left;
        public Node right;
        public Node parent;

        public Node(int data) {
            this.value = data;
        }
    }
    public static boolean InOrderJudgeSearch(Node head){
        int falg=Integer.MIN_VALUE;
        if (head!=null){
            Stack<Node> stack=new Stack<>();
            while (!stack.isEmpty()||head!=null){
                if (head!=null){
                    stack.push(head);
                    head=head.left;
                }else {
                    head=stack.pop();
                    if (head.value>falg){
                        falg=head.value;
                    }else {
                        return false;
                    }
                    head=head.right;
                }
            }
        }
        return true;
    }
    public static void main(String[] args){
        Node head = new Node(4);
        head.left = new Node(3);
        head.right = new Node(8);
        head.left.left = new Node(2);
        head.left.right = new Node(4);
        head.left.left.left = new Node(1);
        head.right.left = new Node(7);
        head.right.left.left = new Node(6);
        head.right.right = new Node(10);
        head.right.right.left = new Node(9);
        head.right.right.right = new Node(11);
        System.out.println( InOrderJudgeSearch(head));
    }
}
