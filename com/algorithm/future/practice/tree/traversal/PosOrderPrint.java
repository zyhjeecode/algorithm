package com.algorithm.future.practice.tree.traversal;

import java.util.Stack;

public class PosOrderPrint {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }

    public static void  posOrderPrint(Node head){
        System.out.print("postnatal-order: ");
        if (head!=null){
            Stack<Node> stack1=new Stack<>();
            Stack<Node> stack2=new Stack<>();
            stack1.push(head);
            while (!stack1.isEmpty()){
                head = stack1.pop();
                stack2.push(head);
                if (head.left!=null){
                    stack1.push(head.left);
                }
                if (head.right!=null){
                    stack1.push(head.right);
                }
            }
            while (!stack2.isEmpty())
            {
                System.out.print(stack2.pop().value+" ");
            }
        }
    }

    public static void main(String[] args){
        Node head = new Node(5);
        head.left = new Node(3);
        head.right = new Node(8);

        posOrderPrint(head);
        System.out.println();
    }


}
