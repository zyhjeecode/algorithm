package com.algorithm.future.practice.tree;

public class BalanceTreeJudge {
    public static class Node {
        public int value;
        public Node left;
        public Node right;
        public Node parent;

        public Node(int data) {
            this.value = data;
        }
    }

    public static class returnData {
        boolean isB;
        int h;
        public returnData(boolean isB, int h) {
            this.isB = isB;
            this.h = h;
        }
    }
    public static  returnData Judge(Node head){
        if (head==null){
            return new returnData(true,0);
        }
        if (!Judge(head.left).isB){ //如果左树不平衡
            return new returnData(false,0); //如果其左树不平衡,这里高度无所谓了
        }
        if (!Judge(head.right).isB){//如果右树不平衡
            return new returnData(false,0);//如果其右树不平衡,这里高度无所谓了
    }
        if (Math.abs(Judge(head.left).h-Judge(head.right).h)>1){//若两者高度差的绝对值
        return new returnData(false,0);
    }
        //如果前面的判断非平衡二叉树都没拦截到,则该子树是平衡二叉树,该结点高度是左右子树高度较大值+1
        return new returnData(true,Math.max(Judge(head.left).h,Judge(head.right).h)+1);
    }
    public static void main(String[] args) {
        Node head = new Node(1);
        head.left = new Node(2);
        head.right = new Node(3);
        head.left.left = new Node(4);
        head.left.right = new Node(5);
        head.right.left = new Node(6);
        head.right.right = new Node(7);
        System.out.println(Judge(head).isB);
    }
}
