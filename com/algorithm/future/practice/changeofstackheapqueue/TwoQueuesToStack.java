package com.algorithm.future.practice.changeofstackheapqueue;

import java.util.LinkedList;
import java.util.Queue;

public  class TwoQueuesToStack {
    private Queue<Integer> data;
    private Queue<Integer> help;

    public TwoQueuesToStack(){
        data=new LinkedList<>();
        help=new LinkedList<>();
    }
    public void pushNum(int num){//添加数据
        data.add(num);
    }
    public int  popNum(){//移出队列中最后一个数据
        if (data.isEmpty()) {
            throw new RuntimeException("Stack is empty!");
        }

        while (data.size()!=1){//用help接收除了最后一个数据的其他全部数据
            help.add(data.poll());
        }

        int res=data.poll();
        swap();
        return res;
    }

    public int  peek(){ //只看数据,不弹最后一个数据
        if (data.isEmpty()) {
            throw new RuntimeException("Stack is empty!");
        }
        while (data.size()!=1){
            help.add(data.poll());
        }
        int res=data.poll();
        help.add(res);
        swap();
        return res;
    }

    private void  swap(){
        Queue<Integer> temp=help;//用temp指向现在不空的数据队列
        help=data;//让help指向现在空的队列
        data=temp;//让data指向刚刚绑定的temp队列,即真正的数据队列
        //以上三步达到一个引用交换的目的
    }

    public static void main(String[] args){
        TwoQueuesToStack test=new TwoQueuesToStack();
        test.pushNum(12);
        test.pushNum(14);
        test.pushNum(11);
        test.pushNum(15);
      System.out.print(test.popNum()+"  ");
      System.out.print(test.popNum()+"  ");

    }
}
