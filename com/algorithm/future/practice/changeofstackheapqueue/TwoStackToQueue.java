package com.algorithm.future.practice.changeofstackheapqueue;

import java.util.Stack;

public class TwoStackToQueue {
    private Stack<Integer> push;
    private Stack<Integer> pop;
    public TwoStackToQueue(){
         push=new Stack<>();
         pop=new Stack<>();
    }

    public void pushNum(int num){
        push.push(num);
    }
    public int popNum(){
        popGet();
        if (pop.size()==0){
            throw new RuntimeException("queue is empty!");
        }
       return pop.pop();
    }

    private void popGet(){
        if (pop.size()!=0){
            return;
        }
        while (push.size()!=0){
            pop.push(push.pop());
        }
    }
    public static void main(String[] args){
        TwoStackToQueue test=new TwoStackToQueue();
        test.pushNum(15);
        test.pushNum(23);
        test.pushNum(13);
        test.pushNum(55);
        System.out.println(test.popNum());
        System.out.println(test.popNum());
        System.out.println(test.popNum());
        System.out.println(test.popNum());
        System.out.println(test.popNum());

    }
}
