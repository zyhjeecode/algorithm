package com.algorithm.future.practice.array;

public class MyMaxGap { //找出数组中相邻两个数的最大差值,要求时间复杂度为(N)
    public static int maxGap(int[] nums) {
        if (nums == null || nums.length < 2) {
            return 0;
        }
        int max=Integer.MIN_VALUE;
        int min=Integer.MAX_VALUE;
        int len=nums.length;
        for(int i=0;i<len;i++){ //找出最大值最小值
            if (nums[i]>max)
                 max=nums[i];
            if (nums[i]<min)
                min=nums[i];
        }
        //分三个桶队列
        boolean[] hasNum = new boolean[len + 1]; //存放每个桶是否为空的判断
        int[] maxs = new int[len + 1];//存放每个桶里的最大值
        int[] mins = new int[len + 1];//存放每个桶里的最小值
        int bid;//判断i上的值在桶中的位置
        for(int i=0;i<len;i++){//遍历数组.将数组中每个数组与对应桶中位置上的数据比对,更新桶中最大值或最小值
            bid = bucket(nums[i], len, min, max);
            maxs[bid]=hasNum[bid]?Math.max(maxs[bid],nums[i]):nums[i];
            mins[bid]=hasNum[bid]?Math.max(maxs[bid],nums[i]):nums[i];
            hasNum[bid]=true;
        }
        int res = Integer.MIN_VALUE;
        int lastMax = maxs[0];
        int i = 1;
        for(;i<len+1;i++){
            if (hasNum[i]){//如果桶里有数据,就用当前桶最小值和前一个桶最大值相减,结果再和res相比
                res=Math.max(mins[i]-lastMax,res);
                lastMax=maxs[i];
            }
        }
        return res;
    }
    public static int bucket(long num, long len, long min, long max) {
        return (int) ((num - min) * len / (max - min));
    }
    
    public static void main(String[] args){  
        int[] arrs=new int[]{2,5,8,9,4,22,44,23,21};
        int res=maxGap(arrs);
        System.out.println(res);
    }

}

