package com.algorithm.future.practice.array;

public class SmallerEqualBigger {
    public static class Node {
        public int value;
        public Node next;

        public Node(int data) {
            this.value = data;
        }
    }
    public  static Node SortSmallerEqualBigger(Node head, int pivot){
        Node sS=null;//小于部分开始和结束
        Node sE=null;
        Node eS=null;//等于部分开始和结束
        Node eE=null;
        Node bS=null;//大于部分开始和结束
        Node bE=null;
        Node next=null;
        while (head!=null){
            next=head.next;
            head.next=null;
            if (head.value<pivot){
                if (sS==null){
                    sS=head;
                    sE=head;
                }else {
                    sE.next=head;
                    sE=head;
                }
            }else if (head.value == pivot) {
                if (eS == null) {
                    eS = head;
                    eE = head;
                } else {
                    eE.next = head;
                    eE = head;
                }
            } else {
                if (bS == null) {
                    bS = head;
                    bE = head;
                } else {
                    bE.next = head;
                    bE = head;
                }
            }
            head=next;
        }
        if (sE != null) {
            sE.next = eS;
            eE = eE == null ? sE : eE;
        }
        // all reconnect
        if (eE != null) {
            eE.next = bS;
        }
        return sS != null ? sS : eS != null ? eS : bS;

    }
    public static void printLinkedList(Node node) {
        System.out.print("Linked List: ");
        while (node != null) {
            System.out.print(node.value + " ");
            node = node.next;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Node head1 = new Node(7);
        head1.next = new Node(9);
        head1.next.next = new Node(1);
        head1.next.next.next = new Node(8);
        head1.next.next.next.next = new Node(5);
        head1.next.next.next.next.next = new Node(2);
        head1.next.next.next.next.next.next = new Node(5);
        printLinkedList(SortSmallerEqualBigger(head1,6));

    }
}
