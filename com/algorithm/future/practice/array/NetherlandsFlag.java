package com.algorithm.future.practice.array;

import com.algorithm.future.comparator.ArrayComparator;

public class NetherlandsFlag extends ArrayComparator {
    public static void main(String[] args){
        /*int[] com.algorithm.com.algorithm.leetcode.leetcode.test=generateRandomArray(10,100);
        int[] copy=copyArray(com.algorithm.com.algorithm.leetcode.leetcode.test);*/
        int [] numarr={99,22,44,88,33,11,55,66};//
        partition(numarr,55);
        printArray(numarr);
    }

    public static void  partition(int[] arr,int num ){
        int less=-1;
        int more=arr.length;
        int curr=0;
        while (curr<more){
          if (arr[curr]>num){
            more--;
            swap(arr,more,curr);
          }
          else if (arr[curr]==num){
            curr++;
          }
          else if (arr[curr]<num){
            less++;
            swap(arr,less,curr);
            curr++;
          }
        }
    }


}
