package com.algorithm.future.practice.array;

public class CountSumHasAim {
    //给你一个数组arr，和一个整数aim。
    // 如果可以任意选择arr中的数字，能不能累加得到aim，返回true或者false
        public static boolean JudegeSumHasAim(int[] arr, int index, int sum,int aim) {
            if (index==arr.length){
                return sum==aim;
            }
            //每次可以把当前值传给下一个,也可以不传,用||符号只要最后一行传过来的有true,则返回true
          return   JudegeSumHasAim(arr,index+1,sum+arr[index],aim)||JudegeSumHasAim(arr,index+1,sum,aim);
        }

        public static void main(String[] args){
            int aim=77;
            int[] arr=new int[]{4,1,5,7,3,12,44,563,234,66};
            boolean judge = JudegeSumHasAim(arr, 0, 0, aim);
            System.out.println(judge);
        }
}
