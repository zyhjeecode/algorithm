package com.algorithm.future.practice;

import java.time.Year;

public class CountCows {
    public static int countCows(int year){
        if (year==1){
            return 2;
        }
        if (year==2){
            return 3;
        }
        if(year==3){
            return 4;
        }
        return countCows(year-1)+countCows(year-3);
    }
    public static void main(String[] args){  
        System.out.println(countCows(5));
    }
}
