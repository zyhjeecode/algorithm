package com.algorithm.future.practice.list;

public class PalindromeListJudge {

    public static class Node {
        public int value;
        public Node next;

        public Node(int data) {
            this.value = data;
        }
    }

    public static boolean isPalindrome3(Node head) {
        /*if (head==null||head.next==null){
            return true;
        }*/
        Node node1=head; //慢结点
        Node node2=head; //快结点
        int flag=1;
        while (node2.next!=null && node2.next.next!=null){
            node1=node1.next;
            node2=node2.next.next;
            flag++;
        }
        //将node1后面的链表给逆序了,node1指向空
        Node pre=null;
        Node next=null;
        while (node1!=null){
            next=node1.next;
            node1.next=pre;
            pre=node1;
            node1=next;
        }
        printLinkedList(pre);
        printLinkedList(head);
        boolean res=true;

        if (flag%2!=0){//如果flag为奇数,则链表为奇数链 node在中间,需要比较flag-1次
            for(int i=0;i<flag-1;i++){
                if (head.value!=node2.value)
                {
                    res=false;
                    break;
                }
            }
        }else{//否则链表为偶数链,node在前一个中间点,需要比较flag次
            for(int i=0;i<flag-1;i++){
                if (head.value!=node2.value)
                {
                    res=false;
                    break;
                }
            }
        }
        return res;
    }


    public static void printLinkedList(Node node) {
        System.out.print("Linked List: ");
        while (node != null) {
            System.out.print(node.value + " ");
            node = node.next;
        }
        System.out.println();
    }
    public static void main(String[] args) {
        Node node=new Node(1);
             node.next=new Node(3);
             node.next.next=new Node(3);
             node.next.next.next=new Node(2);
             node.next.next.next.next=new Node(1);
        boolean judgeRes = isPalindrome3(node);
        System.out.println(judgeRes);
    }

}
