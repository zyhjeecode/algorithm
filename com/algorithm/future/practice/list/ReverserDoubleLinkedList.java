package com.algorithm.future.practice.list;

public class ReverserDoubleLinkedList {
    public static class DoubleNode {
        public int value;
        public DoubleNode last;
        public DoubleNode next;
        public DoubleNode(int data) {
            this.value = data;
        }
    }

    //  A<-> B <->C
    //  p  now/h
    // <-A <-B   ->C
    //       p    now

    public static DoubleNode reverseList(DoubleNode head) {
        if (head==null){
            return head;
        }

return head;


    }

    public static void printDoubleLinkedList(DoubleNode head) {
        System.out.print("Double Linked List: ");
        DoubleNode end = null;
        while (head != null) {
            System.out.print(head.value + " ");
            end = head;
            head = head.next;
        }
        System.out.print("| ");
        while (end != null) {
            System.out.print(end.value + " ");
            end = end.last;
        }
        System.out.println();
    }

}
