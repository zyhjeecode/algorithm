package com.algorithm.future.practice.list;

public class ReverseList {
    public static  class  Node{//单链表结点的实现
        public int value;
        public Node next;
        public Node(int data) {
            this.value = data;
        }
    }
    //  A-> B ->C
    //  p  now/h
    // <-A <-B   ->C
    //       p    now

    public static Node reverseList(Node head) {//逆转单链表
        Node pre=null;
        Node next=null;
        while (head!=null){
            next=head.next;
            head.next=pre;
            pre=head;
            head=next;
        }
        return pre;
    }



    public static Node myReverseList(Node head) {//逆转单链表
        Node pre=null;
        Node curr=head;
        Node next=null;
        while (curr!=null){
            next=curr.next;
            curr.next=pre;
            pre=curr;
            curr=next;
        }
        return pre;
    }




    public static void printLinkedList(Node head){//打印链表
        System.out.print("Linked List: ");
       while (head!=null){
           System.out.print(head.value+" ");
            head=head.next;
       }
        System.out.println();
    }

    //com.algorithm.com.algorithm.leetcode.leetcode.test
    public static void main(String[] args){
        Node head=new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);
        head.next.next.next.next = new Node(4);
        printLinkedList(head);
        Node reL=myReverseList(head);
        printLinkedList(reL);
    }

}
