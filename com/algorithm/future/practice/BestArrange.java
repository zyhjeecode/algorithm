package com.algorithm.future.practice;

import java.util.Arrays;
import java.util.Comparator;

public class BestArrange {
    public class Program {
        int start;
        int end;
        public Program(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }
    public static class UpComparator implements Comparator<Program>{
        @Override
        public int compare(Program o1, Program o2) {
            return o1.end-o2.end;
        }
    }
    public static int bestArrange(Program[] programs, int start) {
        int result=0;
        if (programs==null||programs.length==0){
            return 0;
        }
        Arrays.sort(programs,new UpComparator());
        for(int i=0;i<programs.length;i++) {
            if (start <= programs[i].start) {
                result++;
                start = programs[i].end;
            }
        }
        return result;
    }

}
