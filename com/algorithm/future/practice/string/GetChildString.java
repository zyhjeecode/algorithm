package com.algorithm.future.practice.string;

public class GetChildString {
    //打印当前字符串的字串 比如  abc 有字串 "" ,"a","ab","ac","abc","b","bc","abc"
    public static void  pringChildString(char[] chars,int index,String lastR){
            if (index==chars.length){
                System.out.println(lastR);
                return;
            }
            pringChildString(chars,index+1,lastR+String.valueOf(chars[index]));
            pringChildString(chars,index+1,lastR);
    }
    public static void main(String[] args){
        String str="abc";
        pringChildString(str.toCharArray(),0,"");
    }

}
