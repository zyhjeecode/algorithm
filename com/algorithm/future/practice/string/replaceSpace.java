package com.algorithm.future.practice.string;

public class replaceSpace {
    public static String replaceSpace(StringBuffer str) {
        if(str==null)
        {
            return null;
        }
        for(int i=0;i<str.length();i++){
            if (str.charAt(i)==' '){
                str.replace(i,i+1,"%20");
            }
        }
        return str.toString();
    }
    
    public static void main(String[] args){  
        StringBuffer s=new StringBuffer(" 还会 安师大");
        String s1 = replaceSpace(s);
        System.out.println(s1);
    }
}
