package com.algorithm.future.practice.string;

import java.util.Arrays;
import java.util.Comparator;

public class FindMinGroupInArrs {
    public static class myComparor implements Comparator<String>{
        @Override
        public int compare(String s1, String s2) {
            return (s1+s2).compareTo(s2+s1); //s1.compareTo(s2)  正数代表前面大
        }
    }
    public static String returnMinSringGroup(String[] strs){
        if (strs==null){
            return "";
        }
        Arrays.sort(strs,new myComparor()); //Arrays.sort传入自己比较器后,正数代表交换,负数代表不交换
        String res="";
        for(int i=0;i<strs.length;i++){
            res+=strs[i];
        }
        return res;
    }
    public static void main(String[] args){  
        String[] s=new String[]{"a","ab","ac","b"};
        String s1 = returnMinSringGroup(s);
        System.out.println(s1);

    }

}
