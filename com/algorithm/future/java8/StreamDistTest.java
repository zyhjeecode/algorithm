package com.algorithm.future.java8;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description: java8去重测试
 * @author: zyh
 * @create: 2020-12-17 15:54
 **/
public class StreamDistTest {
    @Test
    public  void  test() {
        tt t1=new tt();
        t1.setAddr("淮北");
        t1.setName("zyh");
        tt t2=new tt();
        t2.setAddr("淮北");
        t2.setName("zmy");
        tt t3=new tt();
        t3.setAddr("南京");
        t3.setName("zmy");
        List<tt> list= new LinkedList<>();
        list.add(t2);
        list.add(t1);
        list.add(t3);
        List<String> collect = list.stream().map(item -> item.getAddr()).distinct().collect(Collectors.toList());
        for (int i = 0; i <collect.size() ; i++) {
            System.out.println(collect.get(i));
        }
    }

    class tt{
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddr() {
            return addr;
        }

        public void setAddr(String addr) {
            this.addr = addr;
        }

        private String name;
        private String addr;
    }
}
