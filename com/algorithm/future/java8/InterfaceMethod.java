package com.algorithm.future.java8;

public interface InterfaceMethod {
    default boolean getRes() {
        return true;
    }
}
