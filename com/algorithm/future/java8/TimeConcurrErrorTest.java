package com.algorithm.future.java8;


import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

/**
 * @author zyh
 */
public class TimeConcurrErrorTest {
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {
        LocalDateTime now=LocalDateTime.now();
        final LocalDateTime time2 = LocalDateTime.of(2021, 9, 6, 9, 23);
        final String s = now.toLocalTime().toString().substring(0,5);
        System.out.println(s);
        System.out.println(time2.toLocalTime().toString().substring(0,5));
    }
}
