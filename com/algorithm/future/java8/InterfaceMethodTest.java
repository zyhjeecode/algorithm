package com.algorithm.future.java8;


import org.junit.Test;

/**
 * @description: 接口方法测试
 * @author: zyh
 * @create: 2021-10-12 17:12
 **/
public class InterfaceMethodTest implements InterfaceMethod {
    @Test
    public void testRes(){
        final boolean res = getRes();
        System.out.println(res);
    }

    @Override
    public boolean getRes() {
        return false;
    }


    public static void main(String[] args) {
        try {
            System.out.println(1/0);
        }catch (Exception exce){
            System.out.println("111");
            System.out.println(1/0);
        }
    }
}


