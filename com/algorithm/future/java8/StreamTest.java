package com.algorithm.future.java8;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @description: java8 Stream
 * @author: zyh
 * @create: 2020-11-24 21:46
 **/
public class StreamTest {
    public static void main(String[] args) {
        Map< Integer, String > coursesMap = new HashMap< Integer, String >();
        coursesMap.put(1, "C");
        coursesMap.put(2, "C++");
        coursesMap.put(3, "Java");
        coursesMap.put(4, "Spring Framework");
        coursesMap.put(5, "Hibernate ORM framework");

        Map<Integer, String> collect = coursesMap.entrySet().stream().filter(item -> !item.getValue().equals("C++")).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
//        for (Map.Entry e:collect.entrySet()
//             ) {
//            System.out.println(e.getKey()+"-----"+e.getValue());
//        }

        int [] intArr= new int[] {1,2,3,4,5};
        List<Integer> collect1 = Arrays.stream(intArr).boxed().collect(Collectors.toList());
//        for (Integer integer : collect1) {
//            System.out.println(integer);
//        }

        String [] strArr=new String[] {"1","2","3","4"};

        List<Integer> strings = Arrays.stream(strArr).map(item->Integer.parseInt(item)*2).collect(Collectors.toList());
        for (int i = 0; i < strings.size(); i++) {
            System.out.println(strings.get(i));
        }
        List<Integer> sortList = Arrays.stream(strArr).map(item -> Integer.parseInt(item) * 2).sorted((i1, i2) -> i2 - i1).filter(item->item!=4).collect(Collectors.toList());
        for (int i = 0; i < sortList.size(); i++) {
            System.out.println(sortList.get(i));
        }


    }
}
