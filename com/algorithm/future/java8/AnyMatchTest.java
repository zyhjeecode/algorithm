package com.algorithm.future.java8;

import java.util.LinkedList;
import java.util.List;

/**
 * @description:
 * @author: zyh
 * @create: 2022-01-14 14:58
 **/
public class AnyMatchTest {
    public static void main(String[] args) {
        List<String> sysArgs = new LinkedList<>();
        sysArgs.add("curScore");
        sysArgs.add("changeTime");
        sysArgs.add("changeScore");
        sysArgs.add("scoreType");
        sysArgs.add("changeRemark");
        sysArgs.add("shop");
        List<String> res = new LinkedList<>();

        List<String> searchStr = new LinkedList<>();
        String str1 = "消费积分：\n 当前积分值：{{curScore}}\n  积分变动时间：{{changeTime}}\n   积分类型：{{scoreType}}\n   积分变更备注：{{changeRemark}}\n  消费金额：{{money}}\n     消费商户：{{shop}}\n   3.cn/102r0n-iD3.cn/1-02r0m2q";
        String str2 = "消费积分：\n 当前积分值：{{curScore}}\n  积分变动时间：{{changeTime}}\n 变动积分值：{{changeScore}}\n   积分类型：{{scoreType}}\n   积分变更备注：{{changeRemark}}\n  消费金额：{{money}}\n     消费商户：{{shop}}\n   3.cn/102r0n-iD3.cn/1-02r0m2q";
        searchStr.add(str1);
        searchStr.add(str2);

        sysArgs.forEach(item -> {
            if (searchStr.stream().anyMatch(l -> l.contains("{{" + item + "}}"))) {
                res.add(item);
            }
        });

        System.out.println(res);
    }

}
