package com.algorithm.future.java8;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * @description: 测试并行流的顺序性
 * @author: zyh
 * @create: 2021-01-04 21:09
 **/
public class ParallelStreamTest {

    @Test
    public void testParallel(){
        List<Integer> list=new LinkedList<>();
        for (int i = 1; i < 100; i++) {
            list.add(i);
        }
        list.parallelStream().forEach(item->{
            item=item*2;
        });

        list.stream().forEach(item->{
            System.out.println(item);
        });
    }
}
