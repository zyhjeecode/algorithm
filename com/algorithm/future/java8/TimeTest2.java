package com.algorithm.future.java8;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @description: test2
 * @author: zyh
 * @create: 2020-12-11 14:55
 **/
public class TimeTest2 {
    public static void main(String[] args) throws InterruptedException {
        AtomicInteger atomicInt=new AtomicInteger(0);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date=new Date();

        for (int i = 0; i < 100; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println(format.format(date));
                    atomicInt.incrementAndGet();
                }
            }).start();
        }
        while (atomicInt.get()!=100){
            Thread.yield();
        }
    }
}
