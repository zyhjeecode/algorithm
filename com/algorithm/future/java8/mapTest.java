package com.algorithm.future.java8;

import org.junit.Test;

import javax.naming.Name;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @description: 测试map里存的啥
 * @author: zyh
 * @create: 2020-12-16 19:32
 **/
public class mapTest {
    @Test
    public  void  test() {
        tt t1=new tt();
        t1.setAddr("淮北");
        t1.setName("zyh");
        t1.setAge(11);
        tt t2=new tt();
        t2.setAddr("淮北");
        t2.setName("zmy");
        t2.setAge(12);
        tt t3=new tt();
        t3.setAddr("淮北");
        t3.setName("aaa");
        t3.setAge(13);
        tt t4=new tt();
        t4.setAddr("淮北");
        t4.setName("bbb");
        t4.setAge(14);
        tt t5=new tt();
        t5.setAddr("淮北");
        t5.setName("ccc");
        t5.setAge(15);
        tt t6=new tt();
        t6.setAddr("淮北");
        t6.setName("zyh");
        t6.setAge(11);
        tt t7=new tt();
        t7.setAddr("淮北");
        t7.setName("zmy");
        t7.setAge(12);
        tt t8=new tt();
        t8.setAddr("淮北");
        t8.setName("aaa");
        t8.setAge(13);
        tt t9=new tt();
        t9.setAddr("淮北");
        t9.setName("bbb");
        t9.setAge(14);
        tt t10=new tt();
        t10.setAddr("淮北");
        t10.setName("ccc");
        t10.setAge(15);
//        Map<String,tt> map=new HashMap<>();
//        map.put(t1.getName(),t1);
//        map.put(t2.getName(),t2);
//        map.get("zyh").setAddr("大淮北");
//        System.out.println(t1.getAddr());
        List<tt> list1=new ArrayList();
        list1.add(t1);
        list1.add(t2);
        list1.add(t3);
        list1.add(t4);
        list1.add(t5);
        list1.add(t6);
        list1.add(t7);
        list1.add(t8);
        list1.add(t9);
        list1.add(t10);
        for (int i = 16; i < 100; i++) {
            tt curr=new tt();
            curr.setAddr("淮北");
            curr.setName("ccc");
            curr.setAge(i);
            list1.add(curr);
        }
//        List<tt> list2=new ArrayList();
//        list2.add(t1);
//        list2.add(t2);
//        list1.get(0).setAddr("大淮北");
//        System.out.println(t1.getAddr());
//        System.out.println(list2.get(0).getAddr());
//        Map<String, tt> collect = list1.stream().collect(Collectors.toMap(tt::getName, Function.identity()));
//        collect.get("zyh").setAddr("大淮北");
        list1.parallelStream().forEach(item->item.setAge(item.getAge()*2));

        list1.stream().forEach(item->System.out.println(item.getName()+"   "+item.getAge()));


    }

    class tt{
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddr() {
            return addr;
        }

        public void setAddr(String addr) {
            this.addr = addr;
        }

        private String name;

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        private String addr;
        private int age;
    }
}
