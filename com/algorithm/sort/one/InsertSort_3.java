package com.algorithm.sort.one;

public class InsertSort_3 {
    public static void main(String[] args) {
        int[] arr = CommonUtils.arr;
        insertSort(arr);
        CommonUtils.pringtln(arr);
    }

    private static void insertSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        for (int i = 1; i <arr.length ; i++) {
            for (int j = i; j-1>=0&&arr[j]<arr[j-1] ; j--) {
                CommonUtils.swap(arr,j,j-1);
            }
        }
    }
}
