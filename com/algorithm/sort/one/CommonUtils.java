package com.algorithm.sort.one;

public class CommonUtils {
    static int arr[] ={4,1,2,8,6,7};
    /**
     * 交换数组中两个数的位置
     * @param arr
     * @param indexa
     * @param indexb
     */
    public static void swap(int[] arr ,int indexa,int indexb){
        int temp=arr[indexa];
        arr[indexa]=arr[indexb];
        arr[indexb]=temp;
    }

    public static void pringtln(int[] arr){
        for (int i : arr) {
            System.out.print(i+" ");
        }
    }
}
