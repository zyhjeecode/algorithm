package com.algorithm.sort.one;

public class BubbleSort_2 {
    public static void main(String[] args) {
        int[] arr = CommonUtils.arr;
        bubbleSort(arr);
        CommonUtils.pringtln(arr);
    }

    private static void bubbleSort(int[] arr) {
        if (arr==null||arr.length<2){
            return;
        }

        int len=arr.length;
        for (int end = len-1; end >0; end--) {
            for (int i = 0; i < end; i++) {
                if (arr[i+1]<arr[i]){
                    CommonUtils.swap(arr,i,i+1);
                }
            }
        }
    }
}
