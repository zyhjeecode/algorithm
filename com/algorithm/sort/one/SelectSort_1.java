package com.algorithm.sort.one;

/**
 * 选择排序
 */
public class SelectSort_1 {
    public static void main(String[] args){
        int[] a={4,1,2,8,6,7};
        selectionSort(a);
        for (int i = 0; i <a.length; i++) {
            System.out.print(a[i]+" ");
        }
    }

    private static void selectionSort(int[] a) {
        if (a==null||a.length<2){
            return;
        }

        for (int i = 0,b=a.length; i < b; i++) {
            int minIndex=i;
            for (int j = i+1; j < b; j++) {
                if (a[j]<a[minIndex]){
                    minIndex=j;
                }
            }
            if (i!=minIndex){
                CommonUtils.swap(a,i,minIndex);
            }
        }
    }
}
