package com.algorithm.tree.recursion.print;
import org.junit.Test;

import java.util.ArrayList;

/**
 * 左序遍历
 */
public class PrintTree_middle {
    public ArrayList<Integer> res=new ArrayList<Integer>();

    public void getMiddleTree(TreeNode root){
        if (root==null){
            return;
        }

        //先遍历左
        getMiddleTree(root.left);

        //再把自己加进去
        res.add(root.val);

        //再遍历右
        getMiddleTree(root.right);
    }


    @Test
    public void  test(){
      TreeNode root=new TreeNode(11);
      root.left=new TreeNode(9);
      root.left.left=new TreeNode(8);
      root.left.right=new TreeNode(10);
      root.right=new TreeNode(13);
      root.right.left=new TreeNode(12);
      root.right.right=new TreeNode(14);
      getMiddleTree(root);
      res.forEach(item->{
          System.out.print(item+" ");
      });
    }
}
